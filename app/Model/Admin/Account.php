<?php

namespace App\Model\Admin;

use Illuminate\Database\Eloquent\Model;

class Account extends Model
{
    public $timestamps = false;
	protected $table = 'tms_account_settings';
}
