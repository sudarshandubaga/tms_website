<?php

namespace App\Model\School_Facilities;

use Illuminate\Database\Eloquent\Model;

class School_Facilities extends Model
{
	public $timestamps = false;
	protected $table = 'tms_school_facilities'; 

}
