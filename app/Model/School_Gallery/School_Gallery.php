<?php

namespace App\Model\School_Gallery;

use Illuminate\Database\Eloquent\Model;

class School_Gallery extends Model
{
	public $timestamps = false;
	protected $table = 'tms_school_gallery'; 

}
