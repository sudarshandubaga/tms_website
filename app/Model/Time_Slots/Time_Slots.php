<?php

namespace App\Model\Time_Slots;

use Illuminate\Database\Eloquent\Model;

class Time_Slots extends Model
{
	public $timestamps = false;
	protected $table = 'tms_time_slots'; 

}

