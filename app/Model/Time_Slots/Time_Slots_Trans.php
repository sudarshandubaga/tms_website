<?php

namespace App\Model\Time_Slots;

use Illuminate\Database\Eloquent\Model;

class Time_Slots_Trans extends Model
{
	public $timestamps = false;
	protected $table = 'tms_time_slots_trans'; 

}

