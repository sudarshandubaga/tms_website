<?php

namespace App\Model\Our_School;

use Illuminate\Database\Eloquent\Model;

class Testimonials extends Model
{
	public $timestamps = false;
	protected $table = 'tms_testimonials'; 

}

