<?php

namespace App\Model\Our_School;

use Illuminate\Database\Eloquent\Model;

class Our_School extends Model
{
	public $timestamps = false;
	protected $table = 'tms_our_schools'; 

}

