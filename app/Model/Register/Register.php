<?php

namespace App\Model\Register;

use Illuminate\Database\Eloquent\Model;

class Register extends Model
{
	public $timestamps = false;
	protected $table = 'tms_admission'; 

}

