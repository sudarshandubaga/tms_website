<?php

namespace App\Model\We_Teach;

use Illuminate\Database\Eloquent\Model;

class We_Teach extends Model
{
	public $timestamps = false;
	protected $table = 'tms_we_teach'; 
}