<?php

namespace App\Model\Important_points;

use Illuminate\Database\Eloquent\Model;

class Important_points extends Model
{
	public $timestamps = false;
	protected $table = 'tms_important_points'; 

}

