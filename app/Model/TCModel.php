<?php
namespace App\Model;

use Illuminate\Database\Eloquent\Model;

class TCModel extends Model {
    public      $timestamps     = false;
    protected   $table          = 'tms_tcs';
    protected   $primaryKey     = 'tc_id';
}
