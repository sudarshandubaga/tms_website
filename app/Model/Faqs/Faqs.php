<?php

namespace App\Model\Faqs;

use Illuminate\Database\Eloquent\Model;

class Faqs extends Model
{
	public $timestamps = false;
	protected $table = 'tms_faqs'; 

}

