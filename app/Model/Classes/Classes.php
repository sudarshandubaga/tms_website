<?php

namespace App\Model\Classes;

use Illuminate\Database\Eloquent\Model;

class Classes extends Model
{
	public $timestamps = false;
	protected $table = 'tms_classes'; 

}

