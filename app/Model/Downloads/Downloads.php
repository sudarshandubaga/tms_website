<?php

namespace App\Model\Downloads;

use Illuminate\Database\Eloquent\Model;

class Downloads extends Model
{
	public $timestamps = false;
	protected $table = 'tms_downloads'; 

}

