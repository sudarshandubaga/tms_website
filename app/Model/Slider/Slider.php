<?php

namespace App\Model\Slider;

use Illuminate\Database\Eloquent\Model;

class Slider extends Model
{
	public $timestamps = false;
	protected $table = 'tms_slider'; 

}

