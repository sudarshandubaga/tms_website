<?php

namespace App\Model\Pages;

use Illuminate\Database\Eloquent\Model;

class PageImage extends Model
{
    //
	public $timestamps = false;
    protected $guarded = [];
}
