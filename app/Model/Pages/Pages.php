<?php

namespace App\Model\Pages;

use Illuminate\Database\Eloquent\Model;

class Pages extends Model
{
	public $timestamps = false;
	protected $table = 'tms_master_pages'; 

	public function getRouteKeyName()
	{
		return 'master_pages_slug';
	}

	public function page_images()
	{
		return $this->hasMany(PageImage::class, 'page_id', 'master_pages_id');
	}

}

