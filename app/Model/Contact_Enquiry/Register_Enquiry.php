<?php

namespace App\Model\Contact_Enquiry;

use Illuminate\Database\Eloquent\Model;

class Register_Enquiry extends Model
{
	public $timestamps = false;
	protected $table = 'tms_admission'; 
}

