<?php

namespace App\Model\Contact_Enquiry;

use Illuminate\Database\Eloquent\Model;

class Contact_Enquiry extends Model
{
	public $timestamps = false;
	protected $table = 'tms_contact_enquirys'; 
}

