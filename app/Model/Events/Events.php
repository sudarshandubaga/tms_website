<?php

namespace App\Model\Events;

use Illuminate\Database\Eloquent\Model;

class Events extends Model
{
	public $timestamps = false;
	protected $table = 'tms_events'; 

}

