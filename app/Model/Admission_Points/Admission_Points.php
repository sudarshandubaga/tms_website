<?php

namespace App\Model\Admission_Points;

use Illuminate\Database\Eloquent\Model;

class Admission_Points extends Model
{
	public $timestamps = false;
	protected $table = 'tms_admission_points'; 

}

