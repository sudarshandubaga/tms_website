<?php

namespace App\Http\Controllers\backend;

use Illuminate\Routing\Controller as BaseController;
use Illuminate\Http\Request;
use DB;
use App\Model\TCModel AS TCmodel;

class TcController extends BaseController {
    public function index(Request $request) {
        $title 	= "View Tc";

        $records = TCmodel::where('tc_is_deleted', 'N')->paginate(10);

        if($request->isMethod('post')) {
             $check = $request->input('check');
             if(!empty($check)) {
                  TCmodel::whereIn('tc_id', $check)->update(['tc_is_deleted' => 'Y']);
             }

             return redirect('sch-panel/tc');
        }

        return $data 	= compact('page', 'title', 'records');
        return view('view_tc', $data);
    }
    public function add(Request $request, $id = null) {
        $edit = [];
        if(!empty($id)) {
            $edit       = TCmodel::where('tc_id', $id)->first();
        }

        $title 	     = "Add Tc";
        $page 	     = "add_tc";

        if($request->isMethod('post')) {
           $record   = $request->input('record');

           if(empty($id)) {
            $id = TCmodel::insertGetId($record);
            // $id = $res->id;
               // $id = DB::getPdo()->lastInsertId();
           } else {
               TCmodel::where('tc_id', $id)->update($record);
           }

           if ($request->hasFile('tc_file')) {

               if(!empty($edit->tc_file) && file_exists(public_path().'/files/tcs/'.$edit->tc_file)) {
                   unlink(public_path().'/files/tcs/'.$edit->tc_file);
               }
               $image           = $request->file('tc_file');
               $name            = 'FILE'.$id.'.'.$image->getClientOriginalExtension();
               $destinationPath = 'public/files/tcs/';
               $image->move($destinationPath, $name);

               if(!empty($edit->tc_file)) {
                   $name .= "?v=".uniqid();
               }

               // $record['tc'] = $name;
               TCmodel::where('tc_id', $id)->update(['tc_file' => $name]);
           }


           return redirect('sch-panel/tc')->with('success', "Success! New record inserted.");
        }

        $data 	= compact('page', 'title', 'edit');
        return view('backend/layout', $data);
    }
}
