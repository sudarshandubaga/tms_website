<?php
  namespace App\Http\Controllers\Admission_Points;
  use Illuminate\Http\Request;
  use App\Http\Requests;  
  use App\Http\Controllers\Controller;
  use DB;
  use App\Model\Admission_Points\Admission_Points; // model name
  use App\Model\Admin\Admin; // model name
  use App\Model\Admin\Account;
  use Response;

  class Admission_PointsController extends Controller {

    public function __construct()
    {
        $this->middleware('ValidateAdmin');
    }

    public function admission_points_view($id = false,Request $request) {
    
      $admin_details = Admin::where(['admin_id' => session('tms_admin_id')])->first();

      if($id){
        $get_record = Admission_Points::where(['admission_point_id' => $id])->first();
      } else {
        $get_record = "";
      }

      if ($request['check'] != "") {
        Admission_Points::whereIn('admission_point_id', $request['check'])->delete();
        return Redirect()->back();
      }

       $query =Admission_Points::query();

      if ($request['search_admission_point'] != "") {
        $query = $query->where('admission_point', 'LIKE', '%'.$request['search_admission_point'].'%');
      }

      $appends = array('search_admission_point'=>$request['search_admission_point']);

      $admission_point = $query->orderBy('admission_point_id','ASC')->paginate(25)->appends($appends); 

      $title  = "Admission Points";
      $data   = compact('title','admin_details','get_record','admission_point');
      return view('admin_panel/admission_point',$data);
    }

    public function save_admission_points($id = false,Request $request){

      if($id != ""){
          $values=array(
              'admission_point'         => $request['admission_point']
          );
          Admission_Points::where('admission_point_id', $id)->update($values);
          return Redirect('admin-panel/admission-points');

      } else {
            $Add = new Admission_Points;
            $Add->admission_point         =  $request['admission_point'];
            $Add->save();
            return redirect()->back();
      }
    }


    public function admission_points_status($id,$value) {
      Admission_Points::where('admission_point_id', $id)->update(['admission_point_status' => $value]);
      return Redirect()->back();
    }

       
  }