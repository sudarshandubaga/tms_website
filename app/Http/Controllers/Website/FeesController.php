<?php

namespace App\Http\Controllers\Website;

use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use App\Model\Admin\Account;
use App\Payment\iPay24Pipe;

class FeesController extends Controller
{
    public function index()
    {
        $title  = "Online Fees Deposite | The Modern School Barmer";
        $account_details = Account::first();
        $bredcum = "Online Fees Deposite";
        $data   = compact('account_details','title','bredcum');
        return view('online_fees_deposite', $data);
    }

    public function store(Request $request)
    {
        $request->validate([]);

        // dd($request->all());

        try {
            //code...
            $currency	    = 356;
            $language	    = "USA";
            $receiptURL	    = route('fees.payment.success');
            $errorURL	    = route('fees.payment.failure');
            $resourcePath   = public_path('bob\cgnfile' . '\\');
            $aliasName      = "IPGTEST";
            
            $myObj      = new iPay24Pipe();
			$rnd        = substr(number_format(time() * rand(),0,'',''),0,10);
			$trackid    = $rnd;
			$myObj->setResourcePath(trim($resourcePath));
			$myObj->setKeystorePath(trim($resourcePath));
			$myObj->setAlias(trim($aliasName));
			$myObj->setAction(1);
			$myObj->setCurrency(trim($currency));
			$myObj->setLanguage(trim($language));
			$myObj->setResponseURL(trim($receiptURL));
			$myObj->setErrorURL(trim($errorURL));
			$myObj->setAmt($request->fee_amount);
			$myObj->setTrackId($trackid);
			// $myObj->setUdf1($request->student_name);
			// $myObj->setUdf2($request->email);
			// $myObj->setUdf3($request->mobile_no);
			// $myObj->setUdf4($request->message);
			// $myObj->setUdf5($request->udf5);
			
			$myObj->setUdf6("The Modern School Barmer");
			$myObj->setUdf7($request->student_name);
			$myObj->setUdf8($request->email);
			$myObj->setUdf9($request->mobile_no);
			$myObj->setUdf10("India");
			
			$myObj->setUdf11($request->fee_amount);
			$myObj->setUdf12($trackid);
			$myObj->setUdf13(url('/'));
			$myObj->setUdf14($request->message);
			// $myObj->setUdf15($request->udf15);
			// $myObj->setUdf16($request->udf16);
			// $myObj->setUdf17($request->udf17);
			// $myObj->setUdf18($request->udf18);
			// $myObj->setUdf19($request->udf19);
			// $myObj->setUdf20($request->udf20);
			
			
			// $myObj->setUdf21($request->udf21);
			// $myObj->setUdf22($request->udf22);
			// $myObj->setUdf23($request->udf23);
			// $myObj->setUdf24($request->udf24);
			// $myObj->setUdf25($request->udf25);
			// $myObj->setUdf26($request->udf26);
			// $myObj->setUdf27($request->udf27);
			// $myObj->setUdf28($request->udf28);
			// $myObj->setUdf29($request->udf29);
			// $myObj->setUdf30($request->udf30);
			
			// $myObj->setUdf31($request->udf31);
			// $myObj->setUdf32($request->udf32);
			
		//	$myObj->setKey("222222222222222222222222");
			if(trim($myObj->performPaymentInitializationHTTP())!=0) {
	  			echo("ERROR OCCURED! SEE CONSOLE FOR MORE DETAILS");
  				return;
			}else{
			//	header("location:".$myObj->getwebAddress()); 
			$url =trim($myObj->getWebAddress());
		    echo "<meta http-equiv='refresh' content='0;url=$url'>";
			}
		} catch(Exception $e) {
			echo 'Message: ' .$e->getFile();
	  		echo 'Message1 : ' .$e->getCode();
		} 
    }
}
