<?php
namespace App\Http\Controllers\Website;
// namespace App\Mail;

use Illuminate\Http\Request;
use App\Http\Requests;
use App\Http\Controllers\Controller;
use DB;
use Illuminate\Support\Facades\Mail;

// wait

use App\Model\Admin\Account; 
use App\Model\Admin\Admin; 
use App\Model\Admission_Points\Admission_Points; 
use App\Model\Register\Register; 
use App\Model\Contact_Enquiry\Contact_Enquiry; 
use App\Model\Faqs\Faqs; 
use App\Model\Important_points\Important_points; 
use App\Model\Our_School\Our_School; 
use App\Model\School_Facilities\School_Facilities; 
use App\Model\We_Teach\We_Teach; 
use App\Model\Classes\Classes; 
use App\Model\Downloads\Downloads; 
use App\Model\School_Gallery\School_Gallery; 
use App\Model\School_Gallery\School_Video_Gallery; 
use App\Model\Events\Events;
use App\Model\News\News;
use App\Model\Pages\Pages; 
use App\Model\Slider\Slider; 
use App\Model\Our_School\Testimonials; 
use App\Model\TCModel as Tc;

use App\Model\Time_Slots\Time_Slots; 
use Redirect;
use Response;
use Session;
use Carbon\Carbon;

class HomeController extends Controller 
{

    public function home() {
    
      $title  = "Neo Dales Play School | Each Child a StarNeo Dales Play School | Each Child a Star";
      $account_details = Account::first();
      $get_events = Events::where(['event_status' => 1])->get();
      $get_pages = Pages::where(['master_pages_id' => 1 ,'master_pages_status' => 1])->first();
      $choose_pages = Pages::where(['master_pages_id' => 19 ,'master_pages_status' => 1])->first();
      $get_our_school = Our_School::where(['our_school_status' => 1])->get();
      $important_points = Important_points::where(['important_point_status' => 1])->get();
      $school_facilities = School_Facilities::where(['school_facilities_status' => 1])->get();
      $get_slider = Slider::where(['slider_status' => 1])->get();
      $testimonials = Testimonials::where('testimonials_status', '1')->get();

      $news = News::latest()->pluck('title', 'id');
      // dd($news);

      $data   = compact('title','account_details','get_events','get_pages','choose_pages','get_our_school','important_points','school_facilities','get_slider','testimonials', 'news');
      return view('index',$data);
    }
    
    public function news(News $news) {
      $title  = $news->title." | The Modern School Barmer";
      $bredcum = $news->title;
      $account_details = Account::first();
      $data   = compact('news','title','bredcum', 'account_details');
      return view('news',$data);
    }
    
    public function contact_us() {
      $title  = "Contact Us | The Modern School Barmer";
      $account_details = Account::first();
      $bredcum = "Contact Us";
      $data   = compact('account_details','title','bredcum');
      return view('contact_us',$data);
    }
    
     public function contact_submit(Request $request){
       $data = array(
        'contact_enquiry_name'     => $request['contact_enquiry_name'],
        'contact_enquiry_email'  => $request['contact_enquiry_email'],
        'contact_enquiry_number'  => $request['contact_enquiry_number'],
        'contact_enquiry_subject'  => $request['contact_enquiry_subject'],
        'contact_enquiry_message'  => $request['contact_enquiry_message'],
    );
      $obj = DB::table('tms_contact_enquirys')->insert($data);
     
      if($obj == true){
        $account_details = Account::first();
        $subject = "Your Service ID : ";
        $email_params = [
            // 'to'        => env('MAIL_USERNAME'),
            'to'        => $account_details->account_email,
            'reciever'  => $account_details->account_name,
            'from'      => $data['contact_enquiry_email'],
            'sender'    => $data['contact_enquiry_name'],
            'subject'   => $subject,
            'data'      => $data,
            'setting'   => $account_details

        ];
        
        // dd($email_params);
        Mail::send('email.contact_enquiry', $email_params, function ($msgEmail) use ($email_params) {
            extract($email_params);

            // $msgEmail->to($to, $reciever)
            // ->subject($subject)
            // ->from($from, $sender)
            // ->replyTo($to, $reciever);
            $msgEmail->to($to, $reciever)
                ->subject($subject)
                ->from($from, $sender)
                ->replyTo($to, $reciever);
        });
      }

        return response()->json(['type'=>"success",'message'=> "Your enquiry has been successfully submitted" ]);  
    }

    public function pages(Pages $pages) {
      $account_details = Account::first();
      $title  = $pages->master_pages_title . " | The Modern School Barmer";
      $bredcum = $pages->master_pages_title;
      $get_pages = $pages;
      $data   = compact('account_details','title','bredcum','get_pages');
      return view('pages',$data);
    }
    
    public function vision_mission() {
      $title  = "Vission Mission | The Modern School Barmer";
      $account_details = Account::first();
      $bredcum = "Vision Mission";
      $get_pages = Pages::where(['master_pages_id' => 15 ,'master_pages_status' => 1])->first();
      $data   = compact('account_details','title','bredcum','get_pages');
      return view('vision_mission',$data);
    }
    
    
    public function principal_message() {
      $title  = "Principal Message | The Modern School Barmer";
      $account_details = Account::first();
      $bredcum = "Principal Message";
      $get_pages = Pages::where(['master_pages_id' => 7,'master_pages_status' => 1])->first();
      $data   = compact('account_details','title','bredcum','get_pages');
      return view('principal_message',$data);
    }
    
    
    public function gallery() {
      $title  = "Gallery | The Modern School Barmer";
      $account_details = Account::first();
      $bredcum = "Gallery";
      $school_gallery = School_Gallery::where(['school_gallery_status' => 1])->orderBy('school_gallery_id','DESC')->get();
      $data   = compact('account_details','title','bredcum','school_gallery');
      return view('gallery',$data);
    }
    
    public function videogallery() {
      $title  = "Video Gallery | The Modern School Barmer";
      $account_details = Account::first();
      $bredcum = "Video Gallery";
      $school_gallery = School_Video_Gallery::where(['school_gallery_status' => 1, 'type' => 'normal'])->orderBy('school_gallery_id','DESC')->get();
      $data   = compact('account_details','title','bredcum','school_gallery');
      return view('videogallery', $data);
    }
    public function virtual_tour() {
      $title  = "Virtual Tour | The Modern School Barmer";
      $account_details = Account::first();
      $bredcum = "Virtual Tour";
      $school_gallery = School_Video_Gallery::where(['school_gallery_status' => 1, 'type' => 'tour'])->orderBy('school_gallery_id','DESC')->get();
      $data   = compact('account_details','title','bredcum','school_gallery');
      return view('videogallery', $data);
    }
     public function admission_policy() {
      $title  = "Admission | The Modern School Barmer";
      $account_details = Account::first();
      $get_pages = Pages::where(['master_pages_id' => 3,'master_pages_status' => 1])->first();
      $bredcum = "Admission";
      $data   = compact('account_details','title','bredcum','get_pages');
      return view('admission_policy',$data);
    }
    
     public function academic_policy() {
      $title  = "Academic | The Modern School Barmer";
      $account_details = Account::first();
      $get_pages = Pages::where(['master_pages_id' => 20,'master_pages_status' => 1])->first();
      $bredcum = "Academic";
      $data   = compact('account_details','title','bredcum','get_pages');
      return view('academic_policy',$data);
    }
    
     public function next_leap() {
      $title  = "The Next Leap | The Modern School Barmer";
      $account_details = Account::first();
      $get_pages = Pages::where(['master_pages_id' => 21,'master_pages_status' => 1])->first();
      $bredcum = "The Next Leap";
      $data   = compact('account_details','title','bredcum','get_pages');
      return view('next_leap',$data);
    }
    
     public function guidelines_policy() {
      $title  = "Guidelines | The Modern School Barmer";
      $account_details = Account::first();
      $get_pages = Pages::where(['master_pages_id' => 22,'master_pages_status' => 1])->first();
      $bredcum = "Guidelines";
      $data   = compact('account_details','title','bredcum','get_pages');
      return view('guidelines_policy',$data);
    }
    
    public function activities() {
      $title  = "Activities | The Modern School Barmer";
      $account_details = Account::first();
      $get_pages = Pages::where(['master_pages_id' => 23,'master_pages_status' => 1])->first();
      $bredcum = "Activities";
      $data   = compact('account_details','title','bredcum','get_pages');
      return view('activities',$data);
    }
    
    public function sports() {
      $title  = "Sports | The Modern School Barmer";
      $account_details = Account::first();
      $get_pages = Pages::where(['master_pages_id' => 24,'master_pages_status' => 1])->first();
      $bredcum = "Sports";
      $data   = compact('account_details','title','bredcum','get_pages');
      return view('sports',$data);
    }
      
          public function laboratories() {
      $title  = "Laboratories | The Modern School Barmer";
      $account_details = Account::first();
      $get_pages = Pages::where(['master_pages_id' => 25,'master_pages_status' => 1])->first();
      $bredcum = "Laboratories";
      $data   = compact('account_details','title','bredcum','get_pages');
      return view('laboratories',$data);
    }
    
       public function building() {
      $title  = "Building | The Modern School Barmer";
      $account_details = Account::first();
      $get_pages = Pages::where(['master_pages_id' => 26,'master_pages_status' => 1])->first();
      $bredcum = "Building";
      $data   = compact('account_details','title','bredcum','get_pages');
      return view('building',$data);
    }
    
     public function transport() {
      $title  = "Transport | The Modern School Barmer";
      $account_details = Account::first();
      $get_pages = Pages::where(['master_pages_id' => 27,'master_pages_status' => 1])->first();
      $bredcum = "Transport";
      $data   = compact('account_details','title','bredcum','get_pages');
      return view('transport',$data);
    }
    
     public function library() {
      $title  = "Library | The Modern School Barmer";
      $account_details = Account::first();
      $get_pages = Pages::where(['master_pages_id' => 30,'master_pages_status' => 1])->first();
      $bredcum = "Library";
      $data   = compact('account_details','title','bredcum','download_record');
      return view('library',$data);
    }
    
      public function disclaimer_policy() {
      $title  = "Disclaimer Policy | The Modern School Barmer";
      $account_details = Account::first();
      $get_pages = Pages::where(['master_pages_id' => 29,'master_pages_status' => 1])->first();
      $bredcum = "Disclaimer Policy";
      $data   = compact('account_details','title','bredcum','get_pages');
      return view('disclaimer_policy',$data);
    }
    
    public function privacy_policy() {
      $title  = "Privacy Policy | The Modern School Barmer";
      $account_details = Account::first();
      $get_pages = Pages::where(['master_pages_id' => 28,'master_pages_status' => 1])->first();
      $bredcum = "Privacy Policy";
      $data   = compact('account_details','title','bredcum','get_pages');
      return view('privacy_policy',$data);
    }
    
     public function shipping_delivery() {
      $title  = "Shipping & Delivery Policy | The Modern School Barmer";
      $account_details = Account::first();
      $get_pages = Pages::where(['master_pages_id' => 5,'master_pages_status' => 1])->first();
      $bredcum = "Shipping & Delivery Policy";
      $data   = compact('account_details','title','bredcum','get_pages');
      return view('shipping_delivery',$data);
    }
    
    public function terms_conditions() {
      $title  = "Terms & Conditions | The Modern School Barmer";
      $account_details = Account::first();
      $get_pages = Pages::where(['master_pages_id' => 6,'master_pages_status' => 1])->first();
      $bredcum = "Terms & Conditions";
      $data   = compact('account_details','title','bredcum','get_pages');
      return view('terms_conditions',$data);
    }
    
    public function refund_policy() {
      $title  = "Refund Policy | The Modern School Barmer";
      $account_details = Account::first();
      $get_pages = Pages::where(['master_pages_id' => 4,'master_pages_status' => 1])->first();
      $bredcum = "Refund Policy";
      $data   = compact('account_details','title','bredcum','get_pages');
      return view('refund_policy',$data);
    }
    
    public function download() {
      $title  = "Downloads | The Modern School Barmer";
      $account_details = Account::first();
      $download_record = Downloads::where(['download_status' => 1])->get();
      $bredcum = "Downloads";
      $data   = compact('account_details','title','bredcum','download_record');
      return view('download',$data);
    }
    
    public function tc() {
      $title  = "Downloads | The Modern School Barmer";
      $account_details = Account::first();
      $download_record = Downloads::where(['download_status' => 1])->get();
      $bredcum = "TC";
      $data   = compact('account_details','title','bredcum','download_record');
      return view('tc',$data);
    }
    
    public function tcfetch(Request $request){
        // $sno = $request['sno'];
        $admission = $request['admission'];
        $users = TC::select('*')
                // ->where('s_number', '=', $sno)
                ->where('admission_number', '=', $admission)
                ->where('tc_is_deleted', '=', 'N')
                ->get();
                if($users->isEmpty())
                {
                     return response()->json(['Fail'=>'Records Not Found']);
                }
                else
                {
                    return response()->json(['Success'=>$users]);
                }
    }


    public function registration() {
      $title  = "Registration | Neo Dales Play School";
      $account_details = Account::first();
      $bredcum = "Registration";
      $data   = compact('account_details','title','bredcum');
      return view('registration',$data);
    }

    
    public function registration_submit(Request $request) {
        DB::table('tms_admission')->insert($request->input());
         	return redirect()->back()->withSuccess('Your form has been submitted successfully');   
    }
    
    
    /*===========================================================================================================================================*/


    public function events_details($slug) {
      
      $title  = "Neo Dales Play School | Each Child a StarNeo Dales Play School | Each Child a Star";
      $account_details = Account::first();
      $get_events = Events::where(['event_slug' => $slug])->first();

      $next_slug = Events::where("event_id", ">", $get_events->event_id)->orderBy('event_id', 'ASC')->first();

      $prev_slug = Events::where("event_id", "<", $get_events->event_id)->orderBy('event_id', 'ASC')->first();

      $current_url = urlencode(url('event-details/'.$slug));

      $bredcum = "$get_events->event_name";
      $data   = compact('title','account_details','get_events','bredcum', 'next_slug', 'prev_slug', 'current_url');
      return view('events_details',$data);
    }


    public function about_us() {
      $title  = "About Us | The Modern School Barmer";
      $account_details = Account::first();
      $get_pages = Pages::where(['master_pages_id' => 1 ,'master_pages_status' => 1])->first();
      $get_teach = We_Teach::where(['we_teach_status' => 1])->get();

      $bredcum = "About Us";

      $data   = compact('account_details','title','bredcum','get_pages','get_teach');
      return view('about_us',$data);
    }


   


    public function infrastructure() {
      $title  = "Infrastructure | The Modern School Barmer";
      $get_pages = Pages::where(['master_pages_id' => 8 ,'master_pages_status' => 1])->first();
      $account_details = Account::first();
      $bredcum = "Infrastructure";
      $data   = compact('account_details','title','bredcum','get_pages');
      return view('infrastructure',$data);
    }


    public function modules_activities() {
      $title  = "Activities | The Modern School Barmer";
      $get_pages = Pages::where(['master_pages_id' => 9 ,'master_pages_status' => 1])->first();
      $account_details = Account::first();
      $bredcum = "Modules &#038; Activities";
      $data   = compact('account_details','title','bredcum','get_pages');
      return view('modules_activities',$data);
    }


    public function rules_regulations() {
      $title  = "Rules & Regulations | The Modern School Barmer";
      $get_pages = Pages::where(['master_pages_id' => 10 ,'master_pages_status' => 1])->first();
      $account_details = Account::first();
      $bredcum = "Rules &#038; Regulations";
      $data   = compact('account_details','title','bredcum','get_pages');
      return view('rules_regulations',$data);
    }


    public function school_uniform() {
      $title  = "School Uniform | The Modern School Barmer";
      $get_pages = Pages::where(['master_pages_id' => 11 ,'master_pages_status' => 1])->first();
      $account_details = Account::first();
      $bredcum = "School Uniform";
      $data   = compact('account_details','title','bredcum','get_pages');
      return view('school_uniform',$data);
    }


    public function information_technology() {
      $title  = "Information Technology | The Modern School Barmer";
      $get_pages = Pages::where(['master_pages_id' => 12 ,'master_pages_status' => 1])->first();
      $account_details = Account::first();
      $bredcum = "Information Technology";
      $data   = compact('account_details','title','bredcum','get_pages');
      return view('information_technology',$data);
    }


    public function conveyance() {
      $title  = "Conveyance | The Modern School Barmer";
      $get_pages = Pages::where(['master_pages_id' => 13 ,'master_pages_status' => 1])->first();
      $account_details = Account::first();
      $bredcum = "Conveyance";
      $data   = compact('account_details','title','bredcum','get_pages');
      return view('conveyance',$data);
    }


    public function school_captains() {
      $title  = "School Captains | The Modern School Barmer";
      $get_pages = Pages::where(['master_pages_id' => 14 ,'master_pages_status' => 1])->first();
      $account_details = Account::first();
      $bredcum = "School Captains";
      $data   = compact('account_details','title','bredcum','get_pages');
      return view('school_captains',$data);
    }


    public function stars_of_yesteryears() {
      $title  = "Stars Of Yesteryears | The Modern School Barmer";
      $get_pages = Pages::where(['master_pages_id' => 15 ,'master_pages_status' => 1])->first();
      $account_details = Account::first();
      $bredcum = "Stars of Yesteryears";
      $data   = compact('account_details','title','bredcum','get_pages');
      return view('stars_of_yesteryears',$data);
    }


    public function school_hours_security() {
      $title  = "School Hours &#038; Security | The Modern School Barmer";
      $get_pages = Pages::where(['master_pages_id' => 16 ,'master_pages_status' => 1])->first();
      $account_details = Account::first();
      $bredcum = "School Hours &#038; Security";
      $data   = compact('account_details','title','bredcum','get_pages');
      return view('school_hours_security',$data);
    }


    public function fee_structure() {
      $title  = "Fee Structure | The Modern School Barmer";
      $account_details = Account::first();
      $get_pages = Pages::where(['master_pages_id' => 17 ,'master_pages_status' => 1])->first();

      $bredcum = "Fee Structure";
      $data   = compact('account_details','title','bredcum','get_pages');
      return view('fee_structure',$data);
    }


    public function online_fee_deposit() {
      $title  = "Online Fee Deposit | The Modern School Barmer";
      $account_details = Account::first();

      $bredcum = "Online Fee Deposit";
      $data   = compact('account_details','title','bredcum');
      return view('online_fee_deposit',$data);
    }


    public function neft_payment() {
      $title  = "NEFT Payment | The Modern School Barmer";
      $account_details = Account::first();
      $get_pages = Pages::where(['master_pages_id' => 18 ,'master_pages_status' => 1])->first();

      $bredcum = "NEFT Payment";
      $data   = compact('account_details','title','bredcum','get_pages');
      return view('neft_payment',$data);
    }


   
    public function register_process() {
      $title  = "Registration | Neo Dales Play School Neo Dales Play School";
      $account_details = Account::first();
      // $admission_record = Admission_Points::where(['admission_point_status' => 1])->get();
      $classes_record = Classes::where(['class_status' => 1])->get();
      $bredcum = "Request Slip for Registration";
      $data   = compact('account_details','title','bredcum','admission_record','classes_record');
      return view('register',$data);
    }

    
    public function state()
    {
       return $state = DB::select('select * from tms_state_masters ');
    }
    public function city(Request $request)
    {
       // return $city = DB::select('select * from tms_city_masters');
     // $city =  DB::table('tms_city_masters')->select('name')->where('state_master_id', '1');
     //   return response()->json($city);
          $cities = DB::table("tms_city_masters")
            ->where("state_master_id",$request->stateId)
            ->orderby("name","ASC")
            ->pluck("name");
            return response()->json($cities);

                    
    }






  

    public function parents_faq() {
      $title  = "FAQs | The Modern School Barmer";
      $account_details = Account::first();
      $faq_record = Faqs::where(['faq_status' => 1])->get();
      $bredcum = "FREQUENTLY ASKED QUESTIONS";
      $data   = compact('account_details','title','bredcum','faq_record');
      return view('parents_faq',$data);
    }


    


   


    public function subit_fees_success(Request $request){

        DB::table('student_fees')->where('transaction_id', $_POST['txnid'])->update(array('transaction_status' => 1));

// $transactionMessage='';
//     $transactionId='';
    if( $_POST['status'] == "success"){
      $response=$_POST['status'];
      
        $transactionMessage='Transaction Successfully';
        $transactionId=$_POST['txnid']; 

        
        $account_details = Account::first();
        $bredcum = "Online Fee Deposit";
        $data   = compact('account_details','transactionMessage','transactionId','bredcum');
        return view('online_fee_deposit',$data);

//        return redirect('/submit-fees-online')->with('success', 'Transaction Successfully');
      
      } else{

        $transactionMessage='Transaction Failed';
        $transactionId='';  

                $account_details = Account::first();
        $bredcum = "Online Fee Deposit";
        $data   = compact('account_details','transactionMessage','transactionId','bredcum');
        return view('online_fee_deposit',$data);
      }

    }



    public function subit_fees_failure(Request $request){

        $transactionMessage='Transaction Failed';
        $transactionId='';  

        $account_details = Account::first();
        $bredcum = "Online Fee Deposit";
        $data   = compact('account_details','transactionMessage','transactionId','bredcum');
        return view('online_fee_deposit',$data);

    }

    




     public function submit_fees(Request $request){


      $data = array(
        'sr_no'     => $_POST['reference_no'],
        'fee_amount'  => $_POST['amount'],
        'student_name'  => $_POST['student_name'],
        'payment_mode'  => $_POST['payment'],
        'card_brand'  => $_POST['card'],
        'person_name'   => $_POST['ship_name'],
        'contact_msg'   => $_POST['address'],
        'city_name'   => $_POST['city'],
        'state_name'  => $_POST['state'],
        'country_name'  => $_POST['country_name'],
        'zip_code'    => $_POST['postal_code'],
        'email_id'    => $_POST['student_email'],
        'mobile_no'   => $_POST['phone']
      );


      DB::table('student_fees')->insert($data);

      $ids = DB::getPdo()->lastInsertId();


      $_SESSION['checkout_data'] = $data;

        $reference_no = $_POST['reference_no'];
        $amount       = $_POST['amount'];
        $name         = $_POST['student_name'];
        $address      = $_POST['address'];
        $city         = $_POST['city'];
        $state        = $_POST['state'];
        // $country   = 'IND';
        $postal_code  = $_POST['postal_code'];
        $email        = $_POST['student_email'];
        $phone        = $_POST['phone'];

        // Merchant key here as provided by Payu
        $MERCHANT_KEY = "3juvuc"; // "adTtA"; // "gtKFFx"; // "rf9jGP"; //Please change this value with live key for production

        $hash_string  = '';
        // Merchant Salt as provided by Payu
        $SALT         = "uOFe8G95"; // "dFdV3hCn"; // "eCwWELxi"; // "996FY4lE"; //Please change this value with live salt for production

        // End point - change to https://secure.payu.in for LIVE mode
        $PAYU_BASE_URL= "https://secure.payu.in/_payment";


        if(empty($posted['txnid'])) {
           // Generate random transaction id
          $txnid = substr(hash('sha256', mt_rand() . microtime()), 0, 20);
        } else {
          $txnid = $posted['txnid'];
        }
        $hash = '';

        DB::table('student_fees')->where('student_fees_id', $ids)->update(array('transaction_id' => $txnid));


        // $checkout = $input['checkout'];
        $nameArr = explode(" ",$name);
        $posted = array(
          "key"       => $MERCHANT_KEY,
          "txnid"     => $txnid,
          "amount"    => round($amount),
          "firstname"   => $name,
          "email"     => "",
          "phone"     => $phone,
          "productinfo" => $reference_no,
        );


        // Hash Sequence
        $hashSequence = "key|txnid|amount|productinfo|firstname|email|udf1|udf2|udf3|udf4|udf5|udf6|udf7|udf8|udf9|udf10";
        if(empty($posted['hash']) && sizeof($posted) > 0) {


          if(empty($posted['key']) || empty($posted['txnid']) || empty($posted['amount']) || empty($posted['firstname']) || empty($posted['phone']) || empty($posted['productinfo'])  ) {
            $formError = 1;

          } else {
            
          $hashVarsSeq = explode('|', $hashSequence);
         
          foreach($hashVarsSeq as $hash_var) {
              $hash_string .= isset($posted[$hash_var]) ? $posted[$hash_var] : '';
              $hash_string .= '|';
            }
              // print_r($hash_string);
              // die;

            $hash_string .= $SALT;

            // echo $hash_string; die;
            $hash = strtolower(hash('sha512', $hash_string));
            $action = $PAYU_BASE_URL . '/_payment';
          }
        } elseif(!empty($posted['hash'])) {
          $hash = $posted['hash'];
          $action = $PAYU_BASE_URL . '/_payment';
        }

        $main_url_success = URL::to("submit-fees-success");
        $main_url_failure = URL::to("submit-fees-failure");

        
        ?><form action="<?php echo $action; ?>" method="post" id="payuForm" name="payuForm" >

        <input type = "hidden" name = "_token" value = "<?php  Session::token()  ?>" />

          <input type="hidden" name="key" value="<?php echo $MERCHANT_KEY ?>" />
          <input type="hidden" name="hash" value="<?php echo $hash ?>"/>
          <input type="hidden" name="txnid" value="<?php echo $txnid ?>" />

          <input type="hidden" name="surl" value="<?php echo $main_url_success  ; ?>" />
          <input type="hidden" name="furl" value="<?php echo $main_url_failure  ; ?>" />

          <input type="hidden" name="amount" value="<?php echo round($amount) ?>" />

          <input type="hidden" name="service_provider" value="">

          <input type="hidden" name="firstname" id="firstname" value="<?php echo $name; ?>" />
          <input type="hidden" name="lastname" id="lastname" value="" />
          <input type="hidden" name="email_id" id="email" value="<?php echo $email; ?>" />
          <input type="hidden" name="phone" value="<?php echo $phone; ?>" />
          <input type="hidden" name="productinfo" value="<?php echo $reference_no; ?>" />
        </form>

        <script type="text/javascript">
          var payuForm = document.forms.payuForm;
            payuForm.submit();
        </script>

        <?php
        die;
    }




    public function save_we_teach($id = false,Request $request){
         
      if($id != ""){
          
        $values=array(
          'we_teach_title'         => $request['we_teach_title'],
          'we_teach_description'   => $request['we_teach_description'],
        );
        We_Teach::where('we_teach_id', $id )->update($values);
        
        $file = $request->file('we_teach_image');
        if($file != ""){
          $destinationPath = 'uploads/we_teach_image';
          $imageName = 'uploads/we_teach_image/'.time().'.'.$file->getClientOriginalExtension();
          $file->move($destinationPath,$imageName);
          $image=array(
              'we_teach_image'         => $imageName
          );
          We_Teach::where('we_teach_id', $id )->update($image);
        }
                        
        return Redirect('admin-panel/view-we-teach');
          
      } else {

        $Add = new We_Teach;
        $Add->we_teach_title            =   $request['we_teach_title'];
        $Add->we_teach_description      =   $request['we_teach_description'];
        $Add->save();
        $we_teach_id = $Add->id;

        $file = $request->file('we_teach_image');
        if($file != ""){
          $destinationPath = 'uploads/we_teach_image';
          $imageName = 'uploads/we_teach_image/'.time().'.'.$file->getClientOriginalExtension();
          $file->move($destinationPath,$imageName);
          $image=array(
              'we_teach_image'         => $imageName
          );
          We_Teach::where('we_teach_id', $we_teach_id)->update($image);
        }

        return redirect('admin-panel/view-we-teach');      
      }
    }

    public function view_we_teach(Request $request){

      $admin_details = Admin::where(['admin_id' => session('tms_admin_id')])->first();

      if ($request['check'] != "") {
        We_Teach::whereIn('we_teach_id', $request['check'])->delete();
        return Redirect()->back();
      }

      $query = We_Teach::query();

      if($request['we_teach_title'] != "") {
        $query = $query->where('we_teach_title', 'LIKE', '%'.$request['we_teach_title'].'%');
      }

      if($request['we_teach_description'] != "") {
        $query = $query->where('we_teach_description', 'LIKE', '%'.$request['we_teach_description'].'%');
      }
      
      $app_data = array('we_teach_title'=>$request['we_teach_title'],'we_teach_description'=>$request['we_teach_description']);

      $get_record = $query->orderBy('we_teach_id','DESC')->paginate(25)->appends($app_data);

      $title         = "View We Teach";
      $data          = compact('title','get_record','admin_details');
      return view('admin_panel/view_we_teach',$data);

    }


    public function we_teach_status($id,$value) {
      We_Teach::where('we_teach_id', $id)->update(['we_teach_status' => $value]);
      return Redirect()->back();
    }        

    /*======================================================================*/

   
    public function career() {
      $title  = "Careers | The Modern School Barmer";
      $account_details = Account::first();
      $pre_state = $per_state = DB::select('select * from tms_state_masters ORDER BY name ASC');
      $bredcum = "Careers";
      $data   = compact('account_details','title','bredcum','pre_state','per_state');
      return view('career',$data);
    } 
    public function career_mail() {
      $title  = "Careers | The Modern School Barmer";
      $account_details = Account::first();
      $pre_state = $per_state = DB::select('select * from tms_state_masters ORDER BY name ASC');
      $bredcum = "Careers";
      $data   = compact('account_details','title','bredcum','pre_state','per_state');
      return view('career_mail',$data);
    } 

     public function career_submit(Request $request) {
         if($request->file('resume'))
         {
             $resume = $request->file('resume');
             $resume_new_name = rand() . '.' . $resume->getClientOriginalExtension();
             $resume->move(public_path('images'), $resume_new_name);
             $resume_ext =  $resume->getClientOriginalExtension();
             $data_resume ='http://neodales.edu.in/playschool/public/images/'.+$resume_new_name. '.' . $resume_ext;
         }
         else{
             $data_resume ="";
         }
         
          if($request->file('salary_proof'))
         {
            $salary_proof = $request->file('salary_proof');
            $salary_proof_new_name = rand() . '.' . $salary_proof->getClientOriginalExtension();
            $salary_proof->move(public_path('images'), $salary_proof_new_name);
            $salary_proof_ext =  $salary_proof->getClientOriginalExtension();
            $data_salary_proof ='http://neodales.edu.in/playschool/public/images/'.+$salary_proof_new_name. '.' . $salary_proof_ext;
         }
         else{
             $data_salary_proof ="";
         }
        
        $menphoto = $request->file('menphoto');
        $signature = $request->file('signature');
        
        $signature_new_name = rand() . '.' . $signature->getClientOriginalExtension();
        $menphoto_new_name = rand() . '.' . $menphoto->getClientOriginalExtension();
        
        $menphoto->move(public_path('images'), $menphoto_new_name);
        $signature->move(public_path('images'), $signature_new_name);
        
        
         $database['file'] = $menphoto_new_name;
         $database['user_mail'] = $request['email'];
        //  $database['data_salary_proof'] = $request['data_salary_proof'];
        //  $database['data_resume'] = $request['data_resume'];
         DB::table('tms_career')->insert($database);
        
    
      
              
      
      
        $data = array(
          'apply_for'                         => $request['apply_for'],
          'post_applied'                      => $request['post_applied'],
          'fsname'                            => $request['fsname'],
          'lname'                             => $request['lname'],
          'career_dob'                        => $request['career_dob'],
          'nationality'                       => $request['nationality'],
          'career_religion'                   => $request['career_religion'],
          'category'                          => $request['category'],
          'mobile'                            => $request['mobile'],
          'email'                             => $request['email'],
          'gender'                             => $request['gender'],
          'pre_add'                           => $request['pre_add'],
          'present_state'                     => $request['present_state'],
          'present_city'                      => $request['present_city'],
          'present_pincode'                   => $request['present_pincode'],
          'per_add'                           => $request['per_add'],
          'permanent_state'                   => $request['permanent_state'],
          'permanent_city'                    => $request['permanent_city'],
          'permanent_pincode'                 => $request['permanent_pincode'],
          'marital_status'                    => $request['marital_status'],
          'child_status'                      => $request['child_status'],
          'child1_name'                       => $request['child1_name'],
          'child1_sex'                        => $request['child1_sex'],
          'child1_dob'                        => $request['child1_dob'],
          'child1_class'                      => $request['child1_class'],
          'child1_school'                     => $request['child1_school'],
          'child2_name'                       => $request['child2_name'],
          'child2_sex'                        => $request['child2_sex'],
          'child2_dob'                        => $request['child2_dob'],
          'child2_class'                      => $request['child2_class'],
          'child2_school'                     => $request['child2_school'],
          'child3_name'                       => $request['child3_name'],
          'child3_sex'                        => $request['child3_sex'],
          'child3_dob'                        => $request['child3_dob'],
          'child3_class'                      => $request['child3_class'],
          'child3_school'                     => $request['child3_school'],
          'fname'                             => $request['fname'],
          'relation'                          => $request['relation'],
          'father_occupation'                 => $request['father_occupation'],
          'career_father_profession'          => $request['career_father_profession_'],
          'any_profession'                    => $request['any_profession'],
          'special_reason'                    => $request['special_reason'],
          'duties'                            => $request['duties'],
          'medical'                           => $request['medical'],
          'period'                            => $request['period'],
          'salary_expected'                   => $request['salary_expected'],
          'resume_new_name'                            => $data_resume,
          'salary_proof_new_name'                            => $data_salary_proof,
          'menphoto_new_name'                            => 'http://neodales.edu.in/playschool/public/images/'.+$menphoto_new_name. '.' . $menphoto->getClientOriginalExtension(),
          'signature_new_name'                            => 'http://neodales.edu.in/playschool/public/images/'.+$signature_new_name. '.' . $signature->getClientOriginalExtension(),
          
          'school_name1'                    => $request['school_name1'],
          'school_name2'                    => $request['school_name2'],
          'board1'                          => $request['board1'],
          'board2'                          => $request['board2'],
          'location1'                       => $request['location1'],
          'location2'                       => $request['location2'],
          'medium1'                         => $request['medium1'],
          'medium2'                         => $request['medium2'],
          'years1'                          => $request['years1'],
          'years2'                          => $request['years2'],
          
          'subject1'                        => $request['subject1'],
          'subject2'                        => $request['subject2'],
          'subject3'                        => $request['subject3'],
          'heigh_board1'                    => $request['heigh_board1'],
          'heigh_board2'                    => $request['heigh_board2'],
          'heigh_board3'                    => $request['heigh_board3'],
          'heigh_location1'                 => $request['heigh_location1'],
          'heigh_location2'                 => $request['heigh_location2'],
          'heigh_location3'                 => $request['heigh_location3'],
          'heigh_medium1'                   => $request['heigh_medium1'],
          'heigh_medium2'                   => $request['heigh_medium2'],
          'heigh_medium3'                   => $request['heigh_medium3'],
          'heigh_years1'                    => $request['heigh_years1'],
          'heigh_years2'                    => $request['heigh_years2'],
          'heigh_years3'                    => $request['heigh_years3'],
          
          'training_subject1'                    => $request['training_subject1'],
          'training_subject2'                    => $request['training_subject2'],
          'training_board1'                    => $request['training_board1'],
          'training_board2'                    => $request['training_board2'],
          'training_location1'                    => $request['training_location1'],
          'training_location2'                    => $request['training_location2'],
          'training_medium1'                    => $request['training_medium1'],
          'training_medium2'                    => $request['training_medium2'],
          'training_years1'                    => $request['training_years1'],
          'training_years2'                    => $request['training_years2'],
          
          
          'experience_board1'                    => $request['experience_board1'],
          'experience_board2'                    => $request['experience_board2'],
          'experience_board3'                    => $request['experience_board3'],
          'experience_location1'                    => $request['experience_location1'],
          'experience_location2'                    => $request['experience_location2'],
          'experience_location3'                    => $request['experience_location3'],
          'standard1'                    => $request['standard1'],
          'standard2'                    => $request['standard2'],
          'standard3'                    => $request['standard3'],
          'subjects1'                    => $request['subjects1'],
          'subjects2'                    => $request['subjects2'],
          'subjects3'                    => $request['subjects3'],
          'work1'                    => $request['work1'],
          'work2'                    => $request['work2'],
          'work3'                    => $request['work3'],
          'from_experience_years1'                    => $request['from_experience_years1'],
          'from_experience_years2'                    => $request['from_experience_years2'],
          'from_experience_years3'                    => $request['from_experience_years3'],
          'to_experience_years1'                    => $request['to_experience_years1'],
          'to_experience_years2'                    => $request['to_experience_years2'],
          'to_experience_years3'                    => $request['to_experience_years3'],
          
          'qualification'                    => $request['qualification'],
          
          'employment_type'                    => $request['employment_type'],
          
          'employment_name'                  => $request['employment_name'],
          'employment_employer'              => $request['employment_employer'],
          'employment_job'                   => $request['employment_job'],
          'employment_salary'                => $request['employment_salary'],
          'reason'                           => $request['reason'],
          
          'sportsandgames1'                           => $request['sportsandgames1'],
          'sportsandgames2'                           => $request['sportsandgames2'],
          'cultural1'                           => $request['cultural1'],
          'cultural2'                           => $request['cultural2'],
          
          'references_name1'                           => $request['references_name1'],
          'references_name2'                           => $request['references_name2'],
          'references_address1'                           => $request['references_address1'],
          'references_address2'                           => $request['references_address2'],
          'references_position1'                           => $request['references_position1'],
          'references_position2'                           => $request['references_position2'],
          'references_mobile1'                           => $request['references_mobile1'],
          'references_mobile2'                           => $request['references_mobile2'],
          
          
      );
       extract($data);

  

    
    
         
        Mail::send(['html' => 'career_mail'], $data, function($message) use ($email, $name) {
            $message->to('info@neodales.edu.in', 'Neo Dales  School')->subject('Career request has been placed - Neo Dales School');
            $message->from($email, $name);
        });
        
        Mail::send(['html' => 'career_mail'], $data, function($message) use($email, $name) {
         $message->to($email, $name)->subject('Career request has been placed - Neo Dales School');
         $message->from('neodales.admissions@gmail.com', 'Neo Dales School');
         });
        
        // Mail::send(['html' => 'career_mail'], $data, function($message) use ($email, $name) {
        //     $message->to('mudit@kempsolutions.in', 'Neo Dales  School')->subject('Career request has been placed - Neo Dales School');
        //     $message->from($email, $name);
        // });
        
        // Mail::send(['html' => 'career_mail'], $data, function($message) use($email, $name) {
        //  $message->to($email, $name)->subject('Career request has been placed - Neo Dales School');
        //  $message->from('mayank.c999@gmail.com', 'Neo Dales School');
        //  });
         
         
             return response()->json(['type'=>"success",'message'=> "Your form has been submitted successfully " ]);  
  
      
       // return $request->input();
    
    }
    
    
    public function admission_register() {
      $title  = "Registration | Neo Dales Play School";
      $account_details = Account::first();
      $guardstate = $motherstate = $fatherstate = $state = DB::select('select * from tms_state_masters ORDER BY name ASC');
      $person       = Time_Slots::where('time_slot_status','1')->where('time_slot_type','In-person Interaction')->whereColumn('total_appointment','>','my_appointment')->get();
      $video = Time_Slots::where('time_slot_status','1')->where('time_slot_type','Through Video Call')->whereColumn('total_appointment','>','my_appointment')->get();
      $classes_record = Classes::where(['class_status' => 1])->get();
      $bredcum = "Registration";
      $data   = compact('account_details','title','bredcum','admission_record','classes_record','state','fatherstate','motherstate','guardstate','person','video');
      return view('admission_register',$data);
    }


public function reg_mail()
{
    	session()->flush();
    $title  = "Careers | The Modern School Barmer";
      $account_details = Account::first();
      $pre_state = $per_state = DB::select('select * from tms_state_masters ORDER BY name ASC');
      $bredcum = "Careers";
      $data   = compact('account_details','title','bredcum','pre_state','per_state');
      return view('mail',$data);
}


 public function register_submitnew(Request $request) {
      
        //   $request->input();
     
        $child_photo = $request->file('child_photo');
        $child_photo_new_name = rand() . '.' . $child_photo->getClientOriginalExtension();
        $child_photo->move(public_path('registeration'), $child_photo_new_name);
        $child_photo_ext =  $child_photo->getClientOriginalExtension();
        $request['child_photo'] = $child_photo ='http://neodales.edu.in/playschool/public/registeration/'.+$child_photo_new_name. '.' . $child_photo_ext;
        
        $father_signature = $request->file('father_signature');
        $father_signature_new_name = rand() . '.' . $father_signature->getClientOriginalExtension();
        $father_signature->move(public_path('registeration'), $father_signature_new_name);
        $father_signature_ext =  $father_signature->getClientOriginalExtension();
        $request['father_signature'] = $father_signature ='http://neodales.edu.in/playschool/public/registeration/'.+$father_signature_new_name. '.' . $father_signature_ext;
        
        
        $mother_signature = $request->file('mother_signature');
        $mother_signature_new_name = rand() . '.' . $mother_signature->getClientOriginalExtension();
        $mother_signature->move(public_path('registeration'), $mother_signature_new_name);
        $mother_signature_ext =  $mother_signature->getClientOriginalExtension();
        $request['mother_signature'] = $mother_signature ='http://neodales.edu.in/playschool/public/registeration/'.+$mother_signature_new_name. '.' . $mother_signature_ext;
        
        
 
        
        if($request->file('child_aadhar'))
         {
             $child_aadhar = $request->file('child_aadhar');
             $child_aadhar_new_name = rand() . '.' . $child_aadhar->getClientOriginalExtension();
             $child_aadhar->move(public_path('registeration'), $child_aadhar_new_name);
             $child_aadhar_ext =  $child_aadhar->getClientOriginalExtension();
             $request['child_aadhar'] = $child_aadhar ='http://neodales.edu.in/playschool/public/registeration/'.+$child_aadhar_new_name. '.' . $child_aadhar_ext;
         }
         else{
            $request['child_aadhar'] = $child_aadhar ="";
         }
         
         
         if($request->file('category_certificate'))
         {
             $category_certificate = $request->file('category_certificate');
             $category_certificate_new_name = rand() . '.' . $category_certificate->getClientOriginalExtension();
             $category_certificate->move(public_path('registeration'), $category_certificate_new_name);
             $category_certificate_ext =  $category_certificate->getClientOriginalExtension();
             $request['category_certificate'] = $category_certificate ='http://neodales.edu.in/playschool/public/registeration/'.+$category_certificate_new_name. '.' . $category_certificate_ext;
         }
         else{
            $request['category_certificate'] = $category_certificate ="";
         }
         
        
    
   
        $blood_group = $request['blood_group'];
      if($blood_group=="A Positive")
      {
          $request['blood_group']="A+";
      }
      if($blood_group=="B Positive")
      {
          $request['blood_group']="B+";
      }
      if($blood_group=="AB Positive")
      {
          $request['blood_group']="AB+";
      }
      if($blood_group=="O Positive")
      {
          $request['blood_group']="O+";
      }

      
      
          $meeting_type = $request['meeting_type'];
          $entrance_test_type = $request['entrance_test_type'];
          $admission_class_name = $request['admission_class_name'];
          
          
             if($meeting_type=="In-person Interaction")
              {
                $request['time_slot_by_user']  =  $request['time_slot_by_user_person'];
                 $time_slot_data       = Time_Slots::where('time_slot_id',$request['time_slot_by_user'])->first();
                  $time_slot_by_user = $time_slot_data['meeting_date'].' '.$time_slot_data['time_slot_from'].' - '.$time_slot_data['time_slot_to'];
              }
              if($meeting_type=="Through Video Call")
              {
                  $request['time_slot_by_user']  =  $request['time_slot_by_user_video'];
                $time_slot_data       = Time_Slots::where('time_slot_id',$request['time_slot_by_user'])->first();
                  $time_slot_by_user = $time_slot_data['meeting_date'].' '.$time_slot_data['time_slot_from'].' - '.$time_slot_data['time_slot_to'];
              }   
          
       
          
          
         $bc_radio = $request['bc_radio'];
         if($bc_radio=="Yes")
          {
             $bc = $request->file('bc');
             $bc_new_name = rand() . '.' . $bc->getClientOriginalExtension();
             $bc->move(public_path('registeration'), $bc_new_name);
             $bc_ext =  $bc->getClientOriginalExtension();
             $request['bc'] = $bc ='http://neodales.edu.in/playschool/public/registeration/'.+$bc_new_name. '.' . $bc_ext;
             $bc_type="Yes";
          }
          if($bc_radio=="No")
          {
              $request['bc'] = $bc =  $request['bc_date'];
              $bc_type="No";
          }
          
          
         $tc_radio = $request['tc_radio'];
         if($tc_radio=="Yes")
          {
             $tc = $request->file('tc');
             $tc_new_name = rand() . '.' . $tc->getClientOriginalExtension();
             $tc->move(public_path('registeration'), $tc_new_name);
             $tc_ext =  $tc->getClientOriginalExtension();
            $request['tc'] = $tc ='http://neodales.edu.in/playschool/public/registeration/'.+$tc_new_name. '.' . $tc_ext;
            $tc_type="Yes";
          }
          if($tc_radio=="No")
          {
              $request['tc'] =$tc =  $request['tc_date'];
              $tc_type="No";
          }
          if($tc_radio=="NotApplicable")
          {
              $request['tc'] =  $tc =  "Not Applicable";
          }
          
 
 $request['admission_name']= $request['admission_name_first'].' '.$request['admission_name_middle'].' '.$request['admission_name_last'];          
$request->request->remove('time_slot_by_user_person');
$request->request->remove('time_slot_by_user_video');
$request->request->remove('bc_radio');
$request->request->remove('tc_radio');
$request->request->remove('bc_date');
$request->request->remove('tc_date');
$request->request->remove('amount');
$request->request->remove('write_captcha');


$request->request->remove('admission_name_first');
$request->request->remove('admission_name_middle');
$request->request->remove('admission_name_last');



    $date = Carbon::now();
    $text = $date->toDateTimeString();
    $text = str_replace(' ', '', $text);
    $text = str_replace('-', '', $text);
    $text = str_replace(':', '', $text);
    
    $text = substr($text, 2);

 $order_id =  $request['order_id']=$text ;
      
  DB::table('tms_admission')->insert(  $request->input() );
      
          $id = DB::getPdo()->lastInsertId();
          
      return response()->json(['type'=>"success",'message'=> "Your form has been submitted successfully","lastid"=>$id]);  
    }
    
    
    
    public function payment($id)
    {
        //$detail[0]->order_id
        // $_SESSION['lastid'] = $request->input("lastid");
        
            $detail = DB::select('select * from tms_admission where admission_id='.$id);
            // Session::put('txnid', $detail[0]->order_id);
            // Session::put('amount', "1.00");
            // Session::put('key', "6exoCM");
            // Session::put('salt', "MyDqpAno");
            // Session::put('email', $detail[0]->admission_father_email);
            // Session::put('firstname', $detail[0]->admission_name);
            // Session::put('mail_count', "0");
            
            session(['txnid' => $detail[0]->order_id]);
            session(['amount' => "2500.00"]);
            session(['key' => "6exoCM"]);
            session(['salt' => "MyDqpAno"]);
            session(['email' => $detail[0]->admission_father_email]);
            session(['firstname' => $detail[0]->admission_name]);
            // session(['mail_count' => "0"]);
            
            $data   = compact('detail');
            return view('payment',$data);
    }

public function response(Request $request)
    {
                   
                      $request->request->remove('cardCategory');
                      $request->request->remove('discount');
                      $request->request->remove('address2');
                      $request->request->remove('city');
                      $request->request->remove('state');
                      $request->request->remove('phone');
                      $request->request->remove('country');
                      $request->request->remove('zipcode');
                      $request->request->remove('name_on_card');
                      $request->request->remove('cardnum');
                      $request->request->remove('cardhash');
                      $request->request->remove('issuing_bank');
                      $request->request->remove('card_type');
                      $request->request->remove('page');
                           $request->input();
                            
                          
                      
                            $checkstatus = $request->input('status');
                                if(empty($checkstatus))
                                {
                                    return view('404');
                                }
                                else
                                {
                                    // $mihpayid = DB::select('select * from tms_admission_transection where mihpayid='.$request->input("mihpayid"));
                                    $txnid = DB::select('select * from tms_admission_transection where txnid='.$request->input("txnid"));
                                    if (($txnid == null)) 
                                    {
                                        DB::table('tms_admission_transection')->insert($request->input());
                                    }   
                                    
                                        // if($request->input("txnid")==session('txnid') )
                                        
                                            $admission = DB::select('select * from tms_admission where order_id='.$request->input("txnid"));
                                            
                                            $admission_father_email = $admission[0]->admission_father_email;
                                            $admission_father_name = $admission[0]->admission_father_name;
                                            $admission_mother_email = $admission[0]->admission_mother_email;
                                            $admission_mother_name = $admission[0]->admission_mother_name;
                                            $time_slot_data       = Time_Slots::where('time_slot_id',$admission[0]->time_slot_by_user)->first();
                                            $time_slot_by_user = $time_slot_data['meeting_date'].' '.$time_slot_data['time_slot_from'].' - '.$time_slot_data['time_slot_to'];
                                           
                                           
                                              $data = array(
                                              'admission_name'                              => $admission[0]->admission_name,
                                              'admission_dob'                               => $admission[0]->admission_dob,
                                              'admission_gender'                            => $admission[0]->admission_gender,
                                              'admission_class_name'                        => $admission[0]->admission_class_name,
                                              'nationality'                                 => $admission[0]->nationality,
                                              'blood_group'                                 => $admission[0]->blood_group,
                                              'admission_religion'                          => $admission[0]->admission_religion,
                                              'category'                                    => $admission[0]->category,
                                              'aadhaar'                                     => $admission[0]->aadhaar,
                                              'admission_address'                           => $admission[0]->admission_address,
                                              'admission_state'                             => $admission[0]->admission_state,
                                              'admission_city'                              => $admission[0]->admission_city,
                                              'admission_pincode'                           => $admission[0]->admission_pincode,
                                              'admission_father_name'                       => $admission[0]->admission_father_name,
                                              'admission_father_age'                        => $admission[0]->admission_father_age,
                                              'admission_father_qualification'              => $admission[0]->admission_father_qualification,
                                              'father_occupation'                           => $admission[0]->father_occupation,
                                              'father_profession'                           => $admission[0]->father_profession,
                                              'father_any_profession'                       => $admission[0]->father_any_profession,
                                              'father_sector'                               => $admission[0]->father_sector,
                                              'father_designation'                          => $admission[0]->father_designation,
                                              'father_office_address'                       => $admission[0]->father_office_address,
                                              'father_state'                                => $admission[0]->father_state,
                                              'father_city'                                 => $admission[0]->father_city,
                                              'father_pincode'                              => $admission[0]->father_pincode,
                                              'father_establishment'                        => $admission[0]->father_establishment,
                                              'admission_father_email'                      => $admission[0]->admission_father_email,
                                              'admission_father_cell_no1'                   => $admission[0]->admission_father_cell_no1,
                                              'admission_father_cell_no2'                   => $admission[0]->admission_father_cell_no2,
                                              'admission_mother_name'                       => $admission[0]->admission_mother_name,
                                              'admission_mother_age'                        => $admission[0]->admission_mother_age,
                                              'admission_mother_qualification'              => $admission[0]->admission_mother_qualification,
                                              'mother_occupation'                           => $admission[0]->mother_occupation,
                                              'mother_profession'                           => $admission[0]->mother_profession,
                                              'mother_any_profession'                       => $admission[0]->mother_any_profession,
                                              'mother_sector'                               => $admission[0]->mother_sector,
                                              'mother_designation'                          => $admission[0]->mother_designation,
                                              'mother_office_address'                       => $admission[0]->mother_office_address,
                                              'mother_state'                                => $admission[0]->mother_state,
                                              'mother_city'                                 => $admission[0]->mother_city,
                                              'mother_pincode'                              => $admission[0]->mother_pincode,
                                              'mother_establishment'                        => $admission[0]->mother_establishment,
                                              'admission_mother_email'                      => $admission[0]->admission_mother_email,
                                              'admission_mother_cell_no1'                   => $admission[0]->admission_mother_cell_no1,
                                              'admission_mother_cell_no2'                   => $admission[0]->admission_mother_cell_no2,
                                              'admission_guardian_name'                     => $admission[0]->admission_guardian_name,
                                              'admission_guardian_age'                      => $admission[0]->admission_guardian_age,
                                              'admission_guardian_occupation'               => $admission[0]->admission_guardian_occupation,
                                              'admission_guardian_address'                  => $admission[0]->admission_guardian_address,
                                              'admission_guardian_cell_no'                  => $admission[0]->admission_guardian_cell_no,
                                              'relationship'                                => $admission[0]->relationship,
                                              'guardian_state'                              => $admission[0]->guardian_state,
                                              'guardian_city'                               => $admission[0]->guardian_city,
                                              'guardian_pincode'                            => $admission[0]->guardian_pincode,
                                              'admission_last_school_name'                  => $admission[0]->admission_last_school_name,
                                              'sibling'                                    => $admission[0]->sibling,
                                              'sibling_details'                            => $admission[0]->sibling_details,
                                              'other_point'                                 => $admission[0]->other_point,
                                              'meeting_type'                                 => $admission[0]->meeting_type,
                                              'entrance_test_type'                                 => $admission[0]->entrance_test_type,
                                              'registration_form_for'                                 => $admission[0]->registration_form_for,
                                              'child_aadhar'                                 => $admission[0]->child_aadhar,
                                              'category_certificate'                                 =>$admission[0]->category_certificate,
                                              'time_slot_by_user'                                 => $time_slot_by_user,
                                              'child_photo'                                 => $admission[0]->child_photo,
                                              'father_signature'                            => $admission[0]->father_signature,
                                              'mother_signature'                            => $admission[0]->mother_signature,
                                              'bc'                                 => $admission[0]->bc,
                                              'bc_type'                                 => $admission[0]->bc_type,
                                              'tc'                                 => $admission[0]->tc,
                                              'tc_type'                                 => $admission[0]->tc_type,
                                              'transaction_id'                               => $request->input("txnid"),
                                              'trans_status'                                 => $request->input("status"),
                                              'final_amount'                                 => $request->input("net_amount_debit"),
                                     
                                           
                                          );
                                           
                                            extract($data);
                                           
                                    
                                          $id = DB::getPdo()->lastInsertId();
                                          $data['form_id'] = $admission[0]->admission_id;
                                    
                                          
                                          
                                           
                                            $time_slot_data       = Time_Slots::where('time_slot_id',$admission[0]->time_slot_by_user)->first();
                                             $time_slot_data['my_appointment'] = $time_slot_data['my_appointment'] + 1;
                                             
                                             if($time_slot_data['my_appointment'] == $time_slot_data['total_appointment'])
                                             {
                                                  $values=array(
                                                  'my_appointment'         => $time_slot_data['my_appointment'],
                                                  'time_slot_status'         => "0",
                                                );
                                                Time_Slots::where('time_slot_id', $admission[0]->time_slot_by_user )->update($values);
                                             }
                                             else
                                             {
                                                  $values=array(
                                                  'my_appointment'         => $time_slot_data['my_appointment'],
                                                );
                                                Time_Slots::where('time_slot_id', $admission[0]->time_slot_by_user )->update($values);
                                             }
                                             
                                    
                                         Mail::send(['html' => 'mail'], $data, function($message) use ($admission_father_email, $admission_father_name) {
                                            $message->to('ndps.admissions@neodales.edu.in', 'Neo Dales  School')->subject('Registration request has been placed - Neo Dales School');
                                            $message->from($admission_father_email, $admission_father_name);
                                        });
                                        
                                        //father
                                        Mail::send(['html' => 'mail'], $data, function($message) use($admission_father_email, $admission_father_name) {
                                         $message->to($admission_father_email, $admission_father_name)->subject('Registration request has been placed - Neo Dales School');
                                         $message->from('neodales.admissions@gmail.com', 'Neo Dales School');
                                         });
                                         
                                         Mail::send(['html' => 'mail'], $data, function($message) use($admission_father_email, $admission_father_name) {
                                         $message->to("mayank.c999@gmail.com", $admission_father_name)->subject('Registration request has been placed - Neo Dales School');
                                         $message->from('neodales.admissions@gmail.com', 'Neo Dales School');
                                         });
                                         
                                         if($admission_mother_email!="")
                                         {
                                              Mail::send(['html' => 'mail'], $data, function($message) use($admission_mother_email, $admission_mother_name) {
                                             $message->to($admission_mother_email, $admission_mother_name)->subject('Registration request has been placed - Neo Dales School');
                                             $message->from('neodales.admissions@gmail.com', 'Neo Dales School');
                                         });
                                         }
                                         
                                        
                                            
                                            
                                            $title  = "Response | Neo Dales Play School Neo Dales Play School";
                                            $account_details = Account::first();
                                            $bredcum = "Response";
                                            $matchstatus = "match";// txnid match
                                            $data   = compact('account_details','title','bredcum','response','msg',"matchstatus");
                                            return view('response',$data);
                                        
                                }
                            
                    
                           
                            
                        
                        
                         
                         

                       
                                           
        
		
        }
                            
                    
                           
                            
                        
                        
                         
                         

                       
                                           
        
		
        
        
        public function trans_details()
        {
            return DB::table('tms_admission_transection')->get();
        }

        public function database_backup()
        {
          $DbName             = env('DB_DATABASE');
          $get_all_table_query = "SHOW TABLES ";
          $result = DB::select(DB::raw($get_all_table_query));
      
          $prep = "Tables_in_$DbName";
          foreach ($result as $res){
              $tables[] =  $res->$prep;
          }
      
      
      
          $connect = DB::connection()->getPdo();
      
          $get_all_table_query = "SHOW TABLES";
          $statement = $connect->prepare($get_all_table_query);
          $statement->execute();
          $result = $statement->fetchAll();
      
      
          $output = '';
          foreach($tables as $table)
          {
              $show_table_query = "SHOW CREATE TABLE " . $table . "";
              $statement = $connect->prepare($show_table_query);
              $statement->execute();
              $show_table_result = $statement->fetchAll();
      
              foreach($show_table_result as $show_table_row)
              {
                  $output .= "\n\n" . $show_table_row["Create Table"] . ";\n\n";
              }
              $select_query = "SELECT * FROM " . $table . "";
              $statement = $connect->prepare($select_query);
              $statement->execute();
              $total_row = $statement->rowCount();
      
              for($count=0; $count<$total_row; $count++)
              {
                  $single_result = $statement->fetch(\PDO::FETCH_ASSOC);
                  $table_column_array = array_keys($single_result);
                  $table_value_array = array_values($single_result);
                  $output .= "\nINSERT INTO $table (";
                  $output .= "" . implode(", ", $table_column_array) . ") VALUES (";
                  $output .= "'" . implode("','", $table_value_array) . "');\n";
              }
          }
          $file_name = 'database_backup_on_' . date('y-m-d') . '.sql';
          $file_handle = fopen($file_name, 'w+');
          fwrite($file_handle, $output);
          fclose($file_handle);
          header('Content-Description: File Transfer');
          header('Content-Type: application/octet-stream');
          header('Content-Disposition: attachment; filename=' . basename($file_name));
          header('Content-Transfer-Encoding: binary');
          header('Expires: 0');
          header('Cache-Control: must-revalidate');
          header('Pragma: public');
          header('Content-Length: ' . filesize($file_name));
          ob_clean();
          flush();
          readfile($file_name);
          unlink($file_name);
        }
        
     

            
            

}
