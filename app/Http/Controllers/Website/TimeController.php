<?php
namespace App\Http\Controllers\Website;
// namespace App\Mail;

use Illuminate\Http\Request;
use App\Http\Requests;
use App\Http\Controllers\Controller;
use DB;
use Illuminate\Support\Facades\Mail;

// wait

use App\Model\Admin\Account; // model name
use App\Model\Admin\Admin; // model name
use App\Model\Admission_Points\Admission_Points; // model name
use App\Model\Time_Slots\Time_Slots; // model name
use App\Model\Time_Slots\Time_Slots_Trans; // model name
use App\Model\Register\Register; // model name

use Redirect;
use Response;
use Session;
use URL;


class TimeController extends Controller 
{
    public function __construct()
  	{
      	$this->middleware('ValidateAdmin');
  	}
  	
  	public function time_slot_view($id = false,Request $request){

        if($id){
        $get_record = Time_Slots::where(['time_slot_id' => $id])->first();
        $get_record['time_slot_from'] = date("H:i", strtotime($get_record['time_slot_from']));
        $get_record['time_slot_to'] = date("H:i", strtotime($get_record['time_slot_to']));
        

      } else {
        $get_record = "";
      }
      
	    $account       = Account::first();
	    $time_slot       = Time_Slots::all()->sortByDesc("time_slot_id");
	    $admin_details = Admin::where(['admin_id' => session('tms_admin_id')])->first();
	 	$title         = "Set Time Set";
	    $data          = compact('title','admin_details','account','time_slot','get_record');
	    return view('admin_panel/view_time_slot',$data);

	}
	
	public function save_time($id = false,Request $request)
	{

  $request['time_slot_from'] =  date("g:ia", strtotime($request['time_slot_from']));
  $request['time_slot_to'] =  date("g:ia", strtotime($request['time_slot_to']));
	   
	    if($id != ""){
          $values=array(
          'meeting_date'                              => $request['meeting_date'],
          'time_slot_from'                               => $request['time_slot_from'],
          'time_slot_to'                            => $request['time_slot_to'],
          'time_slot_type'                            => $request['time_slot_type'],
          'total_appointment'                            => $request['total_appointment'],
          'my_appointment'                            => $request['my_appointment'],
          );
          Time_Slots::where('time_slot_id', $id)->update($values);
          return Redirect('admin-panel/time-slot');

      } else {
            $data = array(
              'meeting_date'                              => $request['meeting_date'],
              'time_slot_from'                               => $request['time_slot_from'],
              'time_slot_to'                            => $request['time_slot_to'],
              'time_slot_type'                            => $request['time_slot_type'],
              'total_appointment'                            => $request['total_appointment'],
              'my_appointment'                            => '0',
              );
    	    DB::table('tms_time_slots')->insert($data);
    	    return redirect()->back();
      }
	}
	

     public function time_slot_status($id,$value) {
      Time_Slots::where('time_slot_id', $id)->update(['time_slot_status' => $value]);
      return Redirect()->back();
    }
      public function time_slot_delete($id) {
       Time_Slots::where('time_slot_id', $id)->delete();
      return Redirect()->back();
    }


    
    
    public function time_slot_information($id = false,Request $request){

     
        // return $get_record = Register::where(['admission_id' => $id])->get();
        //  return $get_record = Time_Slots_Trans::where(['time_slot_id' => $id])->get();
        
        $meeting_detail = DB::table('tms_admission')->where(['time_slot_by_user' => $id])->get();
 
      
	    $account       = Account::first();
	    $time_slot       = Time_Slots::all()->sortByDesc("time_slot_id");
	    $admin_details = Admin::where(['admin_id' => session('tms_admin_id')])->first();
	 	$title         = "Set Time Set";
	    $data          = compact('title','admin_details','account','time_slot','get_record','meeting_detail');
	    return view('admin_panel/view_time_slot',$data);

	}
}






















