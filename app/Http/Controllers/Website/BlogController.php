<?php

namespace App\Http\Controllers\Website;

use App\Model\Blog\Blog;
use App\Model\Admin\Account; 
use Illuminate\Http\Request;
use App\Http\Controllers\Controller;

class BlogController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        $account_details = Account::first();
        $title      = "Blog | The Modern School Barmer";
        $bredcum    = "Blog";
        $blogs      = Blog::paginate(12);
        $data       = compact('account_details','title','bredcum','blogs');
        return view('blog', $data);
    }

    /**
     * Display the specified resource.
     *
     * @param  \App\Model\Blog  $blog
     * @return \Illuminate\Http\Response
     */
    public function show($blog_slug)
    {
        $account_details = Account::first();
        $blog       = Blog::where('slug', $blog_slug)->firstOrFail();
        $title      = $blog->title;
        $bredcum    = $blog->title;
        $data       = compact('account_details','title','bredcum','blog');
        return view('blog-details', $data);
    }
}
