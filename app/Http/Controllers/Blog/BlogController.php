<?php
namespace App\Http\Controllers\Blog;
use Illuminate\Http\Request;
use App\Http\Requests;
use App\Http\Controllers\Controller;
use DB;
use App\Model\Blog\Blog; // model name
use App\Model\Classes\Classes; // model name
use App\Model\Admin\Admin; // model name
use App\Model\Admin\Account;
use Response;

class BlogController extends Controller 
{

    public function __construct()
    {
        $this->middleware('ValidateAdmin');
    }

    public function add_blog($id = false) {
    
      $admin_details = Admin::where(['admin_id' => session('tms_admin_id')])->first();

      if($id){
          $get_record = Blog::where(['id' => $id])->first();
      } else {
          $get_record = "";
      }

      $get_class = Classes::where(['class_status' => 1])->get();

      $title  = "Blog";
      $data   = compact('title','admin_details','get_record','get_class');
      return view('admin_panel/blog/add',$data);
    }


    public function save_blog($id = false,Request $request){

      if($id != ""){

        $values=array(
          'title'         => $request['title'],
          'slug'          => str_slug($request['title']) . '-' . $id,
          'description'         => $request['description'],
        );

        Blog::where('id', $id )->update($values);
        
        $file = $request->file('image');
        if($file != ""){
          $destinationPath = 'uploads/blog';
          $imageName = 'uploads/blog/'.time().'.'.$file->getClientOriginalExtension();
          $file->move($destinationPath,$imageName);
          $image=array(
              'image'         => $imageName
          );
          Blog::where('id', $id )->update($image);
        }
                        
        return Redirect('admin-panel/view-blog');
          
      } else {

        $Add = new Blog;
        $Add->title            =   $request['title'];
        $Add->slug             =   str_slug($request['title']);
        $Add->description      =   $request['description'];
        $Add->save();
        $id = $Add->id;

        $Add->slug = $Add->slug . '-' . $id;
        $Add->save();

        $file = $request->file('image');
        if($file != ""){
          $destinationPath = 'uploads/blog';
          $imageName = 'uploads/blog/'.time().'.'.$file->getClientOriginalExtension();
          $file->move($destinationPath,$imageName);
          $image=array(
              'image'         => $imageName
          );
          Blog::where('id', $id)->update($image);
        }

        return redirect('admin-panel/view-blog');      
      }
    }

    public function view_blog(Request $request){

      $admin_details = Admin::where(['admin_id' => session('tms_admin_id')])->first();

      if ($request['check'] != "") {
        Blog::whereIn('id', $request['check'])->delete();
        return Redirect()->back();
      }

      $query = Blog::query();

      if($request['title'] != "") {
        $query = $query->where('title', 'LIKE', '%'.$request['title'].'%');
      }

      $app_data = array('title'=>$request['title']);

      $get_record = $query->orderBy('id','DESC')->paginate(25)->appends($app_data);

      $title         = "View Blog";
      $data          = compact('title','get_record','admin_details','get_class','select_class');
      return view('admin_panel/blog/index',$data);

    }


    public function blog_status($id,$value) {
      Blog::where('id', $id)->update(['blog_status' => $value]);
      return Redirect()->back();
    }        

}
