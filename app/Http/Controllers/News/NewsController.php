<?php
namespace App\Http\Controllers\News;
use Illuminate\Http\Request;
use App\Http\Requests;
use App\Http\Controllers\Controller;
use DB;
use App\Model\News\News; // model name
use App\Model\Classes\Classes; // model name
use App\Model\Admin\Admin; // model name
use App\Model\Admin\Account;
use Response;

class NewsController extends Controller 
{

    public function __construct()
    {
        $this->middleware('ValidateAdmin');
    }

    public function add_news($id = false) {
    
      $admin_details = Admin::where(['admin_id' => session('tms_admin_id')])->first();

      if($id){
          $get_record = News::findOrFail($id);
      } else {
          $get_record = "";
      }

      $get_class = Classes::where(['class_status' => 1])->get();

      $title  = "News";
      $data   = compact('title','admin_details','get_record','get_class');
      return view('admin_panel/news/add',$data);
    }


    public function save_news($id = false,Request $request){

      // dd($request->files);

      if($id != ""){

        $values=array(
          'title'         => $request['title'],
          'slug'          => str_slug($request['title']),
          'description'   => $request['description'],
        );

        News::where('id', $id )->update($values);
        
        // $file = $request->file('news_image');
        // if($file != ""){
        //   $destinationPath = 'uploads/news_image';
        //   $imageName = 'uploads/news_image/'.time().'.'.$file->getClientOriginalExtension();
        //   $file->move($destinationPath,$imageName);
        //   $image=array(
        //       'news_image'         => $imageName,
        //   );
        //   News::where('id', $id )->update($image);
        // }
          
      } else {

        $Add = new News;
        $Add->title            =   $request['title'];
        $Add->slug            =   str_slug($request['title']);
        $Add->description      =   $request['description'];
        $Add->save();
        $id = $Add->id;

        // $file = $request->file('news_image');
        // if($file != ""){
        //   $destinationPath = 'uploads/news_image';
        //   $imageName = 'uploads/news_image/'.time().'.'.$file->getClientOriginalExtension();
        //   $file->move($destinationPath,$imageName);
        //   $image=array(
        //       'news_image'         => $imageName
        //   );
        //   News::where('id', $news_id)->update($image);
        // }
      }

      if($request->hasFile('attachment'))
      {
        $extension = $request->attachment->getClientOriginalExtension();
        $filename = $id . '.' . $extension;
        $request->attachment->storeAs('public/news', $filename);

        $news = News::find($id);
        $news->attachment = $filename . '?v=' . time();
        $news->save();
      }

      return redirect('admin-panel/view-news');      
    }

    public function view_news(Request $request){

      $admin_details = Admin::where(['admin_id' => session('tms_admin_id')])->first();

      if ($request['check'] != "") {
        News::whereIn('id', $request['check'])->delete();
        return Redirect()->back();
      }

      $query = News::query();

      if($request['title'] != "") {
        $query = $query->where('title', 'LIKE', '%'.$request['title'].'%');
      }

      $app_data = array('title'=>$request['title']);

      $get_record = $query->orderBy('id','DESC')->paginate(25)->appends($app_data);

      $title         = "View News";
      $data          = compact('title','get_record','admin_details','get_class','select_class');
      return view('admin_panel/news/index',$data);

    }


    public function news_status($id,$value) {
      News::where('id', $id)->update(['news_status' => $value]);
      return Redirect()->back();
    }        

}
