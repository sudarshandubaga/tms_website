<?php
namespace App\Http\Controllers\Downloads;
use Illuminate\Http\Request;
use App\Http\Requests;
use App\Http\Controllers\Controller;
use DB;
use App\Model\Downloads\Downloads; // model name
use App\Model\Admin\Admin; // model name
use App\Model\Admin\Account;
use Response;

class DownloadsController extends Controller 
{

    public function __construct()
    {
        $this->middleware('ValidateAdmin');
    }

    public function add_download($id = false) {
    
      $admin_details = Admin::where(['admin_id' => session('tms_admin_id')])->first();

      if($id){
          $get_record = Downloads::where(['download_id' => $id])->first();
      } else {
          $get_record = "";
      }    

      $title  = "Downloads";
      $data   = compact('title','admin_details','get_record');
      return view('admin_panel/add_download',$data);
    }


    public function save_download($id = false,Request $request){
         
      if($id != ""){
          
        $values=array(
          'download_title'         => $request['download_title'],
        );
        Downloads::where('download_id', $id )->update($values);
        
        $file = $request->file('download_file');
        if($file != ""){
          $destinationPath = 'uploads/download_file';
          $imageName = 'uploads/download_file/'.time().'.'.$file->getClientOriginalExtension();
          $file->move($destinationPath,$imageName);
          $image=array(
              'download_file'         => $imageName
          );
          Downloads::where('download_id', $id )->update($image);
        }
                        
        return Redirect('admin-panel/view-download');
          
      } else {

        $Add = new Downloads;
        $Add->download_title            =   $request['download_title'];
        $Add->save();
        $download_id = $Add->id;

        $file = $request->file('download_file');
        if($file != ""){
          $destinationPath = 'uploads/download_file';
          $imageName = 'uploads/download_file/'.time().'.'.$file->getClientOriginalExtension();
          $file->move($destinationPath,$imageName);
          $image=array(
              'download_file'         => $imageName
          );
          Downloads::where('download_id', $download_id)->update($image);
        }

        return redirect('admin-panel/view-download');      
      }
    }

    public function view_download(Request $request){

      $admin_details = Admin::where(['admin_id' => session('tms_admin_id')])->first();

      if ($request['check'] != "") {
        Downloads::whereIn('download_id', $request['check'])->delete();
        return Redirect()->back();
      }

      $query = Downloads::query();

      if($request['download_title'] != "") {
        $query = $query->where('download_title', 'LIKE', '%'.$request['download_title'].'%');
      }
      
      $app_data = array('download_title'=>$request['download_title'] );

      $get_record = $query->orderBy('download_id','DESC')->paginate(25)->appends($app_data);

      $title         = "View Downloads";
      $data          = compact('title','get_record','admin_details');
      return view('admin_panel/view_download',$data);

    }


    public function download_status($id,$value) {
      Downloads::where('download_id', $id)->update(['download_status' => $value]);
      return Redirect()->back();
    }        

}