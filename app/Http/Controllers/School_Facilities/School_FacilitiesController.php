<?php
namespace App\Http\Controllers\School_Facilities;
use Illuminate\Http\Request;
use App\Http\Requests;
use App\Http\Controllers\Controller;
use DB;
use App\Model\School_Facilities\School_Facilities; // model name
use App\Model\Admin\Admin; // model name
use App\Model\Admin\Account;
use Response;

class School_FacilitiesController extends Controller 
{

    public function __construct()
    {
        $this->middleware('ValidateAdmin');
    }

    public function add_school_facilities($id = false) {
    
      $admin_details = Admin::where(['admin_id' => session('tms_admin_id')])->first();

      if($id){
          $get_record = School_Facilities::where(['school_facilities_id' => $id])->first();
      } else {
          $get_record = "";
      }    

      $title  = "School Facilities";
      $data   = compact('title','admin_details','get_record');
      return view('admin_panel/add_school_facilities',$data);
    }


    public function save_school_facilities($id = false,Request $request){
         
      if($id != ""){
          
        $values=array(
          'school_facilities_title'         => $request['school_facilities_title'],
          'school_facilities_description'   => $request['school_facilities_description'],
        );
        School_Facilities::where('school_facilities_id', $id )->update($values);
        
        $file = $request->file('school_facilities_image');
        if($file != ""){
          $destinationPath = 'uploads/school_facilities_image';
          $imageName = 'uploads/school_facilities_image/'.time().'.'.$file->getClientOriginalExtension();
          $file->move($destinationPath,$imageName);
          $image=array(
              'school_facilities_image'         => $imageName
          );
          School_Facilities::where('school_facilities_id', $id)->update($image);
        }
                        
        return Redirect('admin-panel/view-school-facilities');
          
      } else {

        $Add = new School_Facilities;
        $Add->school_facilities_title            =   $request['school_facilities_title'];
        $Add->school_facilities_description      =   $request['school_facilities_description'];
        $Add->save();
        $school_facilities_id = $Add->id;

        $file = $request->file('school_facilities_image');
        if($file != ""){
          $destinationPath = 'uploads/school_facilities_image';
          $imageName = 'uploads/school_facilities_image/'.time().'.'.$file->getClientOriginalExtension();
          $file->move($destinationPath,$imageName);
          $image=array(
              'school_facilities_image'         => $imageName
          );
          School_Facilities::where('school_facilities_id', $school_facilities_id)->update($image);
        }

        return redirect('admin-panel/view-school-facilities');      
      }
    }

    public function view_school_facilities(Request $request){

      $admin_details = Admin::where(['admin_id' => session('tms_admin_id')])->first();

      if ($request['check'] != "") {
        School_Facilities::whereIn('school_facilities_id', $request['check'])->delete();
        return Redirect()->back();
      }

      $query = School_Facilities::query();

      if($request['school_facilities_title'] != "") {
        $query = $query->where('school_facilities_title', 'LIKE', '%'.$request['school_facilities_title'].'%');
      }

      if($request['school_facilities_description'] != "") {
        $query = $query->where('school_facilities_description', 'LIKE', '%'.$request['school_facilities_description'].'%');
      }
      
      $app_data = array('school_facilities_title'=>$request['school_facilities_title'],'school_facilities_description'=>$request['school_facilities_description']);

      $get_record = $query->orderBy('school_facilities_id','DESC')->paginate(25)->appends($app_data);

      $title         = "View School Facilities";
      $data          = compact('title','get_record','admin_details');
      return view('admin_panel/view_school_facilities',$data);

    }


    public function school_facilities_status($id,$value) {
      School_Facilities::where('school_facilities_id', $id)->update(['school_facilities_status' => $value]);
      return Redirect()->back();
    }        

}