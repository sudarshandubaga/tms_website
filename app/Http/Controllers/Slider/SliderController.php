<?php
namespace App\Http\Controllers\Slider;
use Illuminate\Http\Request;
use App\Http\Requests;
use App\Http\Controllers\Controller;
use DB;
use App\Model\Slider\Slider; // model name
use App\Model\Classes\Classes; // model name
use App\Model\Admin\Admin; // model name
use App\Model\Admin\Account;
use Response;

class SliderController extends Controller 
{

    public function __construct()
    {
        $this->middleware('ValidateAdmin');
    }

    public function add_slider($id = false) {
    
      $admin_details = Admin::where(['admin_id' => session('tms_admin_id')])->first();

      if($id){
          $get_record = Slider::where(['slider_id' => $id])->first();
      } else {
          $get_record = "";
      }    

      $get_class = Classes::where(['class_status' => 1])->get();

      $title  = "Slider";
      $data   = compact('title','admin_details','get_record','get_class');
      return view('admin_panel/add_slider',$data);
    }


    public function save_slider($id = false,Request $request){

      if($id != ""){

        $values=array(
          'slider_name'         => $request['slider_name'],
        );

        Slider::where('slider_id', $id )->update($values);
        
        $file = $request->file('slider_image');
        if($file != ""){
          $destinationPath = 'uploads/slider_image';
          $imageName = 'uploads/slider_image/'.time().'.'.$file->getClientOriginalExtension();
          $file->move($destinationPath,$imageName);
          $image=array(
              'slider_image'         => $imageName
          );
          Slider::where('slider_id', $id )->update($image);
        }
                        
        return Redirect('admin-panel/view-slider');
          
      } else {

        $Add = new Slider;
        $Add->slider_name            =   $request['slider_name'];
        $Add->save();
        $slider_id = $Add->id;

        $file = $request->file('slider_image');
        if($file != ""){
          $destinationPath = 'uploads/slider_image';
          $imageName = 'uploads/slider_image/'.time().'.'.$file->getClientOriginalExtension();
          $file->move($destinationPath,$imageName);
          $image=array(
              'slider_image'         => $imageName
          );
          Slider::where('slider_id', $slider_id)->update($image);
        }

        return redirect('admin-panel/view-slider');      
      }
    }

    public function view_slider(Request $request){

      $admin_details = Admin::where(['admin_id' => session('tms_admin_id')])->first();

      if ($request['check'] != "") {
        Slider::whereIn('slider_id', $request['check'])->delete();
        return Redirect()->back();
      }

      $query = Slider::query();

      if($request['slider_name'] != "") {
        $query = $query->where('slider_name', 'LIKE', '%'.$request['slider_name'].'%');
      }

      $app_data = array('slider_name'=>$request['slider_name']);

      $get_record = $query->orderBy('slider_id','DESC')->paginate(25)->appends($app_data);

      $title         = "View Slider";
      $data          = compact('title','get_record','admin_details','get_class','select_class');
      return view('admin_panel/view_slider',$data);

    }


    public function slider_status($id,$value) {
      Slider::where('slider_id', $id)->update(['slider_status' => $value]);
      return Redirect()->back();
    }        

}