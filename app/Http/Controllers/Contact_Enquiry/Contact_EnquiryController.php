<?php
  namespace App\Http\Controllers\Contact_Enquiry;
  use Illuminate\Http\Request;
  use App\Http\Requests;  
  use App\Http\Controllers\Controller;
  use DB;
  use App\Model\Contact_Enquiry\Contact_Enquiry; // model name
  use App\Model\Contact_Enquiry\Student_Fees; // model name
  use App\Model\Contact_Enquiry\Register_Enquiry; // model name
  use App\Model\Admin\Admin; // model name
  use App\Model\Admin\Account;
  use Response;

  class Contact_EnquiryController extends Controller {

    public function __construct()
    {
        $this->middleware('ValidateAdmin');
    }

    protected function array2csv(array &$array)
    {
       if (count($array) == 0) {
         return null;
       }
       ob_start();
       $df = fopen("php://output", 'w');
       fputcsv($df, array_keys(reset($array)));
       foreach ($array as $row) {
          fputcsv($df, $row);
       }
       fclose($df);
       return ob_get_clean();
    }

    protected function download_send_headers($filename) {
      // disable caching
      $now = gmdate("D, d M Y H:i:s");
      header("Expires: Tue, 03 Jul 2001 06:00:00 GMT");
      header("Cache-Control: max-age=0, no-cache, must-revalidate, proxy-revalidate");
      header("Last-Modified: {$now} GMT");
  
      // force download  
      header("Content-Type: application/force-download");
      header("Content-Type: application/octet-stream");
      header("Content-Type: application/download");
  
      // disposition / encoding on response body
      header("Content-Disposition: attachment;filename={$filename}");
      header("Content-Transfer-Encoding: binary");
  }

    public function export()
    {
      $lists = contact_enquiry::orderBy('contact_enquiry_id', 'DESC')->get()->toArray();

      $array = [];

      foreach ($lists as $key => $list) {
        # code...
        if($key === 0) {
          $array[] = array_keys($list);
        }

        $array[] = array_values($list);
      }

      // dd($array);

      $this->download_send_headers("contact_enquiry_" . date("Y-m-d") . ".csv");
      echo $this->array2csv($array);
      die();
    }

    public function export_register()
    {
      $lists = Register_Enquiry::orderBy('admission_id', 'DESC')->get()->toArray();

      $array = [];

      foreach ($lists as $key => $list) {
        # code...
        if($key === 0) {
          $array[] = array_keys($list);
        }

        $array[] = array_values($list);
      }

      // dd($array);

      $this->download_send_headers("register_enquiry_" . date("Y-m-d") . ".csv");
      echo $this->array2csv($array);
      die();
    }

    public function contact_enquiry($id = false,Request $request) {
    
      $admin_details = Admin::where(['admin_id' => session('admin_panel_id')])->first();


      // if($id){
      //   $get_record = Admission_Points::where(['admission_point_id' => $id])->first();
      // } else {
      //   $get_record = "";
      // }

      if ($request['check'] != "") {
        contact_enquiry::whereIn('contact_enquiry_id', $request['check'])->delete();
        return Redirect()->back();
      }

       $query =contact_enquiry::query();

      if ($request['contact_enquiry_name'] != "") {
        $query = $query->where('contact_enquiry_name', 'LIKE', '%'.$request['contact_enquiry_name'].'%');
      }
      if ($request['contact_enquiry_email'] != "") {
        $query = $query->where('contact_enquiry_email', 'LIKE', '%'.$request['contact_enquiry_email'].'%');
      }
      if ($request['contact_enquiry_number'] != "") {
        $query = $query->where('contact_enquiry_number', 'LIKE', '%'.$request['contact_enquiry_number'].'%');
      }

      $appends = array('contact_enquiry_name'=>$request['contact_enquiry_name'] , 'contact_enquiry_email'=>$request['contact_enquiry_email'] , 'contact_enquiry_number'=>$request['contact_enquiry_number']);

      $get_record = $query->orderBy('contact_enquiry_id','ASC')->paginate(25)->appends($appends); 

      $title  = "Contact Enquiry";
      $data   = compact('title','admin_details','get_record');
      return view('admin_panel/contact_enquiry',$data);
    }

    public function register_enquiry($id = false,Request $request) {
      // if($id){
      //   $get_record = Admission_Points::where(['admission_point_id' => $id])->first();
      // } else {
      //   $get_record = "";
      // }

      if ($request['check'] != "") {
        Register_Enquiry::whereIn('admission_id', $request['check'])->delete();
        return Redirect()->back();
      }

      $query = Register_Enquiry::query();

      if ($request['admission_name'] != "") {
        $get_record = DB::table('tms_admission')
                                ->select('tms_admission.*')
                                ->where('admission_name', 'LIKE', '%'.$request['admission_name'].'%')
                                ->orderBy('tms_admission.admission_id','DESC')
                                ->paginate(25);
      }
     
      if ($request['admission_id'] != "") {
        $get_record = DB::table('tms_admission')
                                ->select('tms_admission.*')
                               ->where('admission_id', 'LIKE', '%'.$request['admission_id'].'%')
                                ->orderBy('tms_admission.admission_id','DESC')
                                ->paginate(25);
      }
      

        if (($request['admission_id'] == "") && ($request['admission_name'] == "")) {
         $get_record = DB::table('tms_admission')
                                ->select('tms_admission.*')
                                ->orderBy('tms_admission.admission_id','DESC')
                                ->paginate(25);
        }
        
     
    //   return $appends = array('admission_name'=>$request['admission_name'] , 'admission_id'=>$request['admission_id']);

    //   $get_record = $query->orderBy('admission_id','DESC')->paginate(25)->appends($tms_admission); 

      $title  = "Registration Enquiry";
      $data   = compact('title','admin_details','get_record');
      return view('admin_panel/register_enquiry',$data);
    }






    public function contact_enquiry_status($id,$value) {
      contact_enquiry::where('contact_enquiry_id', $id)->update(['contact_enquiry_status' => $value]);
      return Redirect()->back();
    }



    public function student_fees($id = false,Request $request) {
    
      $admin_details = Admin::where(['admin_id' => session('admin_panel_id')])->first();


      $query =Student_Fees::query();

      if ($request['contact_enquiry_name'] != "") {
        $query = $query->where('contact_enquiry_name', 'LIKE', '%'.$request['contact_enquiry_name'].'%');
      }
      if ($request['contact_enquiry_email'] != "") {
        $query = $query->where('contact_enquiry_email', 'LIKE', '%'.$request['contact_enquiry_email'].'%');
      }
      if ($request['contact_enquiry_number'] != "") {
        $query = $query->where('contact_enquiry_number', 'LIKE', '%'.$request['contact_enquiry_number'].'%');
      }

      $appends = array('contact_enquiry_name'=>$request['contact_enquiry_name'] , 'contact_enquiry_email'=>$request['contact_enquiry_email'] , 'contact_enquiry_number'=>$request['contact_enquiry_number']);

      $get_record = $query->orderBy('student_fees_id','ASC')->paginate(25)->appends($appends); 

      $title  = "Student Fees";
      $data   = compact('title','admin_details','get_record');
      return view('admin_panel/student_fees',$data);
    }


       
  }