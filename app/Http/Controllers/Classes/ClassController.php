<?php
  namespace App\Http\Controllers\Classes;
  use Illuminate\Http\Request;
  use App\Http\Requests;  
  use App\Http\Controllers\Controller;
  use DB;
  use App\Model\Classes\Classes; // model name
  use App\Model\Admin\Admin; // model name
  use App\Model\Admin\Account;
  use Response;

  class ClassController extends Controller {

    public function __construct()
    {
        $this->middleware('ValidateAdmin');
    }

    public function add_class($id = false){
          
      $admin_details = Admin::where(['admin_id' => session('tms_admin_id')])->first();

      if($id){
          $get_record = Classes::where(['class_id' => $id])->first();
      } else {
          $get_record = "";
      }

      $title  = "Add Class";
      $data   = compact('title','admin_details','get_record');
      return view('admin_panel/add_class',$data);
    }


    public function view_class(Request $request){

      $admin_details = Admin::where(['admin_id' => session('tms_admin_id')])->first();

      if ($request['check'] != "") {
        Classes::whereIn('class_id', $request['check'])->delete();
        return Redirect()->back();
      }

      $query =  Classes::query();

      if ($request['class_name'] != "") {
        $query = $query->where('class_name', 'LIKE', '%'.$request['class_name'].'%');
      }

      $app_data = array('class_name'=>$request['class_name']);

      $get_record = $query->orderBy('class_id','ASC')->paginate(25)->appends($app_data); 

      $title         = "View Class";
      $data          = compact('title','get_record','admin_details');
      return view('admin_panel/view_class',$data);
    }


    public function save_class($id =false ,Request $request) {

      $class_slug = trim($request['class_name']);
      $slug = preg_replace("/-$/","",preg_replace('/[^a-z0-9]+/i', "-", strtolower($class_slug)));
      $numHits = Classes::where('class_slug',$slug)->count();  
      if ($numHits > 0) { $new_slug = $slug . '-' . $numHits; } else { $new_slug = $slug; }
      
      if($id != "") {
        $values=array(
          'class_name'             =>  $request['class_name'],
          'class_slug'             => $new_slug,
          'class_age_requirments'  =>  $request['class_age_requirments'],
        );
        
        Classes::where('class_id', $id )->update($values);
        return Redirect('admin-panel/view-class');
      } else {
             
        $Add = new Classes ;
        $Add->class_name              =   $request['class_name'];
        $Add->class_slug              =   $new_slug;
        $Add->class_age_requirments   =   $request['class_age_requirments'];
        $Add->save();
        return Redirect('admin-panel/view-class');
      }
    }


    public function class_status($id,$value) {
      Classes::where('class_id', $id)->update(['class_status' => $value]);
      return Redirect()->back();
    }

       
  }