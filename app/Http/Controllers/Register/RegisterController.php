<?php
  namespace App\Http\Controllers\Register;
  use Illuminate\Http\Request;
  use App\Http\Requests;  
  use App\Http\Controllers\Controller;
  use DB;
  use App\Model\Register\Register; // model name
  use App\Model\Admin\Admin; // model name
  use App\Model\Admin\Account;
  use Response;

  class RegisterController extends Controller {

    public function __construct()
    {
        $this->middleware('ValidateAdmin');
    }

    public function register($id = false){
          
      $admin_details = Admin::where(['admin_id' => session('tms_admin_id')])->first();

      if($id){
          $get_record = Faqs::where(['admission_id' => $id])->first();
      } else {
          $get_record = "";
      }

      $title  = "Add Register";
      $data   = compact('title','admin_details','get_record');
      return view('admin_panel/add_faq',$data);
    }       
  }