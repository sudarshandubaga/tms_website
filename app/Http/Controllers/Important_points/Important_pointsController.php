<?php
  namespace App\Http\Controllers\Important_points;
  use Illuminate\Http\Request;
  use App\Http\Requests;  
  use App\Http\Controllers\Controller;
  use DB;
  use App\Model\Important_points\Important_points; // model name
  use App\Model\Admin\Admin; // model name
  use App\Model\Admin\Account;
  use Response;

  class Important_pointsController extends Controller {

    public function __construct()
    {
        $this->middleware('ValidateAdmin');
    }

    public function important_points_view($id = false,Request $request) {
    
      $admin_details = Admin::where(['admin_id' => session('tms_admin_id')])->first();

      if($id){
        $get_record = Important_Points::where(['important_point_id' => $id])->first();
      } else {
        $get_record = "";
      }

      if ($request['check'] != "") {
        Important_Points::whereIn('important_point_id', $request['check'])->delete();
        return Redirect()->back();
      }

       $query =Important_Points::query();

      if ($request['search_important_point'] != "") {
        $query = $query->where('important_point', 'LIKE', '%'.$request['search_important_point'].'%');
      }

      $appends = array('search_important_point'=>$request['search_important_point']);

      $important_point = $query->orderBy('important_point_id','ASC')->paginate(25)->appends($appends); 

      $title  = "Important Points";
      $data   = compact('title','admin_details','get_record','important_point');
      return view('admin_panel/important_point',$data);
    }

    public function save_important_points($id = false,Request $request){

      if($id != ""){
          $values=array(
              'important_point'         => $request['important_point']
          );
          Important_Points::where('important_point_id', $id)->update($values);
          return Redirect('admin-panel/important-points');

      } else {
            $Add = new Important_Points;
            $Add->important_point         =  $request['important_point'];
            $Add->save();
            return redirect()->back();
      }
    }


    public function important_points_status($id,$value) {
      Important_Points::where('important_point_id', $id)->update(['important_point_status' => $value]);
      return Redirect()->back();
    }

       
  }