<?php
namespace App\Http\Controllers\Our_School;
use Illuminate\Http\Request;
use App\Http\Requests;
use App\Http\Controllers\Controller;
use DB;
use App\Model\Our_School\Our_School; // model name
use App\Model\Our_School\Testimonials; // model name
use App\Model\Admin\Admin; // model name
use App\Model\Admin\Account;
use Response;

class Our_SchoolController extends Controller 
{

    public function __construct()
    {
        $this->middleware('ValidateAdmin');
    }

    public function add_our_school($id = false) {
    
      $admin_details = Admin::where(['admin_id' => session('tms_admin_id')])->first();

      if($id){
          $get_record = Our_School::where(['our_school_id' => $id])->first();
      } else {
          $get_record = "";
      }    

      $title  = "Our School";
      $data   = compact('title','admin_details','get_record');
      return view('admin_panel/add_our_school',$data);
    
    }


    public function save_our_school($id = false,Request $request){
         
      if($id != ""){
          
        $values=array(
          'our_school_title'         => $request['our_school_title'],
          'our_school_description'   => $request['our_school_description'],
        );
        Our_School::where('our_school_id', $id )->update($values);
        
        return Redirect('admin-panel/view-our-school');
          
      } else {

        $Add = new Our_School;
        $Add->our_school_title            =   $request['our_school_title'];
        $Add->our_school_description      =   $request['our_school_description'];
        $Add->save();
        $our_school_id = $Add->id;

        return redirect('admin-panel/view-our-school');      
      }
    }

    public function view_our_school(Request $request){

      $admin_details = Admin::where(['admin_id' => session('tms_admin_id')])->first();

      if ($request['check'] != "") {
        Our_School::whereIn('our_school_id', $request['check'])->delete();
        return Redirect()->back();
      }

      $query = Our_School::query();

      if($request['our_school_title'] != "") {
        $query = $query->where('our_school_title', 'LIKE', '%'.$request['our_school_title'].'%');
      }

      if($request['our_school_description'] != "") {
        $query = $query->where('our_school_description', 'LIKE', '%'.$request['our_school_description'].'%');
      }
      
      $app_data = array('our_school_title'=>$request['our_school_title'],'our_school_description'=>$request['our_school_description']);

      $get_record = $query->orderBy('our_school_id','DESC')->paginate(25)->appends($app_data);

      $title         = "View Our School";
      $data          = compact('title','get_record','admin_details');
      return view('admin_panel/view_our_school',$data);

    }


    public function our_school_status($id,$value) {
      Our_School::where('our_school_id', $id)->update(['our_school_status' => $value]);
      return Redirect()->back();
    }       
    
    
    
    /*=============================================================================================================================================================================*/
    /*=============================================================================================================================================================================*/
    /*=============================================================================================================================================================================*/
    /*=============================================================================================================================================================================*/
    
    public function add_testimonials($id = false) {
    
      $admin_details = Admin::where(['admin_id' => session('tms_admin_id')])->first();

      if($id){
          $get_record = Testimonials::where(['testimonials_id' => $id])->first();
      } else {
          $get_record = "";
      }    

      $title  = "Testimonials";
      $data   = compact('title','admin_details','get_record');
      return view('admin_panel/add_testimonials',$data);
    
    }


    public function save_testimonials($id = false,Request $request){
      
         
      if($id != ""){
          
        $values=array(
          'testimonials_title'         => $request['testimonials_title'],
          'testimonials_description'   => $request['testimonials_description'],
          'testimonials_user_name'   => $request['testimonials_user_name'],
        );
        $file = $request->file('testimonials_image');
        if($file != ""){
          $destinationPath = 'uploads/testimonials';
          $imageName = 'uploads/testimonials/'.time().'.'.$file->getClientOriginalExtension();
          $file->move($destinationPath,$imageName);
          $values['testimonials_image']=$imageName;
        }
       
        Testimonials::where('testimonials_id', $id )->update($values);
        
        return Redirect('admin-panel/view-testimonials');
          
      } else {

        $Add = new Testimonials;
        $Add->testimonials_title            =   $request['testimonials_title'];
        $Add->testimonials_description      =   $request['testimonials_description'];
        $Add->testimonials_user_name      =   $request['testimonials_user_name'];

        $file = $request->file('testimonials_image');
        if($file != ""){
          $destinationPath = 'uploads/testimonials';
          $imageName = 'uploads/testimonials/'.time().'.'.$file->getClientOriginalExtension();
          $file->move($destinationPath,$imageName);
          $Add->testimonials_image=$imageName;
        }
        // dd($Add);

        $Add->save();
        $testimonials_id = $Add->id;

        return redirect('admin-panel/view-testimonials');      
      }
    }

    public function view_testimonials(Request $request){

      $admin_details = Admin::where(['admin_id' => session('tms_admin_id')])->first();

      if ($request['check'] != "") {
        Testimonials::whereIn('testimonials_id', $request['check'])->delete();
        return Redirect()->back();
      }

      $get_record = Testimonials::all();

      $title         = "View Testimonials";
      $data          = compact('title','get_record','admin_details');
      return view('admin_panel/view_testimonials',$data);

    }


    public function testimonials_status($id,$value) {
      Testimonials::where('testimonials_id', $id)->update(['testimonials_status' => $value]);
      return Redirect()->back();
    } 

}