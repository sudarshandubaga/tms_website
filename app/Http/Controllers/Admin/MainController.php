<?php
namespace App\Http\Controllers\Admin;

use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use App\Model\Admin\Admin; // model name
use App\Model\Admin\Account; // model name
use Mail;
use DB;

class MainController extends Controller
{
    public function login(){
		// return $account     = Account::first();
		if(session('tms_admin_id') != ""){
    		return redirect('admin-panel/dashboard');
    	}

    	$account     = Account::first();
  		$data        = compact('account');
  		return view('admin_panel/login',$data);
    }


    public function admin_login(Request $request){
	
    	if(session('tms_admin_id') != ""){
    		return redirect('admin-panel/dashboard');
    	}

	    $admin_login = Admin::where(['admin_email' => $request['admin_email'], 'admin_password' => md5($request['admin_password']) ])->first();
	    if (count($admin_login) == 0) {
	    	return redirect()->back()->withSuccess('Useremail or password is/are incorrect!!');
		} else {
			session(['tms_admin_id' => $admin_login->admin_id]);
			$admin_id = session('admin_id');
			return redirect('admin-panel/dashboard');
		}
	}


	public function forgot_password(Request $request) {
	    $account     = Account::first();
	    if($request['admin_email'] == ""){
	      $error_msg  = "";
	      $data       = compact('error_msg','account');
	      return view('mygo_admin/forgot_password',$data);   
	    
	    } else {

	      $admin_login = Admin::where(['admin_email' => $request['admin_email'] ])->count();
	      if ($admin_login == 0) {
	          $error_msg  = "1";
	          $data       = compact('error_msg','account');
	          return view('mygo_admin/forgot_password',$data);
	      
	      } else {

	      $admin_login = Admin::where(['admin_email' => $request['admin_email'] ])->first();
	      $_REQUEST['admin_email'] = $admin_login['admin_email'];
	      $_REQUEST['admin_name']  = $admin_login['admin_name'];

	      $data=['name'=>$admin_login['admin_name'],'email'=>$admin_login['admin_email'],'id'=>md5($admin_login['admin_id']),'logo'=>$account[0]['account_logo'] ];
	        Mail::send(['html' => 'mygo_admin/forgot_mail'], $data , function ($message) {
	        $message->to($_REQUEST['admin_email'], $_REQUEST['admin_name'])->subject('Forgot Password');
	      });  

	      $values=array( 'reset_password_status' => 0 );
    	  Admin::where('admin_email', $request['admin_email'])->update($values);

	      $admin_name = $admin_login['admin_name'];
	      $data1 = compact('admin_name','account');
	      return view('mygo_admin/confirmation',$data1);
	      }
	        $title      = "Forgot Password";
	        $msg        = "";
	        $data       = compact('title','msg','account'); 
	        return view('mygo_admin/forgot_password',$data);
	      }
	}


	public function reset_password($id){

		$get_record = DB::select('select * from mygo_admin where sha1(admin_id) = "'.$id.'" ');
		
		$account    = Account::first();
		$title      = "Reset Password";
        $msg        = "";
        $data       = compact('title','msg','account','get_record'); 
        return view('mygo_admin/reset_password',$data);
	}

	public function password_reset(Request $request){

		$values=array( 
			'admin_password' 			=> md5($request['confirm_password']),
			'reset_password_status' 	=> 1, 
		);

    	Admin::where('admin_id', $request['admin_id'])->update($values);

    	$admin_login = Admin::where(['admin_id' => $request['admin_id'] ])->first();
	    session(['mygo_admin_id' => $admin_login->admin_id]);
        return redirect('mygo-panel/dashboard');
	}



}
