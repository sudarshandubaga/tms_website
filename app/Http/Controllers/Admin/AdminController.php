<?php
namespace App\Http\Controllers\Admin;

use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use App\Model\Admin\Admin; // model name
use App\Model\Admin\Account; // model name

class AdminController extends Controller
{
	public function __construct()
  	{
      	$this->middleware('ValidateAdmin');
  	}

    public function dashboard(){

	    $account       = Account::first();
	    $admin_details = Admin::where(['admin_id' => session('tms_admin_id')])->first();
	 	$title         = "Dashboard";
	    $data          = compact('title','admin_details','account');
	    return view('admin_panel/dashboard',$data);

	}

	public function profile(Request $request){

	    if($request['admin_name'] != ""){

	    	$values=array(
	      		'admin_name'          => $request['admin_name'],
	      		'admin_number'        => $request['admin_number'],
	      		'admin_email'         => $request['admin_email'],
	    	);
	    	Admin::where('admin_id', session('tms_admin_id'))->update($values);

	    	$file = $request->file('admin_image');
	      	if($file != ""){
	        	$destinationPath = 'uploads/admin';
		        $imageName = 'uploads/admin/'.time().'.'.$file->getClientOriginalExtension();
		        $file->move($destinationPath,$imageName);
		        $image=array(
		            'admin_image'         => $imageName
		        );
		        Admin::where('admin_id', session('tms_admin_id'))->update($image);
	      	}
	      	
	      	return redirect()->back()->withSuccess('Profile update successfully');      
	    }

	    $account       = Account::first();
	    $id            = session('tms_admin_id');
	    $admin_details = Admin::where('admin_id',$id)->first();
	    $title         = "Profile";
	    $data          = compact('title','get_record','account','admin_details');
	    return view('admin_panel/profile',$data);
    }

  	public function account_settings(Request $request){

    	if($request['account_name'] != ""){

	        $values=array(
	          'account_name'        	=> $request['account_name'],
	          'account_email'       	=> $request['account_email'],
	          'account_number_1'      	=> $request['account_number_1'],
	          'account_number_2'      	=> $request['account_number_2'],
	          'account_number_3'      	=> $request['account_number_3'],
	          'account_address'     	=> $request['account_address'],
	          'account_facebook'    	=> $request['account_facebook'],
	          'account_twitter'     	=> $request['account_twitter'],
	          'account_youtube'      	=> $request['account_youtube'],
	          'account_message'      	=> $request['account_message'],
	          'office_timing_day'     	=> $request['office_timing_day'],
	          'office_timing_time'    	=> $request['office_timing_time'],
	        );
	        Account::where('account_id', 1)->update($values);

	        $file = $request->file('account_logo');
      		if($file != ""){
		        $destinationPath = 'uploads/admin';
		        $imageName = 'uploads/admin/'.time().'.'.$file->getClientOriginalExtension();
		        $file->move($destinationPath,$imageName);
		        $images=array(
		            'account_logo'       => $imageName
		        );
		        Account::where('account_id', 1)->update($images);
	      	}
	    
	    	return redirect()->back()->withSuccess('Account settings update successfully');       
    	}

	    $account     = Account::first();
	    $admin_details = Admin::where(['admin_id' => session('tms_admin_id')])->first();
    
    	$title      = "Account Settings";
    	$data       = compact('title','account','admin_details');
    	return view('admin_panel/account_settings',$data);
  	}

  	public function change_password(Request $request) {

    	$account     = Account::first();
    	$admin_details = Admin::where(['admin_id' => session('tms_admin_id')])->first();

   		if($request['new_password'] != ""){
   
		    $admin_id = session('tms_admin_id');
		    $admin_login = Admin::where(['admin_password' => md5($request['current_password']), 'admin_id' => $admin_id ])->count();

		    if($admin_login == ""){
		      $message = "Current password doesnot match";
		      $confirm_message = "";
		    } else {
		      $values=array(
		          'admin_password'          => md5($request['new_password']),
		      );
			    Admin::where('admin_id', $admin_id)->update($values);
			    $message = "";
			    $confirm_message = "Password Changed Successfully";
		    }

		    $title      = "Change Password";
		    $data       = compact('title','message','confirm_message','account','admin_details');
		    return view('admin_panel/change_password',$data);
      
    	} else {
	      	$message = "";
	      	$confirm_message = "";
	      	$title      = "Change Password";
	      	$data       = compact('title','message','confirm_message','account','admin_details');
	      	return view('admin_panel/change_password',$data);
    	}
  	}

  	public function logout(){
    	session()->flush();
  		return redirect('admin-panel');
  	}

}
