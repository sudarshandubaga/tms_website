<?php
namespace App\Http\Controllers\School_Gallery;
use Illuminate\Http\Request;
use App\Http\Requests;
use App\Http\Controllers\Controller;
use DB;
use App\Model\School_Gallery\School_Gallery; // model name
use App\Model\School_Gallery\School_Video_Gallery; // model name
use App\Model\Classes\Classes; // model name
use App\Model\Admin\Admin; // model name
use App\Model\TCModel AS TCmodel;
use App\Model\Admin\Account;
use Response;

class School_GalleryController extends Controller 
{

    public function __construct()
    {
        $this->middleware('ValidateAdmin');
    }

    public function add_school_gallery($id = false) {
    
      $admin_details = Admin::where(['admin_id' => session('tms_admin_id')])->first();

      if($id){
          $get_record = School_Gallery::where(['school_gallery_id' => $id])->first();
      } else {
          $get_record = "";
      }    


      $title  = "School Gallery";
      $data   = compact('title','admin_details','get_record','get_class');
      return view('admin_panel/add_school_gallery',$data);
    }


    public function save_school_gallery($id = false,Request $request){

      if($id != ""){

        $values=array(
          'school_gallery_title'         => $request['school_gallery_title'],
        );

        School_Gallery::where('school_gallery_id', $id )->update($values);
        
        $file = $request->file('school_gallery_image');
        if($file != ""){
          $destinationPath = 'uploads/school_gallery_image';
          $imageName = 'uploads/school_gallery_image/'.time().'.'.$file->getClientOriginalExtension();
          $file->move($destinationPath,$imageName);
          $image=array(
              'school_gallery_image'         => $imageName
          );
          School_Gallery::where('school_gallery_id', $id )->update($image);
        }
                        
        return Redirect('admin-panel/view-school-gallery');
          
      } else {

        $Add = new School_Gallery;
        $Add->school_gallery_title            =   $request['school_gallery_title'];
        $Add->save();
        $school_gallery_id = $Add->id;

        $file = $request->file('school_gallery_image');
        if($file != ""){
          $destinationPath = 'uploads/school_gallery_image';
          $imageName = 'uploads/school_gallery_image/'.time().'.'.$file->getClientOriginalExtension();
          $file->move($destinationPath,$imageName);
          $image=array(
              'school_gallery_image'         => $imageName
          );
          School_Gallery::where('school_gallery_id', $school_gallery_id)->update($image);
        }

        return redirect('admin-panel/view-school-gallery');      
      }
    }

    public function view_school_gallery(Request $request){

      $admin_details = Admin::where(['admin_id' => session('tms_admin_id')])->first();

      if ($request['check'] != "") {
        School_Gallery::whereIn('school_gallery_id', $request['check'])->delete();
        return Redirect()->back();
      }

      $query = School_Gallery::query();

      $get_record = $query->orderBy('school_gallery_id','DESC')->paginate(25);

      $get_class = Classes::where(['class_status' => 1])->get();

      $title         = "View School Gallery";
      $data          = compact('title','get_record','admin_details','get_class','select_class');
      return view('admin_panel/view_school_gallery',$data);

    }


    public function school_gallery_status($id,$value) {
      School_Gallery::where('school_gallery_id', $id)->update(['school_gallery_status' => $value]);
      return Redirect()->back();
    }        
    
    
    
    
    /*============================================================================*/
      public function view_school_video_gallery(Request $request){
      $admin_details = Admin::where(['admin_id' => session('tms_admin_id')])->first();

      if ($request['check'] != "") {
        School_Video_Gallery::whereIn('school_gallery_id', $request['check'])->delete();
        return Redirect()->back();
      }

      $query = School_Video_Gallery::query();

      $get_record = $query->orderBy('school_gallery_id','DESC')->paginate(25);

      $get_class = Classes::where(['class_status' => 1])->get();

      $title         = "View School Video Gallery";
      $data          = compact('title','get_record','admin_details','get_class','select_class');
      return view('admin_panel/view_school_video_gallery',$data);
    }
    
    
     public function add_school_video_gallery($id = false) {
      $admin_details = Admin::where(['admin_id' => session('tms_admin_id')])->first();
      if($id){
          $get_record = School_Video_Gallery::where(['school_gallery_id' => $id])->first();
      } else {
          $get_record = "";
      }    
      $title  = "School Video Gallery";
      $data   = compact('title','admin_details','get_record','get_class');
      return view('admin_panel/add_school_video_gallery',$data);
    }
    
    public function save_school_video_gallery($id = false,Request $request){
      if($id != ""){
        $values=array(
          'school_gallery_title'         => $request['school_gallery_title'],
          'school_gallery_link'         => $request['school_gallery_link'],
          'type'                        => $request['type'],
        );
        School_Video_Gallery::where('school_gallery_id', $id )->update($values);
                  
        return Redirect('admin-panel/view-school-video-gallery');
          
      } else {

        $Add = new School_Video_Gallery;
        $Add->school_gallery_title            =   $request['school_gallery_title'];
        $Add->school_gallery_link            =   $request['school_gallery_link'];
        $Add->type                          =   $request['type'];
        $Add->save();
        

        return redirect('admin-panel/view-school-video-gallery');      
      }
    }
    
     public function school_video_gallery_status($id,$value) {
      School_Video_Gallery::where('school_gallery_id', $id)->update(['school_gallery_status' => $value]);
      return Redirect()->back();
    }  
    
    
    /*======================================================================================*/
    
    
    
       public function view_tc(Request $request) {
           if ($request['check'] != "") {
        TCmodel::whereIn('tc_id', $request['check'])->delete();
        return Redirect()->back();
      }
        $title 	= "View Tc";
        $records = TCmodel::where('tc_is_deleted', 'N')->orderBy('tc_id','DESC')->paginate(10);
        if($request->isMethod('post')) {
            $check = $request->input('check');
            if(!empty($check)) {
                  TCmodel::whereIn('tc_id', $check)->update(['tc_is_deleted' => 'Y']);
            }
            return redirect('sch-panel/tc');
        }

        $data 	= compact('page', 'title', 'records');
        return view('admin_panel/view_tc', $data);
    }
    
    public function add_tc(Request $request, $id = null) {
        $title 	     = "Add Tc";
        $data 	= compact('page', 'title', 'edit');
        return view('admin_panel/add_tc', $data);
    }
    
    
    
    public function save_tc(Request $request, $id = null) {
        if($request->isMethod('post')) {
           $record   = $request->input('record');


           if ($request->hasFile('tc_file')) {
    
                $id = TCmodel::insertGetId($record);
                $image           = $request->file('tc_file');
                $name            = 'FILE'.$id.'.'.$image->getClientOriginalExtension();
                $destinationPath = 'uploads/tcs/';
                $image->move($destinationPath, $name);

                TCmodel::where('tc_id', $id)->update(['tc_file' => $name]);
           }


          
        }

        $data 	= compact('page', 'title', 'edit');
            return redirect('/admin-panel/tc_view')->with('success', 'Records Saved successfully.');
    }
    
    
    
    
    
    
    
    

}
