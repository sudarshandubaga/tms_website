<?php
namespace App\Http\Controllers\Events;
use Illuminate\Http\Request;
use App\Http\Requests;
use App\Http\Controllers\Controller;
use DB;
use App\Model\Events\Events; // model name
use App\Model\Classes\Classes; // model name
use App\Model\Admin\Admin; // model name
use App\Model\Admin\Account;
use Response;

class EventsController extends Controller 
{

    public function __construct()
    {
        $this->middleware('ValidateAdmin');
    }

    public function add_event($id = false) {
    
      $admin_details = Admin::where(['admin_id' => session('tms_admin_id')])->first();

      if($id){
          $get_record = Events::where(['event_id' => $id])->first();
      } else {
          $get_record = "";
      }    

      $get_class = Classes::where(['class_status' => 1])->get();

      $title  = "Events";
      $data   = compact('title','admin_details','get_record','get_class');
      return view('admin_panel/add_event',$data);
    }


    public function save_event($id = false,Request $request){

      $role_slug = trim($request['event_name']);
      $slug = preg_replace("/-$/","",preg_replace('/[^a-z0-9]+/i', "-", strtolower($role_slug)));
      $numHits = Events::where('event_slug',$slug)->count();  
      if ($numHits > 0) { $new_slug = $slug . '-' . $numHits; } else { $new_slug = $slug; }
      
      // if($request['event_date'] != ""){
      //   $request['event_date'] = date('Y-m-d',strtotime($request['event_date']));
      // } else {
      //   $request['event_date'] = $request['event_date'];
      // }

      if($id != ""){

        $values=array(
          'event_name'         => $request['event_name'],
          'event_class'        => implode(',',$request['event_class']),
          'event_date'         => $request['event_date'],
          'event_description'  => $request['event_description'],
          'event_slug'         => $new_slug,
        );

        Events::where('event_id', $id )->update($values);
        
        $file = $request->file('event_image');
        if($file != ""){
          $destinationPath = 'uploads/event_image';
          $imageName = 'uploads/event_image/'.time().'.'.$file->getClientOriginalExtension();
          $file->move($destinationPath,$imageName);
          $image=array(
              'event_image'         => $imageName
          );
          Events::where('event_id', $id )->update($image);
        }
                        
        return Redirect('admin-panel/view-event');
          
      } else {

        $Add = new Events;
        $Add->event_name            =   $request['event_name'];
        $Add->event_class           =   implode(',',$request['event_class']);
        $Add->event_date            =   $request['event_date'];
        $Add->event_description     =   $request['event_description'];
        $Add->event_slug            =   $new_slug;
        $Add->save();
        $event_id = $Add->id;

        $file = $request->file('event_image');
        if($file != ""){
          $destinationPath = 'uploads/event_image';
          $imageName = 'uploads/event_image/'.time().'.'.$file->getClientOriginalExtension();
          $file->move($destinationPath,$imageName);
          $image=array(
              'event_image'         => $imageName
          );
          Events::where('event_id', $event_id)->update($image);
        }

        return redirect('admin-panel/view-event');      
      }
    }

    public function view_event(Request $request){

      $admin_details = Admin::where(['admin_id' => session('tms_admin_id')])->first();

      if ($request['check'] != "") {
        Events::whereIn('event_id', $request['check'])->delete();
        return Redirect()->back();
      }

      $query = Events::query();

      if($request['event_name'] != "") {
        $query = $query->where('event_name', 'LIKE', '%'.$request['event_name'].'%');
      }

      if($request['event_class'] != "") {
          $event_class = $request['event_class'];
          $query = $query->whereRaw('find_in_set('.$event_class.',event_class)');
      }
      
      $select_class = $request['event_class'];
      $app_data = array('event_name'=>$request['event_name'],'event_class'=>$request['event_class']);

      $get_record = $query->orderBy('event_id','DESC')->paginate(25)->appends($app_data);

      $get_class = Classes::where(['class_status' => 1])->get();

      $title         = "View Events";
      $data          = compact('title','get_record','admin_details','get_class','select_class');
      return view('admin_panel/view_event',$data);

    }


    public function event_status($id,$value) {
      Events::where('event_id', $id)->update(['event_status' => $value]);
      return Redirect()->back();
    }        

}