<?php
  namespace App\Http\Controllers\Faqs;
  use Illuminate\Http\Request;
  use App\Http\Requests;  
  use App\Http\Controllers\Controller;
  use DB;
  use App\Model\Faqs\Faqs; // model name
  use App\Model\Admin\Admin; // model name
  use App\Model\Admin\Account;
  use Response;

  class FaqController extends Controller {

    public function __construct()
    {
        $this->middleware('ValidateAdmin');
    }

    public function add_faq($id = false){
          
      $admin_details = Admin::where(['admin_id' => session('tms_admin_id')])->first();

      if($id){
          $get_record = Faqs::where(['faq_id' => $id])->first();
      } else {
          $get_record = "";
      }

      $title  = "Add FAQ";
      $data   = compact('title','admin_details','get_record');
      return view('admin_panel/add_faq',$data);
    }


    public function view_faq(Request $request){

      $admin_details = Admin::where(['admin_id' => session('tms_admin_id')])->first();

      if ($request['check'] != "") {
        Faqs::whereIn('faq_id', $request['check'])->delete();
        return Redirect()->back();
      }

      $query =  Faqs::query();

      if ($request['faq_question'] != "") {
        $query = $query->where('faq_question', 'LIKE', '%'.$request['faq_question'].'%');
      }

      if ($request['faq_answer'] != "") {
        $query = $query->where('faq_answer', 'LIKE', '%'.$request['faq_answer'].'%');
      }

      $app_data = array('faq_answer'=>$request['faq_answer'],'faq_question'=>$request['faq_question']);

      $get_record = $query->orderBy('faq_id','DESC')->paginate(25)->appends($app_data); 

      $title         = "View FAQ";
      $data          = compact('title','get_record','admin_details');
      return view('admin_panel/view_faq',$data);
    }


    public function save_faq($id =false ,Request $request) {
      
      if($id != "") {
        $values=array(
          'faq_question'      =>  $request['faq_question'],
          'faq_answer'        =>  $request['faq_answer'],
        );
        
        Faqs::where('faq_id', $id )->update($values);
        return Redirect('admin-panel/view-faq');
      } else {
             
        $Add = new Faqs ;
        $Add->faq_question       =   $request['faq_question'];
        $Add->faq_answer         =   $request['faq_answer'];
        $Add->save();
        return Redirect('admin-panel/view-faq');
      }
    }


    public function faq_status($id,$value) {
      Faqs::where('faq_id', $id)->update(['faq_status' => $value]);
      return Redirect()->back();
    }

       
  }