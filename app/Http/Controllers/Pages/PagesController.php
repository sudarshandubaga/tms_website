<?php
namespace App\Http\Controllers\Pages;
use Illuminate\Http\Request;
use App\Http\Requests;
use App\Http\Controllers\Controller;
use DB;
use App\Model\Pages\Pages; // model name
use App\Model\Pages\PageImage; // model name
use App\Model\Admin\Admin; // model name
use App\Model\Admin\Account;
use Response;
use Str;

class PagesController extends Controller {

    public function __construct()
    {
        $this->middleware('ValidateAdmin');
    }

    public function master_pages($slug) {
    
        $admin_details = Admin::where(['admin_id' => session('tms_admin_id')])->first();
        $get_record = Pages::with('page_images')->where(['master_pages_slug' => $slug])->first();

        // dd($get_record);
        
        $title  = $get_record['master_pages_title'];
        $data   = compact('title','admin_details','get_record');
        return view('admin_panel/master_pages',$data);
    }

    public function save_pages($id,Request $request){
        
      $get = Pages::where(['master_pages_slug' => str_slug($request['master_pages_title']) ])->whereNotIn('master_pages_id', [$id])->count();

      if($get == 0){
        $values=array(
          'master_pages_excerpt'       => $request['master_pages_excerpt'],
          'master_pages_description'   => $request['master_pages_description'],
        );

        Pages::where('master_pages_id', $id)->update($values);

        $file = $request->file('master_pages_image');
        if($file != ""){
          $destinationPath = 'uploads/master_pages_image';
          $imageName = 'uploads/master_pages_image/'.time().'.'.$file->getClientOriginalExtension();
          $file->move($destinationPath,$imageName);
          $image=array(
              'master_pages_image'         => $imageName
          );
          Pages::where('master_pages_id', $id )->update($image);
        }

        if(!empty($request->file('page_images'))) {
          foreach ($request->file('page_images') as $file) {
            # code...
            $destinationPath = 'uploads/master_pages_image';
            $imageName = 'uploads/master_pages_image/'.uniqid().'.'.$file->getClientOriginalExtension();
            $file->move($destinationPath,$imageName);
            $imageArr = [
              "image" => $imageName,
              "page_id" => $id
            ];
            PageImage::create($imageArr);
          }
        }
        
                          
        return redirect()->back()->with('success', 'Your content has been updated successfully.');     

      } else {
          return Redirect()->back()->withErrors(['Title Name Alredy Exit', 'The Message']);
      }   
  }

  public function destroy(PageImage $pageImage)
  {
    $pageImage->delete();

    return response()->json(["status" => true]);
  }
    
}