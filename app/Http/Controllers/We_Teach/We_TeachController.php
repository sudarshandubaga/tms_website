<?php
namespace App\Http\Controllers\We_Teach;
use Illuminate\Http\Request;
use App\Http\Requests;
use App\Http\Controllers\Controller;
use DB;
use App\Model\We_Teach\We_Teach; // model name
use App\Model\Admin\Admin; // model name
use App\Model\Admin\Account;
use Response;

class We_TeachController extends Controller 
{

    public function __construct()
    {
        $this->middleware('ValidateAdmin');
    }

    public function add_we_teach($id = false) {
    
      $admin_details = Admin::where(['admin_id' => session('tms_admin_id')])->first();

      if($id){
          $get_record = We_Teach::where(['we_teach_id' => $id])->first();
      } else {
          $get_record = "";
      }    

      $title  = "We Teach";
      $data   = compact('title','admin_details','get_record');
      return view('admin_panel/add_we_teach',$data);
    
    }


    public function save_we_teach($id = false,Request $request){
         
      if($id != ""){
          
        $values=array(
          'we_teach_title'         => $request['we_teach_title'],
          'we_teach_description'   => $request['we_teach_description'],
        );
        We_Teach::where('we_teach_id', $id )->update($values);
        
        $file = $request->file('we_teach_image');
        if($file != ""){
          $destinationPath = 'uploads/we_teach_image';
          $imageName = 'uploads/we_teach_image/'.time().'.'.$file->getClientOriginalExtension();
          $file->move($destinationPath,$imageName);
          $image=array(
              'we_teach_image'         => $imageName
          );
          We_Teach::where('we_teach_id', $id )->update($image);
        }
                        
        return Redirect('admin-panel/view-we-teach');
          
      } else {

        $Add = new We_Teach;
        $Add->we_teach_title            =   $request['we_teach_title'];
        $Add->we_teach_description      =   $request['we_teach_description'];
        $Add->save();
        $we_teach_id = $Add->id;

        $file = $request->file('we_teach_image');
        if($file != ""){
          $destinationPath = 'uploads/we_teach_image';
          $imageName = 'uploads/we_teach_image/'.time().'.'.$file->getClientOriginalExtension();
          $file->move($destinationPath,$imageName);
          $image=array(
              'we_teach_image'         => $imageName
          );
          We_Teach::where('we_teach_id', $we_teach_id)->update($image);
        }

        return redirect('admin-panel/view-we-teach');      
      }
    }

    public function view_we_teach(Request $request){

      $admin_details = Admin::where(['admin_id' => session('tms_admin_id')])->first();

      if ($request['check'] != "") {
        We_Teach::whereIn('we_teach_id', $request['check'])->delete();
        return Redirect()->back();
      }

      $query = We_Teach::query();

      if($request['we_teach_title'] != "") {
        $query = $query->where('we_teach_title', 'LIKE', '%'.$request['we_teach_title'].'%');
      }

      if($request['we_teach_description'] != "") {
        $query = $query->where('we_teach_description', 'LIKE', '%'.$request['we_teach_description'].'%');
      }
      
      $app_data = array('we_teach_title'=>$request['we_teach_title'],'we_teach_description'=>$request['we_teach_description']);

      $get_record = $query->orderBy('we_teach_id','DESC')->paginate(25)->appends($app_data);

      $title         = "View We Teach";
      $data          = compact('title','get_record','admin_details');
      return view('admin_panel/view_we_teach',$data);

    }


    public function we_teach_status($id,$value) {
      We_Teach::where('we_teach_id', $id)->update(['we_teach_status' => $value]);
      return Redirect()->back();
    }        

}