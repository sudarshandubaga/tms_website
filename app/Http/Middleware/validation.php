<?php

namespace App\Http\Middleware;

use Closure;

class validation
{
    /**
     * Handle an incoming request.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  \Closure  $next
     * @return mixed
     */
    public function handle($request, Closure $next)
    {

        if(!$request->session()->has('tms_admin_id')) {
            return redirect('admin-panel');
        }

        return $next($request);
    }
}
