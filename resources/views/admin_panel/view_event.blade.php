@extends('admin_panel/layout')
@section('content')

<style type="text/css">

.dt-panelfooter{
  display: none !important;
}
</style>

  <!-- Start: Content-Wrapper -->
    <section id="content_wrapper">
      <header id="topbar" style="margin-top:60px">
        <div class="topbar-left">
          <ol class="breadcrumb">
            <li class="crumb-active">
              <a href="{{ url('/admin-panel/add-event') }}">Add Events</a>
            </li>
            <li class="crumb-icon">
              <a href="{{ url('/admin-panel/dashboard') }}">
                <span class="glyphicon glyphicon-home"></span>
              </a>
            </li>
            <li class="crumb-link">
              <a href="{{ url('/admin-panel/dashboard') }}">Home</a>
            </li>
            <li class="crumb-trail">View Events</li>
          </ol>
        </div>
      </header>

      <div class="" style="margin-top:10px;">
        <div class="col-md-12">
          <div class="panel panel-primary panel-border top mb50">  
            <div class="panel-heading">
              <div class="panel-title hidden-xs">
                <span class="glyphicon glyphicon-tasks"></span>View Events</div>
            </div>

            <div class="panel-menu admin-form theme-primary">
              <div class="row">
                
                {!! Form::open(['url'=>'/admin-panel/view-event']) !!}

                  <div class="col-md-3">
                    <label for="name" class="field prepend-icon">
                      {!! Form::text('event_name', $event_name , array('class' => 'form-control product','placeholder' => 'Search By Name', 'autocomplete' => 'off' )) !!}
                      <label for="name" class="field-icon">
                        <i class="fa fa-search" style="padding-top: 12px;"></i>
                      </label>
                    </label>
                  </div>

                  <div class="col-md-2">
                    <label for="name" class="field prepend-icon">
                      <select class="select2-single form-control" name="event_class" id="event_class" >
                        <option value="">Select Class</option>
                        @foreach($get_class as $class) 
                          <option value="{{$class->class_id}}" {{ $select_class == $class->class_id ? 'selected="selected"' : '' }} > {{$class->class_name}} </option>
                        @endforeach
                      </select>
                      <i class="arrow double"></i>
                    </label>
                  </div>
                   
                  <div class="col-md-1">
                    <button type="submit" name="search" class="button btn-primary"> Search </button>
                  </div>

                {!! Form::close() !!}


                  <div class="col-md-1 pull-right">
                     <a href="{{ url('admin-panel/view-event') }}"> {!! Form::submit('Default', array('class' => 'btn btn-primary', 'id' => 'maskedKey')) !!} </a>
                  </div>

                {!! Form::close() !!}
              </div>
            </div>
            
            <div class="panel-body pn">

              {!! Form::open(['name'=>'form']) !!}

              <div class="table-responsive">
                <table class="table admin-form theme-warning tc-checkbox-1 fs13" id="datatable">
                  <thead>
                    <tr class="bg-light">
                      <th style="width:90px !important;" class="text-left">
                        <label class="option block mn">
                          <input type="checkbox" id="check_all"> 
                          <span class="checkbox mn"></span>
                          Select All
                        </label>
                      </th>
                      <th class="">Name</th>
                      <th class="">Classes</th>
                      <th class="">Event Date</th>
                      <th class="w100">Description</th>
                      <th class="">Image</th>
                      <th class="w150">Created</th>
                      <th class="w150">Action</th>
                    </tr>
                  </thead>
                  <tbody>
            
                     @foreach($get_record as $get_records)  
                    <tr>
                      <td class="text-left" style="padding-left:18px">
                        <label class="option block mn">
                          <input type="checkbox" name="check[]" class="check" value="{{$get_records->event_id}}">
                          <span class="checkbox mn"></span>
                        </label>
                      </td>
                      
                      <td class="" style="padding-left:20px" > {{$get_records->event_name}} </td>
                      
                      @if($get_records->event_class != "")

                      @php
                        $class_list = "";
                        $arr = explode(',', $get_records->event_class);
                        $get_classes =  \App\Model\Classes\Classes::whereIn('class_id' , $arr )->get()->toArray();
                        foreach($get_classes as $get_classes){
                          $class_list .= $get_classes['class_name'].", ";
                          
                        }
                        $class_list = substr($class_list,0,-1);
                      @endphp

                        <td class="" style="padding-left:20px" > {{$class_list}} </td>
                      @else
                        <td class="" style="padding-left:20px" > Available For All  </td>
                      @endif

                      <td class="" style="padding-left:20px" > {{ $get_records->event_date}} </td>                      
                      
                      <td class="" style="padding-left:20px"> 
                        <a href="#" style="text-transform: capitalize; text-decoration:none;" data-toggle="modal" data-target="#view_message{{$get_records->event_id}}" > View Description </a>

                        <!-- Sign In model -->
                        <div id="view_message{{$get_records->event_id}}" class="modal fade in" role="dialog">
                          <div class="modal-dialog" style="width:700px; margin-top:100px;">
                            <div class="modal-content">
                              <div class="modal-header" style="padding-bottom: 35px;">
                                <button type="button" class="close" data-dismiss="modal">&times;</button>
                                <h4 class="modal-title pull-left">Event Description</h4>
                              </div>
                              <div class="modal-body">
                                <section style="">
                                  <div class="row">
                                    <div class="col-md-12 text-justify">
                                      {!! $get_records->event_description !!}
                                    </div>
                                  </div>
                                </section>
                              </div>
                              <div class="clearfix"></div>
                            </div> 
                          </div>
                        </div>
                        
                      </td>

                      <td class="text-center"> 
                        @if ($get_records->event_image == "")
                            No Image Available
                        @else 
                          {!! Html::image($get_records->event_image, '', array('class' => 'data_photo')) !!}   
                        @endif
                      </td>
                      <td class="" style="padding-left:20px" > {{date('d F Y',strtotime($get_records->event_created))}} </td>

                      <td class="text-left">
                        <div class="btn-group text-left">
                          <button type="button" class="btn {{ $get_records->event_status == 1 ? 'btn-success' : 'btn-danger' }}  br2 btn-xs fs12 dropdown-toggle" data-toggle="dropdown" aria-expanded="false"> {{ $get_records->event_status == 1 ? 'Active' : 'Deactive' }}
                            <span class="caret ml5"></span>
                          </button>
                          <ul class="dropdown-menu" role="menu" style="min-width:125px;">
                            <li>
                              <a href="{{ url('/admin-panel/add-event') }}/{{$get_records->event_id}}">Edit</a>
                            </li>
                            
                            <li class="divider"></li>
                            <li class="{{ $get_records->event_status == 1 ? 'active' : '' }}">
                              <a href="{{ url('/admin-panel/event-status') }}/{{$get_records->event_id}}/1">Active</a>
                            </li>
                            <li class=" {{ $get_records->event_status == 0 ? 'active' : '' }} ">
                              <a href="{{ url('/admin-panel/event-status') }}/{{$get_records->event_id}}/0">Deactive</a>
                            </li>
                          </ul>
                        </div>
                      </td>
                    </tr>

                    @endforeach
                  </tbody>
                </table>
              </div>
              {!! Form::close() !!}

            </div>

            <div class="panel-body pn">
              <div class="table-responsive">
                <table class="table admin-form theme-warning tc-checkbox-1 fs13">                                
                  <tbody>
                    <tr class="">
                      
                      <th  class="text-left">
                        <button type="button" class="btn btn-primary" onclick="go_delete()"><i class="glyphicon glyphicon-trash"></i> Delete Multiple </button>
                      </th>    
                      <th  class="text-right">
                        {{ $get_record->links() }}
                      </th>
                    </tr>
                  </tbody>
                </table>
              </div>
            </div>

          </div>
        </div>
      </div>

@endsection


