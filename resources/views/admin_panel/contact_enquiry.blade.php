@extends('admin_panel/layout')

@section('content')

<style type="text/css">

.dt-panelfooter{
  display: none !important;
}
</style>

  <!-- Start: Content-Wrapper -->
    <section id="content_wrapper">
      <header id="topbar" style="margin-top:60px">
        <div class="topbar-left">
          <ol class="breadcrumb">
            <li class="crumb-active">
              <a href="{{ url('/admin-panel/contact-enquiry') }}">Conatct Enquiry</a>
            </li>
            <li class="crumb-icon">
              <a href="{{ url('/admin-panel/dashboard') }}">
                <span class="glyphicon glyphicon-home"></span>
              </a>
            </li>
            <li class="crumb-link">
              <a href="{{ url('/admin-panel/dashboard') }}">Home</a>
            </li>
            <li class="crumb-trail">Conatct Enquiry</li>
          </ol>
        </div>
      </header>

      <div class="" style="margin-top:10px;">
        <div class="col-md-12">
          <div class="panel panel-primary panel-border top mb35">  
            <div class="panel-heading">
              <div class="panel-title hidden-xs">
                <span class="glyphicon glyphicon-tasks"></span>Conatct Enquiry</div>
            </div>

            <div class="panel-menu admin-form theme-primary">
              <div class="row">
                {!! Form::open(['url'=>'admin-panel/contact-enquiry']) !!}
                  <div class="col-md-3">
                    <label for="name" class="field prepend-icon">
                      {!! Form::text('contact_enquiry_name', $contact_enquiry_name , array('class' => 'form-control product','placeholder' => 'Search By Name', 'autocomplete' => 'off' )) !!}
                      <label for="name" class="field-icon">
                        <i class="fa fa-search" style="padding-top: 12px;"></i>
                      </label>
                    </label>
                  </div>
                  <div class="col-md-3">
                    <label for="email" class="field prepend-icon">
                      {!! Form::text('contact_enquiry_email', $contact_enquiry_email , array('class' => 'form-control product','placeholder' => 'Search By Email', 'autocomplete' => 'off' )) !!}
                      <label for="email" class="field-icon">
                        <i class="fa fa-search" style="padding-top: 12px;"></i>
                      </label>
                    </label>
                  </div>
                  <div class="col-md-3">
                    <label for="number" class="field prepend-icon">
                      {!! Form::text('contact_enquiry_number', $contact_enquiry_number , array('class' => 'form-control product','placeholder' => 'Search By Number', 'autocomplete' => 'off' )) !!}
                      <label for="email" class="field-icon">
                        <i class="fa fa-search" style="padding-top: 12px;"></i>
                      </label>
                    </label>
                  </div>
                  
                  <div class="col-md-1">
                    <button type="submit" name="search" class="button btn-primary"> Search </button>
                  </div>
                {!! Form::close() !!}
                  <div class="col-md-1 pull-right">
                     <a href="{{ route('admin.export_contact_enquiry') }}"> {!! Form::submit('Export', array('class' => 'btn btn-info btn-block', 'id' => 'maskedKey')) !!} </a>
                  </div>
                  <div class="col-md-1 pull-right">
                     <a href="{{ url('admin-panel/contact-enquiry') }}"> {!! Form::submit('Default', array('class' => 'btn btn-primary btn-block', 'id' => 'maskedKey')) !!} </a>
                  </div>
              </div>
            </div>
            
            <div class="panel-body pn">

              {!! Form::open(['name'=>'form']) !!}

              <div class="table-responsive">
                <table class="table admin-form theme-warning tc-checkbox-1 fs13" id="datatable">
                  <thead>
                    <tr class="bg-light">
                      <th style="width:90px !important;" class="text-left">
                        <label class="option block mn">
                          <input type="checkbox" id="check_all"> 
                          <span class="checkbox mn"></span>
                          Select All
                        </label>
                      </th>
                      <th class="">Name</th>
                      <th class="">Email</th>
                      <th class="">Number</th>
                      <th class="">Subject</th>
                      <th class="">Message</th>
                      <th class="w150">Actions</th>
                      <th class="w150">Enquiry Received</th>
                    </tr>
                  </thead>
                  <tbody>
                    
                     @foreach($get_record as $get_records)  

                    <tr>
                      <td class="text-left" style="padding-left:18px">
                        <label class="option block mn">
                          <input type="checkbox" name="check[]" class="check" value="{{$get_records->contact_enquiry_id}}">
                          <span class="checkbox mn"></span>
                        </label>
                      </td>  
                      <td class="text-left text-capitalize" style="padding-left:20px" > {{$get_records->contact_enquiry_name}} </td>
                      <td class="" style="padding-left:20px" > {{$get_records->contact_enquiry_email}} </td>
                      <td class="text-capitalize" style="padding-left:20px" > {{$get_records->contact_enquiry_number}} </td>
                      <td class="text-capitalize" style="padding-left:20px" > {{$get_records->contact_enquiry_subject}} </td>
                      <td class="" style="padding-left:20px"> 
                        <a href="#" style="text-transform: capitalize; text-decoration:none;" data-toggle="modal" data-target="#view_message{{$get_records->contact_enquiry_id}}" > View Message </a>

                        <!-- Sign In model -->
                        <div id="view_message{{$get_records->contact_enquiry_id}}" class="modal fade in" role="dialog">
                          <div class="modal-dialog" style="width:700px; margin-top:100px;">
                            <div class="modal-content">
                              <div class="modal-header" style="padding-bottom: 35px;">
                                <button type="button" class="close" data-dismiss="modal">&times;</button>
                                <h4 class="modal-title pull-left">Contact Message</h4>
                              </div>
                              <div class="modal-body">
                                <section style="">
                                  <div class="row">
                                    <div class="col-md-12 text-justify">
                                      {{$get_records->contact_enquiry_message}}
                                    </div>
                                  </div>
                                </section>
                              </div>
                              <div class="clearfix"></div>
                            </div> 
                          </div>
                        </div>
                        
                      </td>
                      <td class="" style="padding-left:20px" > {{date('d F Y',strtotime($get_records->contact_enquiry_created))}} </td>
                      <td class="" style="padding-left:20px" > 
    
                        <div class="btn-group text-left">
                          <button type="button" class="btn {{ $get_records->contact_enquiry_status == 1 ? 'btn-info' : ''}} {{ $get_records->contact_enquiry_status == 2 ? 'btn-success' : '' }}  br2 btn-xs fs12 dropdown-toggle" data-toggle="dropdown" aria-expanded="false"> {{ $get_records->contact_enquiry_status == 1 ? 'Request Received' : '' }} {{ $get_records->contact_enquiry_status == 2 ? 'Completed' : '' }}
                            <span class="caret ml5"></span>
                          </button>
                          <ul class="dropdown-menu" role="menu" style="min-width:125px;">
                            <li class="{{ $get_records->contact_enquiry_status == 1 ? 'active' : '' }}">
                              <a href="#">Request Received</a>  
                            </li>
                            <li class=" {{ $get_records->contact_enquiry_status == 2 ? 'active' : '' }} ">
                              <a href="{{ url('/admin-panel/contact-enquiry-status') }}/{{$get_records->contact_enquiry_id}}/2">Completed</a>
                            </li>
                          </ul>
                        </div>
                      </td>     
                    </tr>

                    @endforeach
                  </tbody>
                </table>
              </div>
              {!! Form::close() !!}

            </div>

            <div class="panel-body pn">
              <div class="table-responsive">
                <table class="table admin-form theme-warning tc-checkbox-1 fs13">                                
                  <tbody>
                    <tr class="">    
                      <th  class="text-left">
                        <button type="button" class="btn btn-primary" onclick="go_delete()"><i class="glyphicon glyphicon-trash"></i> Delete Multiple </button>
                      </th>
                      <th  class="text-right">
                        {{ $get_record->links() }}
                      </th>
                    </tr>
                  </tbody>
                </table>
              </div>
            </div>

          </div>
        </div>
      </div>

@endsection
