@extends('admin_panel/layout')

@section('content')

  <!-- Start: Content-Wrapper -->
    <section id="content_wrapper">
      <header id="topbar">
        <div class="topbar-left">
          <ol class="breadcrumb">
            <li class="crumb-active text-capitalize">
              <a href="{{ url('/admin-panel/master-pages') }}/{{ $get_record['master_pages_slug'] }}"> {{ $get_record['master_pages_title'] }} </a>
            </li>
            <li class="crumb-icon">
              <a href="{{ url('/admin-panel/dashboard') }}">
                <span class="glyphicon glyphicon-home"></span>
              </a>
            </li>
            <li class="crumb-link">
              <a href="{{ url('/admin-panel/dashboard') }}">Home</a>
            </li>
            <li class="crumb-trail text-capitalize"> {{ $get_record['master_pages_title'] }} </li>
          </ol>
        </div>
      </header>

      <div class="row">
        <div class="col-md-12">
          @if (\Session::has('success'))
            <div class="alert alert-success" style="padding: 10px; border: 0px; text-align: center; margin: 10px;">
                {!! \Session::get('success') !!}
            </div>
          @endif
        </div>
      </div>

      <section id="content" class="table-layout animated fadeIn">
        <div class="tray tray-center">
          <div class="center-block">
            <div class="panel panel-primary panel-border top mb35">
              <div class="panel-heading">
                <span class="panel-title text-capitalize"> {{ $get_record['master_pages_title'] }} </span>
              </div>
              <div class="panel-body bg-light dark">
                <div class="tab-content pn br-n admin-form">
                  <div id="tab1_1" class="tab-pane active">
                    
                  @if ($errors->any())
                    <div id="log_error" class="alert alert-danger"><i class="fa fa-thumbs-o-down"> {{$errors->first()}} </i></div>
                  @endif

                    {!! Form::open(['url'=>'admin-panel/save-master-pages/'.$get_record['master_pages_id'].'' , 'name'=>'form', 'enctype' => 'multipart/form-data' , 'id' => 'validation' ] ) !!}

                    <div class="row">
                      <div class="col-md-8">
                        <div class="col-md-12">
                          <div class="section">
                            <label for="master_pages_title" class="field-label" style="font-weight:600;" > Title </label>
                              <label for="master_pages_title" class="field prepend-icon">
                                {!! Form::text('master_pages_title',$get_record['master_pages_title'], array('class' => 'event-name gui-input br-light light focusss','placeholder' => '', 'disabled' => 'disabled' )) !!}
                                <label for="store-currency" class="field-icon">
                                  <i class="glyphicons glyphicons-edit"></i>
                                </label>
                              </label>
                          </div>
                        </div>

                        <div class="col-md-12">
                          <div class="section">
                            <label for="master_pages_excerpt" class="field-label" style="font-weight:600;" > Excerpt </label>
                              <label for="master_pages_excerpt" class="field prepend-icon">
                                {!! Form::textarea('master_pages_excerpt',$get_record['master_pages_excerpt'], array('class' => 'gui-textarea accc','placeholder' => '' )) !!}
                                <label for="store-currency" class="field-icon">
                                  <i class="fa fa-comments"></i>
                                </label>
                              </label>
                          </div>
                        </div>
                        <div class="col-md-12">
                          <div class="section">
                            <label for="master_pages_description" class="field-label" style="font-weight:600;" > Description </label>
                              <label for="master_pages_description" class="field prepend-icon">
                                {!! Form::textarea('master_pages_description',$get_record['master_pages_description'], array('class' => 'gui-textarea accc jqte-test','placeholder' => '' )) !!}
                              </label>
                          </div>
                        </div>    
                      </div>

                      <div class="col-md-4" style="margin-top:25px;">
                        <label for="name" class="field">
                          <div class="fileupload fileupload-new admin-form" data-provides="fileupload" >  
                            @if($get_record != "")
                              <div class="fileupload-preview thumbnail mb15" style="line-height: 136px !important;">
                                @if($get_record['master_pages_image'] != "")
                                  {!! Html::image($get_record['master_pages_image'], '100%x147') !!}
                                @else
                                  <img data-src="holder.js/100%x150" alt="100%x147" src="public/images/dummy.png" data-holder-rendered="true" style="height: 100% !important; width: 100% !important; display: block;">
                                @endif
                              </div>
                              <span class="button btn-system btn-file btn-block ph5" style="margin-bottom:10px">
                                <span class="fileupload-new" >Change Image</span>
                                <span class="fileupload-exists">Change Image</span>  
                                {!! Form::file('master_pages_image') !!}
                              </span>
                            @else
                              <div class="fileupload-preview thumbnail mb15" style="line-height: 136px !important;">
                                <img data-src="holder.js/100%x150" alt="100%x147" src="public/images/dummy.png" data-holder-rendered="true" style="height: 100% !important; width: 100% !important; display: block;">
                              </div>
                              <span class="button btn-system btn-file btn-block ph5" style="margin-bottom:10px">
                                <span class="fileupload-new" >Image</span>
                                <span class="fileupload-exists">Image</span>
                                {!! Form::file('master_pages_image') !!}
                              </span>
                            @endif
                          </div>
                        </label>

                        <div class="form-group">
                          <button type="button" class="btn btn-sm btn-info add-images-btn">Add Images</button>
                        </div>

                        <div class="row" id="page-images-group">
                          @if(!empty($get_record->page_images))
                            @foreach($get_record->page_images as $image)
                            <div class="col-sm-4 page-image-label">
                              <button type="button" class="close-btn delete-image" data-id="{{ $image->id }}">
                                <i class="fa fa-times"></i>
                              </button>
                              <div>
                                <div class="fileupload-preview thumbnail mb15" style="line-height: 136px !important;">
                                  <img data-src="holder.js/100%x150" alt="100%x147" src="{{ url($image->image) }}" data-holder-rendered="true" style="height: 100% !important; width: 100% !important; display: block;">
                                </div>
                              </div>
                            </div>
                            @endforeach
                          @endif
                        </div>
                      </div>

                    </div>

                    <div class="row">
                      
                    </div>


                    <div class="panel-footer text-right">
                      <button type="button" class="button btn-primary" onclick="update_record()"> Update </button>
                      {!! Form::reset('Cancel', array('class' => 'button btn-primary', 'id' => 'maskedKey')) !!}
                    </div>
                    
                    {!! Form::close() !!}

                  </div>
                </div>
              </div>
            </div>
          </div>
        </div>
      </section>
      <!-- End: Content -->
    </section>

<style>
    
  .jqte_editor{
    height: auto !important;
    min-height: 600px!important;
    overflow: auto;
  }
  .jqte_source{
    height: auto !important;
    min-height: 600px!important;
  }
</style>



<script type="text/javascript">
  jQuery(document).ready(function() {
  
    /* @custom validation method (smartCaptcha) 
    ------------------------------------------------------------------ */
    $("#validation").validate({

      /* @validation states + elements 
      ------------------------------------------- */

      errorClass: "state-error",
      validClass: "state-success",
      errorElement: "em",

      /* @validation rules 
      ------------------------------------------ */

      rules: {
        master_pages_title: {
          required: true,
        },

        master_pages_excerpt: {
          required: true,
        },

        master_pages_description: {
          required: true,
        },
       
      },

      /* @validation error messages 
      ---------------------------------------------- */

      // messages: {
      //   master_pages_title: {
      //     required: 'Enter Pages Title'
      //   },

      //   master_pages_excerpt: {
      //     required: 'Enter pages Excerpt'
      //   },

      //   master_pages_description: {
      //     required: 'Enter Pages Description'
      //   },
      // },

      /* @validation highlighting + error placement  
      ---------------------------------------------------- */

      highlight: function(element, errorClass, validClass) {
        $(element).closest('.field').addClass(errorClass).removeClass(validClass);
      },
      unhighlight: function(element, errorClass, validClass) {
        $(element).closest('.field').removeClass(errorClass).addClass(validClass);
      },
      errorPlacement: function(error, element) {
        if (element.is(":radio") || element.is(":checkbox")) {
          element.closest('.option-group').after(error);
        } else {
          error.insertAfter(element.parent());
        }
      }

    });

    $(document).on("click", ".add-images-btn", function (e) {
      e.preventDefault();

      var html = `<div class="col-sm-4 page-image-label">
                    <button type="button" class="close-btn remove-page-image">
                      <i class="fa fa-times"></i>
                    </button>
                    <label>
                      <div class="fileupload-preview thumbnail mb15" style="line-height: 136px !important;">
                        <img data-src="holder.js/100%x150" alt="100%x147" src="public/images/dummy.png" data-holder-rendered="true" style="height: 100% !important; width: 100% !important; display: block;">
                        <input type="file" name="page_images[]" style="display: none;">
                      </div>
                    </label>
                  </div>`;

      $("#page-images-group").append(html);
    });

    $(document).on("change", ".page-image-label input[type=file]", function (e) {
      var output = document.getElementById('output');
      $(this).closest(".page-image-label").find("img").attr("src", URL.createObjectURL(e.target.files[0]));
    });

    $(document).on("click", ".remove-page-image", function (e) {
      e.preventDefault();
      $(this).closest("[class^=col-]").remove();
    });

    $(document).on("click", ".delete-image", function (e) {
      e.preventDefault();

      let self = $(this);
      let id = self.data('id');

      $.ajax({
        url: "{{ url('admin-panel/pages') }}/" + id,
        method: "DELETE",
        success: function (res) {
          self.closest("[class^=col-]").remove();
        }
      });
    });
  });
  </script>

@endsection
