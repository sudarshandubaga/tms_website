@extends('admin_panel/layout')

@section('content')

<style type="text/css">

.dt-panelfooter{
  display: none !important;
}
</style>

  <!-- Start: Content-Wrapper -->
    <section id="content_wrapper">
      <header id="topbar" style="margin-top:60px">
        <div class="topbar-left">
          <ol class="breadcrumb">
            <li class="crumb-active">
              <a href="{{ url('/admin-panel/time-slot') }}">Time Slot</a>
            </li>
            <li class="crumb-icon">
              <a href="{{ url('/admin-panel/dashboard') }}">
                <span class="glyphicon glyphicon-home"></span>
              </a>
            </li>
            <li class="crumb-link">
              <a href="{{ url('/admin-panel/dashboard') }}">Home</a>
            </li>
            <li class="crumb-trail">Time Slot</li>
          </ol>
        </div>
      </header>

      <div class="" style="margin-top:10px;">
        <div class="col-md-12">
          <div class="panel panel-primary panel-border top mb50">  
            <div class="panel-heading">
              <div class="panel-title hidden-xs">
                <span class="glyphicon glyphicon-tasks"></span>Time Slot</div>
            </div>

            <div class="panel-body bg-light dark">
                <div class="tab-content pn br-n admin-form">
                  <div id="tab1_1" class="tab-pane active">
                  
                    <div class="row">
                      <div class="col-md-12">
                        <div class="col-md-12">
                          <div class="section">
                              <div class="container">
                            {!! Form::open(['url'=>'admin-panel/save-time-slot/'.$get_record['time_slot_id'].'' , 'enctype' => 'multipart/form-data' , 'id' => 'validation' ] ) !!}  
                                @if($get_record != "")
                                
                                      <div class="col-md-2"><input type="date" name="meeting_date" class="form-control" required="" value="{{ $get_record['meeting_date'] }}"></div>
                                      <div class="col-md-2"><input type="time" name="time_slot_from"   class="form-control" required="" value="{{ $get_record['time_slot_from'] }}"></div>
                                      <div class="col-md-2"><input type="time" name="time_slot_to" class="form-control" required="" value="{{ $get_record['time_slot_to'] }}"></div>
                                      <div class="col-md-2"><input type="number" name="total_appointment" class="form-control" required="" value="{{ $get_record['total_appointment'] }}"><input type="hidden" name="my_appointment" style="display:none"class="form-control" required="" value="{{ $get_record['my_appointment'] }}"></div>
                                      <div class="col-md-2">
                                          <select name="time_slot_type" required="" class="form-control">
                                               @if($get_record['time_slot_type'] == "In-person Interaction")
                                              <option value="In-person Interaction" selected="selected">In-person Interaction</option>
                                              <option value="Through Video Call">Through Video Call</option>
                                              @else
                                              <option value="In-person Interaction" >In-person Interaction</option>
                                              <option value="Through Video Call" selected="selected">Through Video Call</option>
                                              @endif
                                          </select>
                                      </div>
                                    
                                @else
                                      <div class="col-md-2"><input type="date" name="meeting_date" class="form-control" required=""></div>
                                      <div class="col-md-2"><input type="time" name="time_slot_from" class="form-control" required=""></div>
                                      <div class="col-md-2"><input type="time" name="time_slot_to" class="form-control" required=""></div>
                                      <div class="col-md-2"><input type="number" name="total_appointment" class="form-control" required="" value="1"></div>
                                      <div class="col-md-2">
                                          <select name="time_slot_type" required="" class="form-control">
                                              <option value="">-- Select --</option>
                                              <option value="In-person Interaction">In-person Interaction</option>
                                              <option value="Through Video Call">Through Video Call</option>
                                          </select>
                                      </div>
                                   
                                @endif
                                <div class="col-md-2">{!! Form::submit('Save', array('class' => 'button btn-primary', 'id' => 'maskedKey')) !!}</div>
                                </div>
                              {!! Form::close() !!}
                          </div>
                        </div>
                      </div>
                    </div>
                    
               @if($meeting_detail)     
            <div class="panel-body pn" style="margin-bottom:30px; background:#fff">



              <div class="table-responsive">
                <table class="table admin-form theme-warning tc-checkbox-1 fs13" id="datatable">
                  <thead>
                    <tr class="bg-light">
                      <th class="border">Student Info</th>
                      <th class="border">Father's Info</th>
                      <th class="border">Mother's Info</th>
                      <th class="border">Other Details</th>
                    </tr>
                  </thead>
                  <tbody>
                    
                     @foreach($meeting_detail as $get_records)  

                    <tr>
                        
                      <td class="text-left text-capitalize" style="padding-left:20px"  width="250px!important;">
                        <div class="row">
                          <div class="col-sm-4"><strong>Name</strong></div>
                          <div class="col-sm-8">{{$get_records->admission_name}}</div>
                        </div>
                        <div class="row">
                          <div class="col-sm-4"><strong>DOB</strong></div>
                          <div class="col-sm-8">{{$get_records->admission_dob}}</div>
                        </div>
                        <div class="row">
                          <div class="col-sm-4"><strong>Gender</strong></div>
                          <div class="col-sm-8">{{$get_records->admission_gender}}</div>
                        </div>
                        <div class="row">
                          <div class="col-sm-4"><strong>Religion</strong></div>
                          <div class="col-sm-8">{{$get_records->admission_religion}}</div>
                        </div>
                        <div class="row">
                          <div class="col-sm-4"><strong>Class</strong></div>
                          <div class="col-sm-8">{{$get_records->admission_class_name}}</div>
                        </div>
                        <div class="row">
                          <div class="col-sm-4"><strong>Last School</strong></div>
                          <div class="col-sm-8">{{$get_records->admission_last_school_name}}</div>
                        </div>
                      </td>
                      <td class=""  width="250px!important;">
                        <div class="row">
                          <div class="col-sm-4"><strong>Name</strong></div>
                          <div class="col-sm-8">{{$get_records->admission_father_name}}</div>
                        </div>
                        <div class="row">
                          <div class="col-sm-4"><strong>Cell</strong></div>
                          <div class="col-sm-8">{{$get_records->admission_father_cell_no1}}</div>
                        </div>
                        <div class="row">
                          <div class="col-sm-4"><strong>Email</strong></div>
                          <div class="col-sm-8">{{$get_records->admission_father_email}}</div>
                        </div>
                        
                      </td>
                      <td class=""  width="250px!important;">
                        <div class="row">
                          <div class="col-sm-4"><strong>Name</strong></div>
                          <div class="col-sm-8">{{$get_records->admission_mother_name}}</div>
                        </div>
                        <div class="row">
                          <div class="col-sm-4"><strong>Cell</strong></div>
                          <div class="col-sm-8">{{$get_records->admission_mother_cell_no1}}</div>
                        </div>
                        <div class="row">
                          <div class="col-sm-4"><strong>Email</strong></div>
                          <div class="col-sm-8">{{$get_records->admission_mother_email}}</div>
                        </div>
                       
                      </td>
                      <td class="" >
                        <div class="row">
                          <div class="col-sm-4"><strong>Address</strong></div>
                          <div class="col-sm-8">{{$get_records->admission_city}}, {{$get_records->admission_state}}, {{$get_records->admission_pincode}}</div>
                        </div>
                      
                      </td>
                    </tr>

                    @endforeach
                  </tbody>
                </table>
              </div>

            </div>
            @endif

                    <!--<div class="panel-footer text-right">-->
                      
                    <!--  {!! Form::reset('Cancel', array('class' => 'button btn-primary', 'id' => 'maskedKey')) !!}-->
                    <!--</div>-->
                    
                  </div>
                </div>
              </div>


            
            
          
            
            <div class="panel-body pn">

              {!! Form::open(['name'=>'form']) !!}

              <div class="table-responsive">
                <table class="table admin-form theme-warning tc-checkbox-1 fs13" id="datatable">
                  <thead>
                    <tr class="bg-light">
                      <!--<th style="width:90px !important;" class="text-left">-->
                      <!--  <label class="option block mn">-->
                      <!--    <input type="checkbox" id="check_all"> -->
                      <!--    <span class="checkbox mn"></span><br/>-->
                      <!--    Select All-->
                      <!--  </label>-->
                      <!--</th>-->
                      <th class="w100">S.no.</th>
                      <th class="w150">Date</th>
                      <th class="w150">Time Slot</th>
                      <th class="w150">Type</th>
                      <th class="w150">Total Appointment</th>
                      <th class="w150">My Appointment</th>
                      <th class="w150">Actions </th>
                    </tr>
                  </thead>
                  <tbody>
                    <?php $number = 1; ?>
                     @foreach($time_slot as $time_slot)  

                    <tr>
                      <!--<td class="text-left" style="padding-left:18px">-->
                      <!--  <label class="option block mn">-->
                      <!--    <input type="checkbox" name="check[]" class="check" value="{{$time_slot->time_slot_id}}">-->
                      <!--    <span class="checkbox mn"></span>-->
                      <!--  </label>-->
                      <!--</td>                     -->
                      <td class="text-capitalize" style="padding-left:20px" > {{ $number }} </td>
                      <td class="text-capitalize" style="padding-left:20px" > {{$time_slot->meeting_date}} </td>
                      <td class="text-capitalize" style="padding-left:20px" > {{$time_slot->time_slot_from}} -{{$time_slot->time_slot_to}} </td>
                      <td class="text-capitalize" style="padding-left:20px" > {{$time_slot->time_slot_type}} </td>
                      <td class="text-capitalize" style="padding-left:20px" > {{$time_slot->total_appointment}} </td>
                      <td class="text-capitalize" style="padding-left:20px" > {{$time_slot->my_appointment}} </td>
                       
                      
                      <td class="" style="padding-left:20px" > 

                        <div class="btn-group text-left">
                          <button type="button" class="btn {{ $time_slot->time_slot_status == 1 ? 'btn-success' : ''}} {{ $time_slot->time_slot_status == 0 ? 'btn-danger' : '' }} br2 btn-xs fs12 dropdown-toggle" data-toggle="dropdown" aria-expanded="false"> {{ $time_slot->time_slot_status == 1 ? 'Active' : '' }} {{ $time_slot->time_slot_status == 0 ? 'Deactive' : '' }} 
                          <span class="caret ml5"></span>
                          </button>
                          <ul class="dropdown-menu" role="menu" style="min-width:125px; z-index:99999;">
                            <li>
                              <a href="{{ url('/admin-panel/time-slot') }}/{{$time_slot->time_slot_id}}">Edit</a>
                            </li>
                            
                            <li class="divider"></li>
                            <li>
                                <a href="{{ url('/admin-panel/time-slot-information') }}/{{$time_slot->time_slot_id}}"> View Details </a>
                            </li>
                            <li class="divider"></li>
                            <li class="{{ $time_slot->time_slot_status == 1 ? 'active' : '' }}">
                                <a href="{{ url('/admin-panel/time-slot-status') }}/{{$time_slot->time_slot_id}}/1"> Active </a>
                            </li>
                            <li class="{{ $time_slot->time_slot_status == 0 ? 'active' : '' }}">
                              <a href="{{ url('/admin-panel/time-slot-status') }}/{{$time_slot->time_slot_id}}/0"> Deactive</a>
                            </li>
                            <li class="{{ $time_slot->time_slot_status == 2 ? 'active' : '' }}">
                              <a href="{{ url('/admin-panel/time-slot-delete') }}/{{$time_slot->time_slot_id}}"> Delete</a>
                            </li>
                          </ul>
                        </div>
                      </td>     
                      <!--<td class="" style="padding-left:20px" > {{date('d F Y',strtotime($time_slot->admission_point_created))}} </td>-->
                    </tr>
                    <?php $number++; ?>
                    @endforeach
                  </tbody>
                </table>
              </div>
              {!! Form::close() !!}

            </div>


 
            
            <!--<div class="panel-body pn" style="border:1px red solid">-->
            <!--  <div class="table-responsive">-->
            <!--    <table class="table admin-form theme-warning tc-checkbox-1 fs13">                                -->
            <!--      <tbody>-->
            <!--        <tr class="">-->
            <!--          <th  class="text-left">-->
            <!--            <button type="button" class="btn btn-primary" onclick="go_delete()"><i class="glyphicon glyphicon-trash"></i> Delete Multiple </button>-->
            <!--          </th>    -->
            <!--          <th  class="text-right">-->
                       
            <!--          </th>-->
            <!--        </tr>-->
            <!--      </tbody>-->
            <!--    </table>-->
            <!--  </div>-->
            <!--</div>-->

          </div>
        </div>
      </div>

 <script type="text/javascript">
 </script>

@endsection
