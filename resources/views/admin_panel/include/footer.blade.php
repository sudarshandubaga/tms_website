  <footer id="content-footer" id="topbar" class="affix" >
    <div class="row">
      <div class="col-md-6">
        <span class="footer-legal">© 2017 AdminPanel</span>
      </div>
      
      <div class="col-md-6 text-right">
        <span class="footer-meta">Designed &amp; Developed by <b><a href='http://kempsolutions.in/'>Kemp Solutions</a></b> </span>
        <a href="#content" class="footer-return-top"> <span class="fa fa-arrow-up"></span> </a>
      </div>
    </div>
  </footer>
</section>
 </div>   

 <style type="text/css">
.dt-panelfooter{
  display: none !important;
}
</style>

<!-- jQuery -->
{!!Html::script('public/admin/js/jquery/jquery-1.11.1.min.js') !!}
{!!Html::script('public/admin/js/jquery/jquery_ui/jquery-ui.min.js') !!}
{!!Html::script('public/admin/js/jquery-te-1.4.0.min.js') !!}
{!!Html::script('public/admin/js/admin-forms/js/jquery-ui-datepicker.min.js') !!}
{!!Html::script('public/admin/js/plugins/select2/select2.min.js') !!}
{!!Html::script('public/admin/js/plugins/magnific/jquery.magnific-popup.js') !!}
<!-- FileUpload JS -->
{!!Html::script('public/admin/js/plugins/fileupload/fileupload.js') !!}
{!!Html::script('public/admin/js/plugins/holder/holder.min.js') !!}
  <!-- Datatables -->
  {!!Html::script('public/admin/js/plugins/datatables/extensions/TableTools/js/dataTables.tableTools.min.js') !!}
  {!!Html::script('public/admin/js/plugins/datatables/media/js/jquery.dataTables.js') !!}
  {!!Html::script('public/admin/js/plugins/datatables/extensions/ColReorder/js/dataTables.colReorder.min.js') !!}
  {!!Html::script('public/admin/js/plugins/datatables/media/js/dataTables.bootstrap.js') !!}

<!-- Tagmanager JS -->
{!!Html::script('public/admin/js/plugins/tagsinput/tagsinput.min.js') !!}
{!!Html::script('public/admin/js/bootstrap-formhelpers.min.js') !!}
<!-- jQuery Validate Plugin-->
{!!Html::script('public/admin/js/admin-forms/js/jquery.validate.min.js') !!}
{!!Html::script('public/admin/js/admin-forms/js/additional-methods.min.js') !!}
<!-- Theme Javascript -->
{!!Html::script('public/admin/js/utility/utility.js') !!}
{!!Html::script('public/admin/js/demo/demo.js') !!}
{!!Html::script('public/admin/js/main.js') !!}
{!!Html::script('public/admin/js/app.js') !!}
{!!Html::script('public/admin/js/fileinput.js') !!}
{!!HTML::script('public/admin/js/fastselect.standalone.js') !!}

<script src="https://maps.googleapis.com/maps/api/js?key=AIzaSyANfLNiS0eIsndUmgQh0ZnlhYoUpW2Nce8&libraries=places&callback=initAutocomplete" async defer></script>
<!-- AdminForms JS -->

<script>
$(".select2-single").select2();
// $("#switch-modal").bootstrapSwitch();
// $("#switch-modal2").bootstrapSwitch();
// $("#switch-modal3").bootstrapSwitch();

</script>

<script>

  function initAutocomplete() {
  autocomplete = new google.maps.places.Autocomplete(
      (document.getElementById('user_address')),
      {types: ['geocode'],componentRestrictions: {country: 'nz'} });

  autocomplete1 = new google.maps.places.Autocomplete(
      (document.getElementById('relative_address')),
      {types: ['geocode'],componentRestrictions: {country: 'nz'} });

  }

  // function initAutocomplete() {
  // autocomplete = new google.maps.places.Autocomplete(
  //     (document.getElementById('relative_address')),
  //     {types: ['geocode'],componentRestrictions: {country: 'nz'} });
  // }

  $('.jqte-test').jqte();
  var jqteStatus = true;
  $(".status").click(function()
  {
    jqteStatus = jqteStatus ? false : true;
    $('.jqte-test').jqte({"status" : jqteStatus})
  });

</script>

<script>
$(document).ready(function() {
  $('img').on('dragstart', function(event) { event.preventDefault(); });
});
</script>
<script type="text/javascript">

$(function(){
  $('.numeric').on('input', function (event) { 
      this.value = this.value.replace(/[^0-9]/g, '');
  });
});


$('.class_numeric').keypress(function(event) {
    var $this = $(this);
    if ((event.which != 46 || $this.val().indexOf('.') != -1) && ((event.which < 48 || event.which > 57) && (event.which != 0 && event.which != 8))) {
        event.preventDefault();
    }
    var text = $(this).val();
    if ((event.which == 46) && (text.indexOf('.') == -1)) {
        setTimeout(function() {
            if ($this.val().substring($this.val().indexOf('.')).length > 3) {
                $this.val($this.val().substring(0, $this.val().indexOf('.') + 3));
            }
        }, 1);
    }
    if ((text.indexOf('.') != -1) && (text.substring(text.indexOf('.')).length > 2) && (event.which != 0 && event.which != 8) && ($(this)[0].selectionStart >= text.length - 2)) {
        event.preventDefault();
    }
});
$('.class_numeric').bind("paste", function(e) {
    var text = e.originalEvent.clipboardData.getData('Text');
    if ($.isNumeric(text)) {
        if ((text.substring(text.indexOf('.')).length > 3) && (text.indexOf('.') > -1)) {
            e.preventDefault();
            $(this).val(text.substring(0, text.indexOf('.') + 3));
        }
    } else {
        e.preventDefault();
    }
});


  jQuery(document).ready(function() {
    /* @time picker
     ------------------------------------------------------------------ */
    // $('.inline-tp').timepicker({
    //                 format: 'LT'
    //             }
    // );

    // $('#timepicker1').timepicker({
    //   beforeShow: function(input, inst) {
    //     var newclass = 'admin-form';
    //     var themeClass = $(this).parents('.admin-form').attr('class');
    //     var smartpikr = inst.dpDiv.parent();
    //     if (!smartpikr.hasClass(themeClass)) {
    //       inst.dpDiv.wrap('<div class="' + themeClass + '"></div>');
    //     }
    //   }
    // });

    // $('#timepicker2').timepicker({
    //   showOn: 'both',
    //   buttonText: '<i class="fa fa-clock-o"></i>',
    //   beforeShow: function(input, inst) {
    //     var newclass = 'admin-form';
    //     var themeClass = $(this).parents('.admin-form').attr('class');
    //     var smartpikr = inst.dpDiv.parent();
    //     if (!smartpikr.hasClass(themeClass)) {
    //       inst.dpDiv.wrap('<div class="' + themeClass + '"></div>');
    //     }
    //   }
    // });

    // $('#timepicker3').timepicker({
    //   showOn: 'both',
    //   disabled: true,
    //   buttonText: '<i class="fa fa-clock-o"></i>',
    //   beforeShow: function(input, inst) {
    //     var newclass = 'admin-form';
    //     var themeClass = $(this).parents('.admin-form').attr('class');
    //     var smartpikr = inst.dpDiv.parent();
    //     if (!smartpikr.hasClass(themeClass)) {
    //       inst.dpDiv.wrap('<div class="' + themeClass + '"></div>');
    //     }
    //   }
    // });
    
    /* @date time picker
    ------------------------------------------------------------------ */
    // $('#datetimepicker1').datetimepicker({
    //   prevText: '<i class="fa fa-chevron-left"></i>',
    //   nextText: '<i class="fa fa-chevron-right"></i>',
    //   beforeShow: function(input, inst) {
    //     var newclass = 'admin-form';
    //     var themeClass = $(this).parents('.admin-form').attr('class');
    //     var smartpikr = inst.dpDiv.parent();
    //     if (!smartpikr.hasClass(themeClass)) {
    //       inst.dpDiv.wrap('<div class="' + themeClass + '"></div>');
    //     }
    //   }
    // });
    
    // $('#datetimepicker2').datetimepicker({
    //   showOn: 'both',
    //   buttonText: '<i class="fa fa-calendar-o"></i>',
    //   prevText: '<i class="fa fa-chevron-left"></i>',
    //   nextText: '<i class="fa fa-chevron-right"></i>',
    //   beforeShow: function(input, inst) {
    //     var newclass = 'admin-form';
    //     var themeClass = $(this).parents('.admin-form').attr('class');
    //     var smartpikr = inst.dpDiv.parent();
    //     if (!smartpikr.hasClass(themeClass)) {
    //       inst.dpDiv.wrap('<div class="' + themeClass + '"></div>');
    //     }
    //   }
    // }); 

    // $( "#datepicker" ).datepicker( {
    //   dateFormat: 'd MM yy',
    //   buttonText: '<i class="fa fa-calendar-o"></i>',
    //   prevText: '<i class="fa fa-chevron-left"></i>',
    //   nextText: '<i class="fa fa-chevron-right"></i>',
    // });
    // $( "#datepicker1" ).datepicker( {
    //   dateFormat: 'd MM yy',
    //   buttonText: '<i class="fa fa-calendar-o"></i>',
    //   prevText: '<i class="fa fa-chevron-left"></i>',
    //   nextText: '<i class="fa fa-chevron-right"></i>',
    // });

    // $('#datetimepicker3').datetimepicker({
    //   showOn: 'both',
    //   buttonText: '<i class="fa fa-calendar-o"></i>',
    //   disabled: true,
    //   prevText: '<i class="fa fa-chevron-left"></i>',
    //   nextText: '<i class="fa fa-chevron-right"></i>',
    //   beforeShow: function(input, inst) {
    //     var newclass = 'admin-form';
    //     var themeClass = $(this).parents('.admin-form').attr('class');
    //     var smartpikr = inst.dpDiv.parent();
    //     if (!smartpikr.hasClass(themeClass)) {
    //       inst.dpDiv.wrap('<div class="' + themeClass + '"></div>');
    //     }
    //   }
    // });

    // $('.inline-dtp').datetimepicker({
    //   prevText: '<i class="fa fa-chevron-left"></i>',
    //   nextText: '<i class="fa fa-chevron-right"></i>',
    // });

    /* @date picker
    ------------------------------------------------------------------ */
    // $("#datepicker1").datepicker({
    //   prevText: '<i class="fa fa-chevron-left"></i>',
    //   nextText: '<i class="fa fa-chevron-right"></i>',
    //   showButtonPanel: false,
    //   beforeShow: function(input, inst) {
    //     var newclass = 'admin-form';
    //     var themeClass = $(this).parents('.admin-form').attr('class');
    //     var smartpikr = inst.dpDiv.parent();
    //     if (!smartpikr.hasClass(themeClass)) {
    //       inst.dpDiv.wrap('<div class="' + themeClass + '"></div>');
    //     }
    //   }
    // });
    
    // $("#datepicker").datepicker({
    //   prevText: '<i class="fa fa-chevron-left"></i>',
    //   nextText: '<i class="fa fa-chevron-right"></i>',
    //   showButtonPanel: false,
    //   beforeShow: function(input, inst) {
    //     var newclass = 'admin-form';
    //     var themeClass = $(this).parents('.admin-form').attr('class');
    //     var smartpikr = inst.dpDiv.parent();
    //     if (!smartpikr.hasClass(themeClass)) {
    //       inst.dpDiv.wrap('<div class="' + themeClass + '"></div>');
    //     }
    //   }
    // });

    // $('#datepicker2').datepicker({
    //   numberOfMonths: 3,
    //   showOn: 'both',
    //   buttonText: '<i class="fa fa-calendar-o"></i>',
    //   prevText: '<i class="fa fa-chevron-left"></i>',
    //   nextText: '<i class="fa fa-chevron-right"></i>',
    //   beforeShow: function(input, inst) {
    //     var newclass = 'admin-form';
    //     var themeClass = $(this).parents('.admin-form').attr('class');
    //     var smartpikr = inst.dpDiv.parent();
    //     if (!smartpikr.hasClass(themeClass)) {
    //       inst.dpDiv.wrap('<div class="' + themeClass + '"></div>');
    //     }
    //   }
    // });

    // $('#datepicker3').datepicker({
    //   showOn: 'both',
    //   disabled: true,
    //   buttonText: '<i class="fa fa-calendar-o"></i>',
    //   prevText: '<i class="fa fa-chevron-left"></i>',
    //   nextText: '<i class="fa fa-chevron-right"></i>',
    //   beforeShow: function(input, inst) {
    //     var newclass = 'admin-form';
    //     var themeClass = $(this).parents('.admin-form').attr('class');
    //     var smartpikr = inst.dpDiv.parent();
    //     if (!smartpikr.hasClass(themeClass)) {
    //       inst.dpDiv.wrap('<div class="' + themeClass + '"></div>');
    //     }
    //   }
    // });

    $('.inline-dp').datepicker({
      numberOfMonths: 1,
      prevText: '<i class="fa fa-chevron-left"></i>',
      nextText: '<i class="fa fa-chevron-right"></i>',
      showButtonPanel: false
    });
  });

  // date from to

  $(function () {
    $("#date_from").datepicker({
        dateFormat: 'dd MM yy',
        prevText: '<i class="fa fa-chevron-left"></i>',
        nextText: '<i class="fa fa-chevron-right"></i>',  
        numberOfMonths: 1,
        onSelect: function (selected) {
            var dt = new Date(selected);
            dt.setDate(dt.getDate() + 1);
            $("#date_to").datepicker("option", "minDate", dt);
        }
    });
    $("#date_to").datepicker({
        dateFormat: 'dd MM yy',  
        prevText: '<i class="fa fa-chevron-left"></i>',
        nextText: '<i class="fa fa-chevron-right"></i>',
        numberOfMonths: 1,
        onSelect: function (selected) {
            var dt = new Date(selected);
            dt.setDate(dt.getDate() - 1);
            $("#date_from").datepicker("option", "maxDate", dt);
        }
    });
  });
  
 // Date picker for Add User page  
    $(function () {
        $(".hire_date").datepicker({
            dateFormat: 'dd MM yy',
            prevText: '<i class="fa fa-chevron-left"></i>',
            nextText: '<i class="fa fa-chevron-right"></i>',  
            numberOfMonths: 1,
            onSelect: function (selected) {
                var dt = new Date(selected);
                dt.setDate(dt.getDate() + 1);
                $(".expiry_date").datepicker("option", "minDate", dt);
                $('.hire_date').removeClass('state-error').addClass('state-success');
            }
        });
        $(".expiry_date").datepicker({
            dateFormat: 'dd MM yy',  
            prevText: '<i class="fa fa-chevron-left"></i>',
            nextText: '<i class="fa fa-chevron-right"></i>',
            numberOfMonths: 1,
            onSelect: function (selected) {
                var dt = new Date(selected);
                dt.setDate(dt.getDate() - 1);
                $(".hire_date").datepicker("option", "maxDate", dt);
                $('.expiry_date').removeClass('state-error').addClass('state-success');
            }
        });
    });
    
// Date picker for Add User Discount page  
    $(function () {
        $(".fromdate").datepicker({
            dateFormat: 'dd MM yy',
            minDate: 0,
            prevText: '<i class="fa fa-chevron-left"></i>',
            nextText: '<i class="fa fa-chevron-right"></i>',  
            numberOfMonths: 1,
            onSelect: function (selected) {
                var dt = new Date(selected);
                dt.setDate(dt.getDate() + 1);
                $(".todate").datepicker("option", "minDate", dt);
            }
        });
        $(".todate").datepicker({
            dateFormat: 'dd MM yy',  
            prevText: '<i class="fa fa-chevron-left"></i>',
            nextText: '<i class="fa fa-chevron-right"></i>',
            numberOfMonths: 1,
            onSelect: function (selected) {
                var dt = new Date(selected);
                dt.setDate(dt.getDate() - 1);
                $(".fromdate").datepicker("option", "maxDate", dt);
            }
        });
    });

    </script>

<script type="text/javascript">
jQuery(document).ready(function() {

"use strict";

// Init Theme Core    
Core.init();

// Init Demo JS    
Demo.init();        

// Init Boostrap Multiselects
$('#multiselect2').multiselect({
  includeSelectAllOption: true
});

// Init Boostrap Multiselects
$('#multiselect3').multiselect({
  includeSelectAllOption: true
}); 

// Init Boostrap Multiselects
$('#multiselect4').multiselect({
  includeSelectAllOption: true
}); 
 

// select dropdowns - placeholder like creation
    var selectList = $('.admin-form select');
    selectList.each(function(i, e) {
      $(e).on('change', function() {
        if ($(e).val() == "0") $(e).addClass("empty");
        else $(e).removeClass("empty")
      });
    });
    selectList.each(function(i, e) {
      $(e).change();
    });

    // Init tagsinput plugin
    $("input#tagsinput").tagsinput({
      tagClass: function(item) {
        return 'label label-default';
      }
    });

     // Init DataTables
    $('#datatable').dataTable({
      "sDom": 't<"dt-panelfooter clearfix"ip>',
      "displayLength": -1,
    });

});
</script>

<script type="text/javascript">
// To delete Selected Records
function go_delete() {
    if($(".check:checked").length) {
        if(confirm("Are you sure want to delete selected records?")) {
            $("[name=form]").submit();
        }
    } else {
        alert("Please select atleast one record to delete");
    }
}
$(function() {
    $("#check_all").on("click", function() {
        $(".check").prop("checked",$(this).prop("checked"));
    });

    $(".check").on("click", function() {
        var flag = ( $(".check:checked").length == $(".check").length ) ? true : false
        $("#check_all").prop("checked", flag);
    });
});

function selected() {
    if($(".check:checked").length) {
        $("[name=form]").submit();
    } else {
        alert("Please select atleast one record to download");
    }
}

function update_record() {
    var plan_description = $("#plan_description").val();
    if(plan_description != ""){
      if(confirm("Are you sure want to update records?")) {
        $("[name=form]").submit();    
      }
    }
}

</script>


<script type="text/javascript">
  function generate_code() {
    var text = "";
    var possible = "ABCDEFGHIJKLMNOPQRSTUVWXYZ0123456789";
    for(var i = 0; i < 6; i++) {
        text += possible.charAt(Math.floor(Math.random() * possible.length));
    }
    $("#discount_code").val(text);
  }
</script>




</body>
</html><!---index page ui stop -->
