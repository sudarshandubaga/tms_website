<!DOCTYPE html>
  <html>
    <head>
      <meta charset="utf-8">
        <title> {{ $title }} | AdminPanel</title>
        <meta http-equiv="Cache-Control" content="no-cache, no-store, must-revalidate" />
        <meta http-equiv="Pragma" content="no-cache" />
        <meta http-equiv="Expires" content="0" />
        <meta name="keywords" content="AdminPanel" />
        <meta name="description" content="AdminPanel">
        <meta name="author" content="AdminPanel">
        <meta name="viewport" content="width=device-width, initial-scale=1.0">
        <meta name="csrf-token" content="{{ csrf_token() }}">
  
        {!!Html::style('public/admin/css/css.css') !!}
        {!!Html::style('public/admin/css/bootstrap-formhelpers.min.css') !!}
        {!!Html::style('public/admin/js/plugins/datatables/media/css/dataTables.bootstrap.css') !!}
        {!!Html::style('public/admin/js/plugins/datatables/media/css/dataTables.plugins.css') !!}
        {!!Html::style('public/admin/js/plugins/magnific/magnific-popup.css') !!}
        {!!Html::style('public/admin/css/admin-forms.css') !!}
        {!!Html::style('public/admin/js/plugins/select2/css/core.css') !!}
        {!!Html::style('public/admin/css/theme.css') !!}
        {!!Html::style('public/admin/css/fonts/glyphicons-pro/glyphicons-pro.css') !!}
        {!!Html::style('public/admin/css/fonts/iconsweets/iconsweets.css') !!}
        {!!Html::style('public/admin/css/jquery-te-1.4.0.css') !!}
        {!!Html::style('public/admin/skin/default_skin/css/theme.css') !!}
        {!!Html::style('public/admin/js/plugins/dropzone/css/dropzone.css') !!}
          <!-- jQuery Validate Plugin-->
      {!!Html::script('public/admin/js/jquery/jquery-1.11.1.min.js') !!}
      {!!Html::script('public/admin/js/admin-forms/js/jquery.validate.min.js') !!}
      {!!Html::script('public/admin/js/admin-forms/js/additional-methods.min.js') !!}
        
        <script>
            var BASE_URL = "{{ url('/') }}";
        </script>
  
      <link rel="shortcut icon" href="{{{ asset('public/admin/img/1.png') }}}">
    </head>

<body class="">
    <div id="main" style="min-height:0">
      <header class="navbar navbar-fixed-top">
        <div class="navbar-branding">
          <a class="navbar-brand" href="{{ url('/admin-panel/dashboard') }}"><b>Admin</b>Panel</a>
          <span id="toggle_sidemenu_l" class="ad ad-lines"></span>
        </div>
        
        <ul class="nav navbar-nav navbar-left">  
          <li class="hidden-xs">
            <a class="request-fullscreen toggle-active" href="#">
              <span class="ad ad-screen-full fs18"></span>
            </a>
          </li>
        </ul>

        <ul class="nav navbar-nav navbar-right">

          <li class="dropdown">
            <a href="#" class="dropdown-toggle fw600 p15" data-toggle="dropdown" style="text-transform: capitalize;"> 
              @if($admin_details->admin_image != "")
                  {!! HTML::image($admin_details->admin_image,'avatar', array('class' => 'mw30 br64 mr15')) !!}
              @else
                  {!! HTML::image('public/admin/img/dummy.png','avatar', array('class' => 'mw30 br64 mr15')) !!}
              @endif
                   Mr. {{ $admin_details->admin_name }}   <span class="caret caret-tp hidden-xs"></span>
            </a>
            <ul class="dropdown-menu list-group dropdown-persist w250" role="menu">
              <li class="list-group-item">
                <a href="{{ url('/admin-panel/profile') }}" class="animated animated-short fadeInUp">
                  <span class="fa fa-user"></span> Profile
                </a>
              </li>
        
              <li class="list-group-item">
                <a href="{{ url('/admin-panel/account-settings') }}" class="animated animated-short fadeInUp">
                  <span class="fa fa-gear"></span> Account Settings </a>
              </li>
              
              <li class="list-group-item">
                <a href="{{ url('/admin-panel/change-password') }}" class="animated animated-short fadeInUp">
                  <span class="fa fa-key"></span> Change Password
                </a>
              </li>
              
              <li class="list-group-item">
                <a href="{{ url('/admin-panel/logout') }}" class="animated animated-short fadeInUp">
                  <span class="fa fa-power-off"></span> Logout </a>
              </li>
            </ul>
          </li>
        </ul>
      </header>
