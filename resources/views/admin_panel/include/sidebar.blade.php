<!-- menu selected -->
@php
  $path = Request::path();

  if($path == "admin-panel/dashboard"){ $dashboard = 'active'; } else { $dashboard = ''; };

  if($path == "admin-panel/account-settings" || $path == "admin-panel/account-settings"){ $account = 'active'; } else { $account = ''; };

  if($path == "admin-panel/change-password" || $path == "admin-panel/change-password"){ $password = 'active'; } else { $password = ''; };

  if($path == "admin-panel/admission-points"){ $admission_points = 'active'; } else { $admission_points = ''; };
  
  if($path == "admin-panel/time-slot"){ $time_slot = 'active'; } else { $time_slot = ''; };

  if($path == "admin-panel/important-points"){ $important_points = 'active'; } else { $important_points = ''; };

  if($path == "admin-panel/contact-enquiry"){ $enquiry = 'active'; $enquiry_menu = 'menu-open'; } else { $enquiry = ''; $enquiry_menu = ''; };

  if($path == "admin-panel/student-fees"){ $student_fees = 'active'; $student_fees_menu = 'menu-open'; } else { $student_fees = ''; $student_fees_menu = ''; };

  if($path == "admin-panel/add-our-school" || $path == "admin-panel/view-our-school"){ $our_school = 'active'; $our_school_menu = 'menu-open'; } else { $our_school = ''; $our_school_menu = ''; };

  if($path == "admin-panel/add-faq" || $path == "admin-panel/view-faq"){ $faq = 'active'; $faq_menu = 'menu-open'; } else { $faq = ''; $faq_menu = ''; };

  if($path == "admin-panel/add-we-teach" || $path == "admin-panel/view-we-teach"){ $we_teach = 'active'; $we_teach_menu = 'menu-open'; } else { $we_teach = ''; $we_teach_menu = ''; };

  if($path == "admin-panel/add-school-facilities" || $path == "admin-panel/view-school-facilities"){ $school_facilities = 'active'; $school_facilities_menu = 'menu-open'; } else { $school_facilities = ''; $school_facilities_menu = ''; };

  if($path == "admin-panel/add-class" || $path == "admin-panel/view-class"){ $class = 'active'; $class_menu = 'menu-open'; } else { $class = ''; $class_menu = ''; };

  if($path == "admin-panel/add-download" || $path == "admin-panel/view-download"){ $download = 'active'; $download_menu = 'menu-open'; } else { $download = ''; $download_menu = ''; };

  if($path == "admin-panel/add-event" || $path == "admin-panel/view-event"){ $event = 'active'; $event_menu = 'menu-open'; } else { $event = ''; $event_menu = ''; };

    if($path == "admin-panel/add-slider" || $path == "admin-panel/view-slider"){ $slider = 'active'; $slider_menu = 'menu-open'; } else { $slider = ''; $slider_menu = ''; };

  if($path == "admin-panel/add-school-gallery" || $path == "admin-panel/view-school-gallery"){ $school_gallery = 'active'; $school_gallery_menu = 'menu-open'; } else { $school_gallery = ''; $school_gallery_menu = ''; };
  if($path == "admin-panel/add-school-video-gallery" || $path == "admin-panel/view-school-video-gallery"){ $school_video_gallery = 'active'; $school_video_gallery_menu = 'menu-open'; } else { $school_video_gallery = ''; $school_video_gallery_menu = ''; };


  if($path == "admin-panel/master-pages/about-us" || $path == "admin-panel/master-pages/disclaimer-policy" || $path == "admin-panel/master-pages/privacy-policy" || $path == "admin-panel/master-pages/cancellation-refund-policy" || $path == "admin-panel/master-pages/shipping-delivery-policy" || $path == "admin-panel/master-pages/terms-conditions" || $path == "admin-panel/master-pages/principals-message" || $path == "admin-panel/master-pages/infrastructure" || $path == "admin-panel/master-pages/modules-activities" || $path == "admin-panel/master-pages/rules-regulations" || $path == "admin-panel/master-pages/school-uniform" || $path == "admin-panel/master-pages/information-technology" || $path == "admin-panel/master-pages/conveyance" || $path == "admin-panel/master-pages/school-captains" || $path == "admin-panel/master-pages/stars-of-yesteryears" || $path == "admin-panel/master-pages/school-hours-security"  ){ $masterpages = 'active'; $master_menu = 'menu-open'; } else { $masterpages = ''; $master_menu = ''; };



@endphp

<!-- sub menu selected -->

@php
  $path = Request::path();

  if($path == "admin-panel/contact-enquiry"){ $contact_enquiry = 'active'; } else { $contact_enquiry = ''; };

    if($path == "admin-panel/student-fees"){ $student_fees = 'active'; } else { $student_fees = ''; };

  if($path == "admin-panel/add-our-school"){ $sub_our_school1 = 'active'; } else { $sub_our_school1 = ''; };

  if($path == "admin-panel/view-our-school"){ $sub_our_school2 = 'active'; } else { $sub_our_school2 = ''; };

  if($path == "admin-panel/add-faq"){ $sub_faq1 = 'active'; } else { $sub_faq1 = ''; };

  if($path == "admin-panel/view-faq"){ $sub_faq2 = 'active'; } else { $sub_faq2 = ''; };

  if($path == "admin-panel/add-we-teach"){ $sub_we_teach1 = 'active'; } else { $sub_we_teach1 = ''; };

  if($path == "admin-panel/view-we-teach"){ $sub_we_teach2 = 'active'; } else { $sub_we_teach2 = ''; };

  if($path == "admin-panel/add-school-facilities"){ $sub_school_facilities1 = 'active'; } else { $sub_school_facilities1 = ''; };

  if($path == "admin-panel/view-school-facilities"){ $sub_school_facilities2 = 'active'; } else { $sub_school_facilities2 = ''; };

  if($path == "admin-panel/add-class"){ $sub_class1 = 'active'; } else { $sub_class1 = ''; };

  if($path == "admin-panel/view-class"){ $sub_class2 = 'active'; } else { $sub_class2 = ''; };

  if($path == "admin-panel/add-download"){ $sub_download1 = 'active'; } else { $sub_download1 = ''; };

  if($path == "admin-panel/view-download"){ $sub_download2 = 'active'; } else { $sub_download2 = ''; };

  if($path == "admin-panel/add-event"){ $sub_event1 = 'active'; } else { $sub_event1 = ''; };

  if($path == "admin-panel/view-event"){ $sub_event2 = 'active'; } else { $sub_event2 = ''; };


  if($path == "admin-panel/add-slider"){ $sub_slider1 = 'active'; } else { $sub_slider1 = ''; };

  if($path == "admin-panel/view-slider"){ $sub_slider2 = 'active'; } else { $sub_slider2 = ''; };

  if($path == "admin-panel/add-school-gallery"){ $sub_gallery_school1 = 'active'; } else { $sub_gallery_school1 = ''; };
  if($path == "admin-panel/add-school-video-gallery"){ $sub_video_gallery_school1 = 'active'; } else { $sub_video_gallery_school1 = ''; };

  if($path == "admin-panel/view-school-gallery"){ $sub_gallery_school2 = 'active'; } else { $sub_gallery_school2 = ''; };
  if($path == "admin-panel/view-school-video-gallery"){ $sub_video_gallery_school2 = 'active'; } else { $sub_video_gallery_school2 = ''; };

  

@endphp


<aside id="sidebar_left" class="nano nano-primary affix">
    <div class="sidebar-left-content nano-content">
        <ul class="nav sidebar-menu">
          <li class="sidebar-label pt20"></li>
          
          <li class="{{ $dashboard }}" >
            <a href="{{ url('/admin-panel/dashboard') }}">
              <span class="glyphicons glyphicons-dashboard"></span>
              <span class="sidebar-title">Dashboard</span>
            </a>
          </li>

          <!--<li class="sidebar-label pt15"> Education Management</li>-->

          <!--<li class="{{ $admission_points }}" >-->
          <!--  <a href="{{ url('/admin-panel/admission-points') }}">-->
          <!--    <span class="glyphicon glyphicons-snowflake"></span>-->
          <!--    <span class="sidebar-title">Admission Points</span>-->
          <!--  </a>-->
          <!--</li>-->

          <!--<li class="{{ $important_points }}" >-->
          <!--  <a href="{{ url('/admin-panel/important-points') }}">-->
          <!--    <span class="glyphicon glyphicons-snowflake"></span>-->
          <!--    <span class="sidebar-title">Important Points</span>-->
          <!--  </a>-->
          <!--</li>-->

         


          <li class="sidebar-label pt15"> Enquiry Management</li>

         

          <li class="{{ $enquiry }}">
            <a class="accordion-toggle {{ $enquiry_menu }}"  href="#">
              <span class="fa fa-mail-reply"></span>
              <span class="sidebar-title">Enquiry</span>
              <span class="caret"></span>
            </a>
            <ul class="nav sub-nav">
              <li class="{{ $contact_enquiry }}">
                <a href="{{ url('/admin-panel/contact-enquiry') }}"> Contact Enquiry </a>
              </li>
              <li class="{{ $contact_enquiry }}">
                <a href="{{ url('/admin-panel/register-enquiry') }}"> Registration Enquiry </a>
              </li>
            </ul>
          </li>
    

          

          <li class="sidebar-label pt15"> Master Modules</li>

          <li class="{{ $slider }}" >
            <a class="accordion-toggle {{ $slider_menu }}"  href="#">
              <span class="glyphicon glyphicons-snowflake"></span>
              <span class="sidebar-title">Slider</span>
              <span class="caret"></span>
            </a>
            <ul class="nav sub-nav">
              <li class="{{ $sub_slider1 }}">
                <a href="{{ url('/admin-panel/add-slider') }}"> Add Slider </a>
              </li>
              <li class="{{ $sub_slider2 }}">
                <a href="{{ url('/admin-panel/view-slider') }}"> View Slider </a>
              </li>
            </ul>
          </li>


          <li class="{{ @$news }}" >
            <a class="accordion-toggle {{ @$news_menu }}"  href="#">
              <span class="glyphicon glyphicons-snowflake"></span>
              <span class="sidebar-title">News</span>
              <span class="caret"></span>
            </a>
            <ul class="nav sub-nav">
              <li class="{{ $sub_news1 }}">
                <a href="{{ url('/admin-panel/add-news') }}"> Add News </a>
              </li>
              <li class="{{ $sub_news2 }}">
                <a href="{{ url('/admin-panel/view-news') }}"> View News </a>
              </li>
            </ul>
          </li>

          <li class="{{ @$blog }}" >
            <a class="accordion-toggle {{ @$blog_menu }}"  href="#">
              <span class="glyphicon glyphicons-snowflake"></span>
              <span class="sidebar-title">Blog</span>
              <span class="caret"></span>
            </a>
            <ul class="nav sub-nav">
              <li class="{{ $sub_blog1 }}">
                <a href="{{ url('/admin-panel/add-blog') }}"> Add Blog </a>
              </li>
              <li class="{{ $sub_blog2 }}">
                <a href="{{ url('/admin-panel/view-blog') }}"> View Blog </a>
              </li>
            </ul>
          </li>
          
          
           <li class="{{ $school_gallery }}" >
            <a class="accordion-toggle {{ $school_gallery_menu }}"  href="#">
              <span class="glyphicon glyphicons-snowflake"></span>
              <span class="sidebar-title">Photo Gallery</span>
              <span class="caret"></span>
            </a>
            <ul class="nav sub-nav">
              <li class="{{ $sub_gallery_school1 }}">
                <a href="{{ url('/admin-panel/add-school-gallery') }}">Add Gallery </a>
              </li>
              <li class="{{ $sub_gallery_school2 }}">
                <a href="{{ url('/admin-panel/view-school-gallery') }}">View Gallery </a>
              </li>
            </ul>
          </li>
          
          
           <li class="{{ $school_video_gallery }}" >
            <a class="accordion-toggle {{ $school_video_gallery_menu }}"  href="#">
              <span class="glyphicon glyphicons-snowflake"></span>
              <span class="sidebar-title">Video Gallery</span>
              <span class="caret"></span>
            </a>
            <ul class="nav sub-nav">
              <li class="{{ $sub_video_gallery_school1 }}">
                <a href="{{ url('/admin-panel/add-school-video-gallery') }}">Add Gallery </a>
              </li>
              <li class="{{ $sub_video_gallery_school2 }}">
                <a href="{{ url('/admin-panel/view-school-video-gallery') }}">View Gallery </a>
              </li>
            </ul>
          </li>
          
          

          <!--<li class="{{ $our_school }}" >-->
          <!--  <a class="accordion-toggle {{ $our_school_menu }}"  href="#">-->
          <!--    <span class="glyphicon glyphicons-snowflake"></span>-->
          <!--    <span class="sidebar-title">Our School</span>-->
          <!--    <span class="caret"></span>-->
          <!--  </a>-->
          <!--  <ul class="nav sub-nav">-->
          <!--    <li class="{{ $sub_our_school1 }}">-->
          <!--      <a href="{{ url('/admin-panel/add-our-school') }}"> Add Our School </a>-->
          <!--    </li>-->
          <!--    <li class="{{ $sub_our_school2 }}">-->
          <!--      <a href="{{ url('/admin-panel/view-our-school') }}"> View Our School </a>-->
          <!--    </li>-->
          <!--  </ul>-->
          <!--</li>-->
          
          <li class="{{ $testimonials }}" >
            <a class="accordion-toggle {{ $testimonials_menu }}"  href="#">
              <span class="glyphicon glyphicons-snowflake"></span>
              <span class="sidebar-title">Testimonials</span>
              <span class="caret"></span>
            </a>
            <ul class="nav sub-nav">
              <li class="{{ $sub_our_school1 }}">
                <a href="{{ url('/admin-panel/add-testimonials') }}"> Add Testimonials </a>
              </li>
              <li class="{{ $sub_our_school2 }}">
                <a href="{{ url('/admin-panel/view-testimonials') }}"> View Testimonials </a>
              </li>
            </ul>
          </li>
          
          
            <li class="{{ $tc }}" >
                <a class="accordion-toggle {{ $tc_menu }}"  href="#">
                  <span class="glyphicon glyphicons-snowflake"></span>
                  <span class="sidebar-title">TC</span>
                  <span class="caret"></span>
                </a>
                <ul class="nav sub-nav">
                  <li class="{{ $sub_tc1 }}">
                    <a href="{{ url('/admin-panel/tc_add') }}"> Add TC </a>
                  </li>
                  <li class="{{ $sub_tc2 }}">
                    <a href="{{ url('/admin-panel/tc_view') }}"> View TC </a>
                  </li>
                </ul>
            </li>
          
          

          <li class="{{ $faq }}">
          <!--  <a class="accordion-toggle {{ $faq_menu }}"  href="#">-->
          <!--    <span class="fa fa-question"></span>-->
          <!--    <span class="sidebar-title">FAQ</span>-->
          <!--    <span class="caret"></span>-->
          <!--  </a>-->
          <!--  <ul class="nav sub-nav">-->
          <!--    <li class="{{ $sub_faq1 }}">-->
          <!--      <a href="{{ url('/admin-panel/add-faq') }}"> Add FAQ  </a>-->
          <!--    </li>-->
          <!--    <li class="{{ $sub_faq2 }}">-->
          <!--      <a href="{{ url('/admin-panel/view-faq') }}"> View FAQ </a>-->
          <!--    </li>-->
          <!--  </ul>-->
          <!--</li>-->

          <!--<li class="{{ $school_facilities }}" >-->
          <!--  <a class="accordion-toggle {{ $school_facilities_menu }}"  href="#">-->
          <!--    <span class="glyphicon glyphicons-snowflake"></span>-->
          <!--    <span class="sidebar-title">School Facilities</span>-->
          <!--    <span class="caret"></span>-->
          <!--  </a>-->
          <!--  <ul class="nav sub-nav">-->
          <!--    <li class="{{ $sub_school_facilities1 }}">-->
          <!--      <a href="{{ url('/admin-panel/add-school-facilities') }}"> Add School Facilities </a>-->
          <!--    </li>-->
          <!--    <li class="{{ $sub_school_facilities2 }}">-->
          <!--      <a href="{{ url('/admin-panel/view-school-facilities') }}"> View School Facilities </a>-->
          <!--    </li>-->
          <!--  </ul>-->
          <!--</li>-->

          <!--<li class="{{ $we_teach }}" >-->
          <!--  <a class="accordion-toggle {{ $we_teach_menu }}"  href="#">-->
          <!--    <span class="glyphicon glyphicons-snowflake"></span>-->
          <!--    <span class="sidebar-title">We Teach</span>-->
          <!--    <span class="caret"></span>-->
          <!--  </a>-->
          <!--  <ul class="nav sub-nav">-->
          <!--    <li class="{{ $sub_we_teach1 }}">-->
          <!--      <a href="{{ url('/admin-panel/add-we-teach') }}"> Add We Teach </a>-->
          <!--    </li>-->
          <!--    <li class="{{ $sub_we_teach2 }}">-->
          <!--      <a href="{{ url('/admin-panel/view-we-teach') }}"> View We Teach </a>-->
          <!--    </li>-->
          <!--  </ul>-->
          <!--</li>-->

          <!--<li class="{{ $class }}" >-->
          <!--  <a class="accordion-toggle {{ $class_menu }}"  href="#">-->
          <!--    <span class="glyphicon glyphicons-snowflake"></span>-->
          <!--    <span class="sidebar-title">Classes</span>-->
          <!--    <span class="caret"></span>-->
          <!--  </a>-->
          <!--  <ul class="nav sub-nav">-->
          <!--    <li class="{{ $sub_class1 }}">-->
          <!--      <a href="{{ url('/admin-panel/add-class') }}"> Add Class </a>-->
          <!--    </li>-->
          <!--    <li class="{{ $sub_class2 }}">-->
          <!--      <a href="{{ url('/admin-panel/view-class') }}"> View Class </a>-->
          <!--    </li>-->
          <!--  </ul>-->
          <!--</li>-->

          <li class="{{ $download }}" >
            <a class="accordion-toggle {{ $download_menu }}"  href="#">
              <span class="glyphicon glyphicons-snowflake"></span>
              <span class="sidebar-title">Download</span>
              <span class="caret"></span>
            </a>
            <ul class="nav sub-nav">
              <li class="{{ $sub_download1 }}">
                <a href="{{ url('/admin-panel/add-download') }}"> Add Download File </a>
              </li>
              <li class="{{ $sub_download2 }}">
                <a href="{{ url('/admin-panel/view-download') }}"> View Download File</a>
              </li>
            </ul>
          </li>

          <li class="{{ $event }}" >
            <a class="accordion-toggle {{ $event_menu }}"  href="#">
              <span class="glyphicon glyphicons-snowflake"></span>
              <span class="sidebar-title">Events</span>
              <span class="caret"></span>
            </a>
            <ul class="nav sub-nav">
              <li class="{{ $sub_event1 }}">
                <a href="{{ url('/admin-panel/add-event') }}"> Add Events </a>
              </li>
              <li class="{{ $sub_event2 }}">
                <a href="{{ url('/admin-panel/view-event') }}"> View Events </a>
              </li>
            </ul>
          </li>

          <li class="{{ $masterpages }}">
            <a class="accordion-toggle {{ $master_menu }}"  href="#">
              <span class="glyphicon glyphicon-duplicate"></span>
              <span class="sidebar-title">Master Pages</span>
              <span class="caret"></span>
            </a>
            <ul class="nav sub-nav">
              
              @php          
                $counter = 1;
                $page = \App\Model\Pages\Pages::orderBy('master_pages_slug','asc')->get();
                foreach($page as $pages){
              @endphp             

                <li class="text-capitalize {{ $path == "admin-panel/master-pages/$pages->master_pages_slug" ? 'active' : '' }} " >
                  <a href="{{ url('/admin-panel/master-pages') }}/{{ $pages->master_pages_slug }}"> {{ $pages->master_pages_title }}  </a>
                </li>
              
              @php  }  @endphp
            </ul>
          </li>
          
          <li class="sidebar-label pt20">Systems</li> 
          
          <li class="{{ $account }}">
            <a href="{{ url('/admin-panel/account-settings') }}">
              <span class="fa fa-cogs"></span>
              <span class="sidebar-title">Account Settings</span>
            </a>
          </li>
          
          <li class="{{ $password }}">
            <a href="{{ url('/admin-panel/change-password') }}">
              <span class="glyphicons glyphicons-keys"></span>
              <span class="sidebar-title">Change Password</span>
            </a>
          </li>
          
        </ul>
        
        <div class="sidebar-toggle-mini">
          <a href="#">
            <span class="fa fa-sign-out"></span>
          </a>
        </div>
        
    </div>
</aside>
