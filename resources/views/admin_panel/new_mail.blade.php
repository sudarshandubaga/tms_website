<!DOCTYPE html>
<html>
<head>
	<title>Neo Dales School</title>
</head>
<body>
	<table style="width: 100%; border:2px #000 solid">
		<div style="padding:10px;">
			<div><h3>Applying for “Neo  Play School”</h3> </div>
			<div><h3> Registration No___________</h3> </div>
		</div>
	    <tr><th style="padding:10px;     color: #981f6e !important;" colspan="2">    </th></tr>
		<tr><th style="padding:10px; border:2px #fff solid; background-color: #ccc" colspan="2">Student Details</th></tr>
        <tr>
			<th style="padding:10px;"  align="left">Child Photo:</th> 
			<td style="padding:10px;" > <img  src="{{$child_photo}}" height="150px;" width="150px;"/> </td>
		</tr>
		<tr>
			<th style="padding:10px;" align="left">Form ID</th>
			<td style="padding:10px;">{{ $form_id }}</td>
		</tr>
		<tr>
			<th style="padding:10px;"  align="left">Name of Student:</th>
			<td style="padding:10px;" >{{ $admission_name }} </td>
		</tr>
		<tr>
			<th style="padding:10px;"  align="left">Student Date of Birth:</th>
			<td style="padding:10px;" >{{ $admission_dob }}</td>
		</tr>
		<tr>
			<th style="padding:10px;"  align="left">Student Gender</th>
			<td style="padding:10px;" >{{ $admission_gender }}</td>
		</tr>
		<tr>
			<th style="padding:10px;"  align="left">Class Name:</th>
			<td style="padding:10px; color:red; font-weight:bold" >{{ $admission_class_name }}</td>
		</tr>
		<tr>
			<th style="padding:10px;"  align="left">Nationality:</th>
			<td style="padding:10px;" >{{ $nationality }}</td>
		</tr>
		<tr>
			<th style="padding:10px;"  align="left">Blood Group:</th>
			<td style="padding:10px;" >{{ $blood_group }}</td>
		</tr>
		<tr>
			<th style="padding:10px;"  align="left">Religion:</th>
			<td style="padding:10px;" >{{ $admission_religion }}</td>
		</tr>
		<tr>
			<th style="padding:10px;"  align="left">Category:</th>
			<td style="padding:10px;" >{{ $category }}</td>
		</tr>
		<tr>
			<th style="padding:10px;"  align="left">Aadhaar:</th>
			<td style="padding:10px;" >{{ $aadhaar }}</td>
		</tr>
		<tr>
			<th style="padding:10px;"  align="left">Address:</th>
			<td style="padding:10px;" >{{ $admission_address }}</td>
		</tr>
		<tr>
			<th style="padding:10px;"  align="left">State:</th>
			<td style="padding:10px;" >{{ $admission_state }}</td>
		</tr>
		<tr>
			<th style="padding:10px;"  align="left">City:</th>
			<td style="padding:10px;" >{{ $admission_city }}</td>
		</tr>
		<tr>
			<th style="padding:10px;"  align="left">Pin Code:</th>
			<td style="padding:10px;" >{{ $admission_pincode }}</td>
		</tr>

		<tr><th style="padding:10px; border:2px #fff solid; background-color: #ccc" colspan="2">Student's Father Details</th></tr>
		<tr>
			<th style="padding:10px;" align="left">Name</th>
			<td style="padding:10px;">{{ $admission_father_name }}</td>
		</tr>
		<tr>
			<th style="padding:10px;"  align="left">Age:</th>
			<td style="padding:10px;" >{{ $admission_father_age }}</td>
		</tr>
		<tr>
			<th style="padding:10px;"  align="left">Qualification:</th>
			<td style="padding:10px;" >{{ $admission_father_qualification }}</td>
		</tr>
		<tr>
			<th style="padding:10px;"  align="left">Occupation</th>
			<td style="padding:10px;" >{{ $father_occupation }}</td>
		</tr>
		
		
		@if($father_occupation == "Profession")
		<tr>
			<th style="padding:10px;"  align="left">Profession:</th>
			<td style="padding:10px;" >{{ $father_profession }}</td>
		</tr>
		@endif
			
		@if($father_profession == "Any Other")
		<tr>
			<th style="padding:10px;"  align="left">Other Profession :</th>
			<td style="padding:10px;" >{{ $father_any_profession }}</td>
		</tr>
		@endif
		
		@if($father_sector!="")
		<tr>
			<th style="padding:10px;"  align="left">Sector:</th>
			<td style="padding:10px;" >{{ $father_sector }}</td>
		</tr>
		@endif
		<tr>
			<th style="padding:10px;"  align="left">Establishment:</th>
			<td style="padding:10px;" >{{ $father_establishment }}</td>
		</tr>
		<tr>
			<th style="padding:10px;"  align="left">Designation:</th>
			<td style="padding:10px;" >{{ $father_designation }}</td>
		</tr>
		<tr>
			<th style="padding:10px;"  align="left">Office Address:</th>
			<td style="padding:10px;" >{{ $father_office_address }}</td>
		</tr>
		<tr>
			<th style="padding:10px;"  align="left">State:</th>
			<td style="padding:10px;" >{{ $father_state }}</td>
		</tr>
		<tr>
			<th style="padding:10px;"  align="left">City:</th>
			<td style="padding:10px;" >{{ $father_city }}</td>
		</tr>
		<tr>
			<th style="padding:10px;"  align="left">Pin Code:</th>
			<td style="padding:10px;" >{{ $father_pincode }}</td>
		</tr>
	
		<tr>
			<th style="padding:10px;"  align="left">Email:</th>
			<td style="padding:10px;" >{{ $admission_father_email }}</td>
		</tr>
		<tr>
			<th style="padding:10px;"  align="left">Mobile 1:</th>
			<td style="padding:10px;" >{{ $admission_father_cell_no1 }}</td>
		</tr>
		<tr>
			<th style="padding:10px;"  align="left">Mobile 2:</th>
			<td style="padding:10px;" >{{ $admission_father_cell_no2 }}</td>
		</tr>


		<tr><th style="padding:10px; border:2px #fff solid; background-color: #ccc" colspan="2">Student's Mother Details</th></tr>
		<tr>
			<th style="padding:10px;" align="left">Name</th>
			<td style="padding:10px;">{{ $admission_mother_name }}</td>
		</tr>
		<tr>
			<th style="padding:10px;"  align="left">Age:</th>
			<td style="padding:10px;" >{{ $admission_mother_age }}</td>
		</tr>
		<tr>
			<th style="padding:10px;"  align="left">Qualification:</th>
			<td style="padding:10px;" >{{ $admission_mother_qualification }}</td>
		</tr>
		<tr>
			<th style="padding:10px;"  align="left">Occupation</th>
			<td style="padding:10px;" >{{ $mother_occupation }}</td>
		</tr>
		
		
		
		@if($mother_occupation != "Housewife")
		
        		@if($mother_occupation == "Profession")
        		<tr>
        			<th style="padding:10px;"  align="left">Profession:</th>
        			<td style="padding:10px;" >{{ $mother_profession }}</td>
        		</tr>
        		@endif
        		
        		@if($mother_profession == "Any Other")
        		<tr>
        			<th style="padding:10px;"  align="left">Other Profession :</th>
        			<td style="padding:10px;" >{{ $mother_any_profession }}</td>
        		</tr>
        		@endif
        		
        		<tr>
        			<th style="padding:10px;"  align="left">Sector:</th>
        			<td style="padding:10px;" >{{ $mother_sector }}</td>
        		</tr>
        		<tr>
                	<th style="padding:10px;"  align="left">Establishment:</th>
                	<td style="padding:10px;" >{{ $mother_establishment }}</td>
                </tr>
        		<tr>
        			<th style="padding:10px;"  align="left">Designation:</th>
        			<td style="padding:10px;" >{{ $mother_designation }}</td>
        		</tr>
        		<tr>
        			<th style="padding:10px;"  align="left">Office Address:</th>
        			<td style="padding:10px;" >{{ $mother_office_address }}</td>
        		</tr>
        		<tr>
        			<th style="padding:10px;"  align="left">State:</th>
        			<td style="padding:10px;" >{{ $mother_state }}</td>
        		</tr>
        		<tr>
        			<th style="padding:10px;"  align="left">City:</th>
        			<td style="padding:10px;" >{{ $mother_city }}</td>
        		</tr>
        		<tr>
        			<th style="padding:10px;"  align="left">Pin Code:</th>
        			<td style="padding:10px;" >{{ $mother_pincode }}</td>
        		</tr>
        		
	
	@endif
		<tr>
			<th style="padding:10px;"  align="left">Email:</th>
			<td style="padding:10px;" >{{ $admission_mother_email }}</td>
		</tr>
		<tr>
			<th style="padding:10px;"  align="left">Mobile 1:</th>
			<td style="padding:10px;" >{{ $admission_mother_cell_no1 }}</td>
		</tr>
		<tr>
			<th style="padding:10px;"  align="left">Mobile 2:</th>
			<td style="padding:10px;" >{{ $admission_mother_cell_no2 }}</td>
		</tr>
@if($admission_guardian_name != "")
		<tr><th style="padding:10px; border:2px #fff solid; background-color: #ccc" colspan="2">Student's Guardian Details</th></tr>
		<tr>
			<th style="padding:10px;" align="left">Name</th>
			<td style="padding:10px;">{{ $admission_guardian_name }}</td>
		</tr>
		<tr>
			<th style="padding:10px;"  align="left">Age:</th>
			<td style="padding:10px;" >{{ $admission_guardian_age }}</td>
		</tr>
		<tr>
			<th style="padding:10px;"  align="left">Occupation:</th>
			<td style="padding:10px;" >{{ $admission_guardian_occupation }}</td>
		</tr>
		<tr>
			<th style="padding:10px;"  align="left">Address</th>
			<td style="padding:10px;" >{{ $admission_guardian_address }}</td>
		</tr>
		<tr>
			<th style="padding:10px;"  align="left">Mobile:</th>
			<td style="padding:10px;" >{{ $admission_guardian_cell_no }}</td>
		</tr>
			<tr>
			<th style="padding:10px;"  align="left">Relationship with the child:</th>
			<td style="padding:10px;" >{{ $relationship }}</td>
		</tr>
		<tr>
			<th style="padding:10px;"  align="left">State:</th>
			<td style="padding:10px;" >{{ $guardian_state }}</td>
		</tr>
		<tr>
			<th style="padding:10px;"  align="left">City:</th>
			<td style="padding:10px;" >{{ $guardian_city }}</td>
		</tr>
		<tr>
			<th style="padding:10px;"  align="left">Pin Code:</th>
			<td style="padding:10px;" >{{ $guardian_pincode }}</td>
		</tr>
	@endif
		<tr>
			<th style="padding:10px;"  align="left">Student Last School Name:</th>
			<td style="padding:10px;" >{{ $admission_last_school_name }}</td>
		</tr>
		
		<tr>
			<th style="padding:10px;"  align="left">Any Siblings:</th>
			<td style="padding:10px;" >{{ $sibling }}</td>
		</tr>
		
		@if($sibling == "Yes")
		<tr>
			<th style="padding:10px;"  align="left">Siblings Details:</th>
			<td style="padding:10px;" >{{ $sibling_details }}</td>
		</tr>
		@endif
		
		
		<tr>
			<th style="padding:10px;"  align="left">Any other instructions:</th>
			<td style="padding:10px;" >{{ $other_point }}</td>
		</tr>
		
        
		<tr>
			<th style="padding:10px;"  align="left">Aadhaar:</th> 
			@if($child_aadhar!="")
			<td style="padding:10px;" > <a href="{{$child_aadhar}}">Click here for Photo</a></td>
			@else
			<td style="padding:10px;" > -</td>
			@endif
		</tr>
		
	
		
		
		<tr>
			<th style="padding:10px;"  align="left">Birth Certificate  :</th> 
			@if($bc_type=="Yes")
			<td style="padding:10px;" > <a href="{{$bc}}">Click here for Birth Certificate</a></td>
			@else
			<td style="padding:10px;" > {{$bc}}</td>
			@endif
		</tr>
		
		<tr>
			<th style="padding:10px;"  align="left">Transfer Certificate:</th> 
			@if($tc_type=="Yes")
			<td style="padding:10px;" > <a href="{{$tc}}">Click here for Transfer Certificate</a></td>
			@else
			<td style="padding:10px;" > {{$tc}}</td>
			@endif
		</tr>
		
		@if($admission_class_name == "Pre Nursery")
		<tr>
			<th style="padding:10px;"  align="left">Meeting Type <br/>(with the Principal) :</th> 
			<td style="padding:10px;" >{{$meeting_type}} , {{$time_slot_by_user}}</td>
		</tr>
		@endif
		
		@if($admission_class_name == "Nursery")
		<tr>
			<th style="padding:10px;"  align="left">Meeting Type <br/>(with the Principal) :</th> 
			<td style="padding:10px;" >{{$meeting_type}} , {{$time_slot_by_user}}</td>
		</tr>
		@endif
		
		
		@if($admission_class_name == "LKG")
		<tr>
			<th style="padding:10px;"  align="left">Preferable mode of Entrance Test  :</th> 
			<td style="padding:10px;" >{{$entrance_test_type}}</td>
		</tr>
		@endif
		
		@if($admission_class_name == "UKG")
		<tr>
			<th style="padding:10px;"  align="left">Preferable mode of Entrance Test  :</th> 
			<td style="padding:10px;" >{{$entrance_test_type}}</td>
		</tr>
		@endif
		
	
		
		@if($category_certificate!="")
		<tr>
			<th style="padding:10px;"  align="left">Category Certificate:</th>
			<td style="padding:10px;" > <a href="{{$category_certificate}}">Click here for Details</a></td>
		</tr>
		@endif
		
		
		<tr>
			<th style="padding:10px;"  align="left">I am filling up this registration form for:</th>
			<td style="padding:10px;" > {{$registration_form_for}}</td>
		</tr>
		
	<tr><th style="padding:10px; border:2px #fff solid; background-color: #ccc" colspan="2">Payment Details</th></tr>
	    <tr>
			<th style="padding:10px;"  align="left">Transaction/Order ID:</th>
			<td style="padding:10px;" > {{$transaction_id}}</td>
		</tr>
		<tr>
			<th style="padding:10px;"  align="left">Transaction Status:</th>
			<td style="padding:10px;" > {{$trans_status}}</td>
		</tr>
		
		@if($trans_status=="success")
		<tr>
			<th style="padding:10px;"  align="left">Amount:</th>
			<td style="padding:10px;" ><span>Rs.</span> <span> {{$final_amount}}</span></td>
		</tr>
		@else
		<tr>
			<th style="padding:10px;"  align="left">Amount:</th>
			<td style="padding:10px;" >Rs. 0</td>
		</tr>
			
		@endif
		
		


		
		<tr>
		    <th colspan="2" style="text-align:left; padding:20px;">
                        <p>
                            <strong>
                                Note:<br><br> 1. Submission   of   Registration   cum   Admission   Form   does   not   guarantee   Admission.   
                            </strong>
                        </p>
                        
                        <p>
                            <strong>
                              2. We {{$admission_father_name}} & {{$admission_mother_name}} Parent / guardian of {{$admission_name}} hereby agree that I will abide by the rules and regulations of the school and that whereas the school takes adeqate care to ensure the well being of the child, it is on noway responsible in any manner for any losses, mishaps or unforeseen events. I will not claim refund of admission, registration or composite fees once the admission is granted.
                           </strong>
                        </p>
                       
                        <p>
                            <strong>
                                3. Entrance Test will be conducted for Classes L.K.G. & U.K.G. Syllabus for the same can be checked on our website under Downloads.
                            </strong>
                        </p>
                        <p>
                            <strong>
                                4. Registration Amount will not be refunded even if Admission does not take place.
                            </strong>
                        </p>
                         <p>
                            <strong>
                                5. Grant of admission is at the sole  discretion of the Principal & can be denied without assigning any reason thereof.


                            </strong>
                        </p>
            </th>
		</tr>
			<tr>
			<th style="padding:10px;"  align="left">Father's Signature:</th> 
			<td style="padding:10px;" > <img  src="{{$father_signature}}" max-height="150px;" max-width="150px;" style="max-height:150px; width:150px;"/> </td> 
		</tr>
		<tr>
			<th style="padding:10px;"  align="left">Mother's Signature:</th> 
			<td style="padding:10px;" > <img  src="{{$mother_signature}}" max-height="150px;" max-width="150px;" style="max-height:150px; width:150px;"/> </td> 
		</tr>
	
	</table>
</body>
</html>