@extends('admin_panel/layout')
@section('content')

  <!-- Start: Content-Wrapper -->
    <section id="content_wrapper">
      <header id="topbar">
        <div class="topbar-left">
          <ol class="breadcrumb">
            <li class="crumb-active">
              <a href="{{ url('/admin-panel/change-password') }}">Change Password</a>
            </li>
            <li class="crumb-icon">
              <a href="{{ url('/admin-panel/dashboard') }}">
                <span class="glyphicon glyphicon-home"></span>
              </a>
            </li>
            <li class="crumb-link">
              <a href="{{ url('/admin-panel/dashboard') }}">Home</a>
            </li>
            <li class="crumb-trail">Change Password</li>
          </ol>
        </div>
      </header>


      <section id="content" class="table-layout animated fadeIn">
        <div class="tray tray-center" style="height:100% !important;">
          <div class="mw1000 center-block">
            <div class="content-header">
              <h2> Please fillup below form to  <b class="text-primary">Change your Password</b> </h2>
              <p class="lead"></p>
            </div>

            <div class="panel panel-primary panel-border top mt20 mb35">
              <div class="panel-heading">
                <span class="panel-title">Change Password</span>
              </div>            

              <div class="panel-body bg-light dark">

               @if ($message != "")
                <div class="alert alert-danger" role="alert"><i class="fa fa-thumbs-o-down" aria-hidden="true">
                  {{ $message }}</i>
                </div>
              @endif

              @if ($confirm_message != "")
                <div class="alert alert-success" role="alert"><i class="fa fa-thumbs-o-up" aria-hidden="true">
                  {{ $confirm_message }}</i>
                </div>
              @endif
 

                <div class="admin-form">
                  {!! Form::open(['url'=>'admin-panel/change-password' , 'enctype' => 'multipart/form-data' , 'id' => 'admin-form' ]) !!}
                    <div class="col-md-12">
                      <div class="tab-content pn br-n admin-form">
                        <div id="tab1_1" class="tab-pane active">

                          <div class="section row" style="  margin-bottom: 17px;">
                            <div class="col-md-12">  
                              <label for="current_password" class="field prepend-icon">
                                {!! Form::password('current_password', array('class' => 'gui-input','placeholder' => 'Current Password' )) !!}
                                <label for="current_password" class="field-icon">
                                  <i class="fa fa-key"></i>
                                </label>
                              </label>
                            </div>
                          </div>

                          <div class="section row" style="  margin-bottom: 17px;">
                            <div class="col-md-12">
                              <label for="new_password" class="field prepend-icon">
                                {!! Form::password('new_password', array('class' => 'gui-input','id' =>'new_password','placeholder' => 'New Password' )) !!}
                                <label for="new_password" class="field-icon">
                                  <i class="glyphicons glyphicons-keys"></i>
                                </label>
                              </label>
                            </div>
                          </div>

                          <div class="section row" style="  margin-bottom: 17px;"> 
                            <div class="col-md-12">
                              <label for="confirm_password" class="field prepend-icon">
                                {!! Form::password('confirm_password', array('class' => 'gui-input','placeholder' => 'Confirm Password' )) !!}
                                <label for="confirm_password" class="field-icon">
                                  <i class="glyphicons glyphicons-keys"></i>
                                </label>
                              </label>
                            </div>
                          </div>
                        </div>
                      </div>
                      </div>

                    <div class="form-group">
                    <div class="col-lg-12">
                      <div class="input-group" style="float: right; width: 220px;  ">
                      {!! Form::submit('Change Password', array('class' => 'button btn-primary btn-file btn-block ph5', 'id' => 'maskedKey')) !!}
                      </div>
                    </div>
                    </div>
                  {!! Form::close() !!}
                </div>
              </div>
            </div>
          </div>
        </div>
      </section>
      

<script type="text/javascript">
  jQuery(document).ready(function() {
  
    /* @custom validation method (smartCaptcha) 
    ------------------------------------------------------------------ */
    $("#admin-form").validate({

      /* @validation states + elements 
      ------------------------------------------- */

      errorClass: "state-error",
      validClass: "state-success",
      errorElement: "em",

      /* @validation rules 
      ------------------------------------------ */

      rules: {
        current_password: {
          required: true
        },
        new_password: {
          required: true,
          minlength: 6,
          maxlength: 16
        },
        confirm_password: {
          required: true,
          minlength: 6,
          maxlength: 16,
          equalTo: '#new_password'
        },
      },

      /* @validation error messages 
      ---------------------------------------------- */

      messages: {
        current_password: {
          required: 'Please enter password'
        },
        new_password: {
          required: 'Please enter a password'
        },
        confirm_password: {
          required: 'Please repeat the above password',
          equalTo: 'Password mismatch detected'
        },
      },

      /* @validation highlighting + error placement  
      ---------------------------------------------------- */

      highlight: function(element, errorClass, validClass) {
        $(element).closest('.field').addClass(errorClass).removeClass(validClass);
      },
      unhighlight: function(element, errorClass, validClass) {
        $(element).closest('.field').removeClass(errorClass).addClass(validClass);
      },
      errorPlacement: function(error, element) {
        if (element.is(":radio") || element.is(":checkbox")) {
          element.closest('.option-group').after(error);
        } else {
          error.insertAfter(element.parent());
        }
      }

    });

    // Form Skin Switcher
    $('#skin-switcher a').on('click', function() {
      var btnData = $(this).data('form-skin');

      $('#skin-switcher a').removeClass('item-active');
      $(this).addClass('item-active')

      adminForm.each(function(i, e) {
        var skins = 'theme-primary theme-info theme-success theme-warning theme-danger theme-alert theme-system theme-dark'
        var panelSkins = 'panel-primary panel-info panel-success panel-warning panel-danger panel-alert panel-system panel-dark'
        $(e).removeClass(skins).addClass('theme-' + btnData);
        Panel.removeClass(panelSkins).addClass('panel-' + btnData);
        pageHeader.removeClass().addClass('text-' + btnData);
      });

      $(switches).each(function(i, ele) {
        if ($(ele).hasClass('switch-round')) {
          if ($(ele).hasClass('block')) {
            $(ele).removeClass().addClass('block mt15 switch switch-round switch-' + btnData);
          } else {
            $(ele).removeClass().addClass('switch switch-round switch-' + btnData);
          }
        } else {
          if ($(ele).hasClass('block')) {
            $(ele).removeClass().addClass('block mt15 switch switch-' + btnData);
          } else {
            $(ele).removeClass().addClass('switch switch-' + btnData);
          }
        }

      });
      buttons.removeClass().addClass('button btn-' + btnData);
    });

    setTimeout(function() {
      adminForm.addClass('theme-primary');
      Panel.addClass('panel-primary');
      pageHeader.addClass('text-primary');

      $(options).each(function(i, e) {
        if ($(e).hasClass('block')) {
          $(e).removeClass().addClass('block mt15 option option-primary');
        } else {
          $(e).removeClass().addClass('option option-primary');
        }
      });

      // var sliders = $('.ui-timepicker-div', adminForm).find('.ui-slider');
      $('body').find('.ui-slider').each(function(i, e) {
        $(e).addClass('slider-primary');
      });

      $(switches).each(function(i, ele) {
        if ($(ele).hasClass('switch-round')) {
          if ($(ele).hasClass('block')) {
            $(ele).removeClass().addClass('block mt15 switch switch-round switch-primary');
          } else {
            $(ele).removeClass().addClass('switch switch-round switch-primary');
          }
        } else {
          if ($(ele).hasClass('block')) {
            $(ele).removeClass().addClass('block mt15 switch switch-primary');
          } else {
            $(ele).removeClass().addClass('switch switch-primary');
          }
        }
      });
      buttons.removeClass().addClass('button btn-primary');
    }, 800);



  });
  </script>





@endsection
