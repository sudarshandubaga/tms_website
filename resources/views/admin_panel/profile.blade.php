@extends('admin_panel/layout')

@section('content')

  <!-- Start: Content-Wrapper -->
    <section id="content_wrapper">
      <header id="topbar">
        <div class="topbar-left">
          <ol class="breadcrumb">
            <li class="crumb-active">
              <a href="{{ url('/admin-panel/profile') }}">Profile</a>
            </li>
            <li class="crumb-icon">
              <a href="{{ url('/admin-panel/dashboard') }}">
                <span class="glyphicon glyphicon-home"></span>
              </a>
            </li>
            <li class="crumb-link">
              <a href="{{ url('/admin-panel/dashboard') }}">Home</a>
            </li>
            <li class="crumb-trail">Profile</li>
          </ol>
        </div>
      </header>


      <section id="content" class="table-layout animated fadeIn">
        <div class="tray tray-center" style="height:100% !important;">
          <div class="mw1000 center-block">
            <div class="content-header">
              <h2> You can update your <b class="text-primary"> profile here </b> </h2>
              <p class="lead"></p>
            </div>

            <div class="panel panel-primary panel-border top mt20 mb35">
              <div class="panel-heading">
                <span class="panel-title">Profile</span>
              </div>            

              <div class="row">
                <div class="col-md-12">
                  @if (\Session::has('success'))
                    <div class="alert alert-success" style="padding: 10px; border: 0px; text-align: center; margin: 10px;">
                        {!! \Session::get('success') !!}
                    </div>
                  @endif
                </div>
              </div>

              <div class="panel-body bg-light dark">

                <div class="admin-form">
                  {!! Form::open(['url'=>'admin-panel/profile' , 'enctype' => 'multipart/form-data' , 'id' => 'admin-form' ]) !!}
                    <div class="col-md-9">
                      <div class="tab-content pn br-n admin-form">
                        <div id="tab1_1" class="tab-pane active">

                          <div class="section row" style="  margin-bottom: 17px;">
                            <div class="col-md-12">  
                              <label for="admin_name" class="field prepend-icon">
                                {!! Form::text('admin_name',"$admin_details->admin_name", array('class' => 'gui-input','placeholder' => 'Enter Name' )) !!}
                                <label for="admin_name" class="field-icon">
                                  <i class="fa fa-user"></i>
                                </label>
                              </label>
                            </div>
                          </div>

                          <div class="section row" style="  margin-bottom: 17px;">
                            <div class="col-md-12">
                              <label for="admin_number" class="field prepend-icon">
                                {!! Form::text('admin_number',"$admin_details->admin_number", array('class' => 'gui-input bfh-phone', 'data-format' =>'ddd ddddddd','placeholder' => 'Enter Mobile Number' )) !!}
                                <label for="Mobile Number" class="field-icon">
                                  <i class="fa fa-phone"></i>
                                </label>
                              </label>
                            </div>
                          </div>

                          <div class="section row" style="  margin-bottom: 17px;"> 
                            <div class="col-md-12">
                              <label for="admin_email" class="field prepend-icon">
                                {!! Form::text('admin_email',"$admin_details->admin_email", array('class' => 'gui-input','placeholder' => 'Enter Email' )) !!}
                                <label for="admin_email" class="field-icon">
                                  <i class="fa fa-envelope"></i>
                                </label>
                              </label>
                            </div>
                          </div>
                        </div>
                      </div>
                      </div>


                    <div class="col-md-3" style="  margin-top: 4px;">
                      <div class="fileupload fileupload-new admin-form" data-provides="fileupload">

                      @if($admin_details->admin_image != "")

                        <div class="fileupload-preview thumbnail mb15" style="line-height: 136px !important;">
                          {!! Html::image($admin_details->admin_image, '100%x147') !!}
                        </div>
                        <span class="button btn-system btn-file btn-block ph5" style="margin-bottom:10px">
                          <span class="fileupload-new">Profile Picture</span>
                          <span class="fileupload-exists">Profile Picture</span>
                          {!! Form::file('admin_image') !!}
                        </span>

                      @else

                        <div class="fileupload-preview thumbnail mb15" style="padding: 3px;outline: 1px dashed #d9d9d9;height: 150px">
                          <img data-src="holder.js/100%x147" alt="100%x147" src="" data-holder-rendered="true"  style="height: 150px !important; width: 100%; display: block;">
                        </div>
                        <span class="button btn-system btn-file btn-block ph5" style="margin-bottom:10px">
                          <span class="fileupload-new" >Profile Photo</span>
                          <span class="fileupload-exists">Profile Photo</span>
                          {!! Form::file('admin_image') !!}
                        </span>

                      @endif

                      </div>
                    </div>

                    <div class="form-group">
                    <div class="col-lg-12">
                      <div class="input-group" style="float: right; width: 220px;  ">
                        {!! Form::submit('Update', array('class' => 'button btn-primary btn-file btn-block ph5', 'id' => 'maskedKey')) !!}
                      </div>
                    </div>
                    </div>
                  {!! Form::close() !!}
                </div>
              </div>
            </div>
          </div>
        </div>
      </section>

<script type="text/javascript">
  jQuery(document).ready(function() {

    jQuery.validator.addMethod("lettersonly", function(value, element) 
    {
    return this.optional(element) || /^[a-z," "]+$/i.test(value);
    }, "This field contains alphabets only");
  
    /* @custom validation method (smartCaptcha) 
    ------------------------------------------------------------------ */
    $("#admin-form").validate({

      /* @validation states + elements 
      ------------------------------------------- */

      errorClass: "state-error",
      validClass: "state-success",
      errorElement: "em",

      /* @validation rules 
      ------------------------------------------ */

      rules: {
        admin_name: {
          required: true,
          lettersonly: true,
        },
        admin_number: {
          required: true,
        },
        admin_email: {
          required: true,
          email: true
        },
        admin_image: {
          extension: 'jpeg,jpg,png',
        },
      },

      /* @validation error messages 
      ---------------------------------------------- */

      messages: {
        admin_name: {
          required: 'Enter your name'
        },
        admin_number: {
          required: 'Enter your mobile number'
        },
        admin_email: {
          required: 'Enter email address',
          email: 'Enter a VALID email address'
        },
        admin_image: {
          extension: 'Image Should be in .jpg,.jpeg and .png format only'
        }
      },

      /* @validation highlighting + error placement  
      ---------------------------------------------------- */

      highlight: function(element, errorClass, validClass) {
        $(element).closest('.field').addClass(errorClass).removeClass(validClass);
      },
      unhighlight: function(element, errorClass, validClass) {
        $(element).closest('.field').removeClass(errorClass).addClass(validClass);
      },
      errorPlacement: function(error, element) {
        if (element.is(":radio") || element.is(":checkbox")) {
          element.closest('.option-group').after(error);
        } else {
          error.insertAfter(element.parent());
        }
      }

    });

  });
  </script>





@endsection
