@extends('admin_panel/layout')

@section('content')

<style type="text/css">

.dt-panelfooter{
  display: none !important;
}
th,td{
    border:1px #bbb solid;
}
strong{color:#000;}
</style>

  <!-- Start: Content-Wrapper -->
    <section id="content_wrapper">
      <header id="topbar" style="margin-top:60px">
        <div class="topbar-left">
          <ol class="breadcrumb">
            <li class="crumb-active">
              <a href="{{ url('/admin-panel/register-enquiry') }}">Registration Enquiry</a>
            </li>
            <li class="crumb-icon">
              <a href="{{ url('/admin-panel/dashboard') }}">
                <span class="glyphicon glyphicon-home"></span>
              </a>
            </li>
            <li class="crumb-link">
              <a href="{{ url('/admin-panel/dashboard') }}">Home</a>
            </li>
            <li class="crumb-trail">Registration Enquiry</li>
          </ol>
        </div>
      </header>

      <div class="" style="margin-top:10px;">
        <div class="col-md-12">
          <div class="panel panel-primary panel-border top mb35">
            <div class="panel-heading">
              <div class="panel-title hidden-xs">
                <div class="pull-right">
                     <a href="{{ route('admin.export_register_enquiry') }}"> {!! Form::submit('Export', array('class' => 'btn btn-info btn-sm', 'id' => 'maskedKey')) !!} </a>
                  </div>
                <span class="glyphicon glyphicon-tasks"></span>Registration Enquiry</div>
            </div>

            
            <div class="panel-body pn">

              {!! Form::open(['name'=>'form']) !!}

              <div class="table-responsive">
                <table class="table admin-form theme-warning tc-checkbox-1 fs13" id="datatable">
                  <thead>
                    <tr class="bg-light">
                      <th style="width:40px !important;" class="text-left">
                        <label class="option block mn">
                          <input type="checkbox" id="check_all"> 
                          <span class="checkbox mn"></span><br/>
                        </label>
                      </th>
                      <th class="border">Student Info</th>
                      <th class="border">Personal Info</th>
                      <th class="border">Contact Details</th>
                    </tr>
                  </thead>
                  <tbody>
                    
                     @foreach($get_record as $get_records)  

                    <tr>
                      <td class="text-left" style="padding-left:18px" width="40px!important;">
                        <label class="option block mn">
                          <input type="checkbox" name="check[]" class="check" value="{{$get_records->admission_id}}">
                          <span class="checkbox mn"></span>
                        </label>
                      </td>  
                      <td class="text-left text-capitalize" style="padding-left:20px"  width="250px!important;">
                        <div class="row">
                          <div class="col-sm-4"><strong>Session</strong></div>
                          <div class="col-sm-8">{{$get_records->syear}}</div>
                        </div>
                        <div class="row">
                          <div class="col-sm-4"><strong>Name</strong></div>
                          <div class="col-sm-8" style="font-weight:bold; color:blue">{{$get_records->name}}</div>
                        </div>
                        <div class="row">
                          <div class="col-sm-4"><strong>DOB</strong></div>
                          <div class="col-sm-8">{{$get_records->date}}</div>
                        </div>
                        <div class="row">
                          <div class="col-sm-4"><strong>Gender</strong></div>
                          <div class="col-sm-8">{{$get_records->gender}}</div>
                        </div>
                       
                        <div class="row">
                          <div class="col-sm-4"><strong>Class</strong></div>
                          <div class="col-sm-8">{{$get_records->class}}</div>
                        </div>
                     
                      </td>
                      <td class=""  width="300px!important;">
                        <div class="row">
                          <div class="col-sm-4"><strong>Father Name</strong></div>
                          <div class="col-sm-8">{{$get_records->fname}}</div>
                        </div>
                        
                         <div class="row">
                          <div class="col-sm-4"><strong>Mother Name</strong></div>
                          <div class="col-sm-8">{{$get_records->mname}}</div>
                        </div>
                        
                      </td>
                 
                      <td class="" width="250px!important;">
                        <div class="row">
                          <div class="col-sm-4"><strong>Mobile:</strong></div>
                          <div class="col-sm-8">{{$get_records->mobile}}</div>
                        </div>
                        <div class="row">
                          <div class="col-sm-4"><strong>Phone No.:</strong></div>
                          <div class="col-sm-8">{{$get_records->pno}}</div>
                        </div>
                        
                        <div class="row">
                          <div class="col-sm-4"><strong>Address:</strong></div>
                          <div class="col-sm-8">{{$get_records->address}}</div>
                        </div>
                        <div class="row">
                          <div class="col-sm-4"><strong>City:</strong></div>
                          <div class="col-sm-8">{{$get_records->city}}</div>
                        </div>
                        <div class="row">
                          <div class="col-sm-4"><strong>State:</strong></div>
                          <div class="col-sm-8">{{$get_records->state}}</div>
                        </div>
                      
                      </td>
                    </tr>

                    @endforeach
                  </tbody>
                </table>
              </div>
              {!! Form::close() !!}

            </div>

            <div class="panel-body pn">
              <div class="table-responsive">
                <table class="table admin-form theme-warning tc-checkbox-1 fs13">                                
                  <tbody>
                    <tr class="">    
                      <th  class="text-left">
                        <button type="button" class="btn btn-primary" onclick="go_delete()"><i class="glyphicon glyphicon-trash"></i> Delete Multiple </button>
                      </th>
                      <th  class="text-right">
                        {{ $get_record->links() }}
                      </th>
                    </tr>
                  </tbody>
                </table>
              </div>
            </div>

          </div>
        </div>
      </div>

@endsection
