@extends('admin_panel/layout')

@section('content')

  <!-- Start: Content-Wrapper -->
    <section id="content_wrapper">
      <header id="topbar">
        <div class="topbar-left">
          <ol class="breadcrumb">
            <li class="crumb-active">
              <a href="{{ url('/admin-panel/view-event')}}">View Events</a>
            </li>
            <li class="crumb-icon">
              <a href="{{ url('/admin-panel/dashboard') }}">
                <span class="glyphicon glyphicon-home"></span>
              </a>
            </li>
            <li class="crumb-link">
              <a href="{{ url('/admin-panel/dashboard') }}">Home</a>
            </li>
            <li class="crumb-trail">Add Events</li>
          </ol>
        </div>
      </header>

      <!-- Begin: Content -->
      <section id="content" class="table-layout animated fadeIn">
        <div class="tray tray-center">
          <div class="center-block">
            <div class="panel panel-primary panel-border top mb35">
              <div class="panel-heading">
                <span class="panel-title"> Add Events </span>
              </div>
              <div class="panel-body bg-light dark">
                <div class="tab-content pn br-n admin-form">
                  <div id="tab1_1" class="tab-pane active">

                    {!! Form::open(['url'=>'/admin-panel/save-event/'.$get_record['event_id'].'' , 'enctype' => 'multipart/form-data' , 'id' => 'validation' ] ) !!}
                    <div>       
                      <div class="row">
                          <div class="col-md-9">
                              <div class="col-md-12">
                                <div class="section">
                                  <label for="event_name" class="field-label" style="font-weight:600;" > Name </label>
                                    <label for="event_name" class="field prepend-icon">
                                      
                                      @if($get_record != "")
                                        {!! Form::text('event_name',$get_record['event_name'], array('class' => 'event-name gui-input br-light light focusss','placeholder' => '' )) !!}
                                      @else
                                        {!! Form::text('event_name','', array('class' => 'event-name gui-input br-light light focusss','placeholder' => '' )) !!}
                                      @endif
                                      <label for="store-currency" class="field-icon">
                                        <i class="fa fa-crosshairs"></i>
                                      </label>
                                    </label>
                                </div>
                              </div>

                              <div class="col-md-12">
                                <div class="section">
                                  <label for="event_class" class="field-label" style="font-weight:600;" > Select Class </label>
                                    <label for="event_class" class="field prepend-icon">  
                                        <select class="select2-single form-control" name="event_class[]" id="event_class" multiple>
                                          <option value="">Select Class</option>

                                          @if($get_record != "")
                                          
                                            @foreach($get_class as $get_class) 

                                            @php
                                              $arr = explode(',', $get_record['event_class']);
                                              $sel1 = '';
                                              if ( in_array( $get_class->class_id, $arr) ) {
                                                $sel1 = 'selected="selected"';  
                                              } 
                                            @endphp
                                              <option value="{{$get_class->class_id}}" {{ $sel1 }} > {{$get_class->class_name}} </option>
                                            @endforeach 

                                          @else

                                            @foreach($get_class as $get_class) 
                                              <option value="{{$get_class->class_id}}"> {{$get_class->class_name}} </option>
                                            @endforeach 
                                        
                                          @endif
                                        </select>
                                        <i class="arrow double"></i>

                                    </label>
                                    <div id="descrption_error" class="" ></div>
                                </div>
                              </div>

                              <div class="col-md-12">
                                <div class="section">
                                  <label for="event_date" class="field-label" style="font-weight:600;" > Start Date </label>
                                    <label for="event_date" class="field prepend-icon">
                                      
                                      @if($get_record != "")
                                        {!! Form::text('event_date',$get_record['event_date'], array('class' => 'event-name gui-input br-light light focusss','placeholder' => '' )) !!}
                                      @else
                                        {!! Form::text('event_date','', array('class' => 'event-name gui-input br-light light focusss','placeholder' => ''  )) !!}
                                      @endif
                                      <label for="store-currency" class="field-icon">
                                        <i class="fa fa-crosshairs"></i>
                                      </label>
                                    </label>
                                </div>
                              </div>
                      
                              <div class="col-md-12">
                                  <div class="section">
                                    <label for="event_description" class="field-label" style="font-weight:600;" >Description</label>
                                      <label for="event_description" class="field prepend-icon">
                                          @if($get_record != "")
                                            {!! Form::textarea('event_description',$get_record['event_description'], array('class' => 'gui-textarea accc jqte-test','placeholder' => '' )) !!}
                                          @else
                                            {!! Form::textarea('event_description','', array('class' => 'gui-textarea accc jqte-test','placeholder' => '' )) !!}
                                          @endif
                                          
                                      </label>
                                  </div>
                              </div>
                          </div>

                          <div class="col-md-3" style="margin-top:25px;">
                            <label for="name" class="field">
                              <div class="fileupload fileupload-new admin-form" data-provides="fileupload" >  
                                @if($get_record != "")
                                  <div class="fileupload-preview thumbnail mb15" style="line-height: 136px !important;">
                                    @if($get_record['event_image'] != "")
                                      {!! Html::image($get_record['event_image'], '100%x147') !!}
                                    @else
                                      <img data-src="holder.js/100%x150" alt="100%x147" src="public/images/dummy.png" data-holder-rendered="true" style="height: 100% !important; width: 100% !important; display: block;">
                                    @endif
                                  </div>
                                  <span class="button btn-system btn-file btn-block ph5" style="margin-bottom:10px">
                                    <span class="fileupload-new" >Change Image</span>
                                    <span class="fileupload-exists">Change Image</span>  
                                    {!! Form::file('event_image') !!}
                                  </span>
                                @else
                                  <div class="fileupload-preview thumbnail mb15" style="line-height: 136px !important;">
                                    <img data-src="holder.js/100%x150" alt="100%x147" src="public/images/dummy.png" data-holder-rendered="true" style="height: 100% !important; width: 100% !important; display: block;">
                                  </div>
                                  <span class="button btn-system btn-file btn-block ph5" style="margin-bottom:10px">
                                    <span class="fileupload-new" >Image</span>
                                    <span class="fileupload-exists">Image</span>
                                    {!! Form::file('event_image') !!}
                                  </span>
                                @endif
                              </div>
                            </label>
                          </div>
                    
                          <div class="clearfix"></div>
                          <div class="panel-footer text-right">
                            {!! Form::submit('Save', array('class' => 'button btn-primary', 'id' => 'maskedKey')) !!}
                            {!! Form::reset('Cancel', array('class' => 'button btn-primary', 'id' => 'maskedKey')) !!}
                          </div>
                       {!! Form::close() !!}
                    </div>
                </div>
              </div>
            </div>
          </div>
        </div>
      </section>

<style>
    
  .jqte_editor{
    height: auto !important;
    min-height: 300px!important;
  }
  .jqte_source{
    height: auto !important;
    min-height: 300px!important;
  }
</style>

<script type="text/javascript">

  jQuery(document).ready(function() {

    jQuery.validator.addMethod("lettersonly", function(value, element) 
    {
    return this.optional(element) || /^[a-z," "]+$/i.test(value);
    }, "This field contains alphabets only");
    
    /* @custom validation method (smartCaptcha) 
    ------------------------------------------------------------------ */
    $("#validation").validate({

      /* @validation states + elements 
      ------------------------------------------- */

      errorClass: "state-error",
      validClass: "state-success",
      errorElement: "em",

      /* @validation rules 
      ------------------------------------------ */

      rules: {
        event_name: {
          required: true,
        },
        event_description: {
          required: true,
        },
        event_date: {
          required: true,
        },
        event_image: {
          extension: 'jpeg,jpg,png',
        },
      },

      /* @validation error messages 
      ---------------------------------------------- */

        messages: {
          event_image: {
            extension: 'Image Should be in .jpg,.jpeg and .png format only'
        }


      //   testimonial_name: {
      //     required: 'Enter your name'
      //   },
      //   testimonial_city: {
      //     required: 'Enter Country'
      //   },
      //   testimonial_description: {
      //     required: 'Enter Description'
      //   },  
      //   testimonial_job_profile: {
      //     required: 'Enter Job Profile'
      //   },
      //   testimonial_image: {
      //     required: 'Note :- Select Image also'
      //   }
       },

      /* @validation highlighting + error placement  
      ---------------------------------------------------- */

      highlight: function(element, errorClass, validClass) {
        $(element).closest('.field').addClass(errorClass).removeClass(validClass);
      },
      unhighlight: function(element, errorClass, validClass) {
        $(element).closest('.field').removeClass(errorClass).addClass(validClass);
      },
      errorPlacement: function(error, element) {
        if (element.is(":radio") || element.is(":checkbox")) {
          element.closest('.option-group').after(error);
        } else {
          error.insertAfter(element.parent());
        }
      }

    });

  });
  </script>

@endsection
