<!DOCTYPE html>
<html>
  <head>
  <!-- Meta, title, CSS, favicons, etc. -->
    <meta charset="utf-8">
    <title>Reset Password | AdminPanel</title>
    <meta name="keywords" content="AdminPanel" />
    <meta name="description" content="AdminPanel">
    <meta name="author" content="AdminPanel">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <!-- Font CSS (Via CDN) -->
    {!!Html::style('public/admin/css/css.css') !!}
    <!-- Admin Forms CSS -->
    {!!Html::style('public/admin/css/admin-forms.css') !!}
    <!-- Theme CSS -->
    {!!Html::style('public/admin/css/theme.css') !!}
    {!!Html::style('public/admin/css/fonts/glyphicons-pro/glyphicons-pro.css') !!}
    {!!Html::style('public/admin/css/fonts/iconsweets/iconsweets.css') !!}
    <!-- Favicon -->
    <link rel="shortcut icon" href="{{{ asset('public/admin/img/1.png') }}}">
  </head>

  <body class="external-page sb-l-c sb-r-c">
    <!-- Start: Main -->
    <div id="main" class="animated fadeIn">
      <!-- Start: Content-Wrapper -->
      <section id="content_wrapper">
        <!-- begin canvas animation bg -->
        <div id="canvas-wrapper">
          <canvas id="demo-canvas"></canvas>
        </div>

        <!-- Begin: Content -->
        @if($get_record[0]->reset_password_status == 0)
        <section id="content">
          <div class="admin-form theme-info" id="login1">
            <div class="row mb15 table-layout">     
              <div class="col-xs-2 va-m pln"></div>
              <div class="col-xs-8 va-m pln">
                  <img src="{{asset($account['account_logo'])}}"  height="43"  title="Admin Logo" class="img-responsive">
              </div>
              <div class="col-xs-2 text-right va-b pr5"></div>
            </div>
            <div class="panel panel-info mt10 br-n">
	            <!-- end .form-header section -->
	            {!! Form::open(['url'=>'mygo-panel/password_reset', 'id' => 'validation']) !!}

	                <div class="panel-body bg-light p30" style="border-top:5px solid #3bafda;">       
	                  <div class="row">
	                    <div class="col-sm-12 pr30 loginsssss">         
                        
                          {!! Form::hidden('admin_id', $get_record[0]->admin_id ) !!}
                        
	                      <div class="section">
	                        {!! Form::label('new_password', 'New Password', array('class' => 'field-label text-muted fs18 mb10')) !!}
	                        <label for="new_password" class="field prepend-icon">
	                       		{!! Form::password('new_password', array('class' => 'gui-input','placeholder' => 'New Password', 'required' => 'required'  )) !!}
	                          <label for="username" class="field-icon">
	                            <i class="fa fa-lock"></i>
	                          </label>
	                        </label>
	                      </div>

	                      <div class="section">
	                      	{!! Form::label('confirm_password', 'Confirm Password', array('class' => 'field-label text-muted fs18 mb10')) !!}
	                        <label for="confirm_password" class="field prepend-icon">     
	                        	{!! Form::password('confirm_password', array('class' => 'gui-input','placeholder' => 'Confirm Passowrd','required' => 'required' )) !!}
	                          <label for="password" class="field-icon">
	                            <i class="fa fa-unlock"></i>
	                          </label>
	                        </label>
	                      </div>

	                    </div>
	                  </div>
	                </div>
	                <!-- end .form-body section -->
	              <div class="panel-footer clearfix p10 ph15" style="border-top: 1px solid #DDD;">
	                <input type="submit" class="button btn-primary mr10 pull-right" name="login" id="login" value="Reset Password"/>
	              </div>
                <!-- end .form-footer section -->
                {!! Form::close() !!}
          </div>
        </div>
      </section>
      @else
        <section id="content_wrapper">
          <div id="canvas-wrapper">
            <canvas id="demo-canvas"></canvas>
          </div>
          <section id="content">
            <div class="admin-form theme-info" id="login1">
              <div class="center-block mt70" style="max-width: 625px">
                <div class="row table-layout">
                  <div class="row mb15 table-layout">     
                    <div class="col-xs-2 va-m pln"></div>
                    <div class="col-xs-8 va-m pln">
                        <img src="{{asset($account['account_logo'])}}"  height="43"  title="Admin Logo" class="img-responsive">
                    </div>
                    <div class="col-xs-2 text-right va-b pr5"></div>
                  </div>
                </div>
                <div class="panel mt15">
                  <div class="panel-body">            
                      <p class="lh25 text-muted fs15">Your password reset link has been expired.</p>
                      <p class="text-right mt20"><a href="{{ url('/mygo-panel') }}" class="btn btn-primary btn-rounded ph40">SIGN IN</a></p>
                  </div>
                </div>
              </div>          
            </div>
          </section>
        </section>
      @endif
      <!-- End: Content -->
    </section>
      <!-- End: Content-Wrapper -->
    </div>


  {!!HTML::script('public/admin/js/jquery/jquery-1.11.1.min.js') !!}
  {!!HTML::script('public/admin/js/jquery/jquery_ui/jquery-ui.min.js') !!}
  {!!HTML::script('public/admin/js/plugins/canvasbg/canvasbg.js') !!}
  {!!HTML::script('public/admin/js/utility/utility.js') !!}
  {!!HTML::script('public/admin/js/demo/demo.js') !!}
  {!!HTML::script('public/admin/js/main.js') !!}
  {!!HTML::script('public/admin/js/admin-forms/js/jquery.validate.min.js') !!}
  {!!HTML::script('public/admin/js/admin-forms/js/additional-methods.min.js') !!}

 
  <!-- END: PAGE SCRIPTS -->

  <script>
    $(document).ready(function() {
      $('img').on('dragstart', function(event) { event.preventDefault(); });
    });
  </script>
  
  <!-- Page Javascript -->
  <script type="text/javascript">
  jQuery(document).ready(function() {

    "use strict";
    // Init Theme Core      
    Core.init();

    // Init Demo JS
    Demo.init();

    // Init CanvasBG and pass target starting location
    CanvasBG.init({
      Loc: {
        x: window.innerWidth / 2,
        y: window.innerHeight / 3.3
      },
    });

  });
  </script>



<script type="text/javascript">
  jQuery(document).ready(function() {
  
    /* @custom validation method (smartCaptcha) 
    ------------------------------------------------------------------ */
    $("#validation").validate({

      /* @validation states + elements 
      ------------------------------------------- */

      errorClass: "state-error",
      validClass: "state-success",
      errorElement: "em",

      /* @validation rules 
      ------------------------------------------ */

      rules: {
        new_password: {
          required: true,
          minlength: 6,
          maxlength: 16
        },
        confirm_password: {
          required: true,
          minlength: 6,
          maxlength: 16,
          equalTo: '#new_password'
        },
      },

      /* @validation error messages 
      ---------------------------------------------- */

      messages: {
        new_password: {
          required: 'Please enter a password'
        },
        confirm_password: {
          required: 'Please repeat the above password',
          equalTo: 'Password mismatch detected'
        },
      },

      /* @validation highlighting + error placement  
      ---------------------------------------------------- */

      highlight: function(element, errorClass, validClass) {
        $(element).closest('.field').addClass(errorClass).removeClass(validClass);
      },
      unhighlight: function(element, errorClass, validClass) {
        $(element).closest('.field').removeClass(errorClass).addClass(validClass);
      },
      errorPlacement: function(error, element) {
        if (element.is(":radio") || element.is(":checkbox")) {
          element.closest('.option-group').after(error);
        } else {
          error.insertAfter(element.parent());
        }
      }

    });

    // Form Skin Switcher
    $('#skin-switcher a').on('click', function() {
      var btnData = $(this).data('form-skin');

      $('#skin-switcher a').removeClass('item-active');
      $(this).addClass('item-active')

      adminForm.each(function(i, e) {
        var skins = 'theme-primary theme-info theme-success theme-warning theme-danger theme-alert theme-system theme-dark'
        var panelSkins = 'panel-primary panel-info panel-success panel-warning panel-danger panel-alert panel-system panel-dark'
        $(e).removeClass(skins).addClass('theme-' + btnData);
        Panel.removeClass(panelSkins).addClass('panel-' + btnData);
        pageHeader.removeClass().addClass('text-' + btnData);
      });

      $(switches).each(function(i, ele) {
        if ($(ele).hasClass('switch-round')) {
          if ($(ele).hasClass('block')) {
            $(ele).removeClass().addClass('block mt15 switch switch-round switch-' + btnData);
          } else {
            $(ele).removeClass().addClass('switch switch-round switch-' + btnData);
          }
        } else {
          if ($(ele).hasClass('block')) {
            $(ele).removeClass().addClass('block mt15 switch switch-' + btnData);
          } else {
            $(ele).removeClass().addClass('switch switch-' + btnData);
          }
        }

      });
      buttons.removeClass().addClass('button btn-' + btnData);
    });

    setTimeout(function() {
      adminForm.addClass('theme-primary');
      Panel.addClass('panel-primary');
      pageHeader.addClass('text-primary');

      $(options).each(function(i, e) {
        if ($(e).hasClass('block')) {
          $(e).removeClass().addClass('block mt15 option option-primary');
        } else {
          $(e).removeClass().addClass('option option-primary');
        }
      });

      // var sliders = $('.ui-timepicker-div', adminForm).find('.ui-slider');
      $('body').find('.ui-slider').each(function(i, e) {
        $(e).addClass('slider-primary');
      });

      $(switches).each(function(i, ele) {
        if ($(ele).hasClass('switch-round')) {
          if ($(ele).hasClass('block')) {
            $(ele).removeClass().addClass('block mt15 switch switch-round switch-primary');
          } else {
            $(ele).removeClass().addClass('switch switch-round switch-primary');
          }
        } else {
          if ($(ele).hasClass('block')) {
            $(ele).removeClass().addClass('block mt15 switch switch-primary');
          } else {
            $(ele).removeClass().addClass('switch switch-primary');
          }
        }
      });
      buttons.removeClass().addClass('button btn-primary');
    }, 800);

  });
  </script>

  

</body>
</html>



