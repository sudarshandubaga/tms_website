@extends('admin_panel/layout')

@section('content')

  <!-- Start: Content-Wrapper -->
    <section id="content_wrapper">
      <header id="topbar">
        <div class="topbar-left">
          <ol class="breadcrumb">
            <li class="crumb-active">
              <a href="{{ url('/admin-panel/view-testimonials')}}">View Testimonials</a>
            </li>
            <li class="crumb-icon">
              <a href="{{ url('/admin-panel/dashboard') }}">
                <span class="glyphicon glyphicon-home"></span>
              </a>
            </li>
            <li class="crumb-link">
              <a href="{{ url('/admin-panel/dashboard') }}">Home</a>
            </li>
            <li class="crumb-trail">Add Testimonials</li>
          </ol>
        </div>
      </header>

      <!-- Begin: Content -->
      <section id="content" class="table-layout animated fadeIn">
        <div class="tray tray-center">
          <div class="center-block">
            <div class="panel panel-primary panel-border top mb35">
              <div class="panel-heading">
                <span class="panel-title"> Add Testimonials </span>
              </div>
              <div class="panel-body bg-light dark">
                <div class="tab-content pn br-n admin-form">
                  <div id="tab1_1" class="tab-pane active">

                    {!! Form::open(['url'=>'/admin-panel/save-testimonials/'.$get_record['testimonials_id'].'' , 'enctype' => 'multipart/form-data' , 'id' => 'validation' ] ) !!}
                    <div>
                      <div class="row">
                          <div class="col-md-12">
                              <!--<div class="col-md-12">-->
                              <!--  <div class="section">-->
                              <!--    <label for="testimonials_title" class="field-label" style="font-weight:600;" > Title </label>-->
                              <!--      <label for="testimonials_title" class="field prepend-icon">-->
                                      
                              <!--        @if($get_record != "")-->
                              <!--          {!! Form::text('testimonials_title',$get_record['testimonials_title'], array('class' => 'event-name gui-input br-light light focusss','placeholder' => '' )) !!}-->
                              <!--        @else-->
                              <!--          {!! Form::text('testimonials_title','', array('class' => 'event-name gui-input br-light light focusss','placeholder' => '' )) !!}-->
                              <!--        @endif-->
                              <!--        <label for="store-currency" class="field-icon">-->
                              <!--          <i class="fa fa-crosshairs"></i>-->
                              <!--        </label>-->
                              <!--      </label>-->
                              <!--  </div>-->
                              <!--</div>-->
                              
                              <div class="col-md-6">
                                <div class="section">
                                  <label for="testimonials_user_name" class="field-label" style="font-weight:600;" > Person Name </label>
                                    <label for="testimonials_user_name" class="field prepend-icon">
                                      
                                      @if($get_record != "")
                                        {!! Form::text('testimonials_user_name',$get_record['testimonials_user_name'], array('class' => 'event-name gui-input br-light light focusss','placeholder' => '' )) !!}
                                      @else
                                        {!! Form::text('testimonials_user_name','', array('class' => 'event-name gui-input br-light light focusss','placeholder' => '' )) !!}
                                      @endif
                                      <label for="store-currency" class="field-icon">
                                        <i class="fa fa-crosshairs"></i>
                                      </label>
                                    </label>
                                </div>
                              </div>
                              <div class="col-md-6">
                                <div class="section">
                                  <label for="testimonials_user_name" class="field-label" style="font-weight:600;" > Person Image </label>
                                    <label for="testimonials_user_name" class="field prepend-icon">
                                      
                                      @if($get_record != "")
                                        <!-- {!! Form::file('testimonials_image',$get_record['testimonials_image'], array('class' => 'event-name gui-input br-light light focusss','placeholder' => '' )) !!} -->
                                        <input type="file" name="testimonials_image" value="{{ $get_record['testimonials_image'] }}"  class="event-name gui-input br-light light focusss" id="">
                                      @else
                                      <input type="file" name="testimonials_image"  class="event-name gui-input br-light light focusss" id="">
                                        <!-- {!! Form::file('testimonials_image','', array('class' => 'event-name gui-input br-light light focusss','placeholder' => '' )) !!} -->
                                      @endif
                                      <label for="store-currency" class="field-icon">
                                        <i class="fa fa-crosshairs"></i>
                                      </label>
                                    </label>
                                </div>
                              </div>
                              <div class="col-md-12">
                                  <div class="section">
                                    <label for="testimonials_description" class="field-label" style="font-weight:600;" >Description</label>
                                      <label for="testimonials_description" class="field prepend-icon">
                                          @if($get_record != "")
                                            {!! Form::textarea('testimonials_description',$get_record['testimonials_description'], array('class' => 'gui-textarea accc','placeholder' => '' )) !!}
                                          @else
                                            {!! Form::textarea('testimonials_description','', array('class' => 'gui-textarea accc','placeholder' => '' )) !!}
                                          @endif
                                          <label for="store-currency" class="field-icon">
                                            <i class="fa fa-align-justify"></i>
                                          </label>
                                      </label>
                                  </div>
                              </div>
                              
                             
                              
                              
                          </div>
                    
                          <div class="clearfix"></div>
                          <div class="panel-footer text-right">
                            {!! Form::submit('Save', array('class' => 'button btn-primary', 'id' => 'maskedKey')) !!}
                            {!! Form::reset('Cancel', array('class' => 'button btn-primary', 'id' => 'maskedKey')) !!}
                          </div>
                       {!! Form::close() !!}
                    </div>
                </div>
              </div>
            </div>
          </div>
        </div>
      </section>

<script type="text/javascript">

  jQuery(document).ready(function() {

    jQuery.validator.addMethod("lettersonly", function(value, element) 
    {
    return this.optional(element) || /^[a-z," "]+$/i.test(value);
    }, "This field contains alphabets only");
    
    /* @custom validation method (smartCaptcha) 
    ------------------------------------------------------------------ */
    $("#validation").validate({

      /* @validation states + elements 
      ------------------------------------------- */

      errorClass: "state-error",
      validClass: "state-success",
      errorElement: "em",

      /* @validation rules 
      ------------------------------------------ */

      rules: {
        testimonials_title: {
          required: true,
          lettersonly: true,
        },
        testimonials_description: {
          required: true,
        },
        testimonials_user_name: {
          required: true,
        },
      },

      /* @validation error messages 
      ---------------------------------------------- */

      // messages: {
      //   testimonial_name: {
      //     required: 'Enter your name'
      //   },
      //   testimonial_city: {
      //     required: 'Enter Country'
      //   },
      //   testimonial_description: {
      //     required: 'Enter Description'
      //   },  
      //   testimonial_job_profile: {
      //     required: 'Enter Job Profile'
      //   },
      //   testimonial_image: {
      //     required: 'Note :- Select Image also'
      //   }
      // },

      /* @validation highlighting + error placement  
      ---------------------------------------------------- */

      highlight: function(element, errorClass, validClass) {
        $(element).closest('.field').addClass(errorClass).removeClass(validClass);
      },
      unhighlight: function(element, errorClass, validClass) {
        $(element).closest('.field').removeClass(errorClass).addClass(validClass);
      },
      errorPlacement: function(error, element) {
        if (element.is(":radio") || element.is(":checkbox")) {
          element.closest('.option-group').after(error);
        } else {
          error.insertAfter(element.parent());
        }
      }

    });

  });
  </script>

@endsection
