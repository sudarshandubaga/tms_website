@extends('admin_panel/layout')

@section('content')

  <!-- Start: Content-Wrapper -->
    <section id="content_wrapper">
      <header id="topbar">
        <div class="topbar-left">
          <ol class="breadcrumb">
            <li class="crumb-active">
              <a href="{{ url('/admin-panel/view-faq') }}">View FAQ</a>
            </li>
            <li class="crumb-icon">
              <a href="{{ url('/admin-panel/dashboard') }}">
                <span class="glyphicon glyphicon-home"></span>
              </a>
            </li>
            <li class="crumb-link">
              <a href="{{ url('/admin-panel/dashboard') }}">Home</a>
            </li>
            <li class="crumb-trail">Add FAQ</li>
          </ol>
        </div>
      </header>

      <!-- Begin: Content -->

      <section id="content" class="table-layout animated fadeIn">
        <div class="tray tray-center">
          <div class="center-block">
            <div class="panel panel-primary panel-border top mb35">
              <div class="panel-heading">
                <span class="panel-title"> Add FAQ </span>
              </div>
              <div class="panel-body bg-light dark">
                <div class="tab-content pn br-n admin-form">
                  <div id="tab1_1" class="tab-pane active">
                    
                    {!! Form::open(['url'=>'/admin-panel/save-faq/'.$get_record['faq_id'].'','enctype' => 'multipart/form-data' , 'id' => 'validation' ] ) !!}
                    <div class="row">
                      <div class="col-md-12">
                          <div class="section">
                            <label for="faq_question" class="field-label" style="font-weight:600;" > Question </label>
                              <label for="faq_question" class="field prepend-icon">
                                
                                @if($get_record != "")
                                  {!! Form::text('faq_question',$get_record['faq_question'], array('class' => 'event-name gui-input br-light light focusss','placeholder' => '' )) !!}
                                @else
                                  {!! Form::text('faq_question','', array('class' => 'event-name gui-input br-light light focusss','placeholder' => '' )) !!}
                                @endif

                                <label for="store-currency" class="field-icon">
                                  <i class="fa fa-question"></i>
                                </label>
                              </label>
                          </div>
                        </div>
                        <div class="col-md-12">
                          <div class="section">
                            <label for="faq_answer" class="field-label" style="font-weight:600;" > Answer </label>
                              <label for="faq_answer" class="field prepend-icon">
                                
                                @if($get_record != "")
                                  {!! Form::textarea('faq_answer',$get_record['faq_answer'], array('class' => 'gui-textarea accc','placeholder' => '' )) !!}
                                @else
                                  {!! Form::textarea('faq_answer','', array('class' => 'gui-textarea accc','placeholder' => '' )) !!}
                                @endif

                                <label for="store-currency" class="field-icon">
                                  <i class="fa fa-comment-o"></i>
                                </label>
                              </label>
                          </div>
                        </div>
                      </div>

                      <div class="panel-footer text-right">
                        {!! Form::submit('Save', array('class' => 'button btn-primary', 'id' => 'maskedKey')) !!}
                        {!! Form::reset('Cancel', array('class' => 'button btn-primary', 'id' => 'maskedKey')) !!}
                      </div>
                    
                    {!! Form::close() !!}

                  </div>
                </div>
              </div>
            </div>
          </div>
        </div>
      </section>


<script type="text/javascript">

  jQuery(document).ready(function() {
  
    /* @custom validation method (smartCaptcha) 
    ------------------------------------------------------------------ */
    $("#validation").validate({

      /* @validation states + elements 
      ------------------------------------------- */

      errorClass: "state-error",
      validClass: "state-success",
      errorElement: "em",

      /* @validation rules 
      ------------------------------------------ */

      rules: {
        faq_question: {
          required: true
        },
        faq_answer: {
          required: true,
        },
      },

      /* @validation error messages 
      ---------------------------------------------- */

      // messages: {
      //   faq_question: {
      //     required: 'Enter your Question'
      //   },
      //   faq_answer: {
      //     required: 'Enter Answer',

      //   },
      //   faq_category: {
      //     required: 'Select Category'
      //   },
      // },

      /* @validation highlighting + error placement  
      ---------------------------------------------------- */

      highlight: function(element, errorClass, validClass) {
        $(element).closest('.field').addClass(errorClass).removeClass(validClass);
      },
      unhighlight: function(element, errorClass, validClass) {
        $(element).closest('.field').removeClass(errorClass).addClass(validClass);
      },
      errorPlacement: function(error, element) {
        if (element.is(":radio") || element.is(":checkbox")) {
          element.closest('.option-group').after(error);
        } else {
          error.insertAfter(element.parent());
        }
      }

    });

  });
  </script>



@endsection
