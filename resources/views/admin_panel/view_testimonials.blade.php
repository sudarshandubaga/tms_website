@extends('admin_panel/layout')
@section('content')

<style type="text/css">

.dt-panelfooter{
  display: none !important;
}
</style>

  <!-- Start: Content-Wrapper -->
    <section id="content_wrapper">
      <header id="topbar" style="margin-top:60px">
        <div class="topbar-left">
          <ol class="breadcrumb">
            <li class="crumb-active">
              <a href="{{ url('/admin-panel/add-testimonials') }}">Add Testimonials</a>
            </li>
            <li class="crumb-icon">
              <a href="{{ url('/admin-panel/dashboard') }}">
                <span class="glyphicon glyphicon-home"></span>
              </a>
            </li>
            <li class="crumb-link">
              <a href="{{ url('/admin-panel/dashboard') }}">Home</a>
            </li>
            <li class="crumb-trail">View Testimonials</li>
          </ol>
        </div>
      </header>

      <div class="" style="margin-top:10px;">
        <div class="col-md-12">
          <div class="panel panel-primary panel-border top mb50">  
            <div class="panel-heading">
              <div class="panel-title hidden-xs">
                <span class="glyphicon glyphicon-tasks"></span>View Testimonials</div>
            </div>

            <div class="panel-body pn">

              {!! Form::open(['name'=>'form']) !!}

              <div class="table-responsive">
                <table class="table admin-form theme-warning tc-checkbox-1 fs13" id="datatable">
                  <thead>
                    <tr class="bg-light">
                      <th style="width:90px !important;" class="text-left">
                        <label class="option block mn">
                          &nbsp;&nbsp;<input type="checkbox" id="check_all"> 
                          <span class="checkbox mn"></span>
                        </label>
                      </th>
                      <th class="">Image</th>
                      <th class="">Description</th>
                      <th class="">Person Name</th>
                      <!--<th class="w150">Created</th>-->
                      <th class="w150">Action</th>
                    </tr>
                  </thead>
                  <tbody>
             
                     @foreach($get_record as $get_records)  
                    <tr>
                      <td class="text-left" style="padding-left:18px">
                        <label class="option block mn">
                          <input type="checkbox" name="check[]" class="check" value="{{$get_records->testimonials_id}}">
                          <span class="checkbox mn"></span>
                        </label>
                      </td>
                      
                      <td class="" style="padding-left:20px" > 
                      @if ($get_records->testimonials_image == "")
                            No Image Available
                        @else 
                          {!! Html::image($get_records->testimonials_image, '', array('class' => 'data_photo')) !!}   
                        @endif
                      </td>
                      <td class="" style="padding-left:20px" > {{$get_records->testimonials_description}} </td>
                      <td class="" style="padding-left:20px" > {{$get_records->testimonials_user_name}} </td>
                      <!--<td class="" style="padding-left:20px" > {{date('d F Y',strtotime($get_records->our_school_created))}} </td>-->

                      <td class="text-left">
                        <div class="btn-group text-left">
                          <button type="button" class="btn {{ $get_records->testimonials_status == 1 ? 'btn-success' : 'btn-danger' }}  br2 btn-xs fs12 dropdown-toggle" data-toggle="dropdown" aria-expanded="false"> {{ $get_records->testimonials_status == 1 ? 'Active' : 'Deactive' }}
                            <span class="caret ml5"></span>
                          </button>
                          <ul class="dropdown-menu" role="menu" style="min-width:125px;">
                            <li>
                              <a href="{{ url('/admin-panel/add-testimonials') }}/{{$get_records->testimonials_id}}">Edit</a>
                            </li>
                            
                            <li class="divider"></li>
                            <li class="{{ $get_records->testimonials_status == 1 ? 'active' : '' }}">
                              <a href="{{ url('/admin-panel/testimonials-status') }}/{{$get_records->testimonials_id}}/1">Active</a>
                            </li>
                            <li class=" {{ $get_records->testimonials_status == 0 ? 'active' : '' }} ">
                              <a href="{{ url('/admin-panel/testimonials-status') }}/{{$get_records->testimonials_id}}/0">Deactive</a>
                            </li>
                          </ul>
                        </div>
                      </td>
                    </tr>

                    @endforeach
                  </tbody>
                </table>
              </div>
              {!! Form::close() !!}

            </div>

            <div class="panel-body pn">
              <div class="table-responsive">
                <table class="table admin-form theme-warning tc-checkbox-1 fs13">                                
                  <tbody>
                    <tr class="">
                      
                      <th  class="text-left">
                        <button type="button" class="btn btn-primary" onclick="go_delete()"><i class="glyphicon glyphicon-trash"></i> Delete Multiple </button>
                      </th> 
                    </tr>
                  </tbody>
                </table>
              </div>
            </div>

          </div>
        </div>
      </div>

@endsection


