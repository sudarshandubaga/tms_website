@extends('admin_panel/layout')

@section('content')

<style type="text/css">

.dt-panelfooter{
  display: none !important;
}
</style>

  <!-- Start: Content-Wrapper -->
    <section id="content_wrapper">
      <header id="topbar" style="margin-top:60px">
        <div class="topbar-left">
          <ol class="breadcrumb">
            <li class="crumb-active">
              <a href="{{ url('/admin-panel/admission-points') }}">Admission Points</a>
            </li>
            <li class="crumb-icon">
              <a href="{{ url('/admin-panel/dashboard') }}">
                <span class="glyphicon glyphicon-home"></span>
              </a>
            </li>
            <li class="crumb-link">
              <a href="{{ url('/admin-panel/dashboard') }}">Home</a>
            </li>
            <li class="crumb-trail">Admission Points</li>
          </ol>
        </div>
      </header>

      <div class="" style="margin-top:10px;">
        <div class="col-md-12">
          <div class="panel panel-primary panel-border top mb50">  
            <div class="panel-heading">
              <div class="panel-title hidden-xs">
                <span class="glyphicon glyphicon-tasks"></span>Admission Points</div>
            </div>

            <div class="panel-body bg-light dark">
                <div class="tab-content pn br-n admin-form">
                  <div id="tab1_1" class="tab-pane active">
                    {!! Form::open(['url'=>'admin-panel/save-admission-points/'.$get_record['admission_point_id'].'' , 'enctype' => 'multipart/form-data' , 'id' => 'validation' ] ) !!}
                    <div class="row">
                      <div class="col-md-12">
                        <div class="col-md-12">
                          <div class="section">
                            <label for="admission_point" class="field-label" style="font-weight:600;" > Point </label>
                              <label for="admission_point" class="field prepend-icon">
                                
                                @if($get_record != "")
                                  {!! Form::textarea('admission_point',$get_record['admission_point'], array('class' => 'gui-textarea accc1','placeholder' => '' )) !!}
                                @else
                                  {!! Form::textarea('admission_point','', array('class' => 'gui-textarea accc1','placeholder' => '' )) !!}
                                @endif

                                <label for="store-currency" class="field-icon">
                                  <i class="fa fa-crosshairs"></i>
                                </label>
                              </label>
                          </div>
                        </div>
                      </div>
                    </div>

                    <div class="panel-footer text-right">
                      {!! Form::submit('Save', array('class' => 'button btn-primary', 'id' => 'maskedKey')) !!}
                      {!! Form::reset('Cancel', array('class' => 'button btn-primary', 'id' => 'maskedKey')) !!}
                    </div>
                    {!! Form::close() !!}
                  </div>
                </div>
              </div>

            <div class="panel-menu admin-form theme-primary">
              <div class="row">
                {!! Form::open(['url'=>'admin-panel/admission-points']) !!}
                  <div class="col-md-3">
                    <label for="search_admission_point" class="field prepend-icon">
                      {!! Form::text('search_admission_point', $search_admission_point , array('class' => 'form-control product','placeholder' => 'Search By Point', 'autocomplete' => 'off' )) !!}
                      <label for="search_admission_point" class="field-icon">
                        <i class="fa fa-search" style="padding-top: 12px;"></i>
                      </label>
                    </label>
                  </div>
                                    
                  <div class="col-md-1">
                    <button type="submit" name="search" class="button btn-primary"> Search </button>
                  </div>
                {!! Form::close() !!}

                  <div class="col-md-1 pull-right">
                    <a href="{{ url('admin-panel/admission-points') }}">{!! Form::submit('Default', array('class' => 'btn btn-primary', 'id' => 'maskedKey')) !!}</a>
                  </div>
              </div>
            </div>
            
            <div class="panel-body pn">

              {!! Form::open(['name'=>'form']) !!}

              <div class="table-responsive">
                <table class="table admin-form theme-warning tc-checkbox-1 fs13" id="datatable">
                  <thead>
                    <tr class="bg-light">
                      <th style="width:90px !important;" class="text-left">
                        <label class="option block mn">
                          <input type="checkbox" id="check_all"> 
                          <span class="checkbox mn"></span>
                          Select All
                        </label>
                      </th>
                      <th class="">Admission Point</th>
                      <th class="w100">Actions</th>
                      <th class="w150">Created</th>
                    </tr>
                  </thead>
                  <tbody>
                    
                     @foreach($admission_point as $admission_points)  

                    <tr>
                      <td class="text-left" style="padding-left:18px">
                        <label class="option block mn">
                          <input type="checkbox" name="check[]" class="check" value="{{$admission_points->admission_point_id}}">
                          <span class="checkbox mn"></span>
                        </label>
                      </td>                     
                      <td class="text-capitalize" style="padding-left:20px" > {{$admission_points->admission_point}} </td>
                      <td class="" style="padding-left:20px" > 

                        <div class="btn-group text-left">
                          <button type="button" class="btn {{ $admission_points->admission_point_status == 1 ? 'btn-success' : ''}} {{ $admission_points->admission_point_status == 0 ? 'btn-danger' : '' }} br2 btn-xs fs12 dropdown-toggle" data-toggle="dropdown" aria-expanded="false"> {{ $admission_points->admission_point_status == 1 ? 'Active' : '' }} {{ $admission_points->admission_point_status == 0 ? 'Deactive' : '' }} 
                          <span class="caret ml5"></span>
                          </button>
                          <ul class="dropdown-menu" role="menu" style="min-width:125px; z-index:99999;">
                            <li>
                              <a href="{{ url('/admin-panel/admission-points') }}/{{$admission_points->admission_point_id}}">Edit</a>
                            </li>
                            
                            <li class="divider"></li>
                            <li class="{{ $admission_points->admission_point_status == 1 ? 'active' : '' }}">
                                <a href="{{ url('/admin-panel/admission-points-status') }}/{{$admission_points->admission_point_id}}/1"> Active </a>
                            </li>
                            <li class="{{ $admission_points->admission_point_status == 0 ? 'active' : '' }}">
                              <a href="{{ url('/admin-panel/admission-points-status') }}/{{$admission_points->admission_point_id}}/0"> Deactive</a>
                            </li>
                          </ul>
                        </div>
                      </td>     
                      <td class="" style="padding-left:20px" > {{date('d F Y',strtotime($admission_points->admission_point_created))}} </td>
                    </tr>

                    @endforeach
                  </tbody>
                </table>
              </div>
              {!! Form::close() !!}

            </div>

            <div class="panel-body pn">
              <div class="table-responsive">
                <table class="table admin-form theme-warning tc-checkbox-1 fs13">                                
                  <tbody>
                    <tr class="">
                      <th  class="text-left">
                        <button type="button" class="btn btn-primary" onclick="go_delete()"><i class="glyphicon glyphicon-trash"></i> Delete Multiple </button>
                      </th>    
                      <th  class="text-right">
                        {{ $admission_point->links() }}
                      </th>
                    </tr>
                  </tbody>
                </table>
              </div>
            </div>

          </div>
        </div>
      </div>

<script type="text/javascript">
  jQuery(document).ready(function() {
  
    /* @custom validation method (smartCaptcha) 
    ------------------------------------------------------------------ */
    $("#validation").validate({

      /* @validation states + elements 
      ------------------------------------------- */

      errorClass: "state-error",
      validClass: "state-success",
      errorElement: "em",

      /* @validation rules 
      ------------------------------------------ */

      rules: {
        admission_point: {
          required: true,
        },
       
      },

      /* @validation error messages 
      ---------------------------------------------- */

      // messages: {
      //   role_name: {
      //     required: 'Enter Role Name'
      //   },
      //    role_prefix: {
      //     required: 'Enter Role Prefix'
      //   },
      // },

      /* @validation highlighting + error placement  
      ---------------------------------------------------- */

      highlight: function(element, errorClass, validClass) {
        $(element).closest('.field').addClass(errorClass).removeClass(validClass);
      },
      unhighlight: function(element, errorClass, validClass) {
        $(element).closest('.field').removeClass(errorClass).addClass(validClass);
      },
      errorPlacement: function(error, element) {
        if (element.is(":radio") || element.is(":checkbox")) {
          element.closest('.option-group').after(error);
        } else {
          error.insertAfter(element.parent());
        }
      }

    });

  });
</script>

@endsection
