@extends('admin_panel/layout')

@section('content')
<!-- Start: Content-Wrapper -->
  <section id="content_wrapper">

  	<header id="topbar">
	    <div class="topbar-left">
	      <ol class="breadcrumb">
	        <li class="crumb-active">
	          <a href="{{ url('/admin-panel/dashboard') }}">Dashboard</a>
	        </li>
	        <li class="crumb-icon">
	          <a href="{{ url('/admin-panel/dashboard') }}">
	            <span class="glyphicon glyphicon-home"></span>
	          </a>
	        </li>
	        <li class="crumb-link">
	          <a href="{{ url('/admin-panel/dashboard') }}">Home</a>
	        </li>
	        <li class="crumb-trail">Dashboard</li>
	      </ol>
	    </div>
	</header>
     
      <section id="content" class="animated fadeIn">
    		<div class="content-header" style="margin-top:200px;">
    		  <h1 class="dash_wel"> Welcome to <b class="text-primary" style="color:#0095da">Admin Control Panel</b></h1>
    		  <p class="dash_imgss">
	    		 <!--  @if($account->account_logo != "")
	                  {!! Html::image($account->account_logo) !!}
	              @else
	                  {!! Html::image('public/admin/img/dummy.png') !!}
	              @endif -->
	           </p>
    		</div>
      </section>
  </section>
<!-- End: Content -->	  
@endsection
