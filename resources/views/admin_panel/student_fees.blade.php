@extends('admin_panel/layout')

@section('content')

<style type="text/css">

.dt-panelfooter{
  display: none !important;
}
</style>

  <!-- Start: Content-Wrapper -->
    <section id="content_wrapper">
      <header id="topbar" style="margin-top:60px">
        <div class="topbar-left">
          <ol class="breadcrumb">
            <li class="crumb-active">
              <a href="{{ url('/admin-panel/student-fees') }}">Student Fees</a>
            </li>
            <li class="crumb-icon">
              <a href="{{ url('/admin-panel/dashboard') }}">
                <span class="glyphicon glyphicon-home"></span>
              </a>
            </li>
            <li class="crumb-link">
              <a href="{{ url('/admin-panel/dashboard') }}">Home</a>
            </li>
            <li class="crumb-trail">Student Fees</li>
          </ol>
        </div>
      </header>

      <div class="" style="margin-top:10px;">
        <div class="col-md-12">
          <div class="panel panel-primary panel-border top mb35">  
            <div class="panel-heading">
              <div class="panel-title hidden-xs">
                <span class="glyphicon glyphicon-tasks"></span>Student Fees</div>
            </div>

            <!-- <div class="panel-menu admin-form theme-primary">
              <div class="row">
                {!! Form::open(['url'=>'admin-panel/student-fees']) !!}
                  <div class="col-md-3">
                    <label for="name" class="field prepend-icon">
                      {!! Form::text('contact_enquiry_name', $contact_enquiry_name , array('class' => 'form-control product','placeholder' => 'Search By Name', 'autocomplete' => 'off' )) !!}
                      <label for="name" class="field-icon">
                        <i class="fa fa-search" style="padding-top: 12px;"></i>
                      </label>
                    </label>
                  </div>
                  <div class="col-md-3">
                    <label for="email" class="field prepend-icon">
                      {!! Form::text('contact_enquiry_email', $contact_enquiry_email , array('class' => 'form-control product','placeholder' => 'Search By Email', 'autocomplete' => 'off' )) !!}
                      <label for="email" class="field-icon">
                        <i class="fa fa-search" style="padding-top: 12px;"></i>
                      </label>
                    </label>
                  </div>
                  <div class="col-md-3">
                    <label for="number" class="field prepend-icon">
                      {!! Form::text('contact_enquiry_number', $contact_enquiry_number , array('class' => 'form-control product','placeholder' => 'Search By Number', 'autocomplete' => 'off' )) !!}
                      <label for="email" class="field-icon">
                        <i class="fa fa-search" style="padding-top: 12px;"></i>
                      </label>
                    </label>
                  </div>
                  
                  <div class="col-md-1">
                    <button type="submit" name="search" class="button btn-primary"> Search </button>
                  </div>
                {!! Form::close() !!}

                  <div class="col-md-1 pull-right">
                     <a href="{{ url('admin-panel/contact-enquiry') }}"> {!! Form::submit('Default', array('class' => 'btn btn-primary', 'id' => 'maskedKey')) !!} </a>
                  </div>
              </div>
            </div> -->
            
            <div class="panel-body pn">

              {!! Form::open(['name'=>'form']) !!}

              <div class="table-responsive">
                <table class="table admin-form theme-warning tc-checkbox-1 fs13" id="datatable">
                  <thead>
                    <tr class="bg-light">
                      <th></th>
                      <th class="">Ref. No.</th>
                      <th class="">Transaction ID</th>
                      <th class="">Name</th>
                      <th class="">Email</th>
                      <th class="">Phone</th>
                      <th class="">Fees</th>
                      <th class="w150">Status</th>
                      <th class="w150">Transaction Date</th>
                    </tr>
                  </thead>
                  <tbody>

                     @foreach($get_record as $get_records)  

                    <tr>
                      <td></td>
                      <td class="text-left text-capitalize" style="padding-left:20px" > {{$get_records->sr_no}} </td>
                      <td class="" style="padding-left:20px" > {{$get_records->transaction_id}} </td>
                      <td class="text-capitalize" style="padding-left:20px" > {{$get_records->student_name}} </td>
                      <td class="text-capitalize" style="padding-left:20px" > {{$get_records->email_id}} </td>
                      <td class="text-capitalize" style="padding-left:20px" > {{$get_records->mobile_no}} </td>
                      <td class="text-capitalize" style="padding-left:20px" > {{$get_records->fee_amount}} </td>
                      <td class="" style="padding-left:20px" > 
    
                      <div class="btn-group text-left">
                          <button type="button" class="btn {{ $get_records->transaction_status == 0 ? 'btn-info' : ''}} {{ $get_records->transaction_status == 1 ? 'btn-success' : '' }}  br2 btn-xs fs12 dropdown-toggle" data-toggle="dropdown" aria-expanded="false"> {{ $get_records->transaction_status == 0 ? 'Transaction Failed' : '' }} {{ $get_records->transaction_status == 1 ? 'Transaction Completed' : '' }}
                            <span class="caret ml5"></span>
                          </button>
                          
                        </div>
                      </td>

                      

                      <td class="" style="padding-left:20px" > {{date('d F Y',strtotime($get_records->transaction_created))}} </td>
                           
                    </tr>

                    @endforeach
                  </tbody>
                </table>
              </div>
              {!! Form::close() !!}

            </div>

            

          </div>
        </div>
      </div>

@endsection
