@extends('admin_panel/layout')

@section('content')

  <!-- Start: Content-Wrapper -->
    <section id="content_wrapper">
      <header id="topbar">
        <div class="topbar-left">
          <ol class="breadcrumb">
            <li class="crumb-active">
              <a href="{{ url('/admin-panel/view-school-video-gallery')}}">View School Video Gallery</a>
            </li>
            <li class="crumb-icon">
              <a href="{{ url('/admin-panel/dashboard') }}">
                <span class="glyphicon glyphicon-home"></span>
              </a>
            </li>
            <li class="crumb-link">
              <a href="{{ url('/admin-panel/dashboard') }}">Home</a>
            </li>
            <li class="crumb-trail">Add School Video Gallery</li>
          </ol>
        </div>
      </header>

      <!-- Begin: Content -->
      <section id="content" class="table-layout animated fadeIn">
        <div class="tray tray-center">
          <div class="center-block">
            <div class="panel panel-primary panel-border top mb35">
              <div class="panel-heading">
                <span class="panel-title"> Add School Gallery </span>
              </div>
              <div class="panel-body bg-light dark">
                <div class="tab-content pn br-n admin-form">
                  <div id="tab1_1" class="tab-pane active">

                    {!! Form::open(['url'=>'/admin-panel/save-school-video-gallery/'.$get_record['school_gallery_id'].'' , 'enctype' => 'multipart/form-data' , 'id' => 'validation' ] ) !!}
                    <div>
                      <div class="row">
                          <div class="col-md-12">
                              <div class="col-md-6">
                                <div class="section">
                                  <label for="school_gallery_title" class="field-label" style="font-weight:600;" > Title </label>
                                    <label for="school_gallery_title" class="field prepend-icon">
                                      
                                      @if($get_record != "")
                                        {!! Form::text('school_gallery_title',$get_record['school_gallery_title'], array('class' => 'event-name gui-input br-light light focusss','placeholder' => '' )) !!}
                                      @else
                                        {!! Form::text('school_gallery_title','', array('class' => 'event-name gui-input br-light light focusss','placeholder' => '' )) !!}
                                      @endif
                                      <label for="store-currency" class="field-icon">
                                        <i class="fa fa-crosshairs"></i>
                                      </label>
                                    </label>
                                </div>
                              </div>
                          </div>
                          
                          
                          
                          <div class="col-md-12">
                              <div class="col-md-6">
                                <div class="section">
                                  <label for="school_gallery_link" class="field-label" style="font-weight:600;" > You Tube Link </label>
                                    <label for="school_gallery_link" class="field prepend-icon">
                                      
                                      @if($get_record != "")
                                        {!! Form::text('school_gallery_link',$get_record['school_gallery_link'], array('class' => 'event-name gui-input br-light light focusss','placeholder' => '' )) !!}
                                      @else
                                        {!! Form::text('school_gallery_link','', array('class' => 'event-name gui-input br-light light focusss','placeholder' => '' )) !!}
                                      @endif
                                      <label for="store-currency" class="field-icon">
                                        <i class="fa fa-crosshairs"></i>
                                      </label>
                                    </label>
                                </div>
                              </div>
                          </div>

                          <div class="col-md-12">
                              <div class="col-md-6">
                                <div class="section">
                                  <label for="school_gallery_link" class="field-label" style="font-weight:600;" > Type </label>
                                    <label for="school_gallery_link" class="field prepend-icon">
                                      
                                      <select name="type" class="event-name gui-input br-light light focusss">
                                        <option value="normal" @if(@$get_record['type'] == 'normal') selected @endif>Video</option>
                                        <option value="tour" @if(@$get_record['type'] == 'tour') selected @endif>Virtual Tour</option>
                                      </select>

                                      <label for="store-currency" class="field-icon">
                                        <i class="fa fa-crosshairs"></i>
                                      </label>
                                    </label>
                                </div>
                              </div>
                          </div>
                          
                          
                          

                         
                    
                          <div class="clearfix"></div>
                          <div class="panel-footer text-right">
                            {!! Form::submit('Save', array('class' => 'button btn-primary', 'id' => 'maskedKey')) !!}
                            {!! Form::reset('Cancel', array('class' => 'button btn-primary', 'id' => 'maskedKey')) !!}
                          </div>
                       {!! Form::close() !!}
                    </div>
                </div>
              </div>
            </div>
          </div>
        </div>
      </section>

<script type="text/javascript">

  jQuery(document).ready(function() {

    jQuery.validator.addMethod("lettersonly", function(value, element) 
    {
    return this.optional(element) || /^[a-z," "]+$/i.test(value);
    }, "This field contains alphabets only");
    
    /* @custom validation method (smartCaptcha) 
    ------------------------------------------------------------------ */
    $("#validation").validate({

      /* @validation states + elements 
      ------------------------------------------- */

      errorClass: "state-error",
      validClass: "state-success",
      errorElement: "em",

      /* @validation rules 
      ------------------------------------------ */

      rules: {
        school_gallery_title: {
          required: true,
          lettersonly: true,
        },
        school_gallery_link: {
          required: true,
        },
        school_gallery_image: {
          extension: 'jpeg,jpg,png',
        },
      },

      /* @validation error messages 
      ---------------------------------------------- */

       messages: {
          school_gallery_image: {
            extension: 'Image Should be in .jpg,.jpeg and .png format only'
          }

      //   testimonial_name: {
      //     required: 'Enter your name'
      //   },
      //   testimonial_city: {
      //     required: 'Enter Country'
      //   },
      //   testimonial_description: {
      //     required: 'Enter Description'
      //   },  
      //   testimonial_job_profile: {
      //     required: 'Enter Job Profile'
      //   },
      //   testimonial_image: {
      //     required: 'Note :- Select Image also'
      //   }
       },

      /* @validation highlighting + error placement  
      ---------------------------------------------------- */

      highlight: function(element, errorClass, validClass) {
        $(element).closest('.field').addClass(errorClass).removeClass(validClass);
      },
      unhighlight: function(element, errorClass, validClass) {
        $(element).closest('.field').removeClass(errorClass).addClass(validClass);
      },
      errorPlacement: function(error, element) {
        if (element.is(":radio") || element.is(":checkbox")) {
          element.closest('.option-group').after(error);
        } else {
          error.insertAfter(element.parent());
        }
      }

    });

  });
  </script>

@endsection
