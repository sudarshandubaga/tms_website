@extends('admin_panel/layout')

@section('content')

  <!-- Start: Content-Wrapper -->
    <section id="content_wrapper">
      <header id="topbar">
        <div class="topbar-left">
          <ol class="breadcrumb">
            <li class="crumb-active">
              <a href="{{ url('/admin-panel/view-tc')}}">View TC</a>
            </li>
            <li class="crumb-icon">
              <a href="{{ url('/admin-panel/dashboard') }}">
                <span class="glyphicon glyphicon-home"></span>
              </a>
            </li>
            <li class="crumb-link">
              <a href="{{ url('/admin-panel/dashboard') }}">Home</a>
            </li>
            <li class="crumb-trail">Add TC</li>
          </ol>
        </div>
      </header>

      <!-- Begin: Content -->
      <section id="content" class="table-layout animated fadeIn">
        <div class="tray tray-center">
          <div class="center-block">
            <div class="panel panel-primary panel-border top mb35">
              <div class="panel-heading">
                <span class="panel-title"> Add TC </span>
              </div>
              <div class="panel-body bg-light dark">
                <div class="tab-content pn br-n admin-form">
                  <div id="tab1_1" class="tab-pane active">

                    {!! Form::open(['url'=>'/admin-panel/save_tc/', 'enctype' => 'multipart/form-data' , 'id' => 'validation' ] ) !!}
                    <div>
                      <div class="row">
                          <div class="col-md-9">
                              <div class="col-md-12">
                                <div class="section">
                                  <label for="tc_name" class="field-label" style="font-weight:600;" > Student Name </label>
                                    <label for="tc_name" class="field prepend-icon">
                                        {!! Form::text('record[tc_name]','', array('class' => 'event-name gui-input br-light light focusss','placeholder' => '' )) !!}
                                      <label for="store-currency" class="field-icon">
                                        <i class="fa fa-crosshairs"></i>
                                      </label>
                                    </label>
                                </div>
                              </div>
                              
                              <div class="col-md-6">
                                <div class="section">
                                  <label for="s_number" class="field-label" style="font-weight:600;" > Sr. Number </label>
                                    <label for="s_number" class="field prepend-icon">
                                        {!! Form::text('record[s_number]','', array('class' => 'event-name gui-input br-light light focusss','placeholder' => '' )) !!}
                                      <label for="store-currency" class="field-icon">
                                        <i class="fa fa-crosshairs"></i>
                                      </label>
                                    </label>
                                </div>
                              </div>
                              
                              <div class="col-md-6">
                                <div class="section">
                                  <label for="admission_number" class="field-label" style="font-weight:600;" > Admission Number </label>
                                    <label for="admission_number" class="field prepend-icon">
                                        {!! Form::text('record[admission_number]','', array('class' => 'event-name gui-input br-light light focusss','placeholder' => '' )) !!}
                                      <label for="store-currency" class="field-icon">
                                        <i class="fa fa-crosshairs"></i>
                                      </label>
                                    </label>
                                </div>
                              </div>
                              
                      
                          </div>

                          <div class="col-md-3" style="margin-top:25px;">
                              
                              <div class="section">
                                  <label for="admission_number" class="field-label" style="font-weight:600;" > Select TC </label>
                                    <label for="admission_number" class="field">
                                           <span class="button btn-system btn-file btn-block ph5" style="margin-bottom:10px">
                                            <span class="fileupload-new" >Select TC</span>
                                            <input type="file" name="tc_file" id="tc_file" required=""/>
                                          </span>
                                    </label>
                                </div>
                            
                          </div>
                    
                          <div class="clearfix"></div>
                          <div class="panel-footer text-right">
                            {!! Form::submit('Save', array('class' => 'button btn-primary', 'id' => 'maskedKey')) !!}
                            {!! Form::reset('Cancel', array('class' => 'button btn-primary', 'id' => 'maskedKey')) !!}
                          </div>
                       {!! Form::close() !!}
                    </div>
                </div>
              </div>
            </div>
          </div>
        </div>
      </section>

<script type="text/javascript">

  jQuery(document).ready(function() {

    jQuery.validator.addMethod("lettersonly", function(value, element) 
    {
    return this.optional(element) || /^[a-z," "]+$/i.test(value);
    }, "This field contains alphabets only");
    
    /* @custom validation method (smartCaptcha) 
    ------------------------------------------------------------------ */
    $("#validation").validate({

      /* @validation states + elements 
      ------------------------------------------- */

      errorClass: "state-error",
      validClass: "state-success",
      errorElement: "em",

      /* @validation rules 
      ------------------------------------------ */

      rules: {
        tc_name: {
          required: true,
          lettersonly: true,
        },
        s_number: {
          required: true,
        },
        admission_number: {
          required: true,
        },
        
        tc_file: {
          extension: 'jpeg,jpg,png,pdf',
        },
      },

      /* @validation error messages 
      ---------------------------------------------- */

       messages: {
          school_gallery_image: {
            extension: 'Image Should be in .jpg,.jpeg and .png format only'
          }

      //   testimonial_name: {
      //     required: 'Enter your name'
      //   },
      //   testimonial_city: {
      //     required: 'Enter Country'
      //   },
      //   testimonial_description: {
      //     required: 'Enter Description'
      //   },  
      //   testimonial_job_profile: {
      //     required: 'Enter Job Profile'
      //   },
      //   testimonial_image: {
      //     required: 'Note :- Select Image also'
      //   }
       },

      /* @validation highlighting + error placement  
      ---------------------------------------------------- */

      highlight: function(element, errorClass, validClass) {
        $(element).closest('.field').addClass(errorClass).removeClass(validClass);
      },
      unhighlight: function(element, errorClass, validClass) {
        $(element).closest('.field').removeClass(errorClass).addClass(validClass);
      },
      errorPlacement: function(error, element) {
        if (element.is(":radio") || element.is(":checkbox")) {
          element.closest('.option-group').after(error);
        } else {
          error.insertAfter(element.parent());
        }
      }

    });

  });
  </script>

@endsection
