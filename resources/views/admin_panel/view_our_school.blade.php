@extends('admin_panel/layout')
@section('content')

<style type="text/css">

.dt-panelfooter{
  display: none !important;
}
</style>

  <!-- Start: Content-Wrapper -->
    <section id="content_wrapper">
      <header id="topbar" style="margin-top:60px">
        <div class="topbar-left">
          <ol class="breadcrumb">
            <li class="crumb-active">
              <a href="{{ url('/admin-panel/add-our-school') }}">Add Our School</a>
            </li>
            <li class="crumb-icon">
              <a href="{{ url('/admin-panel/dashboard') }}">
                <span class="glyphicon glyphicon-home"></span>
              </a>
            </li>
            <li class="crumb-link">
              <a href="{{ url('/admin-panel/dashboard') }}">Home</a>
            </li>
            <li class="crumb-trail">View Our School</li>
          </ol>
        </div>
      </header>

      <div class="" style="margin-top:10px;">
        <div class="col-md-12">
          <div class="panel panel-primary panel-border top mb50">  
            <div class="panel-heading">
              <div class="panel-title hidden-xs">
                <span class="glyphicon glyphicon-tasks"></span>View Our School</div>
            </div>

            <div class="panel-menu admin-form theme-primary">
              <div class="row">
                
                {!! Form::open(['url'=>'/admin-panel/view-our-school']) !!}

                  <div class="col-md-3">
                    <label for="name" class="field prepend-icon">
                      {!! Form::text('our_school_title', $our_school_title , array('class' => 'form-control product','placeholder' => 'Search By Title', 'autocomplete' => 'off' )) !!}
                      <label for="name" class="field-icon">
                        <i class="fa fa-search" style="padding-top: 12px;"></i>
                      </label>
                    </label>
                  </div>
                  <div class="col-md-3">
                    <label for="name" class="field prepend-icon">
                      {!! Form::text('our_school_description', $our_school_description , array('class' => 'form-control product','placeholder' => 'Search By Description', 'autocomplete' => 'off' )) !!}
                      <label for="name" class="field-icon">
                        <i class="fa fa-search" style="padding-top: 12px;"></i>
                      </label>
                    </label>
                  </div>
                   
                  <div class="col-md-1">
                    <button type="submit" name="search" class="button btn-primary"> Search </button>
                  </div>

                {!! Form::close() !!}


                  <div class="col-md-1 pull-right">
                     <a href="{{ url('admin-panel/view-our-school') }}"> {!! Form::submit('Default', array('class' => 'btn btn-primary', 'id' => 'maskedKey')) !!} </a>
                  </div>

                {!! Form::close() !!}
              </div>
            </div>
            
            <div class="panel-body pn">

              {!! Form::open(['name'=>'form']) !!}

              <div class="table-responsive">
                <table class="table admin-form theme-warning tc-checkbox-1 fs13" id="datatable">
                  <thead>
                    <tr class="bg-light">
                      <th style="width:90px !important;" class="text-left">
                        <label class="option block mn">
                          <input type="checkbox" id="check_all"> 
                          <span class="checkbox mn"></span>
                          Select All
                        </label>
                      </th>
                      <th class="">Title</th>
                      <th class="">Description</th>
                      <th class="w150">Created</th>
                      <th class="w150">Action</th>
                    </tr>
                  </thead>
                  <tbody>
             
                     @foreach($get_record as $get_records)  
                    <tr>
                      <td class="text-left" style="padding-left:18px">
                        <label class="option block mn">
                          <input type="checkbox" name="check[]" class="check" value="{{$get_records->our_school_id}}">
                          <span class="checkbox mn"></span>
                        </label>
                      </td>
                      
                      <td class="" style="padding-left:20px" > {{$get_records->our_school_title}} </td>
                      <td class="" style="padding-left:20px" > {{$get_records->our_school_description}} </td>
                      <td class="" style="padding-left:20px" > {{date('d F Y',strtotime($get_records->our_school_created))}} </td>

                      <td class="text-left">
                        <div class="btn-group text-left">
                          <button type="button" class="btn {{ $get_records->our_school_status == 1 ? 'btn-success' : 'btn-danger' }}  br2 btn-xs fs12 dropdown-toggle" data-toggle="dropdown" aria-expanded="false"> {{ $get_records->our_school_status == 1 ? 'Active' : 'Deactive' }}
                            <span class="caret ml5"></span>
                          </button>
                          <ul class="dropdown-menu" role="menu" style="min-width:125px;">
                            <li>
                              <a href="{{ url('/admin-panel/add-our-school') }}/{{$get_records->our_school_id}}">Edit</a>
                            </li>
                            
                            <li class="divider"></li>
                            <li class="{{ $get_records->our_school_status == 1 ? 'active' : '' }}">
                              <a href="{{ url('/admin-panel/our-school-status') }}/{{$get_records->our_school_id}}/1">Active</a>
                            </li>
                            <li class=" {{ $get_records->our_school_status == 0 ? 'active' : '' }} ">
                              <a href="{{ url('/admin-panel/our-school-status') }}/{{$get_records->our_school_id}}/0">Deactive</a>
                            </li>
                          </ul>
                        </div>
                      </td>
                    </tr>

                    @endforeach
                  </tbody>
                </table>
              </div>
              {!! Form::close() !!}

            </div>

            <div class="panel-body pn">
              <div class="table-responsive">
                <table class="table admin-form theme-warning tc-checkbox-1 fs13">                                
                  <tbody>
                    <tr class="">
                      
                      <th  class="text-left">
                        <button type="button" class="btn btn-primary" onclick="go_delete()"><i class="glyphicon glyphicon-trash"></i> Delete Multiple </button>
                      </th>    
                      <th  class="text-right">
                        {{ $get_record->links() }}
                      </th>
                    </tr>
                  </tbody>
                </table>
              </div>
            </div>

          </div>
        </div>
      </div>

@endsection


