@extends('admin_panel/layout')
@section('content')

  <!-- Start: Content-Wrapper -->
    <section id="content_wrapper">
      <header id="topbar">
        <div class="topbar-left">
          <ol class="breadcrumb">
            <li class="crumb-active">
              <a href="{{ url('/admin-panel/view-download')}}">View Downloads</a>
            </li>
            <li class="crumb-icon">
              <a href="{{ url('/admin-panel/dashboard') }}">
                <span class="glyphicon glyphicon-home"></span>
              </a>
            </li>
            <li class="crumb-link">
              <a href="{{ url('/admin-panel/dashboard') }}">Home</a>
            </li>
            <li class="crumb-trail">Add Downloads</li>
          </ol>
        </div>
      </header>

      <!-- Begin: Content -->
      <section id="content" class="table-layout animated fadeIn">
        <div class="tray tray-center">
          <div class="center-block">
            <div class="panel panel-primary panel-border top mb35">
              <div class="panel-heading">
                <span class="panel-title"> Add Downloads </span>
              </div>
              <div class="panel-body bg-light dark">
                <div class="tab-content pn br-n admin-form">
                  <div id="tab1_1" class="tab-pane active">

                    {!! Form::open(['url'=>'/admin-panel/save-download/'.$get_record['download_id'].'' , 'enctype' => 'multipart/form-data' , 'id' => 'validation' ] ) !!}
                    <div>
                      <div class="row">
                          <div class="col-md-12">
                              <div class="col-md-12">
                                <div class="section">
                                  <label for="download_title" class="field-label" style="font-weight:600;" > Title </label>
                                    <label for="download_title" class="field prepend-icon">
                                      
                                      @if($get_record != "")
                                        {!! Form::text('download_title',$get_record['download_title'], array('class' => 'event-name gui-input br-light light focusss','placeholder' => '' )) !!}
                                      @else
                                        {!! Form::text('download_title','', array('class' => 'event-name gui-input br-light light focusss','placeholder' => '' )) !!}
                                      @endif
                                      <label for="store-currency" class="field-icon">
                                        <i class="fa fa-crosshairs"></i>
                                      </label>
                                    </label>
                                </div>
                              </div>

                              <div class="col-md-8">
                                <div class="section">
                                  <label for="download_file" class="field-label" style="font-weight:600;" > Upload File </label>
                                    <label for="download_file" class="field prepend-icon">
                                      <input type="file" id="download_file" name="download_file" style="padding: 7px;border: 1px solid #ccc;width: 100%;" aria-invalid="false"/>
                                    </label>
                                </div>
                              </div>

                              @if($get_record['download_file'] != "")

                                @php  
                                  $doc_image = explode("/",$get_record['download_file']);
                                @endphp
                                <div class="col-md-4" style="margin-top:19px;"> 
                                  <a target="_blank" href="{{url($get_record['download_file'])}}"> 
                                    <input type="button" class="button btn-primary" value="View" > 
                                  </a> 
                                </div>
                              @endif
                          </div>

                          <div class="clearfix"></div>
                          <div class="panel-footer text-right">
                            {!! Form::submit('Save', array('class' => 'button btn-primary', 'id' => 'maskedKey')) !!}
                            {!! Form::reset('Cancel', array('class' => 'button btn-primary', 'id' => 'maskedKey')) !!}
                          </div>
                       {!! Form::close() !!}
                    </div>
                </div>
              </div>
            </div>
          </div>
        </div>
      </section>

<script type="text/javascript">

  
    
    /* @custom validation method (smartCaptcha) 
    ------------------------------------------------------------------ */
    $("#validation").validate({

      /* @validation states + elements 
      ------------------------------------------- */

      errorClass: "state-error",
      validClass: "state-success",
      errorElement: "em",

      /* @validation rules 
      ------------------------------------------ */

      rules: {
        download_title: {
          required: true,
          lettersonly: true,
        },
        download_file: {
          extension: 'pdf',
        },
      },

      /* @validation error messages 
      ---------------------------------------------- */

       messages: {
          download_file: {
            extension: 'File Should be in pdf format only'
          }


      //   testimonial_name: {
      //     required: 'Enter your name'
      //   },
      //   testimonial_city: {
      //     required: 'Enter Country'
      //   },
      //   testimonial_description: {
      //     required: 'Enter Description'
      //   },  
      //   testimonial_job_profile: {
      //     required: 'Enter Job Profile'
      //   },
      //   testimonial_image: {
      //     required: 'Note :- Select Image also'
      //   }
       },

      /* @validation highlighting + error placement  
      ---------------------------------------------------- */

      highlight: function(element, errorClass, validClass) {
        $(element).closest('.field').addClass(errorClass).removeClass(validClass);
      },
      unhighlight: function(element, errorClass, validClass) {
        $(element).closest('.field').removeClass(errorClass).addClass(validClass);
      },
      errorPlacement: function(error, element) {
        if (element.is(":radio") || element.is(":checkbox")) {
          element.closest('.option-group').after(error);
        } else {
          error.insertAfter(element.parent());
        }
      }

    });

  });
  </script>

@endsection
