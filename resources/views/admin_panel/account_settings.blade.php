@extends('admin_panel/layout')

@section('content')

  <!-- Start: Content-Wrapper -->
    <section id="content_wrapper">
      <header id="topbar">
        <div class="topbar-left">
          <ol class="breadcrumb">
            <li class="crumb-active">
              <a href="{{ url('/admin-panel/account-settings') }}">Account Settings</a>
            </li>
            <li class="crumb-icon">
              <a href="{{ url('/admin-panel/dashboard') }}">
                <span class="glyphicon glyphicon-home"></span>
              </a>
            </li>
            <li class="crumb-link">
              <a href="{{ url('/admin-panel/dashboard') }}">Home</a>
            </li>
            <li class="crumb-trail">Account Settings</li>
          </ol>
        </div>
      </header>

      <!-- Begin: Content -->
      {!! Form::open(['url'=>'admin-panel/account-settings' , 'enctype' => 'multipart/form-data' , 'id' => 'validation' ]) !!}

      <div class="row">
        <div class="col-md-12">
          @if (\Session::has('success'))
            <div class="alert alert-success" style="padding: 10px; border: 0px; text-align: center; margin: 10px;">
                {!! \Session::get('success') !!}
            </div>
          @endif
        </div>
      </div>
            

      <section id="content" class="table-layout animated fadeIn" style="padding-top:20px">   
        <div class="tray tray-center" style="padding-left: 30px; padding-top: 0px; width: 95%;  width: 95%;">
          <div class="mw1000 center-block">
            <div class="panel panel-primary panel-border top mb35">
              <div class="panel-heading">
                <span class="panel-title">Details</span>
              </div>
              <div class="panel-body bg-light dark">
                <div class="admin-form">
                  <div class="section row mb10">
                    <label for="acc_email" class="field-label col-md-3 text-center">Account Email:</label>
                    <div class="col-md-9">  
                      <label for="account_email" class="field prepend-icon">
                        {!! Form::text('account_email',"$account->account_email", array('class' => 'gui-input','placeholder' => 'Account Email' )) !!}
                        <label for="Account Email" class="field-icon">
                          <i class="fa fa-envelope-o"></i>
                        </label>
                      </label>
                    </div>
                  </div>

                  <div class="section row mb10">
                    <label for="acc_email" class="field-label col-md-3 text-center">Account Mobile 1:</label>
                    <div class="col-md-9">  
                      <label for="account_mobile" class="field prepend-icon">   
                        {!! Form::text('account_number_1',"$account->account_number_1", array('class' => 'gui-input bfh-phone', 'data-format' =>'ddddd ddddd', 'placeholder' => 'Account Mobile' )) !!}
                        <label for="Account Mobile" class="field-icon">
                          <i class="fa fa-phone"></i>
                        </label>
                      </label>
                    </div>
                  </div>

                  <div class="section row mb10">
                    <label for="acc_email" class="field-label col-md-3 text-center">Account Mobile 2:</label>
                    <div class="col-md-9">  
                      <label for="account_mobile" class="field prepend-icon">   
                        {!! Form::text('account_number_2',"$account->account_number_2", array('class' => 'gui-input bfh-phone', 'data-format' =>'ddddd ddddd', 'placeholder' => 'Account Mobile' )) !!}
                        <label for="Account Mobile" class="field-icon">
                          <i class="fa fa-phone"></i>
                        </label>
                      </label>
                    </div>
                  </div>

                  <div class="section row mb10">
                    <label for="acc_email" class="field-label col-md-3 text-center">Account Mobile 3:</label>
                    <div class="col-md-9">  
                      <label for="account_mobile" class="field prepend-icon">   
                        {!! Form::text('account_number_3',"$account->account_number_3", array('class' => 'gui-input bfh-phone', 'data-format' =>'ddddd ddddd', 'placeholder' => 'Account Mobile' )) !!}
                        <label for="Account Mobile" class="field-icon">
                          <i class="fa fa-phone"></i>
                        </label>
                      </label>
                    </div>
                  </div>

                </div>
              </div>
            </div>

            <!-- Store Information -->
            <div class="panel panel-info panel-border top mb35">
              <div class="panel-heading">
                <span class="panel-title">Site Information</span>
              </div>
              <div class="panel-body bg-light dark">
                <div class="admin-form">
                  
                  <div class="section row mb10">
                    <label for="acc_email" class="field-label col-md-3 text-center">Message:</label>
                    <div class="col-md-9">  
                      <label for="account_message" class="field prepend-icon">   
                        {!! Form::text('account_message',"$account->account_message", array('class' => 'gui-input','placeholder' => 'Enter Message' )) !!}
                        <label for="Happy Homes" class="field-icon">
                          <i class="fa fa-comment"></i>
                        </label>
                      </label>
                    </div>
                  </div>

                  <div class="section row mb10">
                    <label for="acc_email" class="field-label col-md-3 text-center">Working Day:</label>
                    <div class="col-md-9">  
                      <label for="office_timing_day" class="field prepend-icon">   
                        {!! Form::text('office_timing_day',"$account->office_timing_day", array('class' => 'gui-input', 'placeholder' => 'Working Day' )) !!}
                        <label for="Working Hours" class="field-icon">
                          <i class="glyphicons glyphicons-clock"></i>
                        </label>
                      </label>
                    </div>
                  </div>
                  
                  <div class="section row mb10">
                    <label for="acc_email" class="field-label col-md-3 text-center">Working Time:</label>
                    <div class="col-md-9">  
                      <label for="office_timing_time" class="field prepend-icon">   
                        {!! Form::text('office_timing_time',"$account->office_timing_time", array('class' => 'gui-input', 'placeholder' => 'Working Time' )) !!}
                        <label for="Working Hours" class="field-icon">
                          <i class="glyphicons glyphicons-clock"></i>
                        </label>
                      </label>
                    </div>
                  </div>

                  <div class="admin-form">
                    <div class="section mb10">
                      <label class="field prepend-icon" for="account_address">
                        {!! Form::textarea('account_address',"$account->account_address", array('class' => 'gui-textarea accc1','placeholder' => 'Site Address' )) !!}
                        <label for="account_address" class="field-icon">
                          <i class="fa fa-map-marker"></i>
                        </label>
                      </label>
                    </div>
                  </div>

                </div>
              </div>
            </div>

            <!-- Soical Link Settings -->
            <div class="panel panel-primary panel-border top mb35">
              <div class="panel-heading">
                <span class="panel-title">Soical Link</span>
              </div>
              <div class="panel-body bg-light dark">
                <div class="admin-form">
                  <div class="section row mb10">
                    <label for="acc_fb" class="field-label col-md-3 text-center">Facebook Id:</label>
                    <div class="col-md-9">  
                      <label for="account_facebook" class="field prepend-icon">
                        {!! Form::text('account_facebook',"$account->account_facebook", array('class' => 'gui-input','placeholder' => 'Facebook Id' )) !!}
                        <label for="Facebook" class="field-icon">
                          <i class="fa fa-facebook"></i>
                        </label>
                      </label>
                    </div>
                  </div>

                  <!--<div class="section row mb10">-->
                  <!--  <label for="acc_tw" class="field-label col-md-3 text-center">Twitter Id:</label>-->
                  <!--  <div class="col-md-9">  -->
                  <!--    <label for="account_twitter" class="field prepend-icon">-->
                  <!--      {!! Form::text('account_twitter',"$account->account_twitter", array('class' => 'gui-input','placeholder' => 'Twitter Id' )) !!}-->
                  <!--      <label for="Twitter" class="field-icon">-->
                  <!--        <i class="fa fa-twitter"></i>-->
                  <!--      </label>-->
                  <!--    </label>-->
                  <!--  </div>-->
                  <!--</div>-->

                  <div class="section row mb10">
                    <label for="acc_go" class="field-label col-md-3 text-center">Youtube:</label>
                    <div class="col-md-9">  
                      <label for="account_youtube" class="field prepend-icon">
                        {!! Form::text('account_youtube',"$account->account_youtube", array('class' => 'gui-input','placeholder' => 'Google Id' )) !!}
                        <label for="Google" class="field-icon">
                          <i class="fa fa-youtube"></i>
                        </label>
                      </label>
                    </div>
                  </div>
          
                </div>
              </div>
            </div>
          </div>
        </div>
        <aside class="tray tray-right tray290" style="right: 0; padding: 5px 15px;">
          <div class="admin-form">

            <h4> Website Options</h4>
            <hr class="short" style="margin: 18px 0;">
            <h5><small>Site Name</small></h5>
            <div class="section mb10">
              <label for="store-name" class="field prepend-icon">
                {!! Form::text('account_name',"$account->account_name", array('class' => 'gui-input','placeholder' => 'Site Name' )) !!}
                <label for="store-name" class="field-icon">
                  <i class="fa fa-home"></i>
                </label>
              </label>
            </div>

            <hr class="short alt br-light">
            <div class="fileupload fileupload-new admin-form" data-provides="fileupload">
              <div class="fileupload-preview thumbnail mb15" style="line-height: 136px !important;">
                {!! Html::image($account->account_logo, '100%x147') !!}
              </div>
              <span class="button btn-system btn-file btn-block ph5" style="margin-bottom:10px">
                <span class="fileupload-new">Change Logo</span>
                <span class="fileupload-exists">Change Logo</span>
                {!! Form::file('account_logo') !!}
              </span>
            </div>
      
            <hr class="short alt br-light">
            <div><input type="submit" id="" class="btn btn-primary btn-gradient dark btn-block acc_submit" value="Update"/></div>
          </div>
        </aside>
        {!! Form::close() !!}
      </section>
      <!-- End: Content -->
    </section>

<script type="text/javascript">
  jQuery(document).ready(function() {
  
    jQuery.validator.addMethod("lettersonly", function(value, element) 
    {
    return this.optional(element) || /^[a-z," "]+$/i.test(value);
    }, "This field contains alphabets only");

    /* @custom validation method (smartCaptcha) 
    ------------------------------------------------------------------ */
    $("#validation").validate({

      /* @validation states + elements 
      ------------------------------------------- */

      errorClass: "state-error",
      validClass: "state-success",
      errorElement: "em",

      /* @validation rules 
      ------------------------------------------ */

      rules: {
        
        account_number_1: {
          required: true,
        },
        account_number_2: {
          required: true,
        },
        account_email: {
          required: true,
          email: true
        },
        account_address: {
          required: true,
        },
        account_facebook: {
          required: true,
        },
        account_twitter: {
          required: true,
        },
        account_youtube: {
          required: true,
        },
        account_name: {
          required: true,
          lettersonly: true,
        },
        account_message: {
          required: true,
        },
        office_timing_day: {
          required: true,
        },
        office_timing_time: {
          required: true,
        },
        account_logo: {
          extension: 'jpeg,jpg,png',
        },
        
      },

      /* @validation error messages 
      ---------------------------------------------- */

       messages: {
          account_logo: {
            extension: 'Image Should be in .jpg,.jpeg and .png format only'
          }            
      //   account_number_1: {
      //     required: 'Enter your mobile number'
      //   },
      //   account_number_2: {
      //     required: 'Enter your mobile number'
      //   },
      //   account_email: {
      //     required: 'Enter email address',
      //     email: 'Enter a VALID email address'
      //   },
      //   account_address: {
      //     required: 'Please enter a address'
      //   },
      //   account_facebook: {
      //     required: 'Enter facebook link'
      //   },
      //   account_youtube: {
      //     required: 'Enter Youtube link'
      //   },
      //   account_twitter: {
      //     required: 'Enter twitter link'
      //   },
      //   account_name: {
      //     required: 'Enter Site Name'
      //   },
      //   happy_homes: {
      //     required: 'Enter Happy Hours'
      //   },
      //   hours_reclaimed: {
      //     required: 'Enter Happy Reclaimed'
      //   },
      //   total_genie: {
      //     required: 'Enter Total Genies'
      //   },
      //   total_suburbs: {
      //     required: 'Enter Total Suburbs'
      //   },
      //   working_hours: {
      //     required: 'Enter Working Hours'
      //   },
      //   account_discount_price: {
      //     required: 'Enter Referral Discount Price'
      //   },

       },

      /* @validation highlighting + error placement  
      ---------------------------------------------------- */

      highlight: function(element, errorClass, validClass) {
        $(element).closest('.field').addClass(errorClass).removeClass(validClass);
      },
      unhighlight: function(element, errorClass, validClass) {
        $(element).closest('.field').removeClass(errorClass).addClass(validClass);
      },
      errorPlacement: function(error, element) {
        if (element.is(":radio") || element.is(":checkbox")) {
          element.closest('.option-group').after(error);
        } else {
          error.insertAfter(element.parent());
        }
      }
    });

  });
  </script>

@endsection
