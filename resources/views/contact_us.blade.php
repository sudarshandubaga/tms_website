@include('header')
   <!-- Section: inner-header -->
    <section class="inner-header divider  layer-overlay overlay-dark-5" data-bg-img="public/assets/images/bg/br.jpg">
      <div class="container pt-70 pb-20">
        <!-- Section Content -->
        <div class="section-content">
          <div class="row"> 
            <div class="col-md-6">
              <h2 class="text-theme-color-yellow font-36">{{$bredcum}}</h2>
              <ol class="breadcrumb text-left mt-10">
                <li class="active text-white">Home</li>
                <li class="active text-white">{{$bredcum}}</li>
              </ol>
            </div>
          </div>
        </div>
      </div>
    </section>
 
  
<!-- Divider: Contact -->
    <section class="divider">
      <div class="container">
        <div class="row pt-30">
          <div class="col-md-5">
            <div class="row">
              <div class="col-xs-12 col-sm-12 col-md-12">
                <div class="icon-box left media bg-deep p-30 mb-20"> <a class="media-left pull-left" href="#"> <i class="pe-7s-map-2 text-theme-color-red"></i></a>
                  <div class="media-body">
                    <h5 class="mt-0">Our Location</h5>
                    <p>{{ $account_details->account_address }}</p>
                  </div>
                </div>
              </div>
              <div class="col-xs-12 col-sm-6 col-md-12">
                <div class="icon-box left media bg-deep p-30 mb-20"> <a class="media-left pull-left" href="#"> <i class="pe-7s-call text-theme-color-blue"></i></a>
                  <div class="media-body">
                    <h5 class="mt-0">Contact Number</h5>
                    <p>+91 {{ $account_details->account_number_1 }}</p>
                  </div>
                </div>
              </div>
              <div class="col-xs-12 col-sm-6 col-md-12">
                <div class="icon-box left media bg-deep p-30 mb-20"> <a class="media-left pull-left" href="#"> <i class="pe-7s-mail text-theme-color-yellow"></i></a>
                  <div class="media-body">
                    <h5 class="mt-0">Email Address</h5>
                    <p>{{ $account_details->account_email }}</p>
                  </div>
                </div>
              </div>
              <div class="col-xs-12 col-sm-6 col-md-12">
                <div class="icon-box left media bg-deep p-30 mb-20">
                <ul class="styled-icons icon-bordered icon-sm mx-auto text-center">
              <li>
                <a href="{{ $account_details->account_facebook }}" target="_blank">
                  <!-- <i class="fa fa-facebook-official"></i> -->
                  <img src="{{ url('public/assets/images/fb.png') }}" alt="Facebook">
                </a>
              </li>
              <li>
                <a href="{{ $account_details->account_youtube }}" target="_blank">
                  <!-- <i class="fa fa-youtube"></i> -->
                  <img src="{{ url('public/assets/images/ytube.png') }}" alt="YouTube">
                </a>
              </li>
              <li>
                <a href="https://www.instagram.com/themodernschoolbarmer/?hl=en" target="_blank">
                  <!-- <i class="fa fa-facebook-official"></i> -->
                  <img src="https://www.themodernschoolbarmer.com/public/assets/images/insta.png" alt="Instagram">
                </a>
              </li>
              <li>
                <a href="https://twitter.com/TheModernSchoo2" target="_blank">
                  <!-- <i class="fa fa-facebook-official"></i> -->
                  <img src="https://www.themodernschoolbarmer.com/public/assets/images/twitter-logo-3.png" alt="Twitter">
                </a>
              </li>
            </ul>
                </div>
              </div>
            </div>
          </div>
          <div class="col-md-7">
            <h3 class="line-bottom mt-0 mb-30">Interested in discussing?</h3>
            <!-- Contact Form -->
            <form id="contact_form" name="contact_form" class="" action="contact_submit" method="post">

              <div class="row">
                <div class="col-sm-12">
                  <div class="form-group">
                    <label>Name <small>*</small></label>
                    <input name="contact_enquiry_name" class="form-control required" type="text" placeholder="Enter Name" >
                  </div>
                </div>
                <div class="col-sm-12">
                  <div class="form-group">
                    <label>Email <small>*</small></label>
                    <input name="contact_enquiry_email" class="form-control required email" type="email" placeholder="Enter Email">
                  </div>
                </div>
              </div>
                
              <div class="row">
                <div class="col-sm-6">
                  <div class="form-group">
                    <label>Subject <small>*</small></label>
                    <input name="contact_enquiry_subject" class="form-control required" type="text" placeholder="Enter Subject">
                  </div>
                </div>
                <div class="col-sm-6">
                  <div class="form-group">
                    <label>Phone</label>
                    <input name="contact_enquiry_number" class="form-control required" type="text" placeholder="Enter Phone">
                  </div>
                </div>
              </div>

              <div class="form-group">
                <label>Message</label>
                <textarea name="contact_enquiry_message" class="form-control required" rows="5" placeholder="Enter Message"></textarea>
              </div>
              <div class="form-group">
                <input name="form_botcheck" class="form-control" type="hidden" value="" />
                <button type="submit" class="btn btn-dark btn-theme-color-sky btn-flat mr-5" data-loading-text="Please wait...">Send your message</button>
                <button type="reset" class="btn btn-flat btn-theme-color-red">Reset</button>
              </div>
            </form>

            <!-- Contact Form Validation-->
            <script type="text/javascript">
              $("#contact_form").validate({
                submitHandler: function(form) {
                  var form_btn = $(form).find('button[type="submit"]');
                  var form_result_div = '#form-result';
                  $(form_result_div).remove();
                  form_btn.before('<div id="form-result" class="alert alert-success" role="alert" style="display: none;"></div>');
                  var form_btn_old_msg = form_btn.html();
                  form_btn.html(form_btn.prop('disabled', true).data("loading-text"));
                  $(form).ajaxSubmit({
                    dataType:  'json',
                    success: function(data) {
                      if( data.status == 'true' ) {
                        $(form).find('.form-control').val('');
                      }
                      form_btn.prop('disabled', false).html(form_btn_old_msg);
                      $(form_result_div).html(data.message).fadeIn('slow');
                      setTimeout(function(){ $(form_result_div).fadeOut('slow') }, 6000);
                    }
                  });
                }
              });
            </script>
          </div>
        </div>
      </div>
    </section>
    
     
    
<!-- Divider: Google Map -->
    <section>
      <div class="container-fluid pt-0 pb-0">
        <div class="row">

          
                <iframe src="https://www.google.com/maps/embed?pb=!1m18!1m12!1m3!1d28759.73608727979!2d71.36474473430499!3d25.705516254370764!2m3!1f0!2f0!3f0!3m2!1i1024!2i768!4f13.1!3m3!1m2!1s0x39443095e7faa5b9%3A0x8ce48600c66c95f5!2sThe+Modern+Senior+Secondary+School%2CBarmer!5e0!3m2!1sen!2sin!4v1536713896750"width="100%" height="450" frameborder="0" style="border:0" allowfullscreen> ></iframe>
        </div>
      </div>
    </section> 
    @include('footer')