@include('header')
   <!-- Section: inner-header -->
    <section class="inner-header divider  layer-overlay overlay-dark-5" data-bg-img="public/assets/images/bg/br.jpg">
      <div class="container pt-70 pb-20">
        <!-- Section Content -->
        <div class="section-content">
          <div class="row"> 
            <div class="col-md-6">
              <h2 class="text-theme-color-yellow font-36">{{$bredcum}}</h2>
              <ol class="breadcrumb text-left mt-10">
                <li class="active text-white">Home</li>
                <li class="active text-white">{{$bredcum}}</li>
              </ol>
            </div>
          </div>
        </div>
      </div>
    </section>
<style>
    .download_item {
    background-color: #FF5252;
    padding: 15px;
    width: 100%;
    text-align: center;
    margin-bottom: 30px;
    color:#fff;
}
</style>
<section class="downloadpage_inner frnt-imp-point">
	<div class="container">
		<h1 style="color:#f26f29 ;text-align: center;">Downloads</h1>
		
		<div class="row">
			 @foreach($download_record as $download_records)
			<div class="col-sm-4">
				<div class="download_item"><a href="{{ $download_records->download_title != "" ? $download_records->download_file : '#' }} " style="color:#fff; font-weight:bold;" target="_blank"> {{ $download_records->download_title }}&nbsp;&nbsp; <i class="fa fa-download"></i></a></div>
			</div>
			@endforeach
		</div>
	</div>
</section>
@include('footer')