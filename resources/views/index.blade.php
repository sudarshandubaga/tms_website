@include('header')

<section id="welcome-popup">
  <div class="popup-bg"></div>
  <div class="popup-body">
    <a href="#close" class="close">&times;</a>
    <img src="{{url('public/assets/images/welcome_popup.png')}}" alt="Welcome Popup Image">
    <span class="btn btn-primary px-5">Get Admission Now</span>
    <a href="{{ url('registration') }}"></a>
  </div>
</section>
    <section id="home">
      <div class="container-fluid p-0">
        
        <!-- Slider Revolution Start -->
        <div class="rev_slider_wrapper">
          <div class="rev_slider rev_slider_default" data-version="5.0">
            <ul>

              <!-- SLIDE 1 -->
              @foreach($get_slider as $get_slider)
              <li data-index="rs-1" data-transition="fade" data-slotamount="default" data-easein="default" data-easeout="default" data-masterspeed="default" data-thumb="{{ $get_slider->slider_image}}" data-rotate="0"  data-fstransition="fade" data-saveperformance="off" data-title="Web Show" data-description="">
                <!-- MAIN IMAGE -->
                <img src="{{ $get_slider->slider_image}}"  alt=""  data-bgposition="center center" data-bgfit="cover" data-bgrepeat="no-repeat" class="rev-slidebg" data-bgparallax="6" data-no-retina>
                <!-- LAYERS -->

                <!-- LAYER NR. 1 -->
                <div class="tp-caption tp-shape tp-shapewrapper tp-resizeme rs-parallaxlevel-0" 
                  id="slide-1-layer-1" 
                  data-x="['center','center','center','center']" data-hoffset="['0','0','0','0']" 
                  data-y="['middle','middle','middle','middle']" data-voffset="['0','0','0','0']" 
                  data-width="full"
                  data-height="full"
                  data-whitespace="normal"
                  data-transform_idle="o:1;"
                  data-transform_in="opacity:0;s:1500;e:Power3.easeInOut;" 
                  data-transform_out="opacity:0;s:1000;e:Power3.easeInOut;s:1000;e:Power3.easeInOut;" 
                  data-start="1000" 
                  data-basealign="slide" 
                  data-responsive_offset="on" 
                  style="z-index: 5;background-color:rgba(0, 0, 0, 0.05);border-color:rgba(0, 0, 0, 1.00);"> 
                </div>

                <!-- LAYER NR. 2 -->
                <div class="tp-caption tp-resizeme sft font-Lobster font-weight-700 text-theme-color-red" 
                  id="rs-1-layer-2"

                  data-x="['left']"
                  data-hoffset="['30']"
                  data-y="['middle']"
                  data-voffset="['-115']"
                  data-fontsize="['28']"
                  data-lineheight="['48']"
                  data-width="none"
                  data-height="none"
                  data-whitespace="nowrap"
                  data-transform_idle="o:1;s:500"
                  data-transform_in="y:100;scaleX:1;scaleY:1;opacity:0;"
                  data-transform_out="x:left(R);s:1000;e:Power3.easeIn;s:1000;e:Power3.easeIn;"
                  data-mask_in="x:0px;y:0px;s:inherit;e:inherit;"
                  data-mask_out="x:inherit;y:inherit;s:inherit;e:inherit;"
                  data-speed="500"
                  data-start="600"
                  data-splitin="none"
                  data-splitout="none" 
                  data-responsive_offset="on" 
                  style="z-index: 5; white-space: nowrap;">
                </div>

                <!-- LAYER NR. 3 -->
                <div class="tp-caption tp-resizeme sft text-theme-color-blue font-Lobster font-weight-700 m-0" 
                  id="rs-1-layer-3"

                  data-x="['left']"
                  data-hoffset="['30']"
                  data-y="['middle']"
                  data-voffset="['-50']"
                  data-fontsize="['78']"
                  data-lineheight="['96']"
                  data-width="none"
                  data-height="none"
                  data-whitespace="nowrap"
                  data-transform_idle="o:1;s:500"
                  data-transform_in="y:100;scaleX:1;scaleY:1;opacity:0;"
                  data-transform_out="x:left(R);s:1000;e:Power3.easeIn;s:1000;e:Power3.easeIn;"
                  data-mask_in="x:0px;y:0px;s:inherit;e:inherit;"
                  data-mask_out="x:inherit;y:inherit;s:inherit;e:inherit;"
                  data-speed="500"
                  data-start="800"
                  data-splitin="none" 
                  data-splitout="none" 
                  data-responsive_offset="on" 
                  style="z-index: 5; white-space: nowrap;"><span class="text-theme-color-red"></span>
                </div>

                <!-- LAYER NR. 4 -->
                <div class="tp-caption tp-resizeme sft text-black" 
                  id="rs-1-layer-4"

                  data-x="['left']"
                  data-hoffset="['30']"
                  data-y="['middle']"
                  data-voffset="['20']"
                  data-fontsize="['16','18',24']"
                  data-lineheight="['28']"
                  data-width="none"
                  data-height="none"
                  data-whitespace="nowrap"
                  data-transform_idle="o:1;s:500"
                  data-transform_in="y:100;scaleX:1;scaleY:1;opacity:0;"
                  data-transform_out="x:left(R);s:1000;e:Power3.easeIn;s:1000;e:Power3.easeIn;"
                  data-mask_in="x:0px;y:0px;s:inherit;e:inherit;"
                  data-mask_out="x:inherit;y:inherit;s:inherit;e:inherit;"
                  data-speed="500"
                  data-start="1200"
                  data-splitin="none" 
                  data-splitout="none" 
                  data-responsive_offset="on"
                  style="z-index: 5; white-space: nowrap; font-weight:500;">
                </div>

                <!-- LAYER NR. 5 -->
                <div class="tp-caption sft" 
                  id="rs-1-layer-5"

                  data-x="['left']"
                  data-hoffset="['30']"
                  data-y="['middle']"
                  data-voffset="['90','100','110']"
                  data-width="none"
                  data-height="none"
                  data-whitespace="nowrap"
                  data-transform_idle="o:1;s:500"
                  data-transform_in="y:100;scaleX:1;scaleY:1;opacity:0;"
                  data-transform_out="x:left(R);s:1000;e:Power3.easeIn;s:1000;e:Power3.easeIn;"
                  data-mask_in="x:0px;y:0px;s:inherit;e:inherit;"
                  data-mask_out="x:inherit;y:inherit;s:inherit;e:inherit;"
                  data-speed="500"
                  data-start="1400"
                  data-splitin="none" 
                  data-splitout="none" 
                  data-responsive_offset="on"
                  style="z-index: 5; white-space: nowrap; letter-spacing:1px;">
                </div>
              </li>

              @endforeach
            </ul>
          </div>
          <!-- end .rev_slider -->
        </div>
        <!-- end .rev_slider_wrapper -->
        <script>
          $(document).ready(function(e) {
            $(document).on('click', '#welcome-popup .popup-body a.close', function (e) {
              e.preventDefault();
              $('#welcome-popup').hide();
            });
            $(".rev_slider_default").revolution({
              sliderType:"standard",
              sliderLayout: "auto",
              dottedOverlay: "none",
              delay: 5000,
              navigation: {
                  keyboardNavigation: "off",
                  keyboard_direction: "horizontal",
                  mouseScrollNavigation: "off",
                  onHoverStop: "off",
                  touch: {
                      touchenabled: "on",
                      swipe_threshold: 75,
                      swipe_min_touches: 1,
                      swipe_direction: "horizontal",
                      drag_block_vertical: false
                  },
                arrows: {
                  style: "gyges",
                  enable: true,
                  hide_onmobile: false,
                  hide_onleave: true,
                  hide_delay: 200,
                  hide_delay_mobile: 1200,
                  tmp: '',
                  left: {
                      h_align: "left",
                      v_align: "center",
                      h_offset: 0,
                      v_offset: 0
                  },
                  right: {
                      h_align: "right",
                      v_align: "center",
                      h_offset: 0,
                      v_offset: 0
                  }
                },
                bullets: {
                  enable: true,
                  hide_onmobile: true,
                  hide_under: 800,
                  style: "hebe",
                  hide_onleave: false,
                  direction: "horizontal",
                  h_align: "center",
                  v_align: "bottom",
                  h_offset: 0,
                  v_offset: 30,
                  space: 5,
                  tmp: '<span class="tp-bullet-image"></span><span class="tp-bullet-imageoverlay"></span><span class="tp-bullet-title"></span>'
                }
              },
              responsiveLevels: [1240, 1024, 778],
              visibilityLevels: [1240, 1024, 778],
              gridwidth: [1170, 1024, 778, 480],
              gridheight: [640, 768, 960, 720],
              lazyType: "none",
              parallax: {
                  origo: "slidercenter",
                  speed: 1000,
                  levels: [5, 10, 15, 20, 25, 30, 35, 40, 45, 46, 47, 48, 49, 50, 100, 55],
                  type: "scroll"
              },
              shadow: 0,
              spinner: "off",
              stopLoop: "on",
              stopAfterLoops: 0,
              stopAtSlide: -1,
              shuffle: "off",
              autoHeight: "off",
              fullScreenAutoWidth: "off",
              fullScreenAlignForce: "off",
              fullScreenOffsetContainer: "",
              fullScreenOffset: "0",
              hideThumbsOnMobile: "off",
              hideSliderAtLimit: 0,
              hideCaptionAtLimit: 0,
              hideAllCaptionAtLilmit: 0,
              debugMode: false,
              fallbacks: {
                  simplifyAll: "off",
                  nextSlideOnWindowFocus: "off",
                  disableFocusListener: false,
              }
            });
          });
        </script>
        <!-- Slider Revolution Ends -->
      </div>
    </section>


    <!-- <section id="home_news">
        <div class="container">
          <h2 class="line-bottom-center mt-5 mb-5">
            <span class="text-theme-color-red">Updated News</span>
          </h2>
          
        </div>
    </section> -->
    
        <!-- Section: About -->
    <section>
      <div class="container">
        <div class="section-content">
          <div class="row">
            <div class="col-md-6">
              <h2 class="text-theme-color-sky line-bottom"><span class="text-theme-color-red">{{ $get_pages->master_pages_title}}</span></h2>
               <p style="color:red; font-weight:bold;">"{{ $get_pages->master_pages_excerpt}}"</p>
              <div style="font-size:14px;"><?php echo htmlspecialchars_decode($get_pages->master_pages_description)?></div>
            </div>
            <div class="col-md-6">
              <!-- <div class="video-popup">                
                <a>
                  <img alt="" src="{{ $get_pages->master_pages_image}}" class="img-responsive img-fullwidth">
                </a>
              </div> -->
              <div class="news-section">
                <h2>Updated News</h2>
                <marquee onmouseover="stop()" onmouseout="start()" behavior="scroll" direction="up">
                  <ol>
                    @php $sn = 0; @endphp
                    @foreach($news as $id => $title)
                      <li><a href="news/{{ $id }}">{{ ++$sn }}. {{ $title }}</a></li>
                    @endforeach
                  </ol>
                </marquee>
              </div>
            </div>
          </div>
        </div>
      </div>
      <div> 
          <img alt="" src="public/assets/images/bg/f2.png" class="img-responsive img-fullwidth">
      </div>
    </section>
    
    
     <!-- Divider: Funfact 
    <section class="divider" data-bg-img="public/assets/images/bg/p5.jpg">
      <div class="container pt-90 pb-90">
        <div class="row">
          <div class="col-xs-12 col-sm-6 col-md-3 mb-md-50">
            <div class="funfact text-center">
              <i class="pe-7s-users mt-5 text-white"></i>
              <h2 data-animation-duration="2000" data-value="864" class="animate-number text-white font-42 font-weight-300 mt-0 mb-0">0</h2>
              <h5 class="text-white">Qualified Teachers</h5>
            </div>
          </div>
          <div class="col-xs-12 col-sm-6 col-md-3 mb-md-50">
            <div class="funfact text-center">
              <i class="pe-7s-study mt-5 text-white"></i>
              <h2 data-animation-duration="2000" data-value="486" class="animate-number text-white font-42 font-weight-300 mt-0 mb-0">0</h2>
              <h5 class="text-white">Successful Kids</h5>
            </div>
          </div>
          <div class="col-xs-12 col-sm-6 col-md-3 mb-md-50">
            <div class="funfact text-center">
              <i class="pe-7s-smile mt-5 text-white"></i>
              <h2 data-animation-duration="2000" data-value="1468" class="animate-number text-white font-42 font-weight-300 mt-0 mb-0">0</h2>
              <h5 class="text-white">Happy Parents</h5>
            </div>
          </div>
          <div class="col-xs-12 col-sm-6 col-md-3 mb-md-50">
            <div class="funfact text-center">
              <i class="pe-7s-medal mt-5 text-white"></i>
              <h2 data-animation-duration="2000" data-value="32" class="animate-number text-white font-42 font-weight-300 mt-0 mb-0">0</h2>
              <h5 class="text-white">Award Won</h5>
            </div>
          </div>
        </div>
      </div>
    </section>-->
    
    
     <!-- Section: Why Choose Us -->
    <section>
      <div class="container">
         <div class="section-content">
            <div class="row" style="text-align:center">
                  <h2 class="line-bottom-center mt-5 mb-5"><span class="text-theme-color-red">{{ $choose_pages->master_pages_title}}</span></h2>
            </div>
            <br/>
            <br/>
        
        <div class="row">
            <div class="col-md-5"> 
             <img src="{{ $choose_pages->master_pages_image}}" class="img-fullwidth" alt="">
            </div>
            <div class="col-md-7 pb-sm-20">
              
              <p style="color:red; font-weight:bold;">{{ $choose_pages->master_pages_excerpt}}</p>
              <p class="mb-20"  style="font-size:12px;"><?php echo htmlspecialchars_decode($choose_pages->master_pages_description)?></p>
              
              
              
            </div>
         </div>
         </div>
       </div>
     
    </section>
    
    
     <!-- Divider: testimonials -->
    <section class="divider" data-bg-img="public/assets/images/bg/b1.jpg">
      <div class="container">
        <div class="section-title text-center">
          <div class="row">
            <div class="col-md-8 col-md-offset-2">
              <h2 class="line-bottom-center mt-0">Happy<span class="text-theme-color-red"> Parent's Say</span></h2>
            </div>
          </div>
        </div>
        <div class="row">
          <div class="col-md-12">
            <div class="owl-carousel-2col testimonial style2 dots-white" data-dots="false">
             
              @foreach($testimonials as $testimonials)
              <div class="item">
                <div class="testimonial-wrapper">                  
                  <div class="content bg-theme-color-orange p-30 pb-40">
                    <p class="font-15 text-white font-weight-600">{{ $testimonials->testimonials_description}}</p>
                    <i class="fa fa-quote-right mt-10 text-white"></i>
                    <h4 class="author text-white mt-20 mb-0">{{ $testimonials->testimonials_user_name}}</h4>
                    <!--<h6 class="title text-white mt-0 mb-15"></h6>-->
                    <div class="thumb mt-20">
                      @if($testimonials->testimonials_image != '')
                      <img src="{{ $testimonials->testimonials_image}}" class="img-circle" alt="">
                      @else
                      <img class="img-circle" alt="" src="public/assets/images/testimonials/1.jpg">
                      @endif
                    </div>
                  </div>                  
                </div>
              </div>
                  @endforeach
             
            </div>
          </div>
        </div>
      </div>
    </section>
    
    
    
    
    
    
    
    
    
    
    
    
    
    
    
    
    
    
    
    
    
    
    
    
    
    
@include('footer')
