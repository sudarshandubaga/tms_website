@include('header')
   <!-- Section: inner-header -->
    <section class="inner-header divider  layer-overlay overlay-dark-5" data-bg-img="public/assets/images/bg/br.jpg">
      <div class="container pt-70 pb-20">
        <!-- Section Content -->
        <div class="section-content">
          <div class="row"> 
            <div class="col-md-6">
              <h2 class="text-theme-color-yellow font-36">{{$bredcum}}</h2>
              <ol class="breadcrumb text-left mt-10">
                <li class="active text-white">Home</li>
                <li class="active text-white">Blog</li>
                <li class="active text-white">{{$bredcum}}</li>
              </ol>
            </div>
          </div>
        </div>
      </div>
    </section>
     <!-- Section: Why Choose Us -->
    <section>
      <div class="container">
         <div class="section-content">
               <div class="row" style="text-align:center">
                  <h2 class="line-bottom-center mt-5 mb-5"><span class="text-theme-color-red">{{ $blog->title}}</span></h2>
            </div>
            <br/>
            <br/>
            @if(!empty($blog->image))
            <div class="form-group">
              <img src="{{ $blog->image }}" alt="" class="blog-image">
            </div>
            @endif
            <div class="row">
            <div class="col-md-12 pb-sm-20">
              <p class="mb-20"><?php echo htmlspecialchars_decode($blog->description)?></p>
            </div>
         </div>
         </div>
       </div>
    </section>
    
    
    
    
    
    
    
    
    
    
    
    
    
    
    
    
    
    
    
    
    
    
    
    
    
    
    
    
    
@include('footer')
