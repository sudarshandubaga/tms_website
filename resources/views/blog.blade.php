@include('header')
   <!-- Section: inner-header -->
    <section class="inner-header divider  layer-overlay overlay-dark-5" data-bg-img="public/assets/images/bg/br.jpg">
      <div class="container pt-70 pb-20">
        <!-- Section Content -->
        <div class="section-content">
          <div class="row"> 
            <div class="col-md-6">
              <h2 class="text-theme-color-yellow font-36">{{$bredcum}}</h2>
              <ol class="breadcrumb text-left mt-10">
                <li class="active text-white">Home</li>
                <li class="active text-white">{{$bredcum}}</li>
              </ol>
            </div>
          </div>
        </div>
      </div>
    </section>
    <!-- Section: Blog -->
    <section>
      <div class="container pb-70">
        <div class="section-title text-center">
          <div class="row">
            <div class="col-md-8 col-md-offset-2">
              <h2 class="line-bottom-center mt-0"><span class="text-theme-color-red">Blog</span></h2>
              <div class="title-flaticon">
                <i class="flaticon-charity-alms"></i>
              </div>
            </div>
          </div>
        </div>
        <div class="section-content blog-section">
          <div class="row mb-30">
              @foreach($blogs as $blog)
            <div class="col-xs-12 col-sm-4 col-md-4">
              <div class="gallery-block">
                <div class="gallery-thumb">
                  <img alt="project" src="{{ $blog->image }}" class="img-fullwidth">
                </div>
                <div class="overlay-shade green"></div>
                <div class="icons-holder">
                  <div class="icons-holder-inner">
                    <div class="gallery-icon">
                      <a href="blog/{{ $blog->slug }}"><i class="pe-7s-science"></i></a>
                    </div>
                  </div>
                </div>
              </div>
              <div>
              <a href="blog/{{ $blog->slug }}"><h5>{{ $blog->title }}</h5></a>
              </div>
            </div>  
              @endforeach
          </div>
          {{ $blogs->links() }}
        </div>
      </div>
    </section>
    
    
    
    
    
    
@include('footer')