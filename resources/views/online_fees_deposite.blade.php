@include('header')
<!-- Section: inner-header -->
<section class="inner-header divider  layer-overlay overlay-dark-5" data-bg-img="public/assets/images/bg/br.jpg">
  <div class="container pt-70 pb-20">
    <!-- Section Content -->
    <div class="section-content">
      <div class="row"> 
        <div class="col-md-6">
          <h2 class="text-theme-color-yellow font-36">{{$bredcum}}</h2>
          <ol class="breadcrumb text-left mt-10">
            <li class="active text-white">Home</li>
            <li class="active text-white">{{$bredcum}}</li>
          </ol>
        </div>
      </div>
    </div>
  </div>
</section>
 
  
<!-- Divider: Contact -->
    <section class="divider">
      <div class="container">
        <div class="row pt-30">
          
          <div class="col-md-7">
            <h3 class="line-bottom mt-0 mb-30">Deposite Fees To School?</h3>
            <!-- Contact Form -->
            <form id="fees_form" name="fees_form" class="" action="{{ route('fee_deposite.submit') }}" method="post">
              <div class="row">
                <div class="col-sm-12">
                  <div class="form-group">
                    <label>Student Name <small>*</small></label>
                    <input name="student_name" class="form-control required" type="text" placeholder="Enter Name" >
                  </div>
                </div>
                <div class="col-sm-12">
                  <div class="form-group">
                    <label>Amount <small>*</small></label>
                    <input name="fee_amount" class="form-control required" type="number" min="1" placeholder="Enter Amount" >
                  </div>
                </div>
                <div class="col-sm-12">
                  <div class="form-group">
                    <label>Email <small>*</small></label>
                    <input name="email" class="form-control required email" type="email" placeholder="Enter Email">
                  </div>
                </div>
                <div class="col-sm-12">
                  <div class="form-group">
                    <label>Mobile No. <small>*</small></label>
                    <input name="mobile_no" class="form-control required" type="tel" placeholder="Enter Mobile No.">
                  </div>
                </div>
              </div>
              <div class="form-group">
                <label>Message</label>
                <textarea name="message" class="form-control" rows="5" placeholder="Enter Message"></textarea>
              </div>
              <div class="form-group">
                <input name="form_botcheck" class="form-control" type="hidden" value="" />
                <button type="submit" class="btn btn-dark btn-theme-color-sky btn-flat mr-5" data-loading-text="Please wait...">Submit</button>
                <button type="reset" class="btn btn-flat btn-theme-color-red">Reset</button>
              </div>
            </form>

            <!-- Contact Form Validation-->
            <script type="text/javascript">
              $("#fees_form").validate({
                submitHandler: function(form) {
                  var form_btn = $(form).find('button[type="submit"]');
                  var form_result_div = '#form-result';
                  $(form_result_div).remove();
                  form_btn.before('<div id="form-result" class="alert alert-success" role="alert" style="display: none;"></div>');
                  var form_btn_old_msg = form_btn.html();
                  form_btn.html(form_btn.prop('disabled', true).data("loading-text"));

                  return true;
                }
              });
            </script>
          </div>
        </div>
      </div>
    </section>
    @include('footer')
