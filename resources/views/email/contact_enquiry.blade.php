<!DOCTYPE html>
<html lang="en" dir="ltr">
    <head>
        <meta charset="utf-8">
        <title>{{ $subject }}</title>
    </head>
    <body>
        <div style="max-width: 800px; margin: auto; border: 1px solid #ccc;">
            <div style="background: #ebebeb; text-align: center; font-size: 35px; padding: 30px 15px;font-weight: bold;">
                <div style="text-align:center">The Modern School</div> 
                
            </div>
            <div style="padding: 15px">
                Dear Admin,<br>
                
                <h3>{{ $data['contact_enquiry_subject'] }}</h3>
                <table style="border-collapse: collapse; width: 100%;">
                    
                    <tr>
                        <td style="border: 1px solid #ccc; padding:5px">
                            <span style="font-weight: bold;">Name : </span> {{ $data['contact_enquiry_name'] }}
                        </td>
                        
                    </tr>
                    <tr>
                        <td style="border: 1px solid #ccc; padding:5px">
                            <span style="font-weight: bold;">Email : </span> {{ $data['contact_enquiry_email'] }}
                        </td>
                        
                    </tr>
                    <tr>
                        <td style="border: 1px solid #ccc; padding:5px">
                            <span style="font-weight: bold;">Mobile No. : </span> {{ $data['contact_enquiry_number'] }}
                        </td>
                        
                    </tr>
                    <tr>
                        <td style="border: 1px solid #ccc; padding:5px">
                            <span style="font-weight: bold;">Subject : </span> {{ $data['contact_enquiry_subject'] }}
                        </td>
                        
                    </tr>
                    <tr>
                        <td style="border: 1px solid #ccc; padding:5px">
                            <span style="font-weight: bold;">Message : </span> {{ $data['contact_enquiry_message'] }}
                        </td>
                        
                    </tr>                   
                    
                </table>
                <p>&nbsp;</p>
                <p>
                    Thanks &amp; Regards<br>
                    {{ $sender }}
                    
                </p>
            </div>
            <div style="background: #ebebeb; text-align: center; padding: 10px 0; font-size:17px">
                <a href="{{ url('/terms-conditions') }}">Terms & conditions</a>
            </div>
        </div>
    </body>
</html>
