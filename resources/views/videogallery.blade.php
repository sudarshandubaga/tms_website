@include('header')
   <!-- Section: inner-header -->
    <section class="inner-header divider  layer-overlay overlay-dark-5" data-bg-img="public/assets/images/bg/br.jpg">
      <div class="container pt-70 pb-20">
        <!-- Section Content -->
        <div class="section-content">
          <div class="row"> 
            <div class="col-md-6">
              <h2 class="text-theme-color-yellow font-36">{{$bredcum}}</h2>
              <ol class="breadcrumb text-left mt-10">
                <li class="active text-white">Home</li>
                <li class="active text-white">{{$bredcum}}</li>
              </ol>
            </div>
          </div>
        </div>
      </div>
    </section>
    <!-- Section: Our Gallery -->
    <section>
      <div class="container pb-70">
        <div class="section-title text-center">
          <div class="row">
            <div class="col-md-8 col-md-offset-2">
              <h2 class="line-bottom-center mt-0"><span class="text-theme-color-red">{{$bredcum}}</span></h2>
              <div class="title-flaticon">
                <i class="flaticon-charity-alms"></i>
              </div>
            </div>
          </div>
        </div>
        <div class="section-content">
          <div class="row mb-30">
              @foreach($school_gallery as $school_gallery)
            <div class="col-xs-12 col-sm-12 col-md-6">
              <div  style="border:2px #ccc solid">
                <div >
                   <iframe width="200" height="200" src="https://www.youtube.com/embed/{{ $school_gallery->school_gallery_link}}"> </iframe>
                </div>
                <div  style="text-align:center; padding:10px;"><h2>{{ $school_gallery->school_gallery_title}}</h2></div>
              </div>
            </div>  
              @endforeach
          </div>
          
        </div>
      </div>
    </section>
    
    
    
    
    
    
@include('footer')
