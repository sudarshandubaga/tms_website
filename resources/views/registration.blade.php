@include('header')
   <!-- Section: inner-header -->
    <section class="inner-header divider  layer-overlay overlay-dark-5" data-bg-img="public/assets/images/bg/br.jpg">
      <div class="container pt-70 pb-20">
        <!-- Section Content -->
        <div class="section-content">
          <div class="row"> 
            <div class="col-md-6">
              <h2 class="text-theme-color-yellow font-36">{{$bredcum}}</h2>
              <ol class="breadcrumb text-left mt-10">
                <li class="active text-white">Home</li>
                <li class="active text-white">{{$bredcum}}</li>
              </ol>
            </div>
          </div>
        </div>
      </div>
    </section>
<style>
 	td{width: 50%; }
	tr{height: 60px;}
</style>

<div class="row">
        <div class="col-md-12">
          @if (\Session::has('success'))
            <div class="alert alert-success" style="padding: 10px; border: 0px; text-align: center; margin: 10px;">
                {!! \Session::get('success') !!}
            </div>
          @endif
        </div>
      </div>
      
<form name="form" method="post" action="registration_submit" enctype="multipart/form-data" id="myForm">
<table width="50%;" style="margin: 30px auto;  font-family: Calibri;  " >
	<tr><td>Admission For Academic Year</td>
		<td><select name="syear" id="syear" class="form-control"> <option value="2021-22">2021-22</option>  </select></td>
	</tr>
	
	<tr><td>Admission Sought to</td>
		<td><select name="class" id="class" class="form-control"> 
							<option value="NURSERY">NURSERY</option>
                            <option value="LKG">LKG</option>
                            <option value="UKG">UKG</option>
                            <option value="Class I">Class I</option>
                            <option value="Class II">Class II</option>
                            <option value="Class III">Class III</option>
                            <option value="Class IV">Class IV</option>
                            <option value="Class V">Class V</option>
                            <option value="Class VI">Class VI</option>
                            <option value="Class VII">Class VII</option>
                            <option value="Class VIII">Class VIII</option>
			                <option value="Class IX">Class IX</option>
			                <option value="Class X">Class X</option>
			                <option value="Class XI Science">Class XI Science</option>
			                <option value="Class XI Commerce">Class XI Commerce</option>
			                <option value="Class XII Science">Class XII Science</option>
			                <option value="Class XII Commerce">Class XII Commerce</option>
		 
            </select>
        </td>
	</tr>

	<tr>
		<td>Student's Name</td>
		<td><input type="text" name="name"  id="name" required="" class="form-control"  placeholder="Name" style="border: 1px solid #ccc;color:#000;"  onkeypress="return (event.charCode > 64 && 
event.charCode < 91) || (event.charCode > 96 && event.charCode < 123) || (event.charCode == 32) "></td>
	</tr>
	
	<tr>
        <td>Father's Name</td>
        <td ><input name="fname" type="text" id="fname" required="" class="form-control" placeholder="Father's Name" style="border: 1px solid #ccc;color:#000;"  onkeypress="return (event.charCode > 64 && 
event.charCode < 91) || (event.charCode > 96 && event.charCode < 123) || (event.charCode == 32) "/></td>
    </tr>
	
	<tr>
        <td>Mother's Name</td>
        <td ><input name="mname" type="text" id="mname" required="" class="form-control" placeholder="Mother's Name" style="border: 1px solid #ccc;color:#000;"  onkeypress="return (event.charCode > 64 && 
event.charCode < 91) || (event.charCode > 96 && event.charCode < 123) || (event.charCode == 32) "/></td>
    </tr>


    <tr>
    <td>Date of Birth (In Figures)</td>
    <td >	<input type="date"  id="" name="date" class="form-control" required="" placeholder="yyyy/mm/dd" style="border: 1px solid #ccc;color:#000;">
							
	</td>
    </tr>


	<tr>
        <td>Gender</td>
        <td><input type="radio" name="gender" value="Male"  id="value1" checked="checked"/>Male
        	<input type="radio"  name="gender" value="Female" id="value2"/>Female</td>
    </tr>
    <tr>

        <td>Residential Address</td>
        <td><textarea name="address" id="address" required="" class="form-control"cols="40" rows="5" placeholder="Address" style="border: 1px solid #ccc;color:#000;" onkeypress="return (event.charCode > 64 && 
event.charCode < 91) || (event.charCode > 96 && event.charCode < 123) || (event.charCode == 32) || (event.charCode == 44)|| (event.charCode == 45)|| (event.charCode == 46) || (event.charCode == 33) || (event.charCode == 63) || (event.charCode > 47 && event.charCode < 58) "></textarea></td>
    </tr>
	
	<tr>
        <td>State</td>
        <td ><input name="state" type="text" id="state" required="" class="form-control" placeholder="State" style="border: 1px solid #ccc; color:#000;"  onkeypress="return (event.charCode > 64 && 
event.charCode < 91) || (event.charCode > 96 && event.charCode < 123) || (event.charCode == 32) "/></td>
    </tr>

    <tr>
        <td>City</td>
        <td ><input name="city" type="text" id="city" required="" class="form-control" placeholder="City" style="border: 1px solid #ccc; color:#000;"  onkeypress="return (event.charCode > 64 && 
event.charCode < 91) || (event.charCode > 96 && event.charCode < 123) || (event.charCode == 32) "/></td>
    </tr>

    <tr>
        <td>Mobile No</td>
        <td ><input name="mobile" type="text" id="mobile" required="" class="form-control" placeholder="Mobile" style="border: 1px solid #ccc; color:#000;" maxlength="10" onkeypress="return isNumber(event)" /></td>
    </tr>
	
	<tr>
        <td>Phone Number </td>
        <td ><input name="pno" type="text" id="pno" required="" class="form-control" placeholder="Phone" style="border: 1px solid #ccc; color:#000;" maxlength="10"  onkeypress="return isNumber(event)" /></td>
    </tr>
	
	<tr>
        <td>E-Mail Address</td>
        <td ><input name="email" type="text" id="email" required="" class="form-control" placeholder="E-mail" style="border: 1px solid #ccc; color:#000;" onblur="validateEmail(this.value)"/></td>
    </tr>
    
    <tr >
    <td colspan="2">Please recheck mobile number before submitting the form as  you will receive a message carrying registration number on the same.</td>
    </tr>
    
  </table>
    <div class="row">
        <div class="col-md-6"></div>
        <div class="col-md-2 col-sm-6">
            <input type="text" class="form-control" name="write_cap" required="" placeholder="Write Captch" onkeypress="return isNumber(event)" style="background:#eee; font-weight:bold;" maxlength="6"/>
       
        </div>
        <?php 
        $str_result = '0123456789'; 
   		$str_result = substr(str_shuffle($str_result),0, 6);
   		?>
        <div class="col-md-4"> 
        <input type="button" name="read_cap1" class="btn btn-flat btn-theme-color-red" value="<?= $str_result ?>">
        <input type="hidden" name="read_cap" class="btn btn-success btn-lg" value="<?= $str_result ?>">
        </div>
    </div>
    <br/>
    <div class="row">
        <div class="col-md-8"></div>
        <div class="col-md-4">
           	<input type="hidden" name="mode" id="mode" value="save" />
			 <button type="submit" class="btn btn-dark btn-theme-color-sky btn-flat mr-5" data-loading-text="Please wait..." name="submit"  id="btn_submit">Save</button>
        </div>
     
    </div>
   
</form>

<script>
   function validateEmail(sEmail) {
  var reEmail = /^(?:[\w\!\#\$\%\&\'\*\+\-\/\=\?\^\`\{\|\}\~]+\.)*[\w\!\#\$\%\&\'\*\+\-\/\=\?\^\`\{\|\}\~]+@(?:(?:(?:[a-zA-Z0-9](?:[a-zA-Z0-9\-](?!\.)){0,61}[a-zA-Z0-9]?\.)+[a-zA-Z0-9](?:[a-zA-Z0-9\-](?!$)){0,61}[a-zA-Z0-9]?)|(?:\[(?:(?:[01]?\d{1,2}|2[0-4]\d|25[0-5])\.){3}(?:[01]?\d{1,2}|2[0-4]\d|25[0-5])\]))$/;

  if(!sEmail.match(reEmail)) {
    alert("Invalid email address");
    document.getElementById('email').value = "";
    return false;
  }
  return true;
}

function isNumber(evt) {
    evt = (evt) ? evt : window.event;
    var charCode = (evt.which) ? evt.which : evt.keyCode;
    if (charCode > 31 && (charCode < 48 || charCode > 57)) {
        return false;
    }
    return true;
}
</script>
@include('footer')