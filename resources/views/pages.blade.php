@include('header')
   <!-- Section: inner-header -->
    <section class="inner-header divider  layer-overlay overlay-dark-5" data-bg-img="public/assets/images/bg/br.jpg">
      <div class="container pt-70 pb-20">
        <!-- Section Content -->
        <div class="section-content">
          <div class="row"> 
            <div class="col-md-6">
              <h2 class="text-theme-color-yellow font-36">{{$bredcum}}</h2>
              <ol class="breadcrumb text-left mt-10">
                <li class="active text-white">Home</li>
                <li class="active text-white">{{$bredcum}}</li>
              </ol>
            </div>
          </div>
        </div>
      </div>
    </section>
     <!-- Section: Why Choose Us -->
    <section>
      <div class="container">
         <div class="section-content">
               <div class="row" style="text-align:center">
                  <h2 class="line-bottom-center mt-5 mb-5"><span class="text-theme-color-red">{{ $get_pages->master_pages_title}}</span></h2>
            </div>
            <br/>
            <br/>
            <div class="row">
              @if(!empty($get_pages->master_pages_image) && file_exists($get_pages->master_pages_image))
                <div class="col-md-3"> 
                <img src="{{ $get_pages->master_pages_image }}" class="img-fullwidth" alt="">
                </div>
              @endif
                <div class="{{!empty($get_pages->master_pages_image) && file_exists($get_pages->master_pages_image) ? 'col-md-7' : 'col-md-12'}} pb-sm-20">
                    <div class="mb-20 editor-content"><?php echo htmlspecialchars_decode($get_pages->master_pages_description)?></div>
                </div>
            </div>
         </div>

         <div class="section-content">
          <div class="row mb-30">
              @foreach($get_pages->page_images as $school_gallery)
            <div class="col-xs-12 col-sm-4 col-md-4">
              <div class="gallery-block">
                <div class="gallery-thumb">
                  <img alt="project" src="{{ $school_gallery->image}}" class="img-fullwidth">
                </div>
                <div class="overlay-shade green"></div>
                <div class="icons-holder">
                  <div class="icons-holder-inner">
                    <div class="gallery-icon">
                      <a href="{{ $school_gallery->image}}"  data-lightbox-gallery="gallery"><i class="pe-7s-science"></i></a>
                    </div>
                  </div>
                </div>
              </div>
            </div>  
              @endforeach
          </div>
          
        </div>
       </div>
    </section>
@include('footer')
