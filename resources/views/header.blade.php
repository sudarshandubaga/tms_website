<!DOCTYPE html>
<html dir="ltr" lang="en">

<head>
<base href="{{ url('/') }}/">
<!-- Meta Tags -->
<meta name="viewport" content="width=device-width,initial-scale=1.0"/>
<meta http-equiv="content-type" content="text/html; charset=UTF-8"/>
<meta name="description" content="The Modern School | Barmer" />
<meta name="keywords" content="CBSE School in Barmer, Best school in Barmer" />
<meta name="author" content="The Modern School | Barmer" />

<!-- Page Title -->
<title>The Modern School | Barmer</title>

<!-- Favicon and Touch Icons -->
<link href="{{ url('public/assets/images/favicon.png') }}" rel="shortcut icon" type="image/png">

<!-- Stylesheet -->
<link href="{{url('public/assets/css/bootstrap.min.css')}}" rel="stylesheet" type="text/css">
<link href="{{url('public/assets/css/jquery-ui.min.css')}}" rel="stylesheet" type="text/css">
<link href="{{url('public/assets/css/animate.css')}}" rel="stylesheet" type="text/css">
<link href="{{url('public/assets/css/css-plugin-collections.css')}}" rel="stylesheet"/>
<!-- CSS | menuzord megamenu skins -->
<link id="menuzord-menu-skins" href="{{url('public/assets/css/menuzord-skins/menuzord-rounded-boxed.css')}}" rel="stylesheet"/>
<!-- CSS | Main style file -->
<link href="{{url('public/assets/css/style-main.css')}}" rel="stylesheet" type="text/css">
<!-- CSS | Preloader Styles -->
<link href="{{url('public/assets/css/preloader.css')}}" rel="stylesheet" type="text/css">
<!-- CSS | Custom Margin Padding Collection -->
<link href="{{url('public/assets/css/custom-bootstrap-margin-padding.css')}}" rel="stylesheet" type="text/css">
<!-- CSS | Responsive media queries -->
<link href="{{url('public/assets/css/responsive.css')}}" rel="stylesheet" type="text/css">
<!-- CSS | Style css. This is the file where you can place your own custom css code. Just uncomment it and use it. -->
<!-- <link href="css/style.css" rel="stylesheet" type="text/css"> -->

<!-- Revolution Slider 5.x CSS settings -->
<link  href="{{url('public/assets/js/revolution-slider/css/settings.css')}}" rel="stylesheet" type="text/css"/>
<link  href="{{url('public/assets/js/revolution-slider/css/layers.css')}}" rel="stylesheet" type="text/css"/>
<link  href="{{url('public/assets/js/revolution-slider/css/navigation.css')}}" rel="stylesheet" type="text/css"/>

<!-- CSS | Theme Color -->
<link href="{{url('public/assets/css/colors/theme-skin-color-set-1.css')}}" rel="stylesheet" type="text/css">

<!-- external javascripts -->
<script src="{{url('public/assets/js/jquery-2.2.4.min.js')}}"></script>
<script src="{{url('public/assets/js/jquery-ui.min.js')}}"></script>
<script src="{{url('public/assets/js/bootstrap.min.js')}}"></script>
<!-- JS | jquery plugin collection for this theme -->
<script src="{{url('public/assets/js/jquery-plugin-collection.js')}}"></script>

<!-- Revolution Slider 5.x SCRIPTS -->
<script src="{{url('public/assets/js/revolution-slider/js/jquery.themepunch.tools.min.js')}}"></script>
<script src="{{url('public/assets/js/revolution-slider/js/jquery.themepunch.revolution.min.js')}}"></script>

<!-- HTML5 shim and Respond.js for IE8 support of HTML5 elements and media queries -->
<!-- WARNING: Respond.js doesn't work if you view the page via file:// -->
<!--[if lt IE 9]>
  <script src="https://oss.maxcdn.com/html5shiv/3.7.2/html5shiv.min.js"></script>
  <script src="https://oss.maxcdn.com/respond/1.4.2/respond.min.js"></script>
<![endif]-->
</head>
<body class="">
<div id="wrapper" class="clearfix">

  
  <!-- Header -->
  <header id="header" class="header">
  
    <div class="header-middle p-0 bg-lightest xs-text-center">
      <div class="container pt-0 pb-0">
        <div class="row">
          <div class="col-xs-12 col-sm-4 col-md-4">
            <div class="widget no-border m-0">
              <a class="menuzord-brand pull-left flip xs-pull-center mb-15" href="./"><img src="{{ url($account_details->account_logo) }}" alt=""></a>
            </div>
          </div>
          <div class="col-sm-8 col-md-8">
              <div class="row">
                <div class="col-xs-12 col-sm-4 col-md-4 col-xs-4">
                  <div class="widget no-border pull-right sm-pull-none sm-text-center mt-10 mb-10 m-0">
                    <ul class="list-inline">
                      <li><i class="fa fa-android text-theme-color-sky font-36 mt-5 sm-display-block"></i></li>
                      <li>
                        <a href="https://play.google.com/store/apps/details?id=com.kempsolutions.tms" target="_blank" class="font-12 text-black text-uppercase">Google Play Store!</a>
                        <h5 class="font-14 text-theme-color-blue m-0"> Download android app</h5>
                      </li>
                    </ul>
                  </div>
                </div>
                <div class="col-xs-12 col-sm-4 col-md-4 col-xs-4">
                  <div class="widget no-border pull-right sm-pull-none sm-text-center mt-10 mb-10 m-0">
                    <ul class="list-inline">
                      <li><i class="fa fa-phone-square text-theme-color-sky font-36 mt-5 sm-display-block"></i></li>
                      <li>
                        <a href="#" class="font-12 text-black text-uppercase">Call us today!</a>
                        <h5 class="font-14 text-theme-color-blue m-0"> +91 {{ $account_details->account_number_1 }}</h5>
                      </li>
                    </ul>
                  </div>
                </div>  
                <div class="col-xs-12 col-sm-4 col-md-4 col-xs-4">
                  <div class="widget no-border pull-right sm-pull-none sm-text-center mt-10 mb-10 m-0">
                    <ul class="list-inline">
                      <li><i class="fa fa-clock-o text-theme-color-red font-36 mt-5 sm-display-block"></i></li>
                      <li>
                        <a href="#" class="font-12 text-black text-uppercase">We are open!</a>
                        <h5 class="font-13 text-theme-color-blue m-0">{{ $account_details->office_timing_time }}</h5>
                      </li>
                    </ul>
                  </div>
                </div>
              </div>
          </div>
        </div>
      </div>
    </div>
    <div class="header-nav">
      <div class="header-nav-wrapper navbar-scrolltofixed bg-theme-color-red border-bottom-theme-color-sky-2px">
        <div class="container">
          <nav id="menuzord" class="menuzord bg-theme-colored pull-left flip menuzord-responsive">
            <ul class="menuzord-menu">
              <li class="active"><a href="./">Home</a></li>
              <li><a href="#">About Us</a>
                <ul class="dropdown">
                  <li><a href="vision-mission">Vision Mission </a></li>
                  <li><a href="principal-message">Principal Message </a></li>
                  <li><a href="#">School Information </a>
                  <ul class= "dropdown">
                      <li><a href="Mandatory.pdf" target="_blank">Mandatory </a></li>
                      <li><a href="Affiliation.pdf" target="_blank">Affiliation </a></li>
                      <li><a href="Sanstha_Reg.pdf" target="_blank">Sanstha </a></li>
                      <li><a href="noc.pdf" target="_blank">NOC </a></li>
                      <li><a href="rte.pdf" target="_blank">RTE </a></li>
                      <li><a href="Building_safety.pdf" target="_blank">Building Safety </a></li>
                      <li><a href="Fire_Safety.pdf" target="_blank">Fire Safety </a></li>
                      <li><a href="Self_Certify.pdf" target="_blank">Self Certify </a></li>
                      <li><a href="health_Certify.pdf" target="_blank">Health Certify </a></li>
                      <li><a href="Fee_Structure.pdf" target="_blank">Fee Structure </a></li>
                      <li><a href="Academic_Calander.pdf" target="_blank">Academic Calander </a></li>
                      <li><a href="SMC.pdf" target="_blank">SMC </a></li>
                      <li><a href="PTA.pdf" target="_blank">PTA </a></li>
                      <li><a href="Board_Results.pdf" target="_blank">Board Results </a></li>
                      <li><a href="text Book Declaration.pdf" target="_blank">Text Book Declaration </a></li>
                      <li><a href="Annual Report 2020-21.pdf" target="_blank">Annual Report 2020-21 </a></li>
                      <li><a href="Staff Details.pdf" target="_blank">Staff Details </a></li>
                      <li><a href="Syallabus 2020-21.pdf" target="_blank">Syallabus 2020-21</a></li>
                      <li><a href="TC FORMAT.pdf" target="_blank">TC Format </a></li>
                      <li><a href="Teachers Training & Workshop.pdf" target="_blank">Teachers Training & Workshop </a></li>
                      
                      </ul>
                  </li>
                  
                </ul>
              </li>
              <li><a href="academic-policy">Academic </a></li>
              <li><a href="next-leap">The Next Leap </a></li>
              <li><a href="#">Facilities</a>
                <ul class="dropdown">
                  <li><a href="activities">Activities</a></li>
                  <li><a href="sports">Sports</a></li>
                  <li><a href="laboratories">Laboratories</a></li>
                  <li><a href="library">Library</a></li>
                  <li><a href="building">Building</a></li>
                  <li><a href="transport">Transport</a></li>
                </ul>
              </li>
              <li><a href="guidelines-policy">Guidelines</a></li>
              <li><a href="#">Admission </a>
                <ul class="dropdown">
                  <li><a href="admission-policy">Admission Policy </a></li>
                  <li><a href="registration">Online Registration</a></li>
                  <li><a href="download">Download Form</a></li>
                </ul>
              </li>
              <li><a href="#">Gallery</a>
                <ul class="dropdown">
                  <li><a href="gallery">Photos</a></li>
                  <li><a href="videogallery">Videos</a></li>
                 <li><a href="virtual-tour">Virtual Tour</a></li>
                </ul>
              </li>
              <li><a href="#">Downloads</a>
                <ul class="dropdown">
                  <li><a href="download">Downloads</a></li>
                  <li><a href="tc">TC</a></li>
                 <!-- <li><a href="#">Virtual Tour</a></li>-->
                </ul>
              </li>
              
             <li><a href="blog">Blogs</a></li>
              <li><a href="contact-us">Contact </a></li>
              <!-- <li><a href="online-fees-deposite">Fees</a></li> -->
             
       
            </ul>
            <ul class="pull-right flip hidden-sm hidden-xs">
              <li>
                <!-- Modal: Book Now Starts -->
                <a class="btn btn-colored btn-flat bg-theme-color-sky text-white font-14 bs-modal-ajax-load mt-24 " href="https://schoolerp.in/tms/" target="_blank" style="padding: 5px 10px; border-radius: 0 0 3px 3px;">ERP Login</a>
                <!-- Modal: Book Now End -->
              </li>
            </ul>
            <div id="top-search-bar" class="collapse">
              <div class="container">
                <form role="search" action="#" class="search_form_top" method="get">
                  <input type="text" placeholder="Type text and press Enter..." name="s" class="form-control" autocomplete="off">
                  <span class="search-close"><i class="fa fa-search"></i></span>
                </form>
              </div>
            </div>
          </nav>
        </div>
      </div>
    </div>
  </header>
  
  <!-- Start main-content -->
  <div class="main-content">
      
   