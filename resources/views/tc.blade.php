@include('header')
   <!-- Section: inner-header -->
    <section class="inner-header divider  layer-overlay overlay-dark-5" data-bg-img="public/assets/images/bg/br.jpg">
      <div class="container pt-70 pb-20">
        <!-- Section Content -->
        <div class="section-content">
          <div class="row"> 
            <div class="col-md-6">
              <h2 class="text-theme-color-yellow font-36">{{$bredcum}}</h2>
              <ol class="breadcrumb text-left mt-10">
                <li class="active text-white">Home</li>
                <li class="active text-white">{{$bredcum}}</li>
              </ol>
            </div>
          </div>
        </div>
      </div>
    </section>
<section>
	<div class="container">
	    <form name="form" method="post" id="myForm"  autocomplete="off" > 
	  
        <div class="row">
             <!-- <div class="form-group col-md-5">
                <input type="text"  placeholder="Sr. No." class="form-control" id="sno" required=""  maxlength="4" minlength="1">
                <span class="error" id="error2"></span>
            </div> -->
            <div class="form-group col-md-5">
                <input type="text"  placeholder="Admission No" class="form-control" id="admission" required="" maxlength="4" minlength="1">
                <span class="error" id="error1"></span>
            </div>
           
            <div class="form-group col-md-2">
                <button class="btn btn-success" id="searchbtn">Search <i class="fa fa-search" aria-hidden="true"></i></button>
            </div>
        </div>
        </form>
        <div id="failresult" class="alert alert-danger" style="display:none">
        </div>
          
        <div class="table-responsive" id="successdiv" style="display:none">
            <table class="table table-bordered" >
                <thead>
                    <tr>
                        <th>Sr. Number</th>
                        <th>Student Name</th>
                        <th>Admission Number</th>
                        <th>Download</th>
                    </tr>
                </thead>
                <tbody>
                    <tr>
                        <td id="tc_number_result"></td>
                        <td id="name_result"></td>
                        <td id="sr_number_result"></td>
                        <td id="downloadfile"></td>
                    </tr>
                </tbody>
            </table>
        </div>
        

	</div>
</section>
    
   
<script type="text/javascript">
    $(document).ready(function(){
        
         $('#myForm').on('submit', function(event){
            event.preventDefault();
            
            var sno =  $("#sno").val();
            var admission =  $("#admission").val();
      
                        $.ajaxSetup({
                      headers: {
                        'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content')
                      }
                    });

                      $.ajax({
                                 type: 'POST',
                                 url: 'tcfetch',
                                 data:{
                                      sno:sno,
                                      admission:admission
                                    },
                                 success: function(response){ 
                                     console.log(response);
                                    //  console.log(response.Success[0]['tc_id']);
                                    if(response.Fail)
                                    {
                                        $("#successdiv").hide();
                                        $("#failresult").show();
                                        $("#failresult").html(response.Fail);
                                    }
                                    else
                                    {
                                        $("#failresult").hide();
                                        $("#successdiv").show();
                                        $("#tc_number_result").html(response.Success[0]['s_number']);
                                        $("#name_result").html(response.Success[0]['tc_name']);
                                        $("#sr_number_result").html(response.Success[0]['admission_number']);
                                        var img = response.Success[0]['tc_file'];
                                        $("#downloadfile").html('<a target="_blank" href="<?php echo url('uploads/tcs/') ?>/'+img+'"><i class="fa fa-download"></i></a>');
                                    }
                                   }
                             });
  
         });
      
           
                    
   
       });

</script> 
    
@include('footer')
