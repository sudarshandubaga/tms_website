  <!-- end main-content -->
  </div>

 
  <footer id="footer" class="footer divider layer-overlay overlay-dark-2" style="color:black; background-image:url({{url('public/assets/mages/bg/f1.png')}}) ">
    <div class="container">
      <div class="row border-bottom">
        
        <div class="col-md-3">
          <div class="widget dark">
            <img class="mt-5 mb-20" alt="" src="{{ $account_details->account_logo}}" >
           
            <ul class="list-inline mt-5">
             <p>{{ $account_details->account_message }}</p>
            </ul>
          </div>
        </div>
        <div class="col-md-1"></div>
        <div class="col-md-2">
          <div class="widget dark">
            <h4 class="widget-title">Useful Links</h4>
            <ul class="list angle-double-right list-border">
              <li><a style="color:black; font-weight:bold" href="disclaimer-policy">Disclaimer Policy</a></li>
              <li><a style="color:black; font-weight:bold" href="privacy-policy">Privacy Policy</a></li>   
              <li><a style="color:black; font-weight:bold" href="refund-policy">Refund Policy</a></li>  
              <li><a style="color:black; font-weight:bold" href="shipping-delivery">Shipping & Delivery Policy</a></li> 
              <li><a style="color:black; font-weight:bold" href="terms-conditions">Terms & Conditions</a></li> 
             
            </ul>
          </div>
        </div>
        <div class="col-md-1"></div>
       
        <div class="col-md-5">
          <div class="widget dark">
            <h4 class="widget-title">Contact Us</h4>
            <ul class="list-inline mt-5">
                <li class="m-0 pl-10 pr-10"> 
                  <i class="fa fa-phone text-theme-color-red mr-5"></i> 
                  <a class="mytext" href="#" style="color:black; font-weight:bold">+91 {{ $account_details->account_number_1 }}</a>, &nbsp;&nbsp;<a style="color:black; font-weight:bold" class="mytext" href="#">+91 {{ $account_details->account_number_2 }}</a> 
                </li>
                <li class="m-0 pl-10 pr-10"> 
                    <i class="fa fa-envelope-o text-theme-color-blue mr-5"></i> 
                    <a class="mytext" href="#" style="color:black; font-weight:bold">{{ $account_details->account_email }}</a> 
                </li>
                <li class="m-0 pl-10 pr-10"> 
                    <i class="fa fa-map-marker text-theme-color-orange mr-5"></i> 
                    <a class="mytext" href="#" style="color:black; font-weight:bold">{{ $account_details->account_address }}</a> 
                </li>
            </ul>
            
            <h5 class="widget-title mt-50 mb-10">Connect With Us</h5>
            <ul class="styled-icons icon-bordered icon-sm">
              <li>
                <a href="{{ $account_details->account_facebook }}" target="_blank">
                  <!-- <i class="fa fa-facebook-official"></i> -->
                  <img src="{{ url('public/assets/images/fb.png') }}" alt="Facebook">
                </a>
              </li>
              <li>
                <a href="{{ $account_details->account_youtube }}" target="_blank">
                  <!-- <i class="fa fa-youtube"></i> -->
                  <img src="{{ url('public/assets/images/ytube.png') }}" alt="YouTube">
                </a>
              </li>
              <li>
                <a href="https://www.instagram.com/themodernschoolbarmer/?hl=en" target="_blank">
                  <!-- <i class="fa fa-facebook-official"></i> -->
                  <img src="https://www.themodernschoolbarmer.com/public/assets/images/insta.png" alt="Instagram">
                </a>
              </li>
              <li>
                <a href="https://twitter.com/TheModernSchoo2" target="_blank">
                  <!-- <i class="fa fa-facebook-official"></i> -->
                  <img src="https://www.themodernschoolbarmer.com/public/assets/images/twitter-logo-3.png" alt="Twitter">
                </a>
              </li>
            </ul>
          </div>
        </div>
        
      </div>
      
    </div>
<div> 
          <img alt="" src="public/assets/images/bg/f2.png" class="img-responsive img-fullwidth">
      </div>
  </footer>
  <a class="scrollToTop" href="#"><i class="fa fa-angle-up"></i></a>
</div>

<script src="public/assets/js/custom.js"></script>

<script type="text/javascript" src="public/assets/js/revolution-slider/js/extensions/revolution.extension.actions.min.js"></script>
<script type="text/javascript" src="public/assets/js/revolution-slider/js/extensions/revolution.extension.carousel.min.js"></script>
<script type="text/javascript" src="public/assets/js/revolution-slider/js/extensions/revolution.extension.kenburn.min.js"></script>
<script type="text/javascript" src="public/assets/js/revolution-slider/js/extensions/revolution.extension.layeranimation.min.js"></script>
<script type="text/javascript" src="public/assets/js/revolution-slider/js/extensions/revolution.extension.migration.min.js"></script>
<script type="text/javascript" src="public/assets/js/revolution-slider/js/extensions/revolution.extension.navigation.min.js"></script>
<script type="text/javascript" src="public/assets/js/revolution-slider/js/extensions/revolution.extension.parallax.min.js"></script>
<script type="text/javascript" src="public/assets/js/revolution-slider/js/extensions/revolution.extension.slideanims.min.js"></script>
<script type="text/javascript" src="public/assets/js/revolution-slider/js/extensions/revolution.extension.video.min.js"></script>

</body>

</html>
