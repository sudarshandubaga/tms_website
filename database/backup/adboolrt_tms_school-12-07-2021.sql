-- phpMyAdmin SQL Dump
-- version 5.0.3
-- https://www.phpmyadmin.net/
--
-- Host: 127.0.0.1
-- Generation Time: Jul 12, 2021 at 05:56 AM
-- Server version: 10.4.14-MariaDB
-- PHP Version: 7.4.11

SET SQL_MODE = "NO_AUTO_VALUE_ON_ZERO";
START TRANSACTION;
SET time_zone = "+00:00";


/*!40101 SET @OLD_CHARACTER_SET_CLIENT=@@CHARACTER_SET_CLIENT */;
/*!40101 SET @OLD_CHARACTER_SET_RESULTS=@@CHARACTER_SET_RESULTS */;
/*!40101 SET @OLD_COLLATION_CONNECTION=@@COLLATION_CONNECTION */;
/*!40101 SET NAMES utf8mb4 */;

--
-- Database: `adboolrt_tms_school`
--

-- --------------------------------------------------------

--
-- Table structure for table `blog`
--

CREATE TABLE `blog` (
  `id` int(10) UNSIGNED NOT NULL,
  `title` varchar(191) COLLATE utf8mb4_unicode_ci NOT NULL,
  `slug` varchar(191) COLLATE utf8mb4_unicode_ci NOT NULL,
  `image` varchar(191) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `description` longtext COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

-- --------------------------------------------------------

--
-- Table structure for table `migrations`
--

CREATE TABLE `migrations` (
  `id` int(10) UNSIGNED NOT NULL,
  `migration` varchar(255) COLLATE utf8mb4_unicode_ci NOT NULL,
  `batch` int(11) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

--
-- Dumping data for table `migrations`
--

INSERT INTO `migrations` (`id`, `migration`, `batch`) VALUES
(1, '2021_07_11_152113_create_blog_table', 1),
(2, '2021_07_11_153414_create_news_table', 1);

-- --------------------------------------------------------

--
-- Table structure for table `news`
--

CREATE TABLE `news` (
  `id` int(10) UNSIGNED NOT NULL,
  `title` varchar(191) COLLATE utf8mb4_unicode_ci NOT NULL,
  `slug` varchar(191) COLLATE utf8mb4_unicode_ci NOT NULL,
  `image` varchar(191) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `description` longtext COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

--
-- Dumping data for table `news`
--

INSERT INTO `news` (`id`, `title`, `slug`, `image`, `description`, `created_at`, `updated_at`) VALUES
(1, 'some title', 'some-title', NULL, '<b>some </b>description', '2021-07-11 21:56:19', '2021-07-11 21:56:19');

-- --------------------------------------------------------

--
-- Table structure for table `tms_account_settings`
--

CREATE TABLE `tms_account_settings` (
  `account_id` int(11) NOT NULL,
  `account_name` varchar(255) DEFAULT NULL,
  `account_email` varchar(255) DEFAULT NULL,
  `account_number_1` varchar(255) DEFAULT NULL,
  `account_number_2` varchar(255) DEFAULT NULL,
  `account_number_3` varchar(255) DEFAULT NULL,
  `account_message` text DEFAULT NULL,
  `account_logo` varchar(255) DEFAULT NULL,
  `office_timing_day` varchar(255) DEFAULT NULL,
  `office_timing_time` varchar(255) DEFAULT NULL,
  `account_address` longtext DEFAULT NULL,
  `account_facebook` varchar(255) DEFAULT NULL,
  `account_twitter` varchar(255) DEFAULT NULL,
  `account_youtube` varchar(255) DEFAULT NULL,
  `account_skype` varchar(255) DEFAULT NULL,
  `account_pintrest` varchar(255) DEFAULT NULL,
  `account_tumblr` varchar(255) DEFAULT NULL,
  `account_stumbleupon` varchar(255) DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data for table `tms_account_settings`
--

INSERT INTO `tms_account_settings` (`account_id`, `account_name`, `account_email`, `account_number_1`, `account_number_2`, `account_number_3`, `account_message`, `account_logo`, `office_timing_day`, `office_timing_time`, `account_address`, `account_facebook`, `account_twitter`, `account_youtube`, `account_skype`, `account_pintrest`, `account_tumblr`, `account_stumbleupon`) VALUES
(1, 'tms', 'contact@themodernschoolbarmer.com', '70232 00444', '92144 06895', NULL, 'Spread in the beautiful surroundings of around 5 Bighas of land, The Modern School, Barmer was established in the year 2007 with the vision of providing unparalleled education that is relevant, sensible and constant.', 'uploads/admin/1618809029.png', 'Monday – Saturday', '08.00 AM – 03.00 PM', 'Near N.H.-68,Gadan Road, Barmer-344001 (RAJ.)', 'https://www.facebook.com/themodernschoolbarmer', NULL, 'https://www.youtube.com/channel/UCY13I5aVywlMp6xMBMaBOYw', 'https://www.skype.com', 'https://www.pintrest.com', 'https://www.tumblr.com', 'https://www.stumbleupon.com');

-- --------------------------------------------------------

--
-- Table structure for table `tms_admin`
--

CREATE TABLE `tms_admin` (
  `admin_id` int(11) NOT NULL,
  `admin_name` varchar(255) NOT NULL,
  `admin_email` varchar(255) NOT NULL,
  `admin_number` varchar(255) NOT NULL,
  `admin_image` varchar(255) NOT NULL,
  `admin_password` varchar(255) NOT NULL,
  `reset_password_status` int(11) NOT NULL DEFAULT 0
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data for table `tms_admin`
--

INSERT INTO `tms_admin` (`admin_id`, `admin_name`, `admin_email`, `admin_number`, `admin_image`, `admin_password`, `reset_password_status`) VALUES
(1, 'tms', 'tms', '999 9999999', 'uploads/admin/1618391870.png', '0192023a7bbd73250516f069df18b500', 1);

-- --------------------------------------------------------

--
-- Table structure for table `tms_admission`
--

CREATE TABLE `tms_admission` (
  `admission_id` int(11) NOT NULL,
  `syear` varchar(10) COLLATE utf8_unicode_ci DEFAULT NULL,
  `class` varchar(255) COLLATE utf8_unicode_ci DEFAULT NULL,
  `name` varchar(255) COLLATE utf8_unicode_ci DEFAULT NULL,
  `fname` varchar(255) COLLATE utf8_unicode_ci DEFAULT NULL,
  `mname` varchar(255) COLLATE utf8_unicode_ci DEFAULT NULL,
  `date` varchar(255) COLLATE utf8_unicode_ci DEFAULT NULL,
  `gender` varchar(255) COLLATE utf8_unicode_ci DEFAULT NULL,
  `address` varchar(255) COLLATE utf8_unicode_ci DEFAULT NULL,
  `state` varchar(255) COLLATE utf8_unicode_ci DEFAULT NULL,
  `city` varchar(255) COLLATE utf8_unicode_ci DEFAULT NULL,
  `mobile` varchar(255) COLLATE utf8_unicode_ci DEFAULT NULL,
  `pno` varchar(255) COLLATE utf8_unicode_ci DEFAULT NULL,
  `email` varchar(255) COLLATE utf8_unicode_ci DEFAULT NULL,
  `write_cap` varchar(10) COLLATE utf8_unicode_ci DEFAULT NULL,
  `read_cap` varchar(20) COLLATE utf8_unicode_ci DEFAULT NULL,
  `mode` varchar(20) COLLATE utf8_unicode_ci DEFAULT NULL,
  `submit` varchar(20) COLLATE utf8_unicode_ci DEFAULT NULL,
  `page` varchar(20) COLLATE utf8_unicode_ci DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;

--
-- Dumping data for table `tms_admission`
--

INSERT INTO `tms_admission` (`admission_id`, `syear`, `class`, `name`, `fname`, `mname`, `date`, `gender`, `address`, `state`, `city`, `mobile`, `pno`, `email`, `write_cap`, `read_cap`, `mode`, `submit`, `page`) VALUES
(1, '2021-22', 'Class IV', 'MK', 'sad', 'sad', '2021-04-28', 'Male', 'sad', 'sad', 'sad', '7489798798', '4654564', 'mayank@gmail.com', '267918', '267918', 'save', NULL, 'registration_submit'),
(2, '2021-22', 'Class IV', 'MK', 'sad', 'sad', '2021-04-28', 'Male', 'sad', 'sad', 'sad', '7489798798', '4654564', 'mayank@gmail.com', '267918', '267918', 'save', NULL, 'registration_submit'),
(3, '2021-22', 'UKG', 'Aaradhya Sharma', 'Kiran Sharma', 'Munmun Sharma', '2016-12-16', 'Female', 'Vill- Kalamjote, PO- Sushrutanagar, Dist- Darjeeling', 'West Bengal', 'Silliguri', '7908697045', '9478089943', 'msabnamh@gmail.com', '256730', '256730', 'save', NULL, 'registration_submit');

-- --------------------------------------------------------

--
-- Table structure for table `tms_admission_points`
--

CREATE TABLE `tms_admission_points` (
  `admission_point_id` int(11) NOT NULL,
  `admission_point` longtext DEFAULT NULL,
  `admission_point_status` int(11) NOT NULL DEFAULT 1 COMMENT '1 - Active 0 -  Deactive',
  `admission_point_created` timestamp NOT NULL DEFAULT current_timestamp()
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data for table `tms_admission_points`
--

INSERT INTO `tms_admission_points` (`admission_point_id`, `admission_point`, `admission_point_status`, `admission_point_created`) VALUES
(1, 'The Registration Form (attached with the prospectus or downloaded from our website) must be completed and the child registered. However, registration does not guarantee a seat reservation. After examining the child the Principal reserves the right to admit the child into the class deemed suitable for him / her.', 0, '2017-12-24 19:15:03'),
(2, 'Registrations may be closed at any time at the discretion of the Principal.', 0, '2017-12-24 19:15:03'),
(3, 'Admissions are done at the discretion of the school authorities and the Principal reserves the right to accept or reject any application for admission without assigning any reason.', 0, '2017-12-24 19:15:39'),
(4, 'A child when selected for admission will be treated as confirmed only if the fee has been paid within the stipulated time.', 0, '2017-12-24 19:15:39'),
(5, 'A copy of Municipal Birth Certificate Or a School Leaving Certificate from the last school attended should be submitted with one stamp / p.p. size photograph at the time of admission.', 0, '2017-12-24 19:16:17'),
(6, 'Admission to Neo Dales is a privilege not a right.', 0, '2017-12-24 19:16:17'),
(7, 'Age Requirements.', 0, '2017-12-24 19:16:26'),
(8, 'A p.p. size pic of the child must be mailed to principal@neodales.edu.in once admission has taken place mentioning the name of the child.', 0, '2018-01-04 08:57:24'),
(9, 'Age Requirement', 0, '2018-01-04 08:59:44'),
(10, 'Click on Admission > Online Registration', 1, '2020-12-26 07:13:27'),
(11, 'Complete the “Registration Cum Admission Form” online and click on submit.\r\n(The following soft copies of documents are required for uploading at the time of filling the form)\r\n•	Photograph of the child\r\n•	Signatures of both parents\r\n•	Self-Attested Municipal Birth Certificate \r\n•	Category Certificate (if applicable)\r\n•	Transfer Certificate of the previous school (if available)', 1, '2020-12-26 07:14:11'),
(13, 'Once you click on submit, you will be redirected to our Payment Gateway to pay the Registration Fees.', 1, '2020-12-26 07:14:40'),
(14, 'Once the form is successfully submitted you will receive an SMS from the school within 2 working days specifying the Interaction date & time with the Principal. (Pre Nursery & Nursery) OR the date & time for Entrance Test. (L.K.G. & U.K.G.)', 1, '2020-12-26 07:15:56'),
(15, 'After assessing the child and interaction with parents the Principal reserves the right to admit the child into the class deemed suitable for him/her.', 1, '2020-12-26 07:16:26'),
(16, 'You will then receive an SMS regarding admission approved or rejected within 2 working days. In case of admission approval, you are expected to pay the fees within 7 days or else the admission stands cancelled.', 1, '2020-12-26 07:17:00'),
(17, 'Registrations may be closed at any time at the discretion of the Principal.', 1, '2020-12-26 07:17:44'),
(18, 'Registration does not guarantee a seat reservation.', 1, '2020-12-26 07:18:18'),
(19, 'Admissions are done at the discretion of the school authorities and the Principal reserves the right to accept or reject any application for admission without assigning any reason thereof.', 1, '2020-12-26 07:20:59'),
(20, 'Age Requirement', 1, '2020-12-26 07:28:40');

-- --------------------------------------------------------

--
-- Table structure for table `tms_career`
--

CREATE TABLE `tms_career` (
  `id` int(10) NOT NULL,
  `file` varchar(255) DEFAULT NULL,
  `user_mail` varchar(255) DEFAULT NULL,
  `upload_at` timestamp NULL DEFAULT current_timestamp()
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data for table `tms_career`
--

INSERT INTO `tms_career` (`id`, `file`, `user_mail`, `upload_at`) VALUES
(1, '68697393.jpg', 'sanyaraheja07@gmail.com', '2020-12-06 11:29:34'),
(2, '1966709283.jpg', 'arora.naina97@gmail.com', '2020-12-10 19:27:02'),
(3, '1532676293.jpeg', 'AROHI@NEODALES.COM', '2020-12-12 08:54:11'),
(4, '2068511280.jpg', 'arohi@neodales.com', '2020-12-16 08:35:52'),
(5, '1816504292.jpg', 'Subiyasiddiqui07@gmail.com', '2020-12-17 18:10:01'),
(6, '616093631.jpg', 'aanchalsikka95@gmail.com', '2020-12-28 11:19:39'),
(7, '945372113.jpg', 'inshasiddiqui7860@gmail.com', '2021-01-06 06:25:11'),
(8, '1748871927.jpg', 'vanshikadhingra17@gmail.com', '2021-01-09 08:25:46'),
(9, '491463381.JPG', 'mohta_priti@rediffmail.com', '2021-01-20 08:39:00');

-- --------------------------------------------------------

--
-- Table structure for table `tms_city_masters`
--

CREATE TABLE `tms_city_masters` (
  `id` int(12) NOT NULL,
  `name` varchar(200) DEFAULT NULL,
  `state_master_id` int(12) DEFAULT NULL,
  `removed` enum('Y','N') DEFAULT 'N',
  `created` datetime DEFAULT NULL,
  `modified` datetime DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data for table `tms_city_masters`
--

INSERT INTO `tms_city_masters` (`id`, `name`, `state_master_id`, `removed`, `created`, `modified`) VALUES
(3, 'Moradabad   ', 2, 'N', NULL, NULL),
(4, 'Fatehpur', 1, 'N', '2018-03-23 22:11:27', '2018-03-23 22:11:27'),
(5, 'Guwahati', 10, 'N', '2019-09-30 15:00:48', '2019-09-30 15:00:48'),
(6, 'Patna', 9, 'N', '2019-09-30 15:01:07', '2019-09-30 15:01:07'),
(7, 'Muzaffarpur', 9, 'N', '2019-09-30 15:01:21', '2019-09-30 15:01:21'),
(8, 'Samastipur', 9, 'N', '2019-09-30 15:01:35', '2019-09-30 15:01:35'),
(9, 'Begusarai', 9, 'N', '2019-09-30 15:01:49', '2019-09-30 15:01:49'),
(10, 'New Delhi', 11, 'N', '2019-09-30 15:02:06', '2019-09-30 15:02:06'),
(11, 'Fatehabad', 7, 'N', '2019-09-30 15:02:26', '2019-09-30 15:02:26'),
(12, 'Panipat', 7, 'N', '2019-09-30 15:02:37', '2019-09-30 15:02:37'),
(13, 'Bokaro', 5, 'N', '2019-09-30 15:02:53', '2019-09-30 15:02:53'),
(14, 'Imphal', 12, 'N', '2019-09-30 15:03:13', '2019-09-30 15:03:13'),
(15, 'Chandigarh', 13, 'N', '2019-09-30 15:03:30', '2019-09-30 15:03:30'),
(16, 'Jhunjhunu', 1, 'N', '2019-09-30 15:07:57', '2019-09-30 15:07:57'),
(17, 'Merta City', 1, 'N', '2019-09-30 15:08:27', '2019-09-30 15:08:27'),
(18, 'Bikaner', 1, 'N', '2019-09-30 15:09:01', '2019-09-30 15:09:01'),
(19, 'Pilani', 1, 'N', '2019-09-30 15:09:25', '2019-10-01 15:48:27'),
(20, 'Khetri', 1, 'N', '2019-09-30 15:09:51', '2019-09-30 15:09:51'),
(21, 'Jaipur', 1, 'N', '2019-09-30 15:10:16', '2019-09-30 15:10:16'),
(22, 'Alwar', 1, 'N', '2019-09-30 15:10:37', '2019-09-30 15:10:37'),
(23, 'Dungargarh', 1, 'N', '2019-09-30 15:10:55', '2019-09-30 15:10:55'),
(24, 'Nawalgarh', 1, 'N', '2019-09-30 15:11:20', '2019-09-30 15:11:20'),
(25, 'Chittorgarh', 1, 'N', '2019-09-30 15:12:07', '2019-09-30 15:12:07'),
(26, 'Bundi', 1, 'N', '2019-09-30 15:12:34', '2019-09-30 15:12:34'),
(27, 'Sikar', 1, 'N', '2019-09-30 15:14:11', '2019-09-30 15:14:11'),
(28, 'Laxmangarh', 1, 'N', '2019-09-30 15:14:36', '2019-09-30 15:14:36'),
(29, 'Kota', 1, 'N', '2019-09-30 15:14:53', '2019-09-30 15:14:53'),
(30, 'Tara Nagar', 1, 'N', '2019-09-30 15:15:30', '2019-09-30 15:15:30'),
(31, 'Chirawa', 1, 'N', '2019-09-30 15:15:40', '2019-09-30 15:15:40'),
(32, 'Bhadra', 1, 'N', '2019-09-30 15:16:02', '2019-09-30 15:16:02'),
(33, 'Varanasi', 2, 'N', '2019-09-30 15:16:38', '2019-09-30 15:16:38'),
(34, 'Mainpuri', 2, 'N', '2019-09-30 15:16:50', '2019-09-30 15:16:50'),
(35, 'Gorakhpur', 2, 'N', '2019-09-30 15:17:02', '2019-09-30 15:17:02'),
(36, 'Mau', 2, 'N', '2019-09-30 15:17:19', '2019-09-30 15:17:19'),
(37, 'Aligarh', 2, 'N', '2019-09-30 15:17:34', '2019-09-30 15:17:34'),
(38, 'Hathras', 2, 'N', '2019-09-30 15:17:45', '2019-09-30 15:17:45'),
(39, 'Meerut', 1, 'N', '2019-09-30 15:17:57', '2019-09-30 15:17:57'),
(40, 'Kanpur', 2, 'N', '2019-09-30 15:18:09', '2019-09-30 15:18:09'),
(41, 'Nainital', 14, 'N', '2019-09-30 15:18:35', '2019-09-30 15:18:35'),
(42, 'Machlandpur', 8, 'N', '2019-09-30 15:18:52', '2019-09-30 15:18:52'),
(43, 'Delhi', 11, 'N', '2019-10-06 12:22:37', '2019-10-06 12:22:37'),
(44, 'Bangalore', 25, 'N', '2019-10-06 12:22:37', '2019-10-06 12:22:37'),
(45, 'Hyderabad', 17, 'N', '2019-10-06 12:22:37', '2019-10-06 12:22:37'),
(46, 'Ahmedabad', 6, 'N', '2019-10-06 12:22:37', '2019-10-06 12:22:37'),
(47, 'Chennai', 18, 'N', '2019-10-06 12:22:37', '2019-10-06 12:22:37'),
(48, 'Kolkata', 8, 'N', '2019-10-06 12:22:37', '2019-10-06 12:22:37'),
(49, 'Surat', 6, 'N', '2019-10-06 12:22:37', '2019-10-06 12:22:37'),
(50, 'Pune', 4, 'N', '2019-10-06 12:22:37', '2019-10-06 12:22:37'),
(51, 'Lucknow', 2, 'N', '2019-10-06 12:22:37', '2019-10-06 12:22:37'),
(52, 'Nagpur', 4, 'N', '2019-10-06 12:22:37', '2019-10-06 12:22:37'),
(53, 'Indore', 3, 'N', '2019-10-06 12:22:37', '2019-10-06 12:22:37'),
(54, 'Thane', 4, 'N', '2019-10-06 12:22:37', '2019-10-06 12:22:37'),
(55, 'Bhopal', 3, 'N', '2019-10-06 12:22:37', '2019-10-06 12:22:37'),
(56, 'Visakhapatnam', 30, 'N', '2019-10-06 12:22:37', '2019-10-06 12:22:37'),
(57, 'Pimpri-Chinchwad', 4, 'N', '2019-10-06 12:22:37', '2019-10-06 12:22:37'),
(58, 'Vadodara', 6, 'N', '2019-10-06 12:22:37', '2019-10-06 12:22:37'),
(59, 'Ghaziabad', 2, 'N', '2019-10-06 12:22:37', '2019-10-06 12:22:37'),
(60, 'Ludhiana', 13, 'N', '2019-10-06 12:22:37', '2019-10-06 12:22:37'),
(61, 'Agra', 2, 'N', '2019-10-06 12:22:37', '2019-10-06 12:22:37'),
(62, 'Nashik', 4, 'N', '2019-10-06 12:22:37', '2019-10-06 12:22:37'),
(63, 'Faridabad', 7, 'N', '2019-10-06 12:22:37', '2019-10-06 12:22:37'),
(64, 'Rajkot', 6, 'N', '2019-10-06 12:22:37', '2019-10-06 12:22:37'),
(65, 'Kalyan-Dombivli', 4, 'N', '2019-10-06 12:22:37', '2019-10-06 12:22:37'),
(66, 'Vasai-Virar', 4, 'N', '2019-10-06 12:22:37', '2019-10-06 12:22:37'),
(67, 'Srinagar', 33, 'N', '2019-10-06 12:22:37', '2019-10-06 12:22:37'),
(68, 'Aurangabad', 4, 'N', '2019-10-06 12:22:37', '2019-10-06 12:22:37'),
(69, 'Dhanbad', 5, 'N', '2019-10-06 12:22:37', '2019-10-06 12:22:37'),
(70, 'Amritsar', 13, 'N', '2019-10-06 12:22:37', '2019-10-06 12:22:37'),
(71, 'Navi Mumbai', 4, 'N', '2019-10-06 12:22:37', '2019-10-06 12:22:37'),
(72, 'Allahabad', 2, 'N', '2019-10-06 12:22:37', '2019-10-06 12:22:37'),
(73, 'Howrah', 8, 'N', '2019-10-06 12:22:37', '2019-10-06 12:22:37'),
(74, 'Ranchi', 5, 'N', '2019-10-06 12:22:37', '2019-10-06 12:22:37'),
(75, 'Gwalior', 3, 'N', '2019-10-06 12:22:37', '2019-10-06 12:22:37'),
(76, 'Jabalpur', 3, 'N', '2019-10-06 12:22:37', '2019-10-06 12:22:37'),
(77, 'Coimbatore', 18, 'N', '2019-10-06 12:22:37', '2019-10-06 12:22:37'),
(78, 'Vijayawada', 30, 'N', '2019-10-06 12:22:37', '2019-10-06 12:22:37'),
(79, 'Jodhpur', 1, 'N', '2019-10-06 12:22:37', '2019-10-06 12:22:37'),
(80, 'Madurai', 18, 'N', '2019-10-06 12:22:37', '2019-10-06 12:22:37'),
(81, 'Raipur', 27, 'N', '2019-10-06 12:22:37', '2019-10-06 12:22:37'),
(82, 'Kota', 1, 'Y', '2019-10-06 12:22:37', '2019-10-15 10:49:25'),
(83, 'Solapur', 4, 'N', '2019-10-06 12:22:37', '2019-10-06 12:22:37'),
(84, 'Hubli?Dharwad', 25, 'N', '2019-10-06 12:22:37', '2019-10-06 12:22:37'),
(85, 'Tiruchirappalli', 18, 'N', '2019-10-06 12:22:37', '2019-10-06 12:22:37'),
(86, 'Bareilly', 2, 'N', '2019-10-06 12:22:37', '2019-10-06 12:22:37'),
(87, 'Mysore', 25, 'N', '2019-10-06 12:22:37', '2019-10-06 12:22:37'),
(88, 'Tiruppur', 18, 'N', '2019-10-06 12:22:37', '2019-10-06 12:22:37'),
(89, 'Gurgaon', 7, 'N', '2019-10-06 12:22:37', '2019-10-06 12:22:37'),
(90, 'Jalandhar', 13, 'N', '2019-10-06 12:22:37', '2019-10-06 12:22:37'),
(91, 'Bhubaneswar', 20, 'N', '2019-10-06 12:22:37', '2019-10-06 12:22:37'),
(92, 'Salem', 18, 'N', '2019-10-06 12:22:37', '2019-10-06 12:22:37'),
(93, 'Mira-Bhayandar', 4, 'N', '2019-10-06 12:22:37', '2019-10-06 12:22:37'),
(94, 'Warangal', 17, 'N', '2019-10-06 12:22:37', '2019-10-06 12:22:37'),
(95, 'Jalgaon', 4, 'N', '2019-10-06 12:22:37', '2019-10-06 12:22:37'),
(96, 'Guntur', 30, 'N', '2019-10-06 12:22:37', '2019-10-06 12:23:22'),
(97, 'Bhiwandi', 4, 'N', '2019-10-06 12:22:37', '2019-10-06 12:22:37'),
(98, 'Saharanpur', 2, 'N', '2019-10-06 12:22:37', '2019-10-06 12:22:37'),
(99, 'Amravati', 4, 'N', '2019-10-06 12:22:37', '2019-10-06 12:22:37'),
(100, 'Noida', 2, 'N', '2019-10-06 12:22:37', '2019-10-06 12:22:37'),
(101, 'Jamshedpur', 5, 'N', '2019-10-06 12:22:37', '2019-10-06 12:22:37'),
(102, 'Bhilai', 27, 'N', '2019-10-06 12:22:37', '2019-10-06 12:22:37'),
(103, 'Cuttack', 20, 'N', '2019-10-06 12:22:37', '2019-10-06 12:22:37'),
(104, 'Firozabad', 2, 'N', '2019-10-06 12:22:37', '2019-10-06 12:22:37'),
(105, 'Kochi', 24, 'N', '2019-10-06 12:22:37', '2019-10-06 12:22:37'),
(106, 'Nellore', 30, 'N', '2019-10-06 12:22:37', '2019-10-06 12:23:35'),
(107, 'Bhavnagar', 6, 'N', '2019-10-06 12:22:37', '2019-10-06 12:22:37'),
(108, 'Dehradun', 14, 'N', '2019-10-06 12:22:37', '2019-10-06 12:22:37'),
(109, 'Durgapur', 8, 'N', '2019-10-06 12:22:37', '2019-10-06 12:22:37'),
(110, 'Asansol', 8, 'N', '2019-10-06 12:22:37', '2019-10-06 12:22:37'),
(111, 'Rourkela', 20, 'N', '2019-10-06 12:22:37', '2019-10-06 12:22:37'),
(112, 'Nanded', 4, 'N', '2019-10-06 12:22:37', '2019-10-06 12:22:37'),
(113, 'Kolhapur', 4, 'N', '2019-10-06 12:22:37', '2019-10-06 12:22:37'),
(114, 'Ajmer', 1, 'N', '2019-10-06 12:22:37', '2019-10-06 12:22:37'),
(115, 'Akola', 4, 'N', '2019-10-06 12:22:37', '2019-10-06 12:22:37'),
(116, 'Gulbarga', 25, 'N', '2019-10-06 12:22:37', '2019-10-06 12:22:37'),
(117, 'Jamnagar', 6, 'N', '2019-10-06 12:22:37', '2019-10-06 12:22:37'),
(118, 'Ujjain', 3, 'N', '2019-10-06 12:22:37', '2019-10-06 12:22:37'),
(119, 'Loni', 2, 'N', '2019-10-06 12:22:37', '2019-10-06 12:22:37'),
(120, 'Siliguri', 8, 'N', '2019-10-06 12:22:37', '2019-10-06 12:22:37'),
(121, 'Jhansi', 2, 'N', '2019-10-06 12:22:37', '2019-10-06 12:22:37'),
(122, 'Ulhasnagar', 4, 'N', '2019-10-06 12:22:37', '2019-10-06 12:22:37'),
(123, 'Jammu', 33, 'N', '2019-10-06 12:22:37', '2019-10-06 12:22:37'),
(124, 'Sangli-Miraj & Kupwad', 4, 'N', '2019-10-06 12:22:37', '2019-10-06 12:22:37'),
(125, 'Mangalore', 25, 'N', '2019-10-06 12:22:37', '2019-10-06 12:22:37'),
(126, 'Erode', 18, 'N', '2019-10-06 12:22:37', '2019-10-06 12:22:37'),
(127, 'Belgaum', 25, 'N', '2019-10-06 12:22:37', '2019-10-06 12:22:37'),
(128, 'Ambattur', 18, 'N', '2019-10-06 12:22:37', '2019-10-06 12:22:37'),
(129, 'Tirunelveli', 18, 'N', '2019-10-06 12:22:37', '2019-10-06 12:22:37'),
(130, 'Malegaon', 4, 'N', '2019-10-06 12:22:37', '2019-10-06 12:22:37'),
(131, 'Gaya', 9, 'N', '2019-10-06 12:22:37', '2019-10-06 12:22:37'),
(132, 'Thiruvananthapuram', 24, 'N', '2019-10-06 12:22:37', '2019-10-06 12:22:37'),
(133, 'Udaipur', 1, 'N', '2019-10-06 12:22:37', '2019-10-06 12:22:37'),
(134, 'Maheshtala', 8, 'N', '2019-10-06 12:22:37', '2019-10-06 12:22:37'),
(135, 'Davanagere', 25, 'N', '2019-10-06 12:22:37', '2019-10-06 12:22:37'),
(136, 'Kozhikode', 24, 'N', '2019-10-06 12:22:37', '2019-10-06 12:22:37'),
(137, 'Kurnool', 30, 'N', '2019-10-06 12:22:37', '2019-10-06 12:22:37'),
(138, 'Rajpur Sonarpur', 8, 'N', '2019-10-06 12:22:37', '2019-10-06 12:22:37'),
(139, 'Rajahmundry', 30, 'N', '2019-10-06 12:22:37', '2019-10-06 12:22:37'),
(140, 'South Dumdum', 8, 'N', '2019-10-06 12:22:37', '2019-10-06 12:22:37'),
(141, 'Bellary', 25, 'N', '2019-10-06 12:22:37', '2019-10-06 12:22:37'),
(142, 'Patiala', 13, 'N', '2019-10-06 12:22:37', '2019-10-06 12:22:37'),
(143, 'Gopalpur', 8, 'N', '2019-10-06 12:22:37', '2019-10-06 12:22:37'),
(144, 'Agartala', 16, 'N', '2019-10-06 12:22:37', '2019-10-06 12:22:37'),
(145, 'Bhagalpur', 9, 'N', '2019-10-06 12:22:37', '2019-10-06 12:22:37'),
(146, 'Muzaffarnagar', 2, 'N', '2019-10-06 12:22:37', '2019-10-06 12:22:37'),
(147, 'Bhatpara', 8, 'N', '2019-10-06 12:22:37', '2019-10-06 12:22:37'),
(148, 'Panihati', 8, 'N', '2019-10-06 12:22:37', '2019-10-06 12:22:37'),
(149, 'Latur', 4, 'N', '2019-10-06 12:22:37', '2019-10-06 12:22:37'),
(150, 'Dhule', 4, 'N', '2019-10-06 12:22:37', '2019-10-06 12:22:37'),
(151, 'Tirupati', 30, 'N', '2019-10-06 12:22:37', '2019-10-06 12:22:37'),
(152, 'Rohtak', 7, 'N', '2019-10-06 12:22:37', '2019-10-06 12:22:37'),
(153, 'Korba', 27, 'N', '2019-10-06 12:22:37', '2019-10-06 12:22:37'),
(154, 'Bhilwara', 1, 'N', '2019-10-06 12:22:37', '2019-10-06 12:22:37'),
(155, 'Berhampur', 20, 'N', '2019-10-06 12:22:37', '2019-10-06 12:22:37'),
(156, 'Ahmednagar', 4, 'N', '2019-10-06 12:22:37', '2019-10-06 12:22:37'),
(157, 'Mathura', 2, 'N', '2019-10-06 12:22:37', '2019-10-06 12:22:37'),
(158, 'Kollam', 24, 'N', '2019-10-06 12:22:37', '2019-10-06 12:22:37'),
(159, 'Avadi', 18, 'N', '2019-10-06 12:22:37', '2019-10-06 12:22:37'),
(160, 'Kadapa', 30, 'N', '2019-10-06 12:22:37', '2019-10-06 12:22:37'),
(161, 'Kamarhati', 8, 'N', '2019-10-06 12:22:37', '2019-10-06 12:22:37'),
(162, 'Sambalpur', 20, 'N', '2019-10-06 12:22:37', '2019-10-06 12:22:37'),
(163, 'Bilaspur', 27, 'N', '2019-10-06 12:22:37', '2019-10-06 12:22:37'),
(164, 'Shahjahanpur', 2, 'N', '2019-10-06 12:22:37', '2019-10-06 12:22:37'),
(165, 'Satara', 4, 'N', '2019-10-06 12:22:37', '2019-10-06 12:22:37'),
(166, 'Bijapur', 25, 'N', '2019-10-06 12:22:37', '2019-10-06 12:22:37'),
(167, 'Kakinada', 30, 'N', '2019-10-06 12:22:37', '2019-10-06 12:22:37'),
(168, 'Rampur', 2, 'N', '2019-10-06 12:22:37', '2019-10-06 12:22:37'),
(169, 'Shimoga', 25, 'N', '2019-10-06 12:22:37', '2019-10-06 12:22:37'),
(170, 'Chandrapur', 4, 'N', '2019-10-06 12:22:37', '2019-10-06 12:22:37'),
(171, 'Junagadh', 6, 'N', '2019-10-06 12:22:37', '2019-10-06 12:22:37'),
(172, 'Thrissur', 24, 'N', '2019-10-06 12:22:37', '2019-10-06 12:22:37'),
(173, 'Bardhaman', 8, 'N', '2019-10-06 12:22:37', '2019-10-06 12:22:37'),
(174, 'Kulti', 8, 'N', '2019-10-06 12:22:37', '2019-10-06 12:22:37'),
(175, 'Nizamabad', 17, 'N', '2019-10-06 12:22:37', '2019-10-06 12:22:37'),
(176, 'Parbhani', 4, 'N', '2019-10-06 12:22:37', '2019-10-06 12:22:37'),
(177, 'Tumkur', 25, 'N', '2019-10-06 12:22:37', '2019-10-06 12:22:37'),
(178, 'Khammam', 17, 'N', '2019-10-06 12:22:37', '2019-10-06 12:22:37'),
(179, 'Ozhukarai', 36, 'N', '2019-10-06 12:22:37', '2019-10-06 12:22:37'),
(180, 'Bihar Sharif', 9, 'N', '2019-10-06 12:22:37', '2019-10-06 12:22:37'),
(181, 'Darbhanga', 9, 'N', '2019-10-06 12:22:37', '2019-10-06 12:22:37'),
(182, 'Bally', 8, 'N', '2019-10-06 12:22:37', '2019-10-06 12:22:37'),
(183, 'Aizawl', 22, 'N', '2019-10-06 12:22:37', '2019-10-06 12:22:37'),
(184, 'Dewas', 3, 'N', '2019-10-06 12:22:37', '2019-10-06 12:22:37'),
(185, 'Ichalkaranji', 4, 'N', '2019-10-06 12:22:37', '2019-10-06 12:22:37'),
(186, 'Karnal', 7, 'N', '2019-10-06 12:22:37', '2019-10-06 12:22:37'),
(187, 'Bathinda', 13, 'N', '2019-10-06 12:22:37', '2019-10-06 12:22:37'),
(188, 'Jalna', 4, 'N', '2019-10-06 12:22:37', '2019-10-06 12:22:37'),
(189, 'Eluru', 30, 'N', '2019-10-06 12:22:37', '2019-10-06 12:22:37'),
(190, 'Barasat', 8, 'N', '2019-10-06 12:22:37', '2019-10-06 12:22:37'),
(191, 'Kirari Suleman Nagar', 11, 'N', '2019-10-06 12:22:37', '2019-10-06 12:22:37'),
(192, 'Purnia', 9, 'N', '2019-10-06 12:22:37', '2019-10-06 12:23:55'),
(193, 'Satna', 3, 'N', '2019-10-06 12:22:37', '2019-10-06 12:22:37'),
(194, 'Sonipat', 7, 'N', '2019-10-06 12:22:37', '2019-10-06 12:22:37'),
(195, 'Farrukhabad', 2, 'N', '2019-10-06 12:22:37', '2019-10-06 12:22:37'),
(196, 'Sagar', 3, 'N', '2019-10-06 12:22:37', '2019-10-06 12:22:37'),
(197, 'Durg', 27, 'N', '2019-10-06 12:22:37', '2019-10-06 12:22:37'),
(198, 'Ratlam', 3, 'N', '2019-10-06 12:22:37', '2019-10-06 12:22:37'),
(199, 'Hapur', 2, 'N', '2019-10-06 12:22:37', '2019-10-06 12:22:37'),
(200, 'Arrah', 9, 'N', '2019-10-06 12:22:37', '2019-10-06 12:22:37'),
(201, 'Anantapur', 30, 'N', '2019-10-06 12:22:37', '2019-10-06 12:22:37'),
(202, 'Karimnagar', 17, 'N', '2019-10-06 12:22:37', '2019-10-06 12:22:37'),
(203, 'Etawah', 2, 'N', '2019-10-06 12:22:37', '2019-10-06 12:22:37'),
(204, 'Ambarnath', 4, 'N', '2019-10-06 12:22:37', '2019-10-06 12:22:37'),
(205, 'North Dumdum', 8, 'N', '2019-10-06 12:22:37', '2019-10-06 12:22:37'),
(206, 'Bharatpur', 1, 'N', '2019-10-06 12:22:37', '2019-10-06 12:22:37'),
(207, 'Gandhidham', 6, 'N', '2019-10-06 12:22:37', '2019-10-06 12:22:37'),
(208, 'Baranagar', 8, 'N', '2019-10-06 12:22:37', '2019-10-06 12:22:37'),
(209, 'Tiruvottiyur', 18, 'N', '2019-10-06 12:22:37', '2019-10-06 12:22:37'),
(210, 'Pondicherry', 36, 'N', '2019-10-06 12:22:37', '2019-10-06 12:22:37'),
(211, 'Thoothukudi', 18, 'N', '2019-10-06 12:22:37', '2019-10-06 12:22:37'),
(212, 'Rewa', 3, 'N', '2019-10-06 12:22:37', '2019-10-06 12:22:37'),
(213, 'Mirzapur', 2, 'N', '2019-10-06 12:22:37', '2019-10-06 12:22:37'),
(214, 'Raichur', 25, 'N', '2019-10-06 12:22:37', '2019-10-06 12:22:37'),
(215, 'Pali', 1, 'N', '2019-10-06 12:22:37', '2019-10-06 12:22:37'),
(216, 'Ramagundam', 17, 'N', '2019-10-06 12:22:37', '2019-10-06 12:22:37'),
(217, 'Haridwar', 14, 'N', '2019-10-06 12:22:37', '2019-10-06 12:22:37'),
(218, 'Vijayanagaram', 30, 'N', '2019-10-06 12:22:37', '2019-10-06 12:22:37'),
(219, 'Katihar', 9, 'N', '2019-10-06 12:22:37', '2019-10-06 12:22:37'),
(220, 'Nagercoil', 18, 'N', '2019-10-06 12:22:37', '2019-10-06 12:22:37'),
(221, 'Sri Ganganagar', 1, 'N', '2019-10-06 12:22:37', '2019-10-06 12:22:37'),
(222, 'Karawal Nagar', 11, 'N', '2019-10-06 12:22:37', '2019-10-06 12:22:37'),
(223, 'Mango', 5, 'N', '2019-10-06 12:22:37', '2019-10-06 12:22:37'),
(224, 'Thanjavur', 18, 'N', '2019-10-06 12:22:37', '2019-10-06 12:22:37'),
(225, 'Bulandshahr', 2, 'N', '2019-10-06 12:22:37', '2019-10-06 12:22:37'),
(226, 'Uluberia', 8, 'N', '2019-10-06 12:22:37', '2019-10-06 12:22:37'),
(227, 'Katni', 3, 'N', '2019-10-06 12:22:37', '2019-10-06 12:22:37'),
(228, 'Sambhal', 2, 'N', '2019-10-06 12:22:37', '2019-10-06 12:22:37'),
(229, 'Singrauli', 3, 'N', '2019-10-06 12:22:37', '2019-10-06 12:22:37'),
(230, 'Nadiad', 6, 'N', '2019-10-06 12:22:37', '2019-10-06 12:22:37'),
(231, 'Secunderabad', 17, 'N', '2019-10-06 12:22:37', '2019-10-06 12:22:37'),
(232, 'Naihati', 8, 'N', '2019-10-06 12:22:37', '2019-10-06 12:22:37'),
(233, 'Yamunanagar', 7, 'N', '2019-10-06 12:22:37', '2019-10-06 12:22:37'),
(234, 'Bidhannagar', 8, 'N', '2019-10-06 12:22:37', '2019-10-06 12:22:37'),
(235, 'Pallavaram', 18, 'N', '2019-10-06 12:22:37', '2019-10-06 12:22:37'),
(236, 'Bidar', 25, 'N', '2019-10-06 12:22:37', '2019-10-06 12:22:37'),
(237, 'Munger', 9, 'N', '2019-10-06 12:22:37', '2019-10-06 12:22:37'),
(238, 'Panchkula', 7, 'N', '2019-10-06 12:22:37', '2019-10-06 12:22:37'),
(239, 'Burhanpur', 3, 'N', '2019-10-06 12:22:37', '2019-10-06 12:22:37'),
(240, 'Raurkela Industrial Township', 20, 'N', '2019-10-06 12:22:37', '2019-10-06 12:22:37'),
(241, 'Kharagpur', 8, 'N', '2019-10-06 12:22:37', '2019-10-06 12:22:37'),
(242, 'Dindigul', 18, 'N', '2019-10-06 12:22:37', '2019-10-06 12:22:37'),
(243, 'Gandhinagar', 6, 'N', '2019-10-06 12:22:37', '2019-10-06 12:22:37'),
(244, 'Hospet', 25, 'N', '2019-10-06 12:22:37', '2019-10-06 12:22:37'),
(245, 'Nangloi Jat', 11, 'N', '2019-10-06 12:22:37', '2019-10-06 12:22:37'),
(246, 'Malda', 8, 'N', '2019-10-06 12:22:37', '2019-10-06 12:22:37'),
(247, 'Ongole', 30, 'N', '2019-10-06 12:22:37', '2019-10-06 12:22:37'),
(248, 'Deoghar', 5, 'N', '2019-10-06 12:22:37', '2019-10-06 12:22:37'),
(249, 'Chapra', 9, 'N', '2019-10-06 12:22:37', '2019-10-06 12:22:37'),
(250, 'Haldia', 8, 'N', '2019-10-06 12:22:37', '2019-10-06 12:22:37'),
(251, 'Khandwa', 3, 'N', '2019-10-06 12:22:37', '2019-10-06 12:22:37'),
(252, 'Nandyal', 30, 'N', '2019-10-06 12:22:37', '2019-10-06 12:22:37'),
(253, 'Morena', 3, 'N', '2019-10-06 12:22:37', '2019-10-06 12:22:37'),
(254, 'Amroha', 2, 'N', '2019-10-06 12:22:37', '2019-10-06 12:22:37'),
(255, 'Anand', 6, 'N', '2019-10-06 12:22:37', '2019-10-06 12:22:37'),
(256, 'Bhind', 3, 'N', '2019-10-06 12:22:37', '2019-10-06 12:22:37'),
(257, 'Bhalswa Jahangir Pur', 11, 'N', '2019-10-06 12:22:37', '2019-10-06 12:22:37'),
(258, 'Madhyamgram', 8, 'N', '2019-10-06 12:22:37', '2019-10-06 12:22:37'),
(259, 'Bhiwani', 7, 'N', '2019-10-06 12:22:37', '2019-10-06 12:22:37'),
(260, 'Berhampore', 8, 'N', '2019-10-06 12:22:37', '2019-10-06 12:22:37'),
(261, 'Ambala', 7, 'N', '2019-10-06 12:22:37', '2019-10-06 12:22:37'),
(262, 'Morbi', 6, 'N', '2019-10-06 12:22:37', '2019-10-06 12:22:37'),
(263, 'Raebareli', 2, 'N', '2019-10-06 12:22:37', '2019-10-06 12:22:37'),
(264, 'Mahaboobnagar', 17, 'N', '2019-10-06 12:22:37', '2019-10-06 12:22:37'),
(265, 'Chittoor', 30, 'N', '2019-10-06 12:22:37', '2019-10-06 12:22:37'),
(266, 'Bhusawal', 4, 'N', '2019-10-06 12:22:37', '2019-10-06 12:22:37'),
(267, 'Orai', 2, 'N', '2019-10-06 12:22:37', '2019-10-06 12:22:37'),
(268, 'Bahraich', 2, 'N', '2019-10-06 12:22:37', '2019-10-06 12:22:37'),
(269, 'Vellore', 18, 'N', '2019-10-06 12:22:37', '2019-10-06 12:22:37'),
(270, 'Mehsana', 6, 'N', '2019-10-06 12:22:37', '2019-10-06 12:22:37'),
(271, 'Raiganj', 8, 'N', '2019-10-06 12:22:37', '2019-10-06 12:22:37'),
(272, 'Sirsa', 7, 'N', '2019-10-06 12:22:37', '2019-10-06 12:22:37'),
(273, 'Danapur', 9, 'N', '2019-10-06 12:22:37', '2019-10-06 12:22:37'),
(274, 'Serampore', 8, 'N', '2019-10-06 12:22:37', '2019-10-06 12:22:37'),
(275, 'Sultan Pur Majra', 11, 'N', '2019-10-06 12:22:37', '2019-10-06 12:22:37'),
(276, 'Guna', 3, 'N', '2019-10-06 12:22:37', '2019-10-06 12:22:37'),
(277, 'Jaunpur', 2, 'N', '2019-10-06 12:22:37', '2019-10-06 12:22:37'),
(278, 'Panvel', 4, 'N', '2019-10-06 12:22:37', '2019-10-06 12:22:37'),
(279, 'Shivpuri', 3, 'N', '2019-10-06 12:22:37', '2019-10-06 12:22:37'),
(280, 'Surendranagar Dudhrej', 6, 'N', '2019-10-06 12:22:37', '2019-10-06 12:22:37'),
(281, 'Unnao', 2, 'N', '2019-10-06 12:22:37', '2019-10-06 12:22:37'),
(282, 'Chinsurah', 8, 'N', '2019-10-06 12:22:37', '2019-10-06 12:22:37'),
(283, 'Alappuzha', 24, 'N', '2019-10-06 12:22:37', '2019-10-06 12:22:37'),
(284, 'Kottayam', 24, 'N', '2019-10-06 12:22:37', '2019-10-06 12:22:37'),
(285, 'Machilipatnam', 30, 'N', '2019-10-06 12:22:37', '2019-10-06 12:22:37'),
(286, 'Shimla', 15, 'N', '2019-10-06 12:22:37', '2019-10-06 12:22:37'),
(287, 'Adoni', 30, 'N', '2019-10-06 12:22:37', '2019-10-06 12:22:37'),
(288, 'Udupi', 25, 'N', '2019-10-06 12:22:37', '2019-10-06 12:22:37'),
(289, 'Tenali', 30, 'N', '2019-10-06 12:22:37', '2019-10-06 12:22:37'),
(290, 'Proddatur', 30, 'N', '2019-10-06 12:22:37', '2019-10-06 12:22:37'),
(291, 'Saharsa', 9, 'N', '2019-10-06 12:22:37', '2019-10-06 12:22:37'),
(292, 'Hindupur', 30, 'N', '2019-10-06 12:22:37', '2019-10-06 12:22:37'),
(293, 'Sasaram', 9, 'N', '2019-10-06 12:22:37', '2019-10-06 12:22:37'),
(294, 'Hajipur', 9, 'N', '2019-10-06 12:22:37', '2019-10-06 12:22:37'),
(295, 'Bhimavaram', 30, 'N', '2019-10-06 12:22:37', '2019-10-06 12:22:37'),
(296, 'Kumbakonam', 18, 'N', '2019-10-06 12:22:37', '2019-10-06 12:22:37'),
(297, 'Dehri', 9, 'N', '2019-10-06 12:22:37', '2019-10-06 12:22:37'),
(298, 'Madanapalle', 30, 'N', '2019-10-06 12:22:37', '2019-10-06 12:22:37'),
(299, 'Siwan', 9, 'N', '2019-10-06 12:22:37', '2019-10-06 12:22:37'),
(300, 'Bettiah', 9, 'N', '2019-10-06 12:22:37', '2019-10-06 12:22:37'),
(301, 'Guntakal', 30, 'N', '2019-10-06 12:22:37', '2019-10-06 12:22:37'),
(302, 'Srikakulam', 30, 'N', '2019-10-06 12:22:37', '2019-10-06 12:22:37'),
(303, 'Motihari', 9, 'N', '2019-10-06 12:22:37', '2019-10-06 12:22:37'),
(304, 'Dharmavaram', 30, 'N', '2019-10-06 12:22:37', '2019-10-06 12:22:37'),
(305, 'Gudivada', 30, 'N', '2019-10-06 12:22:37', '2019-10-06 12:22:37'),
(306, 'Phagwara', 13, 'N', '2019-10-06 12:22:37', '2019-10-06 12:22:37'),
(307, 'Narasaraopet', 30, 'N', '2019-10-06 12:22:37', '2019-10-06 12:22:37'),
(308, 'Suryapet', 17, 'N', '2019-10-06 12:22:37', '2019-10-06 12:22:37'),
(309, 'Miryalaguda', 17, 'N', '2019-10-06 12:22:37', '2019-10-06 12:22:37'),
(310, 'Tadipatri', 30, 'N', '2019-10-06 12:22:37', '2019-10-06 12:22:37'),
(311, 'Karaikudi', 18, 'N', '2019-10-06 12:22:37', '2019-10-06 12:22:37'),
(312, 'Kishanganj', 9, 'N', '2019-10-06 12:22:37', '2019-10-06 12:22:37'),
(313, 'Jamalpur', 9, 'N', '2019-10-06 12:22:37', '2019-10-06 12:22:37'),
(314, 'Kavali', 30, 'N', '2019-10-06 12:22:37', '2019-10-06 12:22:37'),
(315, 'Tadepalligudem', 30, 'N', '2019-10-06 12:22:37', '2019-10-06 12:22:37'),
(316, 'Amaravati', 30, 'N', '2019-10-06 12:22:37', '2019-10-06 12:22:37'),
(317, 'Buxar', 9, 'N', '2019-10-06 12:22:37', '2019-10-06 12:22:37'),
(318, 'Jehanabad', 9, 'N', '2019-10-06 12:22:37', '2019-10-06 12:22:37'),
(319, 'Aurangabad', 9, 'N', '2019-10-06 12:22:37', '2019-10-06 12:22:37'),
(320, 'Gangtok', 19, 'N', '2019-10-06 12:22:37', '2019-10-06 12:22:37'),
(321, 'Baksa', 10, 'N', '2019-10-10 08:58:55', '2019-10-10 08:58:55'),
(322, 'Barpeta', 10, 'N', '2019-10-10 08:59:10', '2019-10-10 08:59:10'),
(323, 'Charkhi Dadri', 7, 'N', '2019-10-10 17:03:23', '2019-10-10 17:03:23'),
(324, 'Gurugram', 7, 'N', '2019-10-10 17:04:08', '2019-10-10 17:04:08'),
(325, 'Hisar', 7, 'N', '2019-10-10 17:04:50', '2019-10-10 17:04:50'),
(326, 'Jhajjar', 7, 'N', '2019-10-10 17:05:15', '2019-10-10 17:05:15'),
(327, 'Jind', 7, 'N', '2019-10-10 17:11:34', '2019-10-10 17:11:34'),
(328, 'Kaithal', 7, 'N', '2019-10-10 17:11:52', '2019-10-10 17:11:52'),
(329, 'Kurukshetra', 7, 'N', '2019-10-10 17:12:19', '2019-10-10 17:12:19'),
(330, 'Mahendragarh', 7, 'N', '2019-10-10 17:12:42', '2019-10-10 17:12:42'),
(331, 'Nuh', 7, 'N', '2019-10-10 17:12:55', '2019-10-10 17:12:55'),
(332, 'Palwal', 7, 'N', '2019-10-10 17:13:13', '2019-10-10 17:13:13'),
(333, 'Rewari', 7, 'N', '2019-10-10 17:13:58', '2019-10-10 17:13:58'),
(334, 'Biswanath', 10, 'N', '2019-10-10 17:15:40', '2019-10-10 17:15:40'),
(335, 'Bongaigaon', 10, 'N', '2019-10-10 17:17:01', '2019-10-10 17:17:01'),
(336, 'Cachar', 10, 'N', '2019-10-10 17:17:22', '2019-10-10 17:17:22'),
(337, 'Charaideo', 7, 'Y', '2019-10-10 17:17:36', '2019-10-15 10:59:59'),
(338, 'Chirang', 10, 'N', '2019-10-10 17:17:50', '2019-10-10 17:17:50'),
(339, 'Darrang', 10, 'N', '2019-10-10 17:18:04', '2019-10-10 17:18:04'),
(340, 'Dhemaji', 10, 'N', '2019-10-10 17:18:22', '2019-10-10 17:18:22'),
(341, 'Dhubri', 10, 'N', '2019-10-10 17:19:05', '2019-10-10 17:19:05'),
(342, 'Dibrugarh', 10, 'N', '2019-10-10 17:19:21', '2019-10-10 17:19:21'),
(343, 'Dima Hasao', 10, 'N', '2019-10-10 17:19:34', '2019-10-10 17:19:34'),
(344, 'Goalpara', 10, 'N', '2019-10-10 17:19:53', '2019-10-10 17:19:53'),
(345, 'Golaghat', 10, 'N', '2019-10-10 17:20:08', '2019-10-10 17:20:08'),
(346, 'Hailakandi', 10, 'N', '2019-10-10 17:20:26', '2019-10-10 17:20:26'),
(347, 'Hojai', 10, 'N', '2019-10-10 17:20:41', '2019-10-10 17:20:41'),
(348, 'Jorhat', 10, 'N', '2019-10-10 17:20:53', '2019-10-10 17:20:53'),
(349, 'Kamrup Metropolitan', 10, 'N', '2019-10-10 17:21:12', '2019-10-10 17:21:12'),
(350, 'Kamrup', 10, 'N', '2019-10-10 17:21:26', '2019-10-10 17:21:26'),
(351, 'Karbi Anglong', 10, 'N', '2019-10-10 17:21:41', '2019-10-10 17:21:41'),
(352, 'Karimganj', 10, 'N', '2019-10-10 17:21:56', '2019-10-10 17:21:56'),
(353, 'Kokrajhar', 10, 'N', '2019-10-10 17:22:11', '2019-10-10 17:22:11'),
(354, 'Lakhimpur', 10, 'N', '2019-10-10 17:22:27', '2019-10-10 17:22:27'),
(355, 'Majuli', 10, 'N', '2019-10-10 17:22:53', '2019-10-10 17:22:53'),
(356, 'Morigaon', 10, 'N', '2019-10-10 17:24:15', '2019-10-10 17:24:15'),
(357, 'Nagaon', 10, 'N', '2019-10-10 17:25:38', '2019-10-10 17:25:38'),
(358, 'Nalbari', 10, 'N', '2019-10-10 17:26:02', '2019-10-10 17:26:02'),
(359, 'Sivasagar', 10, 'N', '2019-10-10 17:26:29', '2019-10-10 17:26:29'),
(360, 'Sonitpur', 10, 'N', '2019-10-10 17:27:01', '2019-10-10 17:27:01'),
(361, 'Tinsukia', 10, 'N', '2019-10-10 17:28:23', '2019-10-10 17:28:23'),
(362, 'Udalguri', 10, 'N', '2019-10-10 17:28:54', '2019-10-10 17:30:09'),
(363, 'West Karbi Anglong', 10, 'N', '2019-10-10 17:29:25', '2019-10-10 17:29:25'),
(364, 'Arwal', 9, 'N', '2019-10-10 18:59:39', '2019-10-10 18:59:39'),
(365, 'Banka', 9, 'N', '2019-10-10 19:00:16', '2019-10-10 19:00:16'),
(366, 'Bhojpur', 9, 'N', '2019-10-10 19:02:00', '2019-10-10 19:02:00'),
(367, 'East Champaran', 9, 'N', '2019-10-10 19:02:40', '2019-10-10 19:02:40'),
(368, 'Hanumangarh', 1, 'N', '2019-10-15 10:45:47', '2019-10-15 10:45:47'),
(369, 'Araria', 9, 'N', '2019-10-15 10:47:23', '2019-10-15 10:47:23'),
(370, 'Banswara', 1, 'N', '2019-10-15 10:52:31', '2019-10-15 10:52:31'),
(371, 'Baran', 1, 'N', '2019-10-15 10:52:49', '2019-10-15 10:52:49'),
(372, 'Barmer', 1, 'N', '2019-10-15 10:53:04', '2019-10-15 10:53:04'),
(373, 'Churu', 1, 'N', '2019-10-15 10:53:43', '2019-10-15 10:53:43'),
(374, 'Dausa', 1, 'N', '2019-10-15 10:53:58', '2019-10-15 10:53:58'),
(375, 'Dholpur', 1, 'N', '2019-10-15 10:54:41', '2019-10-15 10:54:41'),
(376, 'Dungarpur', 1, 'N', '2019-10-15 10:55:03', '2019-10-15 10:55:03'),
(377, 'Jaisalmer', 1, 'N', '2019-10-15 10:55:35', '2019-10-15 10:55:35'),
(378, 'Jalore', 1, 'N', '2019-10-15 10:55:49', '2019-10-15 10:55:49'),
(379, 'Jhalawar', 1, 'N', '2019-10-15 10:56:03', '2019-10-15 10:56:03'),
(380, 'Karauli', 1, 'N', '2019-10-15 10:56:25', '2019-10-15 10:56:25'),
(381, 'Nagaur', 1, 'N', '2019-10-15 10:56:50', '2019-10-15 10:56:50'),
(382, 'Pratapgarh', 1, 'N', '2019-10-15 10:57:12', '2019-10-15 10:57:12'),
(383, 'Rajsamand', 1, 'N', '2019-10-15 10:57:24', '2019-10-15 10:57:24'),
(384, 'Sawai Madhopur', 1, 'N', '2019-10-15 10:57:35', '2019-10-15 10:57:35'),
(385, 'Sirohi', 1, 'N', '2019-10-15 10:57:58', '2019-10-15 10:57:58'),
(386, 'Tonk', 1, 'N', '2019-10-15 10:58:28', '2019-10-15 10:58:28'),
(387, 'Barnala', 13, 'N', '2019-10-15 11:03:03', '2019-10-15 11:03:03'),
(388, 'Faridkot', 13, 'N', '2019-10-15 11:03:24', '2019-10-15 11:03:24'),
(389, 'Fatehgarh Sahib', 13, 'N', '2019-10-15 11:03:43', '2019-10-15 11:03:43'),
(390, 'Firozpur', 13, 'N', '2019-10-15 11:04:09', '2019-10-15 11:04:09'),
(391, 'Fazilka', 13, 'N', '2019-10-15 11:04:26', '2019-10-15 11:04:26'),
(392, 'Gurdaspur', 13, 'N', '2019-10-15 11:04:43', '2019-10-15 11:04:43'),
(393, 'Hoshiarpur', 13, 'N', '2019-10-15 11:04:57', '2019-10-15 11:04:57'),
(394, 'Kapurthala', 13, 'N', '2019-10-15 11:05:23', '2019-10-15 11:05:23'),
(395, 'Mansa', 13, 'N', '2019-10-15 11:05:57', '2019-10-15 11:05:57'),
(396, 'Moga', 13, 'N', '2019-10-15 11:06:13', '2019-10-15 11:06:13'),
(397, 'Sri Muktsar Sahib', 13, 'N', '2019-10-15 11:06:37', '2019-10-15 11:06:37'),
(398, 'Pathankot', 13, 'N', '2019-10-15 11:06:59', '2019-10-15 11:06:59'),
(399, 'Rupnagar', 13, 'N', '2019-10-15 11:07:38', '2019-10-15 11:07:38'),
(400, 'Mohali', 13, 'N', '2019-10-15 11:08:01', '2019-10-15 11:08:01'),
(401, 'Sangrur', 13, 'N', '2019-10-15 11:08:29', '2019-10-15 11:08:29'),
(402, 'Taran Taran Sahib', 13, 'N', '2019-10-15 11:08:48', '2019-10-15 11:08:48'),
(403, 'Shahid Bhagat Singh Nagar', 13, 'N', '2019-10-15 11:09:11', '2019-10-15 11:09:11'),
(404, 'Bishnupur', 12, 'N', '2019-10-15 11:10:18', '2019-10-15 11:10:18'),
(405, 'Thoubal', 12, 'N', '2019-10-15 11:10:36', '2019-10-15 11:10:36'),
(406, 'Imphal East', 12, 'N', '2019-10-15 11:10:53', '2019-10-15 11:10:53'),
(407, 'Imphal West', 12, 'N', '2019-10-15 11:11:11', '2019-10-15 11:11:11'),
(408, 'Senapati', 12, 'N', '2019-10-15 11:11:27', '2019-10-15 11:11:27'),
(409, 'Ukhrul', 12, 'N', '2019-10-15 11:11:41', '2019-10-15 11:11:41'),
(410, 'Chandel', 12, 'N', '2019-10-15 11:11:54', '2019-10-15 11:11:54'),
(411, 'Churachandpur', 12, 'N', '2019-10-15 11:12:10', '2019-10-15 11:12:10'),
(412, 'Tamenglong', 12, 'N', '2019-10-15 11:12:22', '2019-10-15 11:12:22'),
(413, 'Jiribam', 12, 'N', '2019-10-15 11:12:35', '2019-10-15 11:12:35'),
(414, 'Kangpokpi', 12, 'N', '2019-10-15 11:12:50', '2019-10-15 11:12:50'),
(415, 'Kakching', 12, 'N', '2019-10-15 11:13:03', '2019-10-15 11:13:03'),
(416, 'Tengnoupal', 12, 'N', '2019-10-15 11:13:17', '2019-10-15 11:13:17'),
(417, 'Kamjong', 12, 'N', '2019-10-15 11:13:30', '2019-10-15 11:13:30'),
(418, 'Noney', 12, 'N', '2019-10-15 11:13:46', '2019-10-15 11:13:46'),
(419, 'Pherzawl', 12, 'N', '2019-10-15 11:14:00', '2019-10-15 11:14:00'),
(420, 'Dimapur', 21, 'N', '2019-10-15 11:14:28', '2019-10-15 11:14:28'),
(421, 'Kiphire', 21, 'N', '2019-10-15 11:14:41', '2019-10-15 11:14:41'),
(422, 'Kohima', 21, 'N', '2019-10-15 11:14:51', '2019-10-15 11:14:51'),
(423, 'Longleng', 21, 'N', '2019-10-15 11:15:04', '2019-10-15 11:15:04'),
(424, 'Mokokchung', 21, 'N', '2019-10-15 11:15:15', '2019-10-15 11:15:15'),
(425, 'Mon', 21, 'N', '2019-10-15 11:15:27', '2019-10-15 11:15:27'),
(426, 'Peren', 21, 'N', '2019-10-15 11:15:38', '2019-10-15 11:15:38'),
(427, 'Phek', 21, 'N', '2019-10-15 11:15:50', '2019-10-15 11:15:50'),
(428, 'Tuensang', 21, 'N', '2019-10-15 11:16:02', '2019-10-15 11:16:02'),
(429, 'Wokha', 21, 'N', '2019-10-15 11:16:12', '2019-10-15 11:16:12'),
(430, 'Zunheboto', 21, 'N', '2019-10-15 11:16:24', '2019-10-15 11:16:24'),
(431, 'Noklak', 21, 'N', '2019-10-15 11:16:34', '2019-10-15 11:16:34'),
(432, 'Kolasib', 22, 'N', '2019-10-15 11:17:38', '2019-10-15 11:17:38'),
(433, 'Lawngtlai', 22, 'N', '2019-10-15 11:18:03', '2019-10-15 11:18:03'),
(434, 'Lunglei', 22, 'N', '2019-10-15 11:18:17', '2019-10-15 11:18:17'),
(435, 'Mamit', 22, 'N', '2019-10-15 11:18:39', '2019-10-15 11:18:39'),
(436, 'Saiha', 22, 'N', '2019-10-15 11:18:53', '2019-10-15 11:18:53'),
(437, 'Serchhip', 22, 'N', '2019-10-15 11:19:06', '2019-10-15 11:19:06'),
(438, 'Champhai', 22, 'N', '2019-10-15 11:19:21', '2019-10-15 11:19:21'),
(439, 'Garhwa', 5, 'N', '2019-10-15 11:26:49', '2019-10-15 11:26:49'),
(440, 'Palamu', 5, 'N', '2019-10-15 11:27:04', '2019-10-15 11:27:04'),
(441, 'Latehar', 5, 'N', '2019-10-15 11:27:26', '2019-10-15 11:27:26'),
(442, 'Chatra', 5, 'N', '2019-10-15 11:27:38', '2019-10-15 11:27:38'),
(443, 'Hazaribagh', 5, 'N', '2019-10-15 11:27:51', '2019-10-15 11:27:51'),
(444, 'Koderma', 5, 'N', '2019-10-15 11:28:05', '2019-10-15 11:28:05'),
(445, 'Giridih', 5, 'N', '2019-10-15 11:28:17', '2019-10-15 11:28:17'),
(446, 'Ramgarh', 5, 'N', '2019-10-15 11:28:31', '2019-10-15 11:28:31'),
(447, 'Gumla', 5, 'N', '2019-10-15 11:28:45', '2019-10-15 11:28:45'),
(448, 'Lohardaga', 5, 'N', '2019-10-15 11:28:59', '2019-10-15 11:28:59'),
(449, 'Simdega', 5, 'N', '2019-10-15 11:29:10', '2019-10-15 11:29:10'),
(450, 'Khunti', 5, 'N', '2019-10-15 11:29:26', '2019-10-15 11:29:26'),
(451, 'West Singhbhum', 5, 'N', '2019-10-15 11:29:38', '2019-10-15 11:29:38'),
(452, 'Saraikela Kharsawan', 5, 'N', '2019-10-15 11:29:52', '2019-10-15 11:29:52'),
(453, 'East Singhbhum', 5, 'N', '2019-10-15 11:30:09', '2019-10-15 11:30:09'),
(454, 'Jamtara', 5, 'N', '2019-10-15 11:30:22', '2019-10-15 11:30:22'),
(455, 'Dumka', 5, 'N', '2019-10-15 11:30:42', '2019-10-15 11:30:42'),
(456, 'Pakur', 5, 'N', '2019-10-15 11:30:55', '2019-10-15 11:30:55'),
(457, 'Godda', 5, 'N', '2019-10-15 11:31:11', '2019-10-15 11:31:11'),
(458, 'Sahebganj', 5, 'N', '2019-10-15 11:31:25', '2019-10-15 11:31:25'),
(459, 'Gopalganj', 9, 'N', '2019-10-15 11:45:40', '2019-10-15 11:45:40'),
(460, 'Jamui', 9, 'N', '2019-10-15 11:45:50', '2019-10-15 11:45:50'),
(461, 'Khagaria', 9, 'N', '2019-10-15 11:46:02', '2019-10-15 11:46:02'),
(462, 'Kaimur', 9, 'N', '2019-10-15 11:46:12', '2019-10-15 11:46:12'),
(463, 'Lakhisarai', 9, 'N', '2019-10-15 11:46:22', '2019-10-15 11:46:22'),
(464, 'Madhubani', 9, 'N', '2019-10-15 11:46:33', '2019-10-15 11:46:33'),
(465, 'Madhepura', 9, 'N', '2019-10-15 11:46:50', '2019-10-15 11:46:50'),
(466, 'Nalanda', 9, 'N', '2019-10-15 11:47:02', '2019-10-15 11:47:02'),
(467, 'Nawada', 9, 'N', '2019-10-15 11:47:12', '2019-10-15 11:47:12'),
(468, 'Rohtas', 9, 'N', '2019-10-15 11:47:23', '2019-10-15 11:47:23'),
(469, 'Sheohar', 9, 'N', '2019-10-15 11:47:34', '2019-10-15 11:47:34'),
(470, 'Sheikhpura', 9, 'N', '2019-10-15 11:47:44', '2019-10-15 11:47:44'),
(471, 'Saran', 9, 'N', '2019-10-15 11:47:54', '2019-10-15 11:47:54'),
(472, 'Sitamarhi', 9, 'N', '2019-10-15 11:48:09', '2019-10-15 11:48:09'),
(473, 'Supaul', 9, 'N', '2019-10-15 11:48:19', '2019-10-15 11:48:19'),
(474, 'Vaishali', 9, 'N', '2019-10-15 11:48:30', '2019-10-15 11:48:30'),
(475, 'West Champaran', 9, 'N', '2019-10-15 11:48:40', '2019-10-15 11:48:40'),
(476, 'Dispur', 10, 'N', '2019-10-16 08:35:55', '2019-10-16 08:35:55'),
(477, 'Narnaul', 7, 'N', '2019-10-16 08:39:53', '2019-10-16 08:39:53'),
(478, 'Jagdhari', 7, 'N', '2019-10-16 08:42:27', '2019-10-16 08:42:27'),
(479, 'gaziabad', 7, 'N', '2019-10-17 07:39:23', '2019-10-17 07:39:23'),
(480, 'devghar', 5, 'N', '2019-10-17 07:40:00', '2019-10-17 07:40:00'),
(481, 'dinajpur', 21, 'N', '2019-10-17 07:40:34', '2019-10-17 07:40:34'),
(482, 'muktsar', 13, 'N', '2019-10-17 07:41:03', '2019-10-17 07:41:03'),
(483, 'abohar', 13, 'N', '2019-10-17 07:41:32', '2019-10-17 07:41:32'),
(484, 'giddarbha', 13, 'N', '2019-10-17 07:42:02', '2019-10-17 07:42:02'),
(485, 'nagour', 1, 'N', '2019-10-17 07:42:32', '2019-10-17 07:42:32'),
(486, 'bhiwadi', 1, 'N', '2019-10-17 07:43:17', '2019-10-17 07:43:17'),
(487, 'selam', 18, 'N', '2019-10-17 07:43:42', '2019-10-17 07:43:42'),
(488, 'lalitpur', 2, 'N', '2019-10-17 07:44:04', '2019-10-17 07:44:04'),
(489, 'khushi nagar', 2, 'N', '2019-10-17 07:45:04', '2019-10-17 07:45:04'),
(490, 'almora', 14, 'N', '2019-10-17 07:45:28', '2019-10-17 07:45:28'),
(491, 'pargana', 8, 'N', '2019-10-17 07:46:23', '2019-10-17 07:46:23'),
(492, 'burdhman', 8, 'N', '2019-10-17 07:46:51', '2019-10-17 07:46:51'),
(493, 'nokha', 1, 'N', '2019-10-20 11:46:33', '2019-10-20 11:46:33'),
(494, 'Purulia', 8, 'N', '2019-11-13 11:41:04', '2019-11-13 11:41:04'),
(495, 'Ellenabad', 7, 'N', '2019-11-16 09:45:50', '2019-11-16 09:45:50'),
(496, 'Sujangarh', 1, 'N', '2019-11-24 17:46:03', '2019-11-24 17:46:03'),
(497, 'Chamba', 15, 'N', '2020-01-14 08:36:39', '2020-01-14 08:36:39'),
(498, 'Rajasthan', 1, 'N', '2020-03-28 12:34:29', '2020-03-28 12:34:29');

-- --------------------------------------------------------

--
-- Table structure for table `tms_classes`
--

CREATE TABLE `tms_classes` (
  `class_id` int(11) NOT NULL,
  `class_name` varchar(255) DEFAULT NULL,
  `class_slug` varchar(255) DEFAULT NULL,
  `class_age_requirments` varchar(255) NOT NULL,
  `class_status` int(11) NOT NULL DEFAULT 1 COMMENT '1 - Active 0 - Deactive',
  `class_created` timestamp NOT NULL DEFAULT current_timestamp()
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data for table `tms_classes`
--

INSERT INTO `tms_classes` (`class_id`, `class_name`, `class_slug`, `class_age_requirments`, `class_status`, `class_created`) VALUES
(1, 'Pre Nursery', 'pre-nursery', '2 Years', 1, '2017-12-24 19:17:52'),
(2, 'Nursery', 'nursery', '3 Years', 1, '2017-12-24 19:17:52'),
(3, 'L.K.G', 'lkg', '4 Years', 1, '2017-12-24 19:19:24'),
(4, 'U.K.G', 'ukg', '5 Years', 1, '2017-12-24 19:19:24'),
(5, 'I', 'i', '6 Years', 0, '2017-12-24 19:20:16'),
(6, 'II', 'ii', '7 Years', 0, '2017-12-24 19:20:16');

-- --------------------------------------------------------

--
-- Table structure for table `tms_contact_enquirys`
--

CREATE TABLE `tms_contact_enquirys` (
  `contact_enquiry_id` int(11) NOT NULL,
  `contact_enquiry_name` varchar(255) DEFAULT NULL,
  `contact_enquiry_email` varchar(255) DEFAULT NULL,
  `contact_enquiry_number` varchar(255) DEFAULT NULL,
  `contact_enquiry_subject` text DEFAULT NULL,
  `contact_enquiry_message` longtext DEFAULT NULL,
  `contact_enquiry_status` int(11) NOT NULL DEFAULT 1 COMMENT '1 - New Enquiry 2- Completed',
  `contact_enquiry_created` timestamp NOT NULL DEFAULT current_timestamp()
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data for table `tms_contact_enquirys`
--

INSERT INTO `tms_contact_enquirys` (`contact_enquiry_id`, `contact_enquiry_name`, `contact_enquiry_email`, `contact_enquiry_number`, `contact_enquiry_subject`, `contact_enquiry_message`, `contact_enquiry_status`, `contact_enquiry_created`) VALUES
(1, 'Mayank', 'mayank@gmail.com', '9829568999', 'TEST', 'Message', 1, '2021-04-19 08:09:45'),
(2, NULL, NULL, NULL, NULL, 'fasd', 1, '2021-06-29 05:10:12');

-- --------------------------------------------------------

--
-- Table structure for table `tms_downloads`
--

CREATE TABLE `tms_downloads` (
  `download_id` int(11) NOT NULL,
  `download_title` varchar(255) DEFAULT NULL,
  `download_file` varchar(255) DEFAULT NULL,
  `download_status` int(11) NOT NULL DEFAULT 1 COMMENT '1 - Active 0 -  Deactive',
  `download_created` timestamp NOT NULL DEFAULT current_timestamp()
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data for table `tms_downloads`
--

INSERT INTO `tms_downloads` (`download_id`, `download_title`, `download_file`, `download_status`, `download_created`) VALUES
(1, 'TEST FILE', 'uploads/download_file/1618907086.pdf', 0, '2021-04-20 08:24:46'),
(2, 'Mandatory', 'uploads/download_file/1623085042.pdf', 0, '2021-06-07 16:57:22'),
(3, 'Affiliation', 'uploads/download_file/1623085578.pdf', 0, '2021-06-07 17:06:18'),
(4, 'Sanstha', 'uploads/download_file/1623085614.pdf', 0, '2021-06-07 17:06:54'),
(5, 'NOC', 'uploads/download_file/1623085652.pdf', 0, '2021-06-07 17:07:32'),
(6, 'RTE', 'uploads/download_file/1623085689.pdf', 0, '2021-06-07 17:08:09'),
(7, 'Building_Safety', 'uploads/download_file/1623085870.pdf', 0, '2021-06-07 17:11:10'),
(8, 'Fire_Safety', 'uploads/download_file/1623085908.pdf', 0, '2021-06-07 17:11:48'),
(9, 'Self_Certify', 'uploads/download_file/1623085944.pdf', 0, '2021-06-07 17:12:24'),
(10, 'Health_Certify', 'uploads/download_file/1623085981.pdf', 0, '2021-06-07 17:13:01'),
(11, 'fee_Structure', 'uploads/download_file/1623086030.pdf', 0, '2021-06-07 17:13:50'),
(12, 'Academic_Calander', 'uploads/download_file/1623086075.pdf', 0, '2021-06-07 17:14:35'),
(13, 'SMC', 'uploads/download_file/1623086108.pdf', 0, '2021-06-07 17:15:08'),
(14, 'PTA', 'uploads/download_file/1623086144.pdf', 0, '2021-06-07 17:15:44'),
(15, 'Results', 'uploads/download_file/1623086186.pdf', 0, '2021-06-07 17:16:26'),
(16, 'Text Book Declaration', 'uploads/download_file/1623086234.pdf', 0, '2021-06-07 17:17:14'),
(17, 'Annual Report 2020-21', 'uploads/download_file/1623086290.pdf', 0, '2021-06-07 17:18:10'),
(18, 'Staff Details', 'uploads/download_file/1623086322.pdf', 0, '2021-06-07 17:18:42'),
(19, 'Syallabus 2020-21', 'uploads/download_file/1623086360.pdf', 0, '2021-06-07 17:19:20'),
(20, 'TC FORMAT', 'uploads/download_file/1623086395.pdf', 0, '2021-06-07 17:19:55'),
(21, 'Teachers Training & Workshop', 'uploads/download_file/1623086436.pdf', 0, '2021-06-07 17:20:36'),
(22, 'Admission Form', 'uploads/download_file/1624519301.pdf', 1, '2021-06-24 07:15:37');

-- --------------------------------------------------------

--
-- Table structure for table `tms_events`
--

CREATE TABLE `tms_events` (
  `event_id` int(11) NOT NULL,
  `event_name` varchar(255) DEFAULT NULL,
  `event_class` varchar(255) DEFAULT NULL,
  `event_date` varchar(255) DEFAULT NULL,
  `event_image` varchar(255) DEFAULT NULL,
  `event_description` longtext DEFAULT NULL,
  `event_slug` varchar(255) DEFAULT NULL,
  `event_status` int(11) NOT NULL DEFAULT 1 COMMENT '1 - Active 0 - Deactive',
  `event_created` timestamp NOT NULL DEFAULT current_timestamp()
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data for table `tms_events`
--

INSERT INTO `tms_events` (`event_id`, `event_name`, `event_class`, `event_date`, `event_image`, `event_description`, `event_slug`, `event_status`, `event_created`) VALUES
(14, 'Menu & Amazing Fridays', '1,2,3,4', 'For January Month', 'uploads/event_image/1516346390.jpg', '<p class=\"section-fullwidth\"></p><h2>For January Month</h2><p></p><table class=\"table tablev1\" style=\"border: 1px solid #981e71; height: 182px;\" width=\"496\"><thead><tr><th style=\"border: 1px solid #981E71;\">Day</th><th style=\"border: 1px solid #981E71;\">Menu</th><th style=\"border: 1px solid #981E71;\">Tit-Bits</th></tr></thead><tbody><tr><td style=\"border: 1px solid #981E71;\">Monday</td><td style=\"border: 1px solid #981E71;\">N.A.</td><td style=\"border: 1px solid #981E71;\">-</td></tr><tr><td style=\"border: 1px solid #981E71;\">Tuesday</td><td style=\"border: 1px solid #981E71;\">N.A.<br></td><td style=\"border: 1px solid #981E71;\">-<br></td></tr><tr><td style=\"border: 1px solid #981E71;\">Wednesday</td><td style=\"border: 1px solid #981E71;\">N.A.<br></td><td style=\"border: 1px solid #981E71;\">-</td></tr><tr><td style=\"border: 1px solid #981E71;\">Thursday</td><td style=\"border: 1px solid #981E71;\">N.A.<br></td><td style=\"border: 1px solid #981E71;\">-</td></tr><tr><td style=\"border: 1px solid #981E71;\">Friday</td><td style=\"border: 1px solid #981E71;\" colspan=\"2\">N.A.<br></td></tr></tbody></table><p></p><h2>Amazing Fridays for the month of January<br></h2><p></p><table class=\"table tablev1\" style=\"border: 1px solid #981e71; height: 148px;\" width=\"259\"><thead><tr><th style=\"border: 1px solid #981E71;\">Date</th><th style=\"border: 1px solid #981E71;\">Description</th></tr></thead><tbody><tr><td style=\"border: 1px solid #981E71;\">Jan15</td><td style=\"border: 1px solid #981E71;\">Multicolour Pullover<br></td></tr><tr><td style=\"border: 1px solid #981E71;\">Jan 22</td><td style=\"border: 1px solid #981E71;\"><span style=\"color: rgb(34, 34, 34); font-size: small;\">O</span>range</td></tr><tr><td style=\"border: 1px solid #981E71;\">Jan 29</td><td style=\"border: 1px solid #981E71;\">Checks</td></tr><tr><td style=\"border: 1px solid #981E71;\"><br></td><td style=\"border: 1px solid #981E71;\"><br></td></tr></tbody></table><h1 style=\"color:#f37735 !important; font-size: 34px !important; text-align: left; font-style:normal;\">MENU</h1><p class=\"box_spreater\"></p><p class=\"spreater\" style=\"text-align: left;\"></p><p class=\"fullwidth-sepratore\"></p><p class=\"dividerstyle\"></p><p></p><p></p><p></p><p></p><p class=\"  \"></p><p class=\"cs-section-title\"></p><p><b>As you all know by now that we follow the system of having same menu for all kids, I would like you to know about the reasons behind it - </b></p><p></p><p class=\"liststyle\"></p><ul class=\"cs-iconlist\"><li class=\"has_border\"> Three reasons for following Menu –<br><span style=\"padding-left: 15px;\">a. Child may learn to eat a variety of food.</span><br><span style=\"padding-left: 15px;\">b.Uniformity in what his peers are having and the child is not tempted to have what the other has got.<br><span style=\"padding-left: 15px;\">c.They learn about the food &amp; fruit that they bring.</span> </span></li><li class=\"has_border\"> Please ensure that your ward’s tiffin is not over stuffed and limited to his appetite. </li><li class=\"has_border\"> Please do not send eatables in pockets of your child. </li><li class=\"has_border\"> You can send Chips in tiffins only if it is mentioned in the menu and not otherwise. </li><li class=\"has_border\"> Don’t send eggs in any form in tiffin. </li><li class=\"has_border\"> Children should get steel fork &amp; spoon and not plastic ones as they are difficult to use. Also send a paper napkin daily. </li><li class=\"has_border\"> U.K.G. onwards children to bring one fruit daily in a separate box. </li><li class=\"has_border\"> We also issue a Certificate of Appreciaton to the Parents of the child getting maximum stars for Menu follow up in each quarter. </li><li class=\"has_border\"> We do not accept tiffins during school hours at the gate. Please send them with children in the morning itself. </li> </ul><p></p><p></p><p class=\" col-md-12\"></p><h1 style=\"color:#f37735 !important; font-size: 34px !important; text-align: left; font-style:normal;\">AMAZING FRIDAYS</h1><p class=\"box_spreater\"></p><p class=\"spreater\" style=\"text-align: left;\"></p><p class=\"fullwidth-sepratore\"></p><p class=\"dividerstyle\"></p><p></p><p></p><p></p><p></p><p class=\"lightbox\">Children are expected to come in a specified dress /colour code on every Friday. The idea behind it – children can learn colours in an interesting way. In case a child doesn’t have that particular colour/ theme dress, it is not compulsory for you to buy a new dress. Rather in some creative way the code can be followed. That colour / theme can be a part of a dress and not necessarily be the complete dress.</p><br>Note-: Goggles, Caps, Slippers &amp; Sandals are not permitted on Colour Fridays.<p></p><p><br></p>', 'menu-amazing-fridays-1', 1, '2017-12-29 14:26:28'),
(15, 'Holidays of the Month', '1,2,3,4', 'Holidays of the Month', 'uploads/event_image/1516346646.jpg', '<p class=\"section-fullwidth\"></p><p></p><p class=\"cs-section-title\"></p><h2><br></h2><p></p><table class=\"table tablev1\" style=\"border: 1px solid #981E71;\"><thead><tr><th style=\"border: 1px solid #981E71;\">Day</th><th style=\"border: 1px solid #981E71;\">Date            </th><th style=\"border: 1px solid #981E71;\">Holiday on Account of</th></tr></thead><tbody><tr><th style=\"border: 1px solid #981E71;\">Friday to Sunday</th><th style=\"border: 1px solid #981E71;\">Jan 1 to Jan 10</th><th style=\"border: 1px solid #981E71;\">Winter Break</th></tr><tr><th style=\"border: 1px solid #981E71;\">Thursday</th><th style=\"border: 1px solid #981E71;\">Jan 14</th><th style=\"border: 1px solid #981E71;\">Makar Sankranti</th></tr><tr><th style=\"border: 1px solid #981E71;\">Wednesday</th><th style=\"border: 1px solid #981E71;\">Jan 20</th><th style=\"border: 1px solid #981E71;\">Guru Govind Singh Jayanti</th></tr><tr><th style=\"border: 1px solid #981E71;\"><br></th><th style=\"border: 1px solid #981E71;\"><br></th><th style=\"border: 1px solid #981E71;\"><br></th></tr></tbody></table>', 'holidays-of-the-month-1', 1, '2017-12-29 14:28:51'),
(83, 'Vegetable Day', '3', 'Wednesday, December 9', 'uploads/event_image/1606466452.JPG', '<p>We at Neo Dales, also ensure that children learn through conventional means to make learning more fun &amp; interesting. So, to educate and create awareness about the importance of vegetables &amp; to inculcate good eating habits amongst students we have planned an event “Vegetable Day” for our little Dalians.&nbsp;</p><p>Children to be ready with the allotted vegetable &amp; should speak 2-3 lines on the same.&nbsp;</p><p>(The teacher will be allotting the vegetable on Nov 28 during the interactive session).</p><p>A child will start once the class teacher gives a go ahead. ONLY the child should be visible on screen while speaking.</p><p>Dress Code: Colour that matches with the vegetable allotted.</p><p><br></p>', 'vegetable-day', 0, '2020-11-27 08:40:52'),
(84, 'Shapes Day', '1,2', 'Thursday,  December 10', 'uploads/event_image/1606466613.JPG', '<span style=\"font-size:16px;\"><p style=\"font-size: 16px;\">From an early age, kids notice different objects in different shapes around them &amp; this&nbsp;<span style=\"font-size: 16px;\">arouses their curiosity. To expand their understanding about different shapes an event&nbsp;</span><span style=\"font-size: 16px;\">has been planned where they will recognize &amp; revise various shapes through fun filled&nbsp;</span><span style=\"font-size: 16px;\">activities. Children are supposed to come dressed up in checks &amp; should be ready with&nbsp;</span><span style=\"font-size: 16px;\">1 or more object of different shape.</span></p></span><p><br></p>', 'shapes-day', 0, '2020-11-27 08:43:33'),
(85, 'Story Telling', '4', 'Thurs, Dec 17 / Fri, Dec 18', 'uploads/event_image/1606466792.jpg', '<span style=\"font-size:16px;\"><p style=\"font-size: 16px;\">Stories become a part of kids lives since they hear it from their grandparents or parents. The first utterance of “Once there was a king &amp; queen…….” Instantly opens up every child’s world of imagination and thus allows them to visualize &amp; get inspired by what does not exist, most of the times. So, to enhance this imagination &amp; skill of narrating stories, we have planned this event.</p><p style=\"font-size: 16px;\">Kindly make your ward prepare a short story in Hindi/English along with the introduction focusing on Choice of Story, Pronunciation, Expression, Pitch &amp; Clarity. Props are optional. The entire narration should not be more than one and a half minute including introduction. Dress Code: Casuals</p></span><p><br></p>', 'story-telling', 0, '2020-11-27 08:46:32'),
(87, 'Christmas Celebration', '1,2,3,4', 'Thursday, December 24', 'uploads/event_image/1606466912.jpeg', '<p style=\"text-align: center; \">Bells are ringing, Children are singing,</p><p style=\"text-align: center; \">It’s all merry &amp; bright.</p><p style=\"text-align: center; \">Hang your stockings &amp; sing your prayers,</p><p style=\"text-align: center; \">Because Santa is here with all his might.</p><p>Therefore, we have organized a celebration for kids so that they can have some fun &amp; can enjoy singing &amp; dancing to the tunes of Christmas Carols.</p><p>Dress Code: Red &amp; White along with Santa Cap.</p>', 'christmas-celebration-1', 0, '2020-11-27 08:48:32');

-- --------------------------------------------------------

--
-- Table structure for table `tms_faqs`
--

CREATE TABLE `tms_faqs` (
  `faq_id` int(11) NOT NULL,
  `faq_question` text DEFAULT NULL,
  `faq_answer` longtext DEFAULT NULL,
  `faq_status` int(11) NOT NULL DEFAULT 1 COMMENT '1 - Active 0 -  Deactive',
  `faq_created` timestamp NOT NULL DEFAULT current_timestamp()
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data for table `tms_faqs`
--

INSERT INTO `tms_faqs` (`faq_id`, `faq_question`, `faq_answer`, `faq_status`, `faq_created`) VALUES
(1, 'HOW TO LOGIN THROUGH THE WEBSITE?', 'Click on Parents Login on the top right corner > you will be redirected to our school ERP > under User Name type your ward\'s S.R.No. and password will be father\'s mobile number as per our records > you will then be asked to change your password.', 1, '2017-12-24 19:34:05'),
(2, 'HOW TO START AVAILING CONVEYANCE?', 'Download the Conveyance Undertaking Form at Parents Login and submit the duly filled form at the front office. We will intimate you in 2 working days once the form is processed.', 1, '2017-12-24 19:34:05'),
(3, 'IS IT COMPULSORY TO SEND TIFFIN AS PER MENU?', 'No, however, the idea behind this concept is to make and encourage children eat a variety of food and since everybody in the class gets the same menu the child easily finishes the tiffin and is not tempted by different varieties. If your ward absolutely doesn’t eat the specified menu you may send something else alongwith the prescribed menu.', 1, '2017-12-24 19:34:05'),
(4, 'IS IT COMPULSORY TO FOLLOW THE COLOUR FRIDAY CODE IN IT\'S ENTIRETY?', 'In case a child doesn’t have that particular colour/theme dress, it is not compulsory for parents to buy a new dress. Rather in some creative way the code can be followed. That colour/theme can be a part of a dress and not necessarily be the complete dress. The idea behind having Colour Fridays is to make children learn colours in an interesting way.', 1, '2017-12-24 19:34:05'),
(5, 'HOW CAN I CELEBRATE MY CHILD\'S BIRTHDAY IN SCHOOL?', 'Parents can handover the sweets to the Class Teacher in the morning which will be distributed to kids. Parents can distribute only two sweets on child’s birthday. Anything more will be sent back.The child can come in casuals on his / her birthday. Cutting of cake is not permitted in school. Parents are not allowed to join the celebrations in school.', 1, '2017-12-24 19:34:05'),
(6, 'HOW CAN I COMMUNICATE WITH THE TEACHER FOR ANYTHING URGENT?', 'Communication should be mailed at principal@neodales.edu.in', 1, '2017-12-24 19:34:05'),
(7, 'I HAVE LOST MY COPY OF COURSE / CIRCULAR, HOW CAN I GET THE SAME?', 'Mail is sent to parents with attached Circular and Course time to time so they can check the same from their mailbox. However, you may request us on mail for the same and the same shall be resent to you on your Registered email ID.', 1, '2017-12-24 19:34:05'),
(8, 'HOW TO INFORM SCHOOL REGARDING LEAVE OF ABSENCE?', 'You may mail us the same at principal@neodales.edu.in clearly mentioning the name of your ward, class, section, dates and reason for leave.', 1, '2017-12-24 19:34:05'),
(9, 'IN CASE MY CHILD IS ABSENT, WILL THE TEACHER COMPLETE THE WORK OR MARK THE PAGES IN THE BOOKS AND NOTEBOOKS?WILL THE LATE WORK BE CHECKED?', 'No', 1, '2017-12-24 19:34:05'),
(11, 'WHAT IS THE CONCEPT OF ESCORT CARDS?', 'For security purpose, two escort cards are issued at the beginning of the session for all classes. So any unfamiliar person coming to school should present this card to the teacher failing which the ward will not be handed over to that person.', 0, '2017-12-24 19:34:05'),
(12, 'IS IT COMPULSORY TO ATTEND P.T.M.?', 'Yes, P.T.M. should preferably be attended by both the parents.', 1, '2017-12-24 19:34:05'),
(13, 'I AM UNABLE TO ATTEND THE P.T.M. CAN IT BE ADJUSTED ON ANY OTHER DAY?', 'In case parents are unable to attend the P.T.M., any relative can meet the Class Teacher but the information regarding the same should be mailed to the Principal at principal@neodales.edu.in well in advance.', 1, '2017-12-24 19:34:05'),
(14, 'ARE COMPETITIONS HELD FOR ALL CLASSES ?', 'No, only for class 1  and Presentations for Pre Nursery to U.K.G.', 1, '2017-12-24 19:34:05'),
(15, 'ASSESSMENTS ARE HELD FOR WHICH ALL CLASSES?IS MARKING DONE AND RANKS GIVEN?', '1. Children are assessed twice in a session once in September / October and then in March ( Nursery onwards ). <br/>\r\n2. Pre Nursery Assessment is done on class performance. <br/>\r\n3. No class ranking is done in accordance with our motto \"Each Child a Star\". <br/>\r\n4. Hard copies of Term 1 Report Card will not be issued and the same should be viewed online at Parents Login', 1, '2017-12-24 19:34:05'),
(17, 'CAN ASSESSMENTS BE PREPONED OR POSTPONED?', 'No, Assessments can be neither preponed nor postponed. If a child is absent on the day of Assessment , ‘Absent’ will be marked in the Report Card.', 1, '2017-12-24 19:34:05');

-- --------------------------------------------------------

--
-- Table structure for table `tms_important_points`
--

CREATE TABLE `tms_important_points` (
  `important_point_id` int(11) NOT NULL,
  `important_point` longtext NOT NULL,
  `important_point_status` int(11) NOT NULL DEFAULT 1 COMMENT '1 - Active 0 - Deactive',
  `important_point_created` timestamp NOT NULL DEFAULT current_timestamp()
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data for table `tms_important_points`
--

INSERT INTO `tms_important_points` (`important_point_id`, `important_point`, `important_point_status`, `important_point_created`) VALUES
(1, 'First Term Assessments will commence from Monday, September 24 for all classes. Details of the same will be available with the scheme shortly. There will be no assessment for Pre Nursery & Report Card will be made based on the overall class performance of the child. Please note that hard copy of Term 1 Report Card will not be given & the same will be uploaded & can be viewed online on Parents Login from our website. It will not be available on the School App.', 0, '2017-12-24 19:45:15'),
(2, 'We feel happy and proud that Shabeeb Ahmad Siddiqui of class IC & Aarika Agarwal of class IB were investituted as School Captains for the Academic Year 2018 -19. Congratulations to them & their parents and we sincerely hope that they will live up to the title bestowed on them!', 0, '2017-12-24 19:45:15'),
(3, 'Fee details for the 2nd quarter can be viewed online. Kindly pay online or through NEFT from our payment gateway. Once fee is submitted online kindly mail us all details to accounts@neodales.edu.in or else the amount will lie in suspense. Last date for fee submission is Monday, July 16. Hard copy of fee bill will not be issued anymore.', 0, '2017-12-24 19:45:23'),
(4, 'Parents to kindly collect T.C. on 28.04.2018 (for classes Pre Nursery to U.K.G. & Class 2) & 05.05.2018 (for Class 1) provided they have submitted the Withdrawal Form and cleared all dues including Imprest Money negative balance if any. Date of birth will not be amended under any circumstances.', 0, '2018-01-03 07:03:23'),
(5, 'New session will begin on following dates: Pre Nursery - 4 April 2018 	 Nursery onwards - 9 April 2018', 0, '2018-01-03 07:03:39'),
(6, 'Last date for fee deposit is Monday, January 15, 2018.', 0, '2018-01-03 07:03:53'),
(7, 'Registrations for the session 2018- 2019 will open on 06.01.2018 for Pre Nursery to class ONE from 9:30 am till 3:00 pm. Forms will be given on first come first serve basis till seats last. It is advised that forms are taken on day 1 itself. Also please ensure that Municipal Birth Certificate & Aadhaar Card of the ward has been made and available.', 0, '2018-01-03 07:04:07'),
(8, 'During assessments, Nursery onwards children are supposed to carry tiffin & water bottle only. L.K.G. onwards can also get a writing pad.', 0, '2018-09-20 12:32:20'),
(9, 'School timings for classes U.K.G. onwards w.e.f. September 24 to October 3 will be 8:00 a.m. to 11:00 a.m. on account of assessments.', 0, '2018-09-20 12:33:19'),
(10, 'Winter Uniform becomes optional w.e.f. October 1 and compulsory w.e.f. November 1.', 0, '2018-09-20 12:33:51'),
(11, 'Fee details for the 3rd quarter can be viewed online. Kindly pay online or through NEFT\r\nfrom our payment gateway. Once fee is submitted online kindly mail us all details to accounts@neodales.edu.in or else the amount will lie in suspense.  Last date for fee submission is October 15.', 0, '2018-09-20 12:35:17'),
(12, 'With utmost pleasure, we announce that the school will be holding Annual Day Celebrations (NURSERY ONWARDS) for 3 consecutive days from December 18 to December 20 with great zeal & enthusiasm. It’s the most awaited event not only for the students, but also for every member of Neo Dales family.', 0, '2018-09-20 12:36:02'),
(13, 'From Thursday, November 1 winter uniform becomes mandatory.', 0, '2018-10-24 17:57:59'),
(14, 'There will be regular school on Saturday, November 17 for all classes & ensure your ward comes in school uniform on the above mentioned date.', 0, '2018-10-24 17:58:18'),
(15, 'There will be regular school on Saturday, December 8 & 15 for all classes. Ensure your ward comes in school uniform & can get anything of their choice in tiffin on the above mentioned dates.', 0, '2018-11-26 04:50:02'),
(16, 'School Jackets are compulsory from Monday, December 3.', 0, '2018-11-26 04:50:38'),
(17, 'Details of Annual Function will be sent to you in due course of time & participants are expected to be regular to school as irregularity will result in disqualification.', 0, '2018-11-26 04:51:35'),
(18, 'School will reopen on Tuesday, 15 January 2019.', 0, '2018-11-26 05:10:11'),
(19, 'Class Photography dates:\r\n L.K.G. & Class 1- February 25                      Pre Nursery & U.K.G. - February 26                                    Nursery - February 27', 0, '2019-01-19 08:52:42'),
(20, 'Term II Assessments will begin from: \r\nU.K.G. & Class 1 - February 28		          Nursery & L.K.G. - March 1', 0, '2019-01-19 08:53:02'),
(21, 'Scheme & Syllabus will be uploaded in due course of time. You will receive an e-diary note for the same.', 0, '2019-01-19 08:56:17'),
(22, 'Assessments will not be held for class Pre Nursery & result will be given on the overall assessment of the child.', 0, '2019-01-19 08:56:39'),
(23, 'Parents wishing to withdraw their ward must collect the Withdrawal Form from the front office or may even download it from our website www.neodales.edu.in after logging in and submit it duly filled at the front office latest by Friday, February 15. Late submission will attract a  \r\npenalty of  500/-', 0, '2019-01-19 08:57:29'),
(24, 'School timings w.e.f. Saturday, March 2 for U.K.G. & Class 1 on account of assessments will be 9:00 am - 12:00 noon.', 0, '2019-02-20 09:31:36'),
(25, 'No specified menu to be followed in the month of March. You may send anything of your choice.', 0, '2019-02-20 09:31:50'),
(26, 'Last working day of this session will be:\r\nPre Nursery - March 15 	            Nursery & L.K.G. - March 8	                     U.K.G. & Class 1 - March 11', 0, '2019-02-20 09:32:19'),
(27, 'Parents to kindly collect T.C. from Monday, May 6 between 2 - 4 p.m. provided they have submitted the Withdrawal Form and cleared all dues including Imprest Money negative balance if any. Date of birth will not be amended under any circumstances.', 0, '2019-02-20 09:32:41'),
(28, 'School will not be handing over T.C. to those students who are continuing in Neo Dales Montessori School and the same will be prepared \r\n& kept for Montessori school records.  However, kindly ensure that the Withdrawal Form is duly filled & submitted at the front office.', 0, '2019-02-20 09:33:20'),
(29, 'New session will begin on following dates: \r\nPre Nursery - 1 April 2019 	 Nursery onwards - 4 April 2019', 0, '2019-02-20 09:33:37'),
(30, 'Children of class Pre Nursery to follow Menu from July.', 0, '2019-05-26 09:37:48'),
(31, 'Fee details for the 2nd quarter can be viewed online. Kindly pay online or through NEFT from our payment gateway. Once fee is submitted online Kindly mail us all details to accounts@neodales.edu.in or else the amount will lie in suspense.\r\nLast date for fee submission is July 15.', 0, '2019-05-26 09:40:23'),
(32, 'First Term Assessments will commence from Monday, September 16 for L.K.G. & U.K.G; Friday, September 20 for Nursery. Details of the same will be available with the scheme & syllabus shortly. There will be no assessment for \r\n        Pre Nursery & Report Card will be made based on the overall class performance of the child. Please note that hard copy of Term 1 Report Card will not be given & the same will be uploaded & can be viewed online on Parents Login \r\n        from our website. It will not be available on the School App.', 0, '2019-08-22 06:29:57'),
(33, 'During assessments, children are supposed to carry tiffin & water bottle only. L.K.G. & U.K.G. can also get a writing pad.', 0, '2019-08-22 06:30:44'),
(34, 'School timings for classes L.K.G. & U.K.G. w.e.f. Monday, September 16 till Friday, September 27 will be 8:00 am to 11:00 am on account of assessments.', 0, '2019-08-22 06:31:38'),
(36, 'Fee details for the 3rd quarter can be viewed online. Kindly pay online from our payment gateway or through NEFT. If fee is submitted through NEFT kindly mail all details to \r\naccounts@neodales.edu.in or else the amount will lie in suspense and fee pending will reflect of your ward. Fee bills will be issued in due course of time. Last date for fee submission is October 16. Please note that from the next quarter, fee bills will not be issued & if needed you may take print of the same online through our website by selecting offline option under Payment Mode.', 0, '2019-09-17 15:34:44'),
(41, 'Registrations for the session 2020 - 2021 will open on Tuesday, January 7 for classes Pre Nursery & Nursery only. No seats are available in Nursery, L.K.G. & U.K.G.', 0, '2020-01-08 07:48:37'),
(49, 'Parents wishing to withdraw their ward must collect the Withdrawal Form from the front office or may even download it from our website www.neodales.edu.in and submit it duly filled at the front office latest by Saturday, February 1. Late submission will attract a penalty of 500/-.', 0, '2020-01-08 07:51:15'),
(52, 'Parents to kindly collect T.C. on Saturday, May 9 provided they have submitted the Withdrawal Form and cleared all dues including Imprest Money negative balance if any. Date of birth will not be amended under any circumstances.', 0, '2020-02-25 18:57:18'),
(53, 'School will not be handing over T.C. to those students who are continuing in Neo Dales Montessori School and the same will be prepared & kept for Montessori school records.  However, kindly ensure that the Withdrawal Form is submitted at the front office and all dues are cleared including  \r\nImprest Money negative balance if any.', 0, '2020-02-25 18:58:06'),
(54, 'All students to ensure punctuality in class.', 1, '2020-07-27 21:48:31'),
(55, 'No online class will be held the day the particular class has an event.', 1, '2020-07-27 21:49:20'),
(56, 'Parents should not feed anything to their children during the online class.', 1, '2020-07-31 11:09:43'),
(58, 'Students should present themselves neatly dressed and with the hair properly combed / tied or else they can be asked to tidy themselves and then rejoin.', 1, '2020-10-22 05:38:07'),
(59, 'We would also like to appreciate that our Neo Dalians exhibit good behaviour & manners during online classes & we hope this continues.', 1, '2020-10-22 05:38:31');

-- --------------------------------------------------------

--
-- Table structure for table `tms_master_pages`
--

CREATE TABLE `tms_master_pages` (
  `master_pages_id` int(11) NOT NULL,
  `master_pages_title` varchar(255) DEFAULT NULL,
  `master_pages_slug` varchar(255) DEFAULT NULL,
  `master_pages_excerpt` text DEFAULT NULL,
  `master_pages_image` varchar(255) DEFAULT NULL,
  `master_pages_description` longtext DEFAULT NULL,
  `master_pages_status` int(11) NOT NULL DEFAULT 1 COMMENT '1 - Active 0 - Deactive',
  `master_pages_created` timestamp NOT NULL DEFAULT current_timestamp()
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data for table `tms_master_pages`
--

INSERT INTO `tms_master_pages` (`master_pages_id`, `master_pages_title`, `master_pages_slug`, `master_pages_excerpt`, `master_pages_image`, `master_pages_description`, `master_pages_status`, `master_pages_created`) VALUES
(1, 'About Us', 'about-us', '”Where the mind is without fear and the head is held high” –Rabindranath Tagore', 'uploads/master_pages_image/1624262112.JPG', '<p><span style=\"color: rgb(97, 97, 97); font-family: \"Open Sans\", sans-serif; font-size: 13px; text-align: justify;\">Spread in the beautiful surroundings of around 12 Bighas of land, The Modern School, Barmer was established in the year 2007 with the vision of providing unparalleled education that is relevant, sensible and constant. Registered under the Ojasvi  Sansthan which is group of eminent, accomplished and knowledgeable people, our school has always believed that learning is a shared responsibility between parent, teachers and of course, the students! The school is well recognized for its high pedagogic & extra-curricular atmosphere and runs from Play Group to 12</span><span style=\"font-size: 9.75px; line-height: 0; position: relative; vertical-align: baseline; top: -0.5em; color: rgb(97, 97, 97); font-family: \"Open Sans\", sans-serif; text-align: justify;\">th</span><span style=\"color: rgb(97, 97, 97); font-family: \"Open Sans\", sans-serif; font-size: 13px; text-align: justify;\"> Std along with Science and Commerce streams. We are also open for franchise and entertain enquiry for the same.</span><br></p><p></p><p></p>', 1, '2017-12-24 18:57:58'),
(2, 'Facilities', 'facilities', 'TMS', 'uploads/master_pages_image/1618817564.jpg', '<p style=\"margin-bottom: 10px; line-height: 22px; color: rgb(97, 97, 97); font-family: &quot;Open Sans&quot;, sans-serif; font-size: 13px; text-align: justify;\">While the outdoor premises of our school comprise green serene views, the indoors are also never less in quality and feel. The spacious, well-equipped and air-conditioned classrooms provide a suitable learning atmosphere to the kids where their minds can fully concentrate on what is going on in the classroom. We also have special art, drama, music and indoor as well as outdoor games facilities.</p><p style=\"margin-bottom: 10px; line-height: 22px; color: rgb(97, 97, 97); font-family: &quot;Open Sans&quot;, sans-serif; font-size: 13px; text-align: justify;\"><b>Our facilities include:</b></p><ul style=\"margin-bottom: 10px; color: rgb(97, 97, 97); font-family: &quot;Open Sans&quot;, sans-serif; font-size: 13px; list-style-type: square;\"><li style=\"text-align: justify;\">Spacious, airy classrooms with the facility of smart class access</li><li style=\"text-align: justify;\">Ample space for outdoor &amp; indoor sports</li><li style=\"text-align: justify;\">Separate facilities for music, drama, singing, and painting</li><li style=\"text-align: justify;\">Purified cool water</li><li style=\"text-align: justify;\">Experienced &amp; well qualified faculty</li><li style=\"text-align: justify;\">Multiple co-curricular activities</li><li style=\"text-align: justify;\">Special Multi-purpose hall for all kind of Recreational activities</li></ul>', 1, '2017-12-24 19:50:24'),
(3, 'Admission Policy', 'admission-policy', 'Admission Policy', NULL, '<ol class=\"breadcrumb\" style=\"background-color: rgb(250, 250, 250); color: rgb(102, 102, 102); font-family: \" open=\"\" sans\",=\"\" helvetica,=\"\" arial,=\"\" sans-serif;\"=\"\"><p style=\"margin-bottom: 0.14in; direction: ltr; line-height: 18.399999618530273px; widows: 2; orphans: 2; caret-color: rgb(0, 0, 0); color: rgb(0, 0, 0);\"><font style=\"font-size: 12pt;\"><b>Rules For Admission &amp; Withdrawal&nbsp;</b></font></p><p style=\"margin-bottom: 0.14in; direction: ltr; line-height: 18.399999618530273px; widows: 2; orphans: 2; caret-color: rgb(0, 0, 0); color: rgb(0, 0, 0);\"><font style=\"font-size: 12pt;\">The principal reserves the right to admit or reject any student. At the time of admission, the following documents are to be submitted to the School Office. Two passport size (recent) photographs. Original Transfer Certificate (once the admission is confirmed). Copy of Birth Certificate by Municipal Board. Copy of Aadhaar Card of the student (Mandatory).Copy of mark sheet / report card of the previous class.Copy of the registration card of class IX given by the CBSE Board for admission to class X. Copy of the Transfer Order for admission to classes — X or XI I. Copy of Serving Certificate in case of defence personnel. Original Migration Certificate for Class XI only.&nbsp;</font></p><p style=\"margin-bottom: 0.14in; direction: ltr; line-height: 18.399999618530273px; widows: 2; orphans: 2; caret-color: rgb(0, 0, 0); color: rgb(0, 0, 0);\"><font style=\"font-size: 12pt;\">No leaving certificate is issued until all sums due to the school are paid in full.&nbsp;</font></p><p style=\"margin-bottom: 0.14in; direction: ltr; line-height: 18.399999618530273px; widows: 2; orphans: 2; caret-color: rgb(0, 0, 0); color: rgb(0, 0, 0);\"><br><br></p><p style=\"margin-bottom: 0.14in; direction: ltr; line-height: 18.399999618530273px; widows: 2; orphans: 2; caret-color: rgb(0, 0, 0); color: rgb(0, 0, 0);\"><font style=\"font-size: 12pt;\"><b>Rules For Depositing Fee&nbsp;</b></font></p><p style=\"margin-bottom: 0.14in; direction: ltr; line-height: 18.399999618530273px; widows: 2; orphans: 2; caret-color: rgb(0, 0, 0); color: rgb(0, 0, 0);\"><font style=\"font-size: 12pt;\">1. All fees are to be paid strictly in advance.&nbsp;</font></p><p style=\"margin-bottom: 0.14in; direction: ltr; line-height: 18.399999618530273px; widows: 2; orphans: 2; caret-color: rgb(0, 0, 0); color: rgb(0, 0, 0);\"><font style=\"font-size: 12pt;\">2. Fees once deposited will not be refunded.&nbsp;</font></p><p style=\"margin-bottom: 0.14in; direction: ltr; line-height: 18.399999618530273px; widows: 2; orphans: 2; caret-color: rgb(0, 0, 0); color: rgb(0, 0, 0);\"><font style=\"font-size: 12pt;\">3. All fees except new admission, are to be paid in cash or cheque.&nbsp;</font></p><p style=\"margin-bottom: 0.14in; direction: ltr; line-height: 18.399999618530273px; widows: 2; orphans: 2; caret-color: rgb(0, 0, 0); color: rgb(0, 0, 0);\"><font style=\"font-size: 12pt;\">4. All fees are to be paid before 10th day of first month of the quarter.&nbsp;</font></p><p style=\"margin-bottom: 0.14in; direction: ltr; line-height: 18.399999618530273px; widows: 2; orphans: 2; caret-color: rgb(0, 0, 0); color: rgb(0, 0, 0);\"><font style=\"font-size: 12pt;\">5. Small children should not be entrusted with the fee.&nbsp;</font></p><p style=\"margin-bottom: 0.14in; direction: ltr; line-height: 18.399999618530273px; widows: 2; orphans: 2; caret-color: rgb(0, 0, 0); color: rgb(0, 0, 0);\"><font style=\"font-size: 12pt;\">6. Students will be allowed to appear for final Examination only on payment of all the dues.&nbsp;</font></p></ol>', 1, '2017-12-24 19:52:07'),
(4, 'Refund Policy', 'refund-policy', 'lorem', NULL, '<p class=\"section-fullwidth\"></p><p class=\"element-size-100\"></p><p class=\" lightbox col-md-12\"> </p><h3><b>Refund Policy</b></h3><br><span style=\"caret-color: rgb(97, 97, 97); color: rgb(97, 97, 97); font-family: \" open=\"\" sans\",=\"\" sans-serif;=\"\" font-size:=\"\" 13px;\"=\"\">The Modern School does not have any refund policy.</span><br><br><h3><br></h3><p></p><p class=\" lightbox col-md-12\"><br></p><p class=\" lightbox col-md-12\"><br></p><p class=\" lightbox col-md-12\"><br></p><p></p><p></p>', 1, '2017-12-24 19:52:07'),
(5, 'Shipping & Delivery Policy', 'shipping_delivery', 'Shipping & Delivery Policy', NULL, '<p><span style=\"caret-color: rgb(97, 97, 97); color: rgb(97, 97, 97); font-family: &quot;Open Sans&quot;, sans-serif; font-size: 13px;\">We do not have any Shipping or Delivery Policy as we do not sell any products.</span><br></p>', 1, '2017-12-24 19:53:33'),
(6, 'Terms & Conditions', 'terms-conditions', 'Terms & Conditions', NULL, '<ul><li><p style=\"margin-bottom: 10px; line-height: 22px; caret-color: rgb(97, 97, 97); color: rgb(97, 97, 97); font-family: &quot;Open Sans&quot;, sans-serif; font-size: 13px; text-align: justify;\">Welcome to our website. If you continue to browse and use this website you are agreeing to comply with and be bound by the following terms and conditions of use, which together with our privacy policy govern Blue Flame’s relationship with you in relation to this website.</p><p style=\"margin-bottom: 10px; line-height: 22px; caret-color: rgb(97, 97, 97); color: rgb(97, 97, 97); font-family: &quot;Open Sans&quot;, sans-serif; font-size: 13px; text-align: justify;\">The term ‘The Modern School’ or ‘us’ or ‘we’ refers to the owner of the website whose registered office is Near N.H.-15, Barmer Gadan, Barmer-344001. Our school Affiliation number is CBSE-1730452 &amp; School No. 10797. The term ‘you’ refers to the user or viewer of our website.</p><p style=\"margin-bottom: 10px; line-height: 22px; caret-color: rgb(97, 97, 97); color: rgb(97, 97, 97); font-family: &quot;Open Sans&quot;, sans-serif; font-size: 13px; text-align: justify;\">&nbsp;</p><p style=\"margin-bottom: 10px; line-height: 22px; caret-color: rgb(97, 97, 97); color: rgb(97, 97, 97); font-family: &quot;Open Sans&quot;, sans-serif; font-size: 13px; text-align: justify;\"><b>The use of this website is subject to the following terms of use:&nbsp;</b></p><ul style=\"margin-bottom: 10px; caret-color: rgb(97, 97, 97); color: rgb(97, 97, 97); font-family: &quot;Open Sans&quot;, sans-serif; font-size: 13px;\"><li style=\"text-align: justify;\">Welcome to our website. If you continue to browse and use this website you are agreeing to comply with and be bound by the following terms and conditions of use, which together with our privacy policy govern Blue Flame’s relationship with you in relation to this website. The term ‘The Modern School’ or ‘us’ or ‘we’ refers to the owner of the website whose registered office is Near N.H.-15, Barmer Gadan , Barmer-344001. Our school Affiliation number is CBSE-1730452 &amp; School No. is 10797. The term ‘you’ refers to the user or viewer of our website. The use of this website is subject to the following terms of use:</li><li style=\"text-align: justify;\">Neither we nor any third parties provide any warranty or guarantee as to the accuracy, timeliness, performance, completeness or suitability of the information and materials found or offered on this website for any particular purpose. You acknowledge that such information and materials may contain inaccuracies or errors and we expressly exclude liability for any such inaccuracies or errors to the fullest extent permitted by law.</li><li style=\"text-align: justify;\">Your use of any information or materials on this website is entirely at your own risk, for which we shall not be liable. It shall be your own responsibility to ensure that any services or information available through this website meet your specific requirements.</li><li style=\"text-align: justify;\">This website contains material which is owned by or licensed to us. This material includes, but is not limited to, the design, layout, look, appearance and graphics. Reproduction is prohibited other than in accordance with the copyright notice, which forms part of these terms and conditions.</li><li style=\"text-align: justify;\">All trade marks reproduced in this website which are not the property of, or licensed to, the operator are acknowledged on the website.</li><li style=\"text-align: justify;\">Unauthorised use of this website may give rise to a claim for damages and/or be a criminal offence.</li><li style=\"text-align: justify;\">From time to time this website may also include links to other websites. These links are provided for your convenience to provide further information. They do not signify that we endorse the website(s). We have no responsibility for the content of the linked website(s).</li><li style=\"text-align: justify;\">You may not create a link to this website from another website or document without The Modern School’s prior written consent.</li><li style=\"text-align: justify;\">Your use of this website and any dispute arising out of such use of the website is subject to the laws of India or other regulatory authority.</li></ul></li></ul><p></p>', 1, '2017-12-24 19:53:33'),
(7, 'Principals Message', 'principals-message', 'Principal Message', 'uploads/master_pages_image/1624263569.JPG', '<h4><p style=\"margin-bottom: 0.14in; direction: ltr; line-height: 18.399999618530273px; widows: 2; orphans: 2; caret-color: rgb(0, 0, 0); color: rgb(0, 0, 0); font-weight: normal;\"><font style=\"font-size: 12pt;\">In the pursuit of raising a platform for young generation and finding the process of preparing students to succeed in the future, which is unpredictable and constantly changing, our daunting journey of research, unlearning and introspection began. And we emerged with an education model to which we gave the name \'The Modern School\'. Why \'The Modern\'? Today the term \'modernization\' is the most misunderstood one. \'MODERNization\' don\'t mean \'Westernization\'. Our belief about modernization is providing international exposure and opportunities, to study the cultures, events and practices of various societies; to pick up good aspects and imbibe them in ours. Having a community of teachers and students from diverse nations and cultures compliments this very approach. The other aspects that we lay a strong emphasis upon are strong Human and Social Values and receptivity and respect for different opinions. The educators at The Modern are specialists who ingrain in our students the finer nuances of life and pave every single step of their ascent to a socially, morally, emotionally, spiritually and intellectually responsible citizens. To transform them into \'Citizens Of The World\'.The world has changed phenomenally since we started our journey. Today will live in the times of \'Artificial Intelligence\', \'Augmented Reality\' and \'Internet Of Things\'. Lifestyles are changing. Several traditional jobs and careers are fast becoming obsolete. At The Modern we remain committed to keep reinventing our curriculum, pedagogy, systems and practices to well prepare our students to adapt to this new world order. While doing so, we never lose focus from our endeavour of creating happy and joyful communities. We always believe in standing by our beliefs and never resting on our laurels. Because we seek to enhance upon what we have already achieved. We are proud of the heights we have achieved. But there are loftier peaks to scale. Day after day. Year after year.</font></p><p style=\"margin-bottom: 0.14in; direction: ltr; line-height: 18.399999618530273px; widows: 2; orphans: 2; caret-color: rgb(0, 0, 0); color: rgb(0, 0, 0); font-weight: normal;\"><font style=\"font-size: 12pt;\">Mrs. Navneet Pachauri</font></p><p style=\"margin-bottom: 0.14in; direction: ltr; line-height: 18.399999618530273px; widows: 2; orphans: 2; caret-color: rgb(0, 0, 0); color: rgb(0, 0, 0); font-weight: normal;\"><font style=\"font-size: 12pt;\">Principal</font></p></h4>', 1, '2017-12-28 13:49:49'),
(8, 'Infrastructure', 'infrastructure', 'lorem', NULL, '<h1 style=\"text-align: center;\">Infrastructure</h1><p>Infrastructure is crucial for running a school. We ought to ensure that school is well equipped for it to function in a proper manner &amp; to adhere to the quality standards that we have set for ourselves. Just to name a few…..</p><p><ul><li>Well ventilated and lighted rooms.<br></li><li>Air conditioned classrooms supported by 100% power backup as small children get very restless because of the intense heat conditions during summer months and a comfortable environment will only help increase their output &amp; efficiency<br></li><li>A well maintained hygienic medium sized splash water pool in which children simply freak out during summers.<br></li><li>We are also well equipped with Fun stations, educational toys, games, tricycles, swings, slides etc.<br></li><li>Considering the fact that a two year old child becomes restless in a very short span of time in the class so he needs equal time in fresh air and what better way to provide him an open courtyard in the Pre Nursery compound. Not to mention the fact that the immense greenery makes one feel in the lap of nature.<br></li><li>This play school also has a Computer Lab for a practical approach in computers for class 1 students.<br></li><li>Environment friendly silent gensets.<br></li></ul></p><h1 style=\"text-align: center;\">Digital Teaching Systems</h1><p>The one and only play school in Moradabad that has initiated the smart class program. Our progressive and forward thinking approach makes us have this teaching tool right from Pre Nursery.</p><p>What a smartclass is-</p><p>It is a digital interactive learning tool which:</p><p><ul><li>Brings abstract and difficult curriculum concepts to life.<br></li><li>Makes learning an enjoyable experience for students.<br></li><li>Improves academic performance of students.<br></li></ul></p><p>SAS (Smart Assessment System)</p><p><br></p><p>Installed in class 1 for instant electronic assessment of students.</p>', 0, '2017-12-28 13:50:30'),
(9, 'Modules Activities', 'modules-activities', 'lorem', NULL, '<p class=\"heading-description\" style=\"color:#333333 !important; text-align: justify; font-style:normal;\">To help us to stay focussed on our vision, we have implemented a number of unique teaching methods that this city has not experienced so far. Just to name a few:</p>      <p></p>      <br>      <p class=\"heading-description\" style=\"color:#333333 !important; text-align: justify; font-style:normal;\">First and foremost we create an atmosphere of fun….. A place where they can look forward to come. As Winston Churchill once said “I am always ready to learn, but I do not always like being taught”. Nothing excites the child’s imagination like play. And this enhances their learning capability so that they will be achievers in life.</p>      <p></p>      <br>      <p class=\"heading-description\" style=\"color:#333333 !important; text-align: justify; font-style:normal;\">Second, we reckon that every human being craves for attention and children demand tons of it. Attention helps them to learn and grow. At Neo Dales we ensure that every child gets individual attention. Unique to our school reflected by the least number of students per teacher.</p>      <p></p>      <br>      <p class=\"heading-description\" style=\"color:#333333 !important; text-align: justify; font-style:normal;\">Read on for our co-curricular activities which make the children of this school extremely confident and zealous in life- </p>      <p>&nbsp;</p>    <p class=\"row text-center\">      <p class=\"col-sm-4\">      <article class=\"cs-services modren top-center \">         <figure><img alt=\"\" src=\"public/wp-content/uploads/2015/07/rsz_2.png\"></figure>         <p class=\"text\"></p>         <h4>Art and Craft Classes</h4>         <p style=\"color: #991872 !important\">Little children have a very creative mind which one often tends to overlook. We at Neo Dales understand this very well and our Art and Craft Classes stimulate their creative minds to bring out the best in them</p>         <p></p>      </article>  </p>      <p class=\"col-sm-4\">      <article class=\"cs-services modren top-center \">         <figure><img alt=\"\" src=\"public/wp-content/uploads/2015/07/rsz_6.png\"></figure>         <p class=\"text\"></p>         <h4>Market Day</h4>         <p style=\"color: #991872 !important\">Which is held every year during Diwali / Christmas time is a a very exciting event for children as they shop on their own with a fixed amount of money directed by the school thereby discipline themselves about the value of money at an early stage.</p>         <p></p>      </article>  </p>  <p class=\"col-sm-4\">      <article class=\"cs-services modren top-center \">         <figure><img alt=\"\" src=\"public/wp-content/uploads/2015/07/rsz_4.png\"></figure>         <p class=\"text\"></p>         <h4>Oratory Skills</h4>         <p style=\"color: #991872 !important\">Are taken right from Pre Nursery to class 1 and ensure the overall development of the child as apart from academics this equally plays an important part in a child’s life. We would like to emphasize that a good command over English is followed religiously by all</p>         <p></p>      </article>  </p></p><p class=\"row text-center\">  <p class=\"col-sm-4\">      <article class=\"cs-services modren top-center \">         <figure><img alt=\"\" src=\"public/wp-content/uploads/2015/07/rsz_3.png\"></figure>         <p class=\"text\"></p>         <h4>Sports Fanatics</h4>         <p style=\"color: #991872 !important\">Sports is an integral part in moulding the personality of the child, we at Neo Dales give due weightage to Sports and Physical Education. Our children take active part in sports from classes Pre Nursery itself.</p>         <p></p>      </article>  </p>  <p class=\"col-sm-4\">      <article class=\"cs-services modren top-center \">         <figure><img alt=\"\" src=\"public/wp-content/uploads/2015/07/rsz_8.png\"></figure>         <p class=\"text\"></p>         <h4>Dance &amp; Singing Classes</h4>         <p style=\"color: #991872 !important\">Emphasis is laid on general form of dance and singing and it is seen that the little ones have a gala time doing these activities.  It helps a great deal in audio visual and body movement.</p>         <p></p>      </article>  </p>  <p class=\"col-sm-4\">      <article class=\"cs-services modren top-center \">         <figure><img alt=\"\" src=\"public/wp-content/uploads/2015/07/rsz_7.png\"></figure>         <p class=\"text\"></p>         <h4>Educational / Nature trips (local)</h4>         <p style=\"color: #991872 !important\">Are organized every year and there is no doubt of the fact that Neo Dalians have loads of fun during these outings. It is an excellent learning experience for them.</p>         <p></p>      </article>  </p></p><p class=\"row text-center\">  <p class=\"col-sm-4\">      <article class=\"cs-services modren top-center \">         <figure><img alt=\"\" src=\"public/wp-content/uploads/2015/07/rsz_1.png\"></figure>         <p class=\"text\"></p>         <h4>Annual Function</h4>         <p style=\"color: #991872 !important\">Held normally in the month of December is awaited by one &amp; all where skills &amp; talents of our Neo Dalians are highlighted on stage with a lot of gaiety.</p>         <p></p>      </article>  </p>  <p class=\"col-sm-4\">      <article class=\"cs-services modren top-center \">         <figure><img alt=\"\" src=\"public/wp-content/uploads/2015/07/rsz_5.png\"></figure>         <p class=\"text\"></p>         <h4>Festivals and Important days</h4>         <p style=\"color: #991872 !important\">are celebrated with enthusiasm so as to expose these precious gems of Neo Dales to lovely and colourful Indian tradition at an early age.</p>         <p></p>      </article>  </p>  <p class=\"col-sm-4\">      <article class=\"cs-services modren top-center \">         <figure><img alt=\"\" src=\"public/wp-content/uploads/2015/07/rsz_10.png\"></figure>         <p class=\"text\"></p>         <h4>Competitions &amp; Presentations</h4>         <p style=\"color: #991872 !important\">[Presentations for classes Pre Nursery to U.K.G. &amp; Competitions for class 1] A common platform is set for them by organizing cultural activities and ensure participation of every child not by compulsion but by encouragement without the fear of winning and losing which is immaterial. It’s the zeal &amp; spirit of participation and not the result that we inculcate in our children.</p>         <p></p>      </article>  </p>   </p>', 0, '2017-12-28 13:50:30'),
(10, 'Rules Regulations', 'rules-regulations', 'lorem', NULL, NULL, 0, '2017-12-28 13:51:10'),
(11, 'School Uniform', 'school-uniform', 'lorem', NULL, '<ul class=\"nav nav-tabs\">   <li class=\"active\"> <a href=\"#cs-tab-summer9261\" data-toggle=\"tab\" aria-expanded=\"true\"> SUMMER</a></li>   <li class=\"\"> <a href=\"#cs-tab-winter8904\" data-toggle=\"tab\" aria-expanded=\"false\"> WINTER</a></li></ul><p class=\"tab-content\"></p><p class=\"tab-pane fade\" id=\"cs-tab-summer9261\">               </p><p class=\"row\">                   </p><p class=\"col-sm-6\">                      </p><h1>FOR BOYS</h1>                  <ul class=\"cs-iconlist\">   <li class=\"has_border\">Half sleeves striped shirt (white, purple &amp; blue)</li>   <li class=\"has_border\">Purple shorts</li>   <li class=\"has_border\">School belt</li>   <li class=\"has_border\">White socks with purple stripes</li>   <li class=\"has_border\">Black leather shoes</li></ul><p></p><p class=\"col-sm-6\"></p><h1>FOR GIRLS</h1><ul class=\"cs-iconlist\">   <li class=\"has_border\">Striped tunic (white, purple &amp; blue)</li>   <li class=\"has_border\">White socks with purple stripes</li>   <li class=\"has_border\">Black leather shoes</li>   <li class=\"has_border\">Black hair band / clips</li></ul><p></p><p></p><p></p><p class=\"tab-pane fade active\" id=\"cs-tab-winter8904\"></p><h1>FOR BOYS &amp; GIRLS</h1><ul class=\"cs-iconlist\">   <li class=\"has_border\">Full sleeves chinese collared striped shirt (white, purple &amp; blue)</li>   <li class=\"has_border\">Greyish blue worsted trousers</li>   <li class=\"has_border\">Magenta pullover</li>   <li class=\"has_border\">School jacket</li>   <li class=\"has_border\">School belt</li>   <li class=\"has_border\">Grey woollen socks with purple stripes</li>   <li class=\"has_border\">Black leather shoes</li>   <li class=\"has_border\">School cap (optional)</li></ul><p></p><p></p><h1>Note:</h1><ul class=\"cs-iconlist\">   <li class=\"has_border\">Students to put on ONLY white canvas shoes on the day of their Sports Fanatics / P.T. period.</li>   <li class=\"has_border\">Winter Uniform becomes applicable w.e.f. 1st November &amp; Jacket becomes compulsory   w.e.f. 1st December.</li>   <li class=\"has_border\">Children should come to school in proper uniform and neatly dressed. Students are expected to be habitually clean and neatly turned out.</li>   <li class=\"has_border\">Pre Nursery and Nursery students to get an extra set of clothes daily to school in a labeled bag.</li></ul><p>&nbsp;</p><h1>IMPORTANT</h1><ul class=\"cs-iconlist\">   <li class=\"has_border\">Suitable action will be taken if children are not in proper uniform. </li></ul>', 0, '2017-12-28 13:51:10'),
(12, 'Information Technology', 'information-technology', 'lorem', NULL, '<p></p><p style=\"color: rgb(0, 0, 0);\">We feel proud of the fact that we are one of the most digitally advanced schools, all for the convenience of you parents. So please read on below as to how you can make best use of this-</p><p style=\"color: rgb(0, 0, 0);\"></p><ul style=\"color: rgb(0, 0, 0);\"><li style=\"color: rgb(0, 0, 0);\">We have our very dynamic &amp; user friendly website www.neodales.edu.in which hosts all information of our school &amp; which is constantly updated.</li><li style=\"color: rgb(0, 0, 0);\">Click on Parents Login on our website &gt; you will be redirected to our School ERP &gt; Under User Name type your ward’s S.R. No. &amp; Password will be father’s mobile no. as per our records &gt; you will then be asked to change your password. Once you have logged in you can access-<br style=\"color: rgb(0, 0, 0);\"></li></ul><p style=\"color: rgb(0, 0, 0);\"></p><p style=\"color: rgb(0, 0, 0);\">a. Attendance of your ward </p><p style=\"color: rgb(0, 0, 0);\">b.Time Table</p><p style=\"color: rgb(0, 0, 0);\">c.Forthcoming Events</p><p style=\"color: rgb(0, 0, 0);\">d. School Calendar</p><p style=\"color: rgb(0, 0, 0);\">e.Report Card</p><p style=\"color: rgb(0, 0, 0);\">f. Update address and cell nos.</p><p style=\"color: rgb(0, 0, 0);\">g.View fee status &amp; fee bill &amp; pay online from there itself </p><p style=\"color: rgb(0, 0, 0);\">h. View Imprest Money Ledger</p><p style=\"color: rgb(0, 0, 0);\">i. View Transport Details</p><p style=\"color: rgb(0, 0, 0);\">j. Download Conveyance Form, Student Record Updation and Withdrawal Form</p><p style=\"color: rgb(0, 0, 0);\">k. E-Diary</p><p style=\"color: rgb(0, 0, 0);\">l. Video Gallery</p><p style=\"color: rgb(0, 0, 0);\"></p><ul><li style=\"color: rgb(0, 0, 0);\">Forthcoming Events are updated on Website / App. You will receive an e-diary note to check the same.<br style=\"color: rgb(0, 0, 0);\"></li><li style=\"color: rgb(0, 0, 0);\">Hard copies of Term 1 Report Card will not be issued and the same should be viewed online at Parents Login.<br style=\"color: rgb(0, 0, 0);\"></li><li style=\"color: rgb(0, 0, 0);\">School sends an SMS to convey any urgent message. However, the school takes no responsibility for the same in case of internet / network failure or failure from the service provider’s end. <br style=\"color: rgb(0, 0, 0);\"></li><li style=\"color: rgb(0, 0, 0);\">As you all know that pictures are taken for various events so parents wishing to see pictures may please click on Gallery icon on our website from where you can view the same. The pictures are taken at random to showcase the event.<br style=\"color: rgb(0, 0, 0);\"></li><li style=\"color: rgb(0, 0, 0);\">Fee details can be viewed online and not be dependent on hard copy of fee bill. <br style=\"color: rgb(0, 0, 0);\"></li><li style=\"color: rgb(0, 0, 0);\">Preferred modes of payment:&nbsp;<span style=\"color: rgb(0, 0, 0);\">NEFT, Online from our Payment Gateway &amp; Cheque.<br style=\"color: rgb(0, 0, 0);\"></span></li><li><font color=\"#000000\">We&nbsp;do not&nbsp;accept cash.</font></li><li style=\"color: rgb(0, 0, 0);\">Parents are required to make best use of this facility by logging to our website www.neodales.edu.in. You may also pay the same through our mobile app. If fee is submitted through NEFT kindly mail us all details to accounts@neodales.edu.in or else the amount will lie in suspense and fee pending will reflect of your ward.</li></ul><p style=\"color: rgb(0, 0, 0);\"><br></p><p style=\"color: rgb(0, 0, 0);\"><b style=\"font-size: 22px;\">School Mobile App</b><br></p><p style=\"color: rgb(0, 0, 0);\"></p><p style=\"color: rgb(0, 0, 0);\">We also have a School Mobile App which is seamlessly integrated with our School’s ERP Software much for the convenience of parents. Hence, most of the above features are also available on the app. Apple phone &amp; Android users should download the same by searching for “Neo dales play school” from the App Store / Play Store &amp; have access to all info of school &amp; your ward on your palm.</p><p></p>', 0, '2017-12-28 13:51:47'),
(13, 'Conveyance', 'conveyance', 'lorem', NULL, '<p><br></p><h4><span style=\"color:rgb(152, 30, 113);\">The school has a fleet of buses, maxi vans, vans and covered rickshaws to facilitate children to and fro from home to school covering the entire city of Moradabad.</span></h4><p></p><ul><li>Any parent wishing to avail school conveyance for their ward are required to submit a conveyance undertaking form in office. Conveyance Undertaking Form can be downloaded online from Parents Login.Once we receive the form, confirmation and other details will be intimated to you in 2 days which is the time required to process the form. However, please note that school is under no obligation to provide conveyance if because of certain reasons it is not feasible.<br></li><li>Also, please note that school will not be able to provide conveyance to children residing in congested areas.<br></li><li>Children residing in narrow lanes will be picked and dropped on the main road, hence guardians should take care of that.<br></li><li>In case your ward is availing &nbsp;WINGER / TRAVELLER facility he / she will be picked up / dropped from the Main Road pre decided Bus Stops. Children will neither be picked up from by lanes nor from inside gated colonies / apartments, nor from individual houses. In this manner children will also save on commuting time.<br></li><li>The school has Camera &amp; GPS installed conveyance all for the convenience of parents to track live location of their ward\'s school vehicle.<br></li><li>The students are accompanied by a female attendant in each vehicle.</li><li>Students availing conveyance should be ready in time as communicated to them failing which the vehicle will move further without waiting.<br></li><li>Conveyance charges will be paid for all 12 months beginning April to March irrespective of Summer Break.<br></li><li>If on any specified day your ward is to be picked up by the guardian instead of going by the regular school conveyance, it should be mailed to the Principal latest by 10 am failing which the school will not be responsible if your ward is sent by school conveyance. Please take a note that any such information on phone will not be entertained.<br></li><li>The sequence in which your ward will be picked / dropped will not be changed as it is well planned by the driver.</li><li>Parents are requested not to send messages through the driver / attendant of school conveyance. You may email us for any query or information. School will not take any responsibility for messages conveyed to driver or attendant.<br></li><li>Any conveyance related matter should be intimated / discussed with the Conveyance Incharge Mr. Ankur Pandey.<br></li><li>Unnecessary phone calls to Drivers, should be avoided and they have also been instructed not to use mobiles while driving.<br></li></ul><p></p>', 0, '2017-12-28 13:51:47'),
(14, 'School Captains', 'school-captains', 'lorem', NULL, '<p class=\"element-size-100\">                            </p><p class=\"col-md-12  \">                                </p><table class=\"table table-bordered\">                                    <thead>                                        <tr>                                            <th>Year</th>                                            <th>Boy Name</th>                                            <th>Girl Name</th>                                        </tr>                                    </thead>                                    <tbody>                                        <tr>                                            <td>2012</td>                                            <td>Aryan Jain</td>                                            <td>Manya Garg</td>                                        </tr>                                        <tr>                                            <td>2013</td>                                            <td>Yash Saxena</td>                                            <td>Khushi Khattar</td>                                        </tr>                                        <tr>                                            <td>2014</td>                                            <td>Sukhraj Singh</td>                                            <td>Rishika Rastogi</td>                                        </tr>                                        <tr>                                            <td>2015</td>                                            <td>Daksh Saraswat</td>                                            <td>Samridhi Bhutani</td>                                        </tr>                                        <tr>                                            <td>2016</td>                                            <td>Ansh Mehrotra</td>                                            <td>Yashita Gandhi</td>                                        </tr>                                        <tr>                                            <td>2017</td>                                            <td>Atharv Nijhawan</td>                                            <td>Kaashvi Wadhwa</td>                                        </tr>   <tr>                                            <td>2018</td>                                            <td>Shabeeb Ahmad Siddiqui</td>                                            <td>Aarika Agarwal</td>                                        </tr>                                   </tbody>                                </table>                            <p></p>                        <p></p>', 0, '2017-12-28 13:52:41'),
(15, 'Vision Mission', 'vision-ission', 'Vision Mission', 'uploads/master_pages_image/1618819920.jpg', '<p style=\"margin-bottom: 0.14in; direction: ltr; line-height: 18.399999618530273px; widows: 2; orphans: 2; caret-color: rgb(0, 0, 0); color: rgb(0, 0, 0);\"><font style=\"font-size: 14pt;\"><b>CORE PURPOSE</b></font></p><p style=\"margin-bottom: 0.14in; direction: ltr; line-height: 18.399999618530273px; widows: 2; orphans: 2; caret-color: rgb(0, 0, 0); color: rgb(0, 0, 0);\"><font style=\"font-size: 12pt;\">To create leaders for tomorrow</font></p><p style=\"margin-bottom: 0.14in; direction: ltr; line-height: 18.399999618530273px; widows: 2; orphans: 2; caret-color: rgb(0, 0, 0); color: rgb(0, 0, 0);\"><font style=\"font-size: 14pt;\"><b>CORE VALUES&nbsp;</b></font></p><p style=\"margin-bottom: 0.14in; direction: ltr; line-height: 18.399999618530273px; widows: 2; orphans: 2; caret-color: rgb(0, 0, 0); color: rgb(0, 0, 0);\"><font style=\"font-size: 12pt;\">Care</font></p><p style=\"margin-bottom: 0.14in; direction: ltr; line-height: 18.399999618530273px; widows: 2; orphans: 2; caret-color: rgb(0, 0, 0); color: rgb(0, 0, 0);\"><font style=\"font-size: 12pt;\">Integrity</font></p><p style=\"margin-bottom: 0.14in; direction: ltr; line-height: 18.399999618530273px; widows: 2; orphans: 2; caret-color: rgb(0, 0, 0); color: rgb(0, 0, 0);\"><font style=\"font-size: 12pt;\">Innovation</font></p><p style=\"margin-bottom: 0.14in; direction: ltr; line-height: 18.399999618530273px; widows: 2; orphans: 2; caret-color: rgb(0, 0, 0); color: rgb(0, 0, 0);\"><font style=\"font-size: 14pt;\"><b>B.H.A.G.</b></font></p><p style=\"margin-bottom: 0.14in; direction: ltr; line-height: 18.399999618530273px; widows: 2; orphans: 2; caret-color: rgb(0, 0, 0); color: rgb(0, 0, 0);\"><font style=\"font-size: 12pt;\">To revolutionize the education</font></p>', 1, '2017-12-28 13:52:41'),
(16, 'School Hours Security', 'school-hours-security', 'lorem', '', '<h1>School Hours</h1><p class=\"table-responsive\"></p><table class=\"table table-bordered\">   <thead>      <tr>         <th>Class</th>         <th>Summer</th>         <th>Winter</th>      </tr>   </thead>   <tbody>      <tr>         <td>Pre Nursery &amp; Nursery</td>         <td>9:00 am to 12:00 noon</td>         <td>10:00 am to 1:00 pm</td>      </tr>      <tr>         <td>L.K.G. &amp; U.K.G.</td>         <td>8:00 am to 12:45 pm</td>         <td>9:00 am to 1:45 pm</td>      </tr>     </tbody></table><p></p><p></p><h4><b><b>Office Timings</b> </b></h4><br> 09:00 am to 03:00 pm<br><br>Parents may meet the Principal between 10:00 am to 12:00 noon<p style=\"font-weight: bold;\"></p><p style=\"font-weight: bold;\"></p><h1 style=\"font-weight: bold;\">Security</h1><p><b>Children are very small that they can be lured by strangers, a fact which cannot be denied. Hence, utmost care is taken by the school authorities for the safety of your child.</b></p><ul class=\"cs-iconlist\" style=\"\">   <li class=\"has_border\" style=\"\">A child is never handed over to any stranger.</li>   <li class=\"has_border\" style=\"\">Any unknown person coming to pick your ward must carry his / her mobile phone so that the OTP generated at random from school to parents mobile numbers can be shared by either of the parent with the stranger for Security Check and Authentication.Phone calls from parents guaranteeing the authenticity of the person will not be entertained.</li></ul>', 0, '2017-12-28 13:53:00');
INSERT INTO `tms_master_pages` (`master_pages_id`, `master_pages_title`, `master_pages_slug`, `master_pages_excerpt`, `master_pages_image`, `master_pages_description`, `master_pages_status`, `master_pages_created`) VALUES
(17, 'Fee Structure', 'fee-structure', 'lorem', NULL, '<div  class=\"col-md-12\"></p>\r\n                            <table class=\"table tablev1\" style=\"margin-bottom: 0px;\">\r\n                                <thead>\r\n                                    <tr>\r\n                                        <th></th>\r\n                                        <th style=\"text-align: center;\" colspan=\"2\">1st Installment<br />\r\n                                        (April to June)</th>\r\n                                        <th style=\"text-align: center;\" colspan=\"2\">2nd Installment<br />\r\n                                        (July to Sept)</th>\r\n                                        <th style=\"text-align: center;\" colspan=\"2\">3rd Installment<br />\r\n                                        (Oct. to Dec.)</th>\r\n                                        <th style=\"text-align: center;\" colspan=\"2\">4th Installment<br />\r\n                                        (Jan. to Mar.)</th>\r\n                                    </tr>\r\n                                </thead>\r\n                                <tbody>\r\n                                    <tr>\r\n                                        <td></td>\r\n                                        <td>Pre-Nursery, Nursery, L.K.G. &amp; U.K.G.</td>\r\n                                        <td>I &amp; II</td>\r\n                                        <td>Pre-Nursery, Nursery, L.K.G. &amp; U.K.G.</td>\r\n                                        <td>I &amp; II</td>\r\n                                        <td>Pre-Nursery, Nursery, L.K.G. &amp; U.K.G.</td>\r\n                                        <td>I &amp; II</td>\r\n                                        <td>Pre-Nursery, Nursery, L.K.G. &amp; U.K.G.</td>\r\n                                        <td>I &amp; II</td>\r\n                                    </tr>\r\n                                    <tr>\r\n                                        <td>Admission Fee (Non Refundable)</td>\r\n                                        <td style=\"text-align: right;\">15000</td>\r\n                                        <td style=\"text-align: right;\">15000</td>\r\n                                        <td style=\"text-align: right;\">&#8211;</td>\r\n                                        <td style=\"text-align: right;\">&#8211;</td>\r\n                                        <td style=\"text-align: right;\">&#8211;</td>\r\n                                        <td style=\"text-align: right;\">&#8211;</td>\r\n                                        <td style=\"text-align: right;\">&#8211;</td>\r\n                                        <td style=\"text-align: right;\">&#8211;</td>\r\n                                    </tr>\r\n                                    <tr>\r\n                                        <td>Security Deposit (Refundable)</td>\r\n                                        <td style=\"text-align: right;\">3000</td>\r\n                                        <td style=\"text-align: right;\">3000</td>\r\n                                        <td style=\"text-align: right;\">&#8211;</td>\r\n                                        <td style=\"text-align: right;\">&#8211;</td>\r\n                                        <td style=\"text-align: right;\">&#8211;</td>\r\n                                        <td style=\"text-align: right;\">&#8211;</td>\r\n                                        <td style=\"text-align: right;\">&#8211;</td>\r\n                                        <td style=\"text-align: right;\">&#8211;</td>\r\n                                    </tr>\r\n                                    <tr>\r\n                                        <td>Annual Charges</td>\r\n                                        <td style=\"text-align: right;\">2500</td>\r\n                                        <td style=\"text-align: right;\">2500</td>\r\n                                        <td style=\"text-align: right;\">&#8211;</td>\r\n                                        <td style=\"text-align: right;\">&#8211;</td>\r\n                                        <td style=\"text-align: right;\">2500</td>\r\n                                        <td style=\"text-align: right;\">2500</td>\r\n                                        <td style=\"text-align: right;\">&#8211;</td>\r\n                                        <td style=\"text-align: right;\">&#8211;</td>\r\n                                    </tr>\r\n                                    <tr>\r\n                                        <td>Tuition Fee</td>\r\n                                        <td style=\"text-align: right;\">7000</td>\r\n                                        <td style=\"text-align: right;\">5900</td>\r\n                                        <td style=\"text-align: right;\">7000</td>\r\n                                        <td style=\"text-align: right;\">5900</td>\r\n                                        <td style=\"text-align: right;\">7000</td>\r\n                                        <td style=\"text-align: right;\">5900</td>\r\n                                        <td style=\"text-align: right;\">7000</td>\r\n                                        <td style=\"text-align: right;\">5900</td>\r\n                                    </tr>\r\n                                    <tr>\r\n                                        <td>Educational Technology Fee</td>\r\n                                        <td style=\"text-align: right;\">750</td>\r\n                                        <td style=\"text-align: right;\">920</td>\r\n                                        <td style=\"text-align: right;\">750</td>\r\n                                        <td style=\"text-align: right;\">920</td>\r\n                                        <td style=\"text-align: right;\">750</td>\r\n                                        <td style=\"text-align: right;\">920</td>\r\n                                        <td style=\"text-align: right;\">750</td>\r\n                                        <td style=\"text-align: right;\">920</td>\r\n                                    </tr>\r\n                                    <tr>\r\n                                        <td>Sports Fee</td>\r\n                                        <td style=\"text-align: right;\">230</td>\r\n                                        <td style=\"text-align: right;\">230</td>\r\n                                        <td style=\"text-align: right;\">230</td>\r\n                                        <td style=\"text-align: right;\">230</td>\r\n                                        <td style=\"text-align: right;\">230</td>\r\n                                        <td style=\"text-align: right;\">230</td>\r\n                                        <td style=\"text-align: right;\">230</td>\r\n                                        <td style=\"text-align: right;\">230</td>\r\n                                    </tr>\r\n                                    <tr>\r\n                                        <td>Assessment Fee</td>\r\n                                        <td style=\"text-align: right;\">&#8211;</td>\r\n                                        <td style=\"text-align: right;\">&#8211;</td>\r\n                                        <td style=\"text-align: right;\">225</td>\r\n                                        <td style=\"text-align: right;\">280</td>\r\n                                        <td style=\"text-align: right;\">&#8211;</td>\r\n                                        <td style=\"text-align: right;\">&#8211;</td>\r\n                                        <td style=\"text-align: right;\">225</td>\r\n                                        <td style=\"text-align: right;\">280</td>\r\n                                    </tr>\r\n                                    <tr>\r\n                                        <td>*Imprest Money (Refundable)</td>\r\n                                        <td style=\"text-align: right;\">2000</td>\r\n                                        <td style=\"text-align: right;\">2000</td>\r\n                                        <td style=\"text-align: right;\">&#8211;</td>\r\n                                        <td style=\"text-align: right;\">&#8211;</td>\r\n                                        <td style=\"text-align: right;\">&#8211;</td>\r\n                                        <td style=\"text-align: right;\">&#8211;</td>\r\n                                        <td style=\"text-align: right;\">&#8211;</td>\r\n                                        <td style=\"text-align: right;\">&#8211;</td>\r\n                                    </tr>\r\n                                    <tr>\r\n                                        <td>Conveyance Fee<br />\r\n                                        ( As Applicable )</td>\r\n                                        <td style=\"text-align: right;\">&#8211;</td>\r\n                                        <td style=\"text-align: right;\">&#8211;</td>\r\n                                        <td style=\"text-align: right;\">&#8211;</td>\r\n                                        <td style=\"text-align: right;\">&#8211;</td>\r\n                                        <td style=\"text-align: right;\">&#8211;</td>\r\n                                        <td style=\"text-align: right;\">&#8211;</td>\r\n                                        <td style=\"text-align: right;\">&#8211;</td>\r\n                                        <td style=\"text-align: right;\">&#8211;</td>\r\n                                    </tr>\r\n                                    <tr>\r\n                                        <td><b>TOTAL</b></td>\r\n                                        <td style=\"text-align: right;\"><b>30480</b></td>\r\n                                        <td style=\"text-align: right;\"><b>29550</b></td>\r\n                                        <td style=\"text-align: right;\"><b>8205</b></td>\r\n                                        <td style=\"text-align: right;\"><b>7330</b></td>\r\n                                        <td style=\"text-align: right;\"><b>10480</b></td>\r\n                                        <td style=\"text-align: right;\"><b>9550</b></td>\r\n                                        <td style=\"text-align: right;\"><b>8205</b></td>\r\n                                        <td style=\"text-align: right;\"><b>7330</b></td>\r\n                                    </tr>\r\n                                </tbody>\r\n                            </table>\r\n                        <p></div>\r\n                        <div class=\"liststyle\">\r\n                            <ul class=\"cs-iconlist\">\r\n                                <li class=\"has_border\"><i class=\"icon-check\"></i>Fee will be charged from April, irrespective of the month the admission takes place, subject to availability of seats.</li>\r\n                                <li class=\"has_border\"><i class=\"icon-check\"></i>Fee will be deposited for the 1st time at the time of admission in school by cheque / NEFT / online only &amp; thereafter during bank hours i.e. between 9:30 am to 3:30 pm directly in the school’s bank account through cheque or through NEFT / online in HDFC Bank Ltd., Pandit Dutt Sharma Marg, Near Civil Lines Police Station, Opp. Election Office, Moradabad.</li>\r\n                                <li class=\"has_border\"><i class=\"icon-check\"></i>Last date for fee deposit is 15th of the first month of the pertaining quarter. Late fine of Rs.10/- per day will be levied after 15th.</li>\r\n                                <li class=\"has_border\"><i class=\"icon-check\"></i>Unpaid fee after the 15th of the month it falls due may result in suitable action by the school authorities as it may deem fit. Action includes withholding Report Card.</li>\r\n                                <li class=\"has_border\"><i class=\"icon-check\"></i>Conveyance Fee is charged for all the months irrespective of Summer Break or any other vacation.</li>\r\n                                <li class=\"has_border\"><i class=\"icon-check\"></i>Air conditioned Pre Nursery, Nursery, L.K.G., &amp; U.K.G. classes.</li>\r\n                                <li class=\"has_border\"><i class=\"icon-check\"></i>Educational Technology Fee is charged towards Educomp / TeachNext smartclass E- techs, school website, sms facility, school mobile application, parent login facility through ERP software &amp; on / Mobile App &amp; computer lab (for classes 1 &amp; 2)</li>\r\n                                <li class=\"has_border\">* Amount towards excursions, Annual Function expenses, issuance of stationery (If required), I- Cards etc. are deducted from the Imprest Money A/c and the balance (if any) is refunded at the time of withdrawal.</li>\r\n                            </ul>\r\n                        </div>', 0, '2018-01-01 10:19:07'),
(18, 'Neft Payment', 'neft-payment', 'lorem', NULL, '<section class=\"page-section \">    <!-- Container Start here -->    <p class=\"container\">        <!-- Row Start -->        </p><p class=\"row\">            </p><p class=\"section-fullwidth\">                </p><p class=\"element-size-100\">                    </p><p class=\" lightbox\"> Kindly note down NEFT details mentioned below for payment of fee directly in our bank account.<br><br>                        <b>Please remember to intimate us via email on <a href=\"%27mailto_principal%40neodales.edu.html\">accounts@neodales.edu.in</a>  once you make the payment along with the reference no. or else the amount will lie with us in the suspense account.</b></p></section><section><table class=\"table table-bordered\">                <tbody>                    <tr>                        <td><b>Account Holder\'s Name</b></td>                        <td><b>Bank A/c No</b></td>                        <td><b>IFSC Code</b></td>                        <td><b>Bank Name</b></td>                        <td><b>Branch Address</b></td>                        <td><b>Branch Code</b></td>                    </tr>                                       <td>NEO DALES PLAY SCHOOL</td>                        <td>50100066633861</td>                        <td>HDFC0000303</td>                        <td>HDFC Bank</td>                        <td>PANDIT DUTT SHARMA MARG, OPP.ELECTION OFFICE, CIVIL LINES,  MORADABAD.</td>                        <td>0303</td>                    </tr>                </tbody>            </table>        <p></p>        <p></p><p></p>       <p></p></section>', 0, '2018-01-01 10:19:07'),
(19, 'Why Choose Us ', 'why-choose-us', '', 'uploads/master_pages_image/1618814078.jpg', '<p>The core purpose of The Modern School, Barmer is to create leaders for tomorrow who bring a positive difference in the society thorough our enriched core values – Care, Integrity &amp; Innovation. Imparting meaningful &amp; valuable education for transforming, we strive to revolutionize education as well as wisdom. Helping our students know and understand the three significant pillars called – Care, Share &amp; Dare, we give unique environment for academic excellence and an over all development.</p><p><br></p><p>We promise the best learning environment through well-equipped infrastructure &amp; experienced faculties</p><p>Through best of means and resources, we give opportunity of self-development &amp; self-discovery</p><p>Focusing on over all development of our students, we organize multiple creative, knowledgeable &amp; creative activities</p><p>We encourage our future citizens through knowledge, practice &amp; skills so that they are fully confident to explore the world</p><p></p><p></p>', 1, '2017-12-24 18:57:58'),
(20, 'Academics', 'academic-policy', 'Academic', NULL, '<p class=\"MsoNormal\" style=\"margin: 0cm 0cm 10pt; line-height: 16.866666793823242px; font-size: 11pt; font-family: Calibri, sans-serif; caret-color: rgb(0, 0, 0); color: rgb(0, 0, 0);\"><span lang=\"EN-US\" style=\"font-size: 12pt; line-height: 18.399999618530273px;\">The Modern School, Barmer is now a recognised landmark in the pursuit of excellence in education and an institution deeply dedicated in the mission of building \"Global citizens with Indian values\".&nbsp;<o:p></o:p></span></p><p class=\"MsoNormal\" style=\"margin: 0cm 0cm 10pt; line-height: 16.866666793823242px; font-size: 11pt; font-family: Calibri, sans-serif; caret-color: rgb(0, 0, 0); color: rgb(0, 0, 0);\"><span lang=\"EN-US\" style=\"font-size: 12pt; line-height: 18.399999618530273px;\">The Modern School is set up by a registered society, Ojasvi Sansthan. This society comprises of a group of enterprising people, who have considerable experience in education and are recognized for \'professional development initiatives\' for teachers. The Modern School is set up with a purpose to establish educational institution in Barmer that would successfully integrate academic excellence with an emphasis on value education and respect for our rich cultural heritage.<o:p></o:p></span></p><p class=\"MsoNormal\" style=\"margin: 0cm 0cm 10pt; line-height: 16.866666793823242px; font-size: 11pt; font-family: Calibri, sans-serif; caret-color: rgb(0, 0, 0); color: rgb(0, 0, 0);\"><span lang=\"EN-US\" style=\"font-size: 12pt; line-height: 18.399999618530273px;\">TMS\'s vision is to give our young minds the Schooling for Life, Freedom to Contemplate, Endow them with quality education, holistic learning, broadening their horizons, enabling them to discover their latent talents and developing their skills for molding them into independent and confident individuals. We continue to provide more and more students the opportunity to benefit from the rich legacy of the Great Educationists like Giju Bhai, John Halt, A.S. Neel ,Tetsuko Kuroyonagi ,Barbiyana School of Father Milano, V.V. John, Anil Bordiya Kamala V Mukunda, Nand Kishor Acharya, Azim Premji, Amir khan-thought of school and their unique teaching methodologies, providing them with opportunities to recognize their latent potential.&nbsp;<o:p></o:p></span></p><p class=\"MsoNormal\" style=\"margin: 0cm 0cm 10pt; line-height: 16.866666793823242px; font-size: 11pt; font-family: Calibri, sans-serif; caret-color: rgb(0, 0, 0); color: rgb(0, 0, 0);\"><span lang=\"EN-US\" style=\"font-size: 12pt; line-height: 18.399999618530273px;\">Co-curricular activities are given as much importance as the academics. With a team of committed staff who have a passion for what they do, the students learn to raise questions and understand the thing in total.<o:p></o:p></span></p><p class=\"MsoNormal\" style=\"margin: 0cm 0cm 10pt; line-height: 16.866666793823242px; font-size: 11pt; font-family: Calibri, sans-serif; caret-color: rgb(0, 0, 0); color: rgb(0, 0, 0);\"><span lang=\"EN-US\" style=\"font-size: 12pt; line-height: 18.399999618530273px;\">The school curriculum, combined with personal support extended by the teachers who choose to be role models rather than mere facilitators, inspire students to realize their full potential and mould them into better beings. In addition to the school curriculum, we offer academic enrichment programmes that enhance creativity, foster a better understanding of the world, and make their overall educational experience more enjoyable. They are designed to complement the core courses. The School through the daily transactions and practices provides a distinctive educational environment that enables young pupils to grow not only in intellectual capacities, but also in various dimensions of their being.&nbsp;<o:p></o:p></span></p><p class=\"MsoNormal\" style=\"margin: 0cm 0cm 10pt; line-height: 16.866666793823242px; font-size: 11pt; font-family: Calibri, sans-serif; caret-color: rgb(0, 0, 0); color: rgb(0, 0, 0);\"><span lang=\"EN-US\" style=\"font-size: 12pt; line-height: 18.399999618530273px;\">While developing the students\' intellectual faculties, there is a conscious effort towards creating a wider awareness of the world and giving space for the development of aesthetic, moral and emotional dimensions.&nbsp;<o:p></o:p></span></p><p class=\"MsoNormal\" style=\"margin: 0cm 0cm 10pt; line-height: 16.866666793823242px; font-size: 11pt; font-family: Calibri, sans-serif; caret-color: rgb(0, 0, 0); color: rgb(0, 0, 0);\"><span lang=\"EN-US\" style=\"font-size: 12pt; line-height: 18.399999618530273px;\">We know that students passing out from The Modern School would grow into self-aware, sensitive and responsible human beings, who remain life-long learners; building a better tomorrow. The golden triangle that is form by the students, the teacher and the parent forms an integral part of The Modern School. When your child is enrolled, you become a partner in education for life.&nbsp;<o:p></o:p></span></p><p class=\"MsoNormal\" style=\"margin: 0cm 0cm 10pt; line-height: 16.866666793823242px; font-size: 11pt; font-family: Calibri, sans-serif; caret-color: rgb(0, 0, 0); color: rgb(0, 0, 0);\"><span lang=\"EN-US\" style=\"font-size: 12pt; line-height: 18.399999618530273px;\">If you wish to see happy, smiling faces, eager hands and curious minds, laughter ringing out carrying sunshine through, The Modern School is the right place for you and your child!<o:p></o:p></span></p><p class=\"MsoNormal\" style=\"margin: 0cm 0cm 10pt; line-height: 16.866666793823242px; font-size: 11pt; font-family: Calibri, sans-serif; caret-color: rgb(0, 0, 0); color: rgb(0, 0, 0);\"><span lang=\"EN-US\" style=\"font-size: 12pt; line-height: 18.399999618530273px;\"><o:p>&nbsp;</o:p></span></p><p class=\"MsoNormal\" style=\"margin: 0cm 0cm 10pt; line-height: 16.866666793823242px; font-size: 11pt; font-family: Calibri, sans-serif; caret-color: rgb(0, 0, 0); color: rgb(0, 0, 0);\"><b><span lang=\"EN-US\" style=\"font-size: 12pt; line-height: 18.399999618530273px;\">A. Inspiration for our school&nbsp;<o:p></o:p></span></b></p><p class=\"MsoNormal\" style=\"margin: 0cm 0cm 10pt; line-height: 16.866666793823242px; font-size: 11pt; font-family: Calibri, sans-serif; caret-color: rgb(0, 0, 0); color: rgb(0, 0, 0);\"><span lang=\"EN-US\" style=\"font-size: 12pt; line-height: 18.399999618530273px;\">We at the modern have created a curriculum that is rich in curiosity and imagination. This is reinforced by a learning environment that encourages a student\'s growth through not just theoretical education but also its practical applications.&nbsp;<o:p></o:p></span></p><p class=\"MsoNormal\" style=\"margin: 0cm 0cm 10pt; line-height: 16.866666793823242px; font-size: 11pt; font-family: Calibri, sans-serif; caret-color: rgb(0, 0, 0); color: rgb(0, 0, 0);\"><span lang=\"EN-US\" style=\"font-size: 12pt; line-height: 18.399999618530273px;\">Motivated by his flair for diversity, we continue to follow the dictum that says: different people with different ideas may all be right because confluence is the most enriching way to meaningful growth and development.&nbsp;<o:p></o:p></span></p><p class=\"MsoNormal\" style=\"margin: 0cm 0cm 10pt; line-height: 16.866666793823242px; font-size: 11pt; font-family: Calibri, sans-serif; caret-color: rgb(0, 0, 0); color: rgb(0, 0, 0);\"><span lang=\"EN-US\" style=\"font-size: 12pt; line-height: 18.399999618530273px;\">Prompted by his amazing multi-disciplinary abilities, we encourage our students to reach beyond textbooks and push the very definition of the word \'possible\'. Taking a leaf from this master\'s ability of farsightedness, we prepare our students to become independent and lifelong learners to take on future global opportunities.<o:p></o:p></span></p><p class=\"MsoNormal\" style=\"margin: 0cm 0cm 10pt; line-height: 16.866666793823242px; font-size: 11pt; font-family: Calibri, sans-serif; caret-color: rgb(0, 0, 0); color: rgb(0, 0, 0);\"><span lang=\"EN-US\" style=\"font-size: 12pt; line-height: 18.399999618530273px;\"><o:p>&nbsp;</o:p></span></p><p class=\"MsoNormal\" style=\"margin: 0cm 0cm 10pt; line-height: 16.866666793823242px; font-size: 11pt; font-family: Calibri, sans-serif; caret-color: rgb(0, 0, 0); color: rgb(0, 0, 0);\"><b><span lang=\"EN-US\" style=\"font-size: 12pt; line-height: 18.399999618530273px;\">B. Our Curriculum&nbsp;</span></b></p><p class=\"MsoNormal\" style=\"margin: 0cm 0cm 10pt; line-height: 16.866666793823242px; font-size: 11pt; font-family: Calibri, sans-serif; caret-color: rgb(0, 0, 0); color: rgb(0, 0, 0);\"><span lang=\"EN-US\" style=\"font-size: 12pt; line-height: 18.399999618530273px;\">The Modern School with English as a medium of instruction follows the curriculum guidelines and syllabus laid down by the Central Board of Secondary Education (CBSE). The school is affiliated with CBSE New Delhi.&nbsp;<o:p></o:p></span></p><p class=\"MsoNormal\" style=\"margin: 0cm 0cm 10pt; line-height: 16.866666793823242px; font-size: 11pt; font-family: Calibri, sans-serif; caret-color: rgb(0, 0, 0); color: rgb(0, 0, 0);\"><span lang=\"EN-US\" style=\"font-size: 12pt; line-height: 18.399999618530273px;\">The Modern School syllabus is built on a rich foundation of reading, writing, and arithmetic. That foundation is then layered with History, Science, Music, Geography, and the Arts to ensure no gaps in instruction. The time-tested curriculum is built on the principle of subject integration. Topics that lend assistance to each other irrespective of the Subject catering diverse intellectual groups. This reinforced study across subject areas develops higher-order thinking skills and promotes content mastery.<o:p></o:p></span></p><p class=\"MsoNormal\" style=\"margin: 0cm 0cm 10pt; line-height: 16.866666793823242px; font-size: 11pt; font-family: Calibri, sans-serif; caret-color: rgb(0, 0, 0); color: rgb(0, 0, 0);\"><span lang=\"EN-US\" style=\"font-size: 12pt; line-height: 18.399999618530273px;\"><o:p>&nbsp;</o:p></span></p><p class=\"MsoNormal\" style=\"margin: 0cm 0cm 10pt; line-height: 16.866666793823242px; font-size: 11pt; font-family: Calibri, sans-serif; caret-color: rgb(0, 0, 0); color: rgb(0, 0, 0);\"><b><span lang=\"EN-US\" style=\"font-size: 12pt; line-height: 18.399999618530273px;\">C. Online Education&nbsp;<o:p></o:p></span></b></p><p class=\"MsoNormal\" style=\"margin: 0cm 0cm 10pt; line-height: 16.866666793823242px; font-size: 11pt; font-family: Calibri, sans-serif; caret-color: rgb(0, 0, 0); color: rgb(0, 0, 0);\"><span lang=\"EN-US\" style=\"font-size: 12pt; line-height: 18.399999618530273px;\">The school has its own online education platform and mobile application for Android and IOS. It is committed to provide digital education and a hybrid environment of learning. Our online learning platform has vast features including online tests and assessments.<o:p></o:p></span></p><p class=\"MsoNormal\" style=\"margin: 0cm 0cm 10pt; line-height: 16.866666793823242px; font-size: 11pt; font-family: Calibri, sans-serif; caret-color: rgb(0, 0, 0); color: rgb(0, 0, 0);\"><b><span lang=\"EN-US\" style=\"font-size: 12pt; line-height: 18.399999618530273px;\"><o:p>&nbsp;</o:p></span></b></p><p class=\"MsoNormal\" style=\"margin: 0cm 0cm 10pt; line-height: 16.866666793823242px; font-size: 11pt; font-family: Calibri, sans-serif; caret-color: rgb(0, 0, 0); color: rgb(0, 0, 0);\"><b><span lang=\"EN-US\" style=\"font-size: 12pt; line-height: 18.399999618530273px;\">D. Assessment&nbsp;<o:p></o:p></span></b></p><p class=\"MsoNormal\" style=\"margin: 0cm 0cm 10pt; line-height: 16.866666793823242px; font-size: 11pt; font-family: Calibri, sans-serif; caret-color: rgb(0, 0, 0); color: rgb(0, 0, 0);\"><span lang=\"EN-US\" style=\"font-size: 12pt; line-height: 18.399999618530273px;\">To ensure ongoing learning without anxiety and fear an approach of holistic assessment will be observed using variety of tools. Continuous assessment will cover both scholastic and non-scholastic aspects of pupil\'s growth including health status, attitudes, aptitude, interests, personal and social qualities, proficiency in co-curricular activities and academic competence.<o:p></o:p></span></p><p class=\"MsoNormal\" style=\"margin: 0cm 0cm 10pt; line-height: 16.866666793823242px; font-size: 11pt; font-family: Calibri, sans-serif; caret-color: rgb(0, 0, 0); color: rgb(0, 0, 0);\"><span lang=\"EN-US\" style=\"font-size: 12pt; line-height: 18.399999618530273px;\"><o:p>&nbsp;</o:p></span></p><p class=\"MsoNormal\" style=\"margin: 0cm 0cm 10pt; line-height: 16.866666793823242px; font-size: 11pt; font-family: Calibri, sans-serif; caret-color: rgb(0, 0, 0); color: rgb(0, 0, 0);\"><b><span lang=\"EN-US\" style=\"font-size: 12pt; line-height: 18.399999618530273px;\">E. Promotion&nbsp;<o:p></o:p></span></b></p><p class=\"MsoNormal\" style=\"margin: 0cm 0cm 10pt; line-height: 16.866666793823242px; font-size: 11pt; font-family: Calibri, sans-serif; caret-color: rgb(0, 0, 0); color: rgb(0, 0, 0);\"><span lang=\"EN-US\" style=\"font-size: 12pt; line-height: 18.399999618530273px;\">1. Promotions are based on the academic and behavioral conduct of the student throughout the year and also on the performance in terminal examinations.&nbsp;<o:p></o:p></span></p><p class=\"MsoNormal\" style=\"margin: 0cm 0cm 10pt; line-height: 16.866666793823242px; font-size: 11pt; font-family: Calibri, sans-serif; caret-color: rgb(0, 0, 0); color: rgb(0, 0, 0);\"><span lang=\"EN-US\" style=\"font-size: 12pt; line-height: 18.399999618530273px;\">2. A student must obtain minimum of grade D in all subjects under scholastic areas.&nbsp;<o:p></o:p></span></p><p class=\"MsoNormal\" style=\"margin: 0cm 0cm 10pt; line-height: 16.866666793823242px; font-size: 11pt; font-family: Calibri, sans-serif; caret-color: rgb(0, 0, 0); color: rgb(0, 0, 0);\"><span lang=\"EN-US\" style=\"font-size: 12pt; line-height: 18.399999618530273px;\">3. The Principal reserves the right to detain students with-<o:p></o:p></span></p><p class=\"MsoNormal\" style=\"margin: 0cm 0cm 10pt; line-height: 16.866666793823242px; font-size: 11pt; font-family: Calibri, sans-serif; caret-color: rgb(0, 0, 0); color: rgb(0, 0, 0);\"><span lang=\"EN-US\" style=\"font-size: 12pt; line-height: 18.399999618530273px;\">&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;(a) Irregular attendance&nbsp;<o:p></o:p></span></p><p class=\"MsoNormal\" style=\"margin: 0cm 0cm 10pt; line-height: 16.866666793823242px; font-size: 11pt; font-family: Calibri, sans-serif; caret-color: rgb(0, 0, 0); color: rgb(0, 0, 0);\"><span lang=\"EN-US\" style=\"font-size: 12pt; line-height: 18.399999618530273px;\">&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;(b) Unsatisfactory progress in studies&nbsp;<o:p></o:p></span></p><p class=\"MsoNormal\" style=\"margin: 0cm 0cm 10pt; line-height: 16.866666793823242px; font-size: 11pt; font-family: Calibri, sans-serif; caret-color: rgb(0, 0, 0); color: rgb(0, 0, 0);\"><span lang=\"EN-US\" style=\"font-size: 12pt; line-height: 18.399999618530273px;\">&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;(c) Unfair behavior and conduct in common&nbsp;<o:p></o:p></span></p><p class=\"MsoNormal\" style=\"margin: 0cm 0cm 10pt; line-height: 16.866666793823242px; font-size: 11pt; font-family: Calibri, sans-serif; caret-color: rgb(0, 0, 0); color: rgb(0, 0, 0);\"><span lang=\"EN-US\" style=\"font-size: 12pt; line-height: 18.399999618530273px;\">&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;(d) Minimal participation in co-curricular activities and defaulter in fee payments.&nbsp;Honesty, cleanliness, good manners and loyalty are expected from every child.&nbsp;Anyone not conforming to the school ideals may after due notice to the parents be&nbsp;asked to leave.<o:p></o:p></span></p>', 1, '2017-12-24 19:52:07'),
(21, 'The Next Leap', 'next-leap', 'The Next Leap', NULL, '<p class=\"MsoNormal\" style=\"margin: 0cm 0cm 10pt; line-height: 16.866666793823242px; font-size: 11pt; font-family: Calibri, sans-serif; caret-color: rgb(0, 0, 0); color: rgb(0, 0, 0);\"><span lang=\"EN-US\" style=\"font-size: 12pt; line-height: 18.399999618530273px;\">Together towards a beautiful tomorrow TMS provides an education that enhances and stimulates intellectual curiosity and critical thinking from a very early age. The curriculum is structured in a way that enables students to become independent and responsible citizens. The school aims at taking forward such good practices. It is clear that emotional stability, intellectual flexibility. sound judgement and genuine independence will be qualities vital for their future. Our aim thus at TMS, is to provide all our children with a well rounded education which will guide them to find their own path in life. An education that will not only equip them with skills and knowledge but also make them inwardly free, secure and creative adults. It shall always be our endeavour to prepare the students to enter adulthood with self - discipline, social awareness, wonder and reverence for the world.&nbsp;<b>special career advisory cell for individual preferences!! The next leap in UPSC IIAS EXAMS I IIT/JEE / CA/ CSI NIFT / CLAT/ NLUNDA renowned and internationally acclaimed universities like the christ/ delhi university</b>. and the list goes on Special counselling sessions with ALUMNI holding proud placements!!<o:p></o:p></span></p>', 1, '2017-12-24 19:52:07'),
(22, 'Guidelines', 'guidelines-policy', 'Guidelines', NULL, '<p class=\"MsoNormal\" style=\"margin: 0cm 0cm 10pt; line-height: 16.866666793823242px; font-size: 11pt; font-family: Calibri, sans-serif; caret-color: rgb(0, 0, 0); color: rgb(0, 0, 0);\"><b><span lang=\"EN-US\" style=\"font-size: 12pt; line-height: 18.399999618530273px;\">Important Notice:&nbsp;<o:p></o:p></span></b></p><p class=\"MsoNormal\" style=\"margin: 0cm 0cm 10pt; line-height: 16.866666793823242px; font-size: 11pt; font-family: Calibri, sans-serif; caret-color: rgb(0, 0, 0); color: rgb(0, 0, 0);\"><span lang=\"EN-US\" style=\"font-size: 12pt; line-height: 18.399999618530273px;\">The Principal is considerably occupied in supervising, teaching, seeing to the children\'s individual problems and administrative work and is therefore unable to see parents without prior appointment. She is available in her office on week days between 8.00 a.m. to 9.00 a.m.&nbsp;<o:p></o:p></span></p><p class=\"MsoNormal\" style=\"margin: 0cm 0cm 10pt; line-height: 16.866666793823242px; font-size: 11pt; font-family: Calibri, sans-serif; caret-color: rgb(0, 0, 0); color: rgb(0, 0, 0);\"><span lang=\"EN-US\" style=\"font-size: 12pt; line-height: 18.399999618530273px;\"><o:p>&nbsp;</o:p></span></p><p class=\"MsoNormal\" style=\"margin: 0cm 0cm 10pt; line-height: 16.866666793823242px; font-size: 11pt; font-family: Calibri, sans-serif; caret-color: rgb(0, 0, 0); color: rgb(0, 0, 0);\"><b><span lang=\"EN-US\" style=\"font-size: 12pt; line-height: 18.399999618530273px;\">Uniform:&nbsp;<o:p></o:p></span></b></p><p class=\"MsoNormal\" style=\"margin: 0cm 0cm 10pt; line-height: 16.866666793823242px; font-size: 11pt; font-family: Calibri, sans-serif; caret-color: rgb(0, 0, 0); color: rgb(0, 0, 0);\"><span lang=\"EN-US\" style=\"font-size: 12pt; line-height: 18.399999618530273px;\">The school uniform is a discipline in itself and should be adhered to strictly. The uniform pattern and colour should be correctly followed. It is compulsory for the students to wear the uniform.&nbsp;<o:p></o:p></span></p><p class=\"MsoNormal\" style=\"margin: 0cm 0cm 10pt; line-height: 16.866666793823242px; font-size: 11pt; font-family: Calibri, sans-serif; caret-color: rgb(0, 0, 0); color: rgb(0, 0, 0);\"><span lang=\"EN-US\" style=\"font-size: 12pt; line-height: 18.399999618530273px;\"><o:p>&nbsp;</o:p></span></p><p class=\"MsoNormal\" style=\"margin: 0cm 0cm 10pt; line-height: 16.866666793823242px; font-size: 11pt; font-family: Calibri, sans-serif; caret-color: rgb(0, 0, 0); color: rgb(0, 0, 0);\"><b><span lang=\"EN-US\" style=\"font-size: 12pt; line-height: 18.399999618530273px;\">Parents must also note:&nbsp;<o:p></o:p></span></b></p><p class=\"MsoNormal\" style=\"margin: 0cm 0cm 10pt; line-height: 16.866666793823242px; font-size: 11pt; font-family: Calibri, sans-serif; caret-color: rgb(0, 0, 0); color: rgb(0, 0, 0);\"><span lang=\"EN-US\" style=\"font-size: 12pt; line-height: 18.399999618530273px;\">It is the responsibility of the parents to see that : Their ward comes to school in proper uniform. Their ward is punctual in coming to school. Their ward attends school regularly and does not miss school without a valid reason and prior permission. Their ward appears for all school test regularly. Their ward does the home work regularly and submits all assignments in time. The Almanac of their ward is checked regularly. All school dues are paid on time. The school is intimated about any change in address and/or telephone number.<o:p></o:p></span></p>', 1, '2017-12-24 19:52:07'),
(23, 'Activities', 'activities', 'Activities', NULL, '<p class=\"MsoNormal\" style=\"margin: 8.15pt 0cm 8.15pt 18pt; font-size: 11pt; font-family: Calibri, sans-serif; caret-color: rgb(0, 0, 0); color: rgb(0, 0, 0);\"><span lang=\"EN-US\" style=\"font-size: 16pt; font-family: &quot;Times New Roman&quot;, serif; color: black;\">Apart from academic knowledge, Co-curricular activities are meant to bring&nbsp;<b><span style=\"font-weight: normal;\">social</span>&nbsp;</b><b><span style=\"font-weight: normal;\">skills</span></b>, intellectual skills,&nbsp;<b><span style=\"font-weight: normal;\">moral</span>&nbsp;</b><b><span style=\"font-weight: normal;\">values</span></b>, personality progress and&nbsp;<b><span style=\"font-weight: normal;\">character</span>&nbsp;</b><b><span style=\"font-weight: normal;\">appeal</span></b>&nbsp;in students. We at TMS are highly dedicated to nurture these qualities and essence of human life within the kids through handicrafts, drawing,&nbsp;singing and dancing, musical instrumental teaching&nbsp;<b><span style=\"font-weight: normal;\">creative</span>&nbsp;</b><b><span style=\"font-weight: normal;\">arts</span>&nbsp;</b><b><span style=\"font-weight: normal;\">with useless stuffs</span>&nbsp;</b>and Yoga &amp; meditation etc. to ensure the leaders of tomorrow that is our tenet.&nbsp;&nbsp;TMS hosts a variety of programs and events in every academic year. We are proud of our own innovation of “Mother’s Recipe” to bring the socialization among toddlers as a core value.&nbsp;<o:p></o:p></span></p>', 1, '2017-12-24 19:52:07'),
(24, 'Sports & Athletics', 'sports', 'Sports', NULL, '<p class=\"MsoNormal\" style=\"margin: 8.15pt 0cm 8.15pt 18pt; font-size: 11pt; font-family: Calibri, sans-serif; caret-color: rgb(0, 0, 0); color: rgb(0, 0, 0);\"><span lang=\"EN-US\" style=\"font-size: 16pt; font-family: &quot;Times New Roman&quot;, serif; color: rgb(40, 40, 40);\">The comradery and togetherness created when a school is collectively proud of their athletics is nothing short of outstanding and there are many life lessons for students to learn through sports and athletics. The value of sports activities and athletics in schools are significant and cannot be overlooked.&nbsp;TMS constantly endeavors to instill the essential life skills into the students such as Teamwork, Self-discipline, Effort and Determination to battle against the obstacle in future life through sports.</span><b><span lang=\"EN-US\" style=\"font-size: 24pt; font-family: &quot;Times New Roman&quot;, serif; color: black;\"><o:p></o:p></span></b></p><p class=\"MsoNormal\" style=\"margin: 8.15pt 0cm 8.15pt 18pt; font-size: 11pt; font-family: Calibri, sans-serif; caret-color: rgb(0, 0, 0); color: rgb(0, 0, 0);\"><span lang=\"EN-US\" style=\"font-size: 16pt; font-family: &quot;Times New Roman&quot;, serif; color: rgb(40, 40, 40);\">We provide both indoor &amp; outdoor sports/athletics. We have updated sports equipment and playground for Archery, cycling, Badminton, Frisbee, soccer, volleyball, handball, tennis, Mountaineering and many more to be named.<o:p></o:p></span></p><p class=\"MsoNormal\" style=\"margin: 8.15pt 0cm 8.15pt 18pt; font-size: 11pt; font-family: Calibri, sans-serif; caret-color: rgb(0, 0, 0); color: rgb(0, 0, 0);\"><span lang=\"EN-US\" style=\"font-size: 16pt; font-family: &quot;Times New Roman&quot;, serif; color: rgb(40, 40, 40);\">In indoor games we facilitate the students with the best of the indoor like Chess, carrom, Pictionary, Judo, Balancing the Beam, Puzzles card games etc. we have a plan on table for swimming practice too.</span><br></p>', 1, '2017-12-24 19:52:07'),
(25, 'Laboratories', 'laboratories', 'Laboratories', NULL, '<p class=\"MsoNormal\" style=\"margin: 8.15pt 0cm 8.15pt 18pt; font-size: 11pt; font-family: Calibri, sans-serif; caret-color: rgb(0, 0, 0); color: rgb(0, 0, 0);\"><i><span lang=\"EN-US\" style=\"font-size: 16pt; color: rgb(25, 35, 45); letter-spacing: 0.25pt;\">A laboratory is always considered as a relevant and essential part so far as the teaching of Science and Computer is concerned.</span></i><i><span lang=\"EN-US\" style=\"font-size: 16pt; color: rgb(25, 35, 45); letter-spacing: 0.25pt; font-style: normal;\"><o:p></o:p></span></i></p><p class=\"MsoNormal\" style=\"margin: 8.15pt 0cm 8.15pt 18pt; font-size: 11pt; font-family: Calibri, sans-serif; caret-color: rgb(0, 0, 0); color: rgb(0, 0, 0);\"><i><span lang=\"EN-US\" style=\"font-size: 16pt; color: rgb(25, 35, 45); letter-spacing: 0.25pt;\">TMS boasts of having spacious and separate laboratories&nbsp;</span></i><span lang=\"EN-US\" style=\"font-size: 16pt; font-family: &quot;Times New Roman&quot;, serif; color: rgb(25, 35, 45); letter-spacing: 0.25pt;\">where students perform researches, experiments or even learn new things. In order to develop interactions between teacher and students and peer-to-peer experimentation, research, and exploration TMS provides&nbsp;</span><span lang=\"EN-US\" style=\"font-size: 16pt; font-family: &quot;Times New Roman&quot;, serif; color: rgb(25, 35, 45); letter-spacing: 0.25pt;\">Physics Lab, Chemistry Lab, Biology Lab, Computer Lab with all kind of latest a modernized instruments such as&nbsp;</span><span lang=\"EN-US\" style=\"font-size: 16pt; font-family: &quot;Times New Roman&quot;, serif; color: rgb(25, 35, 45); letter-spacing: 0.25pt;\">Burettes, Tongs, Tweezers, Forceps, Test tubes, Conical flasks, Microscopes Specimens Slides Chemicals with high speed internet connectivity devised with all the latest models of computers and projectors available. All of this is on ground with all the safety measures employed what makes us the highly professional in education sector in rapidly growing Barmer District of Rajasthan.<o:p></o:p></span></p>', 1, '2017-12-24 19:52:07'),
(26, 'Building', 'building', 'Building', NULL, '<p class=\"MsoNormal\" style=\"margin: 8.15pt 0cm 8.15pt 18pt; font-size: 11pt; font-family: Calibri, sans-serif; caret-color: rgb(0, 0, 0); color: rgb(0, 0, 0);\"><span lang=\"EN-US\" style=\"font-size: 16pt; font-family: &quot;Times New Roman&quot;, serif;\">The school building must have the potential to drive the educational values and ethos. Stretching in twelve “Beeghah” of land, surrounded by the beauty and charm of low height mountains hills around and three storey building of TMS is all set to provide fresh air, immense natural light and blessings of mother nature within the campus.&nbsp;<o:p></o:p></span></p><p class=\"MsoNormal\" style=\"margin: 8.15pt 0cm 8.15pt 18pt; font-size: 11pt; font-family: Calibri, sans-serif; caret-color: rgb(0, 0, 0); color: rgb(0, 0, 0);\"><span lang=\"EN-US\" style=\"font-size: 16pt; font-family: &quot;Times New Roman&quot;, serif;\">It includes all the necessary space in abundance with 61<sup>+</sup>&nbsp;&nbsp;well equipped audio-visual<sup></sup>&nbsp;classrooms, a spacious library with thousands of books, Labs, space for transportation vehicles, ground for games and physical activities, cultural auditorium, computer lab, IT solution cell, science park, parking space for personal vehicle and last but not the least school office and entrance decorated with a great many numbers of sweet smelling flowers and natural greenery along with fountain which gives you a feeling of heaven on earth.<o:p></o:p></span></p>', 1, '2017-12-24 19:52:07'),
(27, 'Transport', 'transport', 'Transport', NULL, '<p class=\"MsoNormal\" style=\"margin: 8.15pt 0cm 8.15pt 18pt; font-size: 11pt; font-family: Calibri, sans-serif; caret-color: rgb(0, 0, 0); color: rgb(0, 0, 0);\"><span lang=\"EN-US\" style=\"font-size: 12pt;\">Buses are provided for commuting to school and for trips organized by the school. It is expected that all the students using bus would maintain discipline. In case availing/discontinuing/change of bus service, the parent should inform the school in advance</span><span lang=\"EN-US\" style=\"font-size: 16pt; font-family: &quot;Times New Roman&quot;, serif; color: rgb(25, 35, 45); letter-spacing: 0.25pt;\"><o:p></o:p></span></p>', 1, '2017-12-24 19:52:07'),
(28, 'Privacy Policy', 'privacy-policy', 'Privacy Policy', NULL, '<p class=\"section-fullwidth\"></p><p class=\"element-size-100\"></p><p class=\" lightbox col-md-12\"> </p><h3><b>Privacy Policy</b></h3><br><p style=\"margin-bottom: 10px; line-height: 22px; caret-color: rgb(97, 97, 97); color: rgb(97, 97, 97); font-family: &quot;Open Sans&quot;, sans-serif; font-size: 13px; text-align: justify;\">This privacy policy sets out how The Modern School uses and protects any information that you give The Modern School when you use this website.</p><p style=\"margin-bottom: 10px; line-height: 22px; caret-color: rgb(97, 97, 97); color: rgb(97, 97, 97); font-family: &quot;Open Sans&quot;, sans-serif; font-size: 13px; text-align: justify;\">The Modern School is committed to ensuring that your privacy is protected. Should we ask you to provide certain information by which you can be identified when using this website, then you can be assured that it will only be used in accordance with this privacy statement.</p><p style=\"margin-bottom: 10px; line-height: 22px; caret-color: rgb(97, 97, 97); color: rgb(97, 97, 97); font-family: &quot;Open Sans&quot;, sans-serif; font-size: 13px; text-align: justify;\">The Modern School may change this policy from time to time by updating this page. You should check this page from time to time to ensure that you are happy with any changes. This policy is effective from 1st April 2020.</p><br><h3><br></h3><p></p><p class=\" lightbox col-md-12\"><br></p><p class=\" lightbox col-md-12\"><br></p><p class=\" lightbox col-md-12\"><br></p><p></p><p></p>', 1, '2017-12-24 19:52:07'),
(29, 'Disclaimer Policy', 'disclaimer-policy', 'Disclaimer Policy', NULL, '<p class=\"section-fullwidth\"></p><p class=\"element-size-100\"></p><p class=\" lightbox col-md-12\"> </p><h3><b>Disclaimer Policy</b></h3><br><p style=\"margin-bottom: 10px; line-height: 22px; caret-color: rgb(97, 97, 97); color: rgb(97, 97, 97); font-family: &quot;Open Sans&quot;, sans-serif; font-size: 13px; text-align: justify;\">The information contained in this website is for general information purposes only. The information is provided by The Modern School and while we endeavour to keep the information up to date and correct, we make no representations or warranties of any kind, express or implied, about the completeness, accuracy, reliability, suitability or availability with respect to the website or the information, services, or related graphics contained on the website for any purpose. Any reliance you place on such information is therefore strictly at your own risk.</p><p style=\"margin-bottom: 10px; line-height: 22px; caret-color: rgb(97, 97, 97); color: rgb(97, 97, 97); font-family: &quot;Open Sans&quot;, sans-serif; font-size: 13px; text-align: justify;\">In no event will we be liable for any loss or damage including without limitation, indirect or consequential loss or damage, or any loss or damage whatsoever arising from loss of data or profits arising out of, or in connection with, the use of this website.</p><p style=\"margin-bottom: 10px; line-height: 22px; caret-color: rgb(97, 97, 97); color: rgb(97, 97, 97); font-family: &quot;Open Sans&quot;, sans-serif; font-size: 13px; text-align: justify;\">Through this website you are able to link to other websites which are not under the control of The Modern School. We have no control over the nature, content and availability of those sites. The inclusion of any links does not necessarily imply a recommendation or endorse the views expressed within them.</p><p style=\"margin-bottom: 10px; line-height: 22px; caret-color: rgb(97, 97, 97); color: rgb(97, 97, 97); font-family: &quot;Open Sans&quot;, sans-serif; font-size: 13px; text-align: justify;\">Every effort is made to keep the website up and running smoothly. However, The Modern School takes no responsibility for, and will not be liable for, the website being temporarily unavailable due to technical issues beyond our control.</p><br><h3><br></h3><p></p><p class=\" lightbox col-md-12\"><br></p><p class=\" lightbox col-md-12\"><br></p><p class=\" lightbox col-md-12\"><br></p><p></p><p></p>', 1, '2017-12-24 19:52:07');
INSERT INTO `tms_master_pages` (`master_pages_id`, `master_pages_title`, `master_pages_slug`, `master_pages_excerpt`, `master_pages_image`, `master_pages_description`, `master_pages_status`, `master_pages_created`) VALUES
(30, 'Library', 'library', 'Library', NULL, '<p class=\"MsoNormal\" style=\"margin: 8.15pt 0cm 8.15pt 18pt; font-size: 11pt; font-family: Calibri, sans-serif; caret-color: rgb(0, 0, 0); color: rgb(0, 0, 0);\"><span lang=\"EN-US\" style=\"font-size: 16pt; font-family: &quot;Times New Roman&quot;, serif;\">The school building must have the potential to drive the educational values and ethos. Stretching in twelve “Beeghah” of land, surrounded by the beauty and charm of low height mountains hills around and three storey building of TMS is all set to provide fresh air, immense natural light and blessings of mother nature within the campus.&nbsp;<o:p></o:p></span></p><p class=\"MsoNormal\" style=\"margin: 8.15pt 0cm 8.15pt 18pt; font-size: 11pt; font-family: Calibri, sans-serif; caret-color: rgb(0, 0, 0); color: rgb(0, 0, 0);\"><span lang=\"EN-US\" style=\"font-size: 16pt; font-family: &quot;Times New Roman&quot;, serif;\">It includes all the necessary space in abundance with 61<sup>+</sup>&nbsp;&nbsp;well equipped audio-visual<sup></sup>&nbsp;classrooms, a spacious library with thousands of books, Labs, space for transportation vehicles, ground for games and physical activities, cultural auditorium, computer lab, IT solution cell, science park, parking space for personal vehicle and last but not the least school office and entrance decorated with a great many numbers of sweet smelling flowers and natural greenery along with fountain which gives you a feeling of heaven on earth.<o:p></o:p></span></p>', 1, '2017-12-24 19:52:07');

-- --------------------------------------------------------

--
-- Table structure for table `tms_our_schools`
--

CREATE TABLE `tms_our_schools` (
  `our_school_id` int(11) NOT NULL,
  `our_school_title` varchar(255) DEFAULT NULL,
  `our_school_description` longtext DEFAULT NULL,
  `our_school_status` int(11) NOT NULL DEFAULT 1 COMMENT '1 - Active 0 -  Deactive',
  `our_school_created` timestamp NOT NULL DEFAULT current_timestamp()
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data for table `tms_our_schools`
--

INSERT INTO `tms_our_schools` (`our_school_id`, `our_school_title`, `our_school_description`, `our_school_status`, `our_school_created`) VALUES
(1, 'Active Learning', 'Active learning is a model of instruction that focuses the responsibility of learning on learners. Active learning engages students in two aspects – doing things and thinking about the things they are doing. We have a team of trained, experienced, affectionate staff who understand the psychology of small children and deals them accordingly.', 1, '2017-12-24 19:42:33'),
(2, 'Kids Play Ground', 'A playground is a place with a specific design to allow children to play there. Playgrounds often have recreational equipment.', 1, '2017-12-24 19:42:33'),
(3, 'Centrally Located', 'Located away from the hustle bustle of life amongst peaceful surroundings in the central heart of the city, easily accessible by any means of transport with no hassle of parking………… Neo Dales is a landmark of this posh Civil Lines area.', 1, '2017-12-24 19:43:15'),
(4, 'Cute Environment', 'We have an extremely good and lovable environment in school and activities are closely and meticulously monitored. We can vouch for the fact that Neo Dalians simply love their school !.', 1, '2017-12-24 19:43:15');

-- --------------------------------------------------------

--
-- Table structure for table `tms_school_facilities`
--

CREATE TABLE `tms_school_facilities` (
  `school_facilities_id` int(11) NOT NULL,
  `school_facilities_title` varchar(255) DEFAULT NULL,
  `school_facilities_image` varchar(255) DEFAULT NULL,
  `school_facilities_description` longtext DEFAULT NULL,
  `school_facilities_status` int(11) NOT NULL DEFAULT 1 COMMENT '1 - Active 0 - Deactive',
  `school_facilities_created` timestamp NOT NULL DEFAULT current_timestamp()
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data for table `tms_school_facilities`
--

INSERT INTO `tms_school_facilities` (`school_facilities_id`, `school_facilities_title`, `school_facilities_image`, `school_facilities_description`, `school_facilities_status`, `school_facilities_created`) VALUES
(1, 'Mobile App', 'uploads/school_facilities_image/1.png', 'Our school mobile app is available on both Google Play Store for android users and on App Store for iOS users for dissemination of a host of info to parents on the go!', 1, '2017-12-24 19:48:48'),
(2, 'GPS Enabled School Vehicles', 'uploads/school_facilities_image/2.png', 'Auto generated messages are delivered to parents availing transport in morning and afternoon hours to facilitate smoother operations.', 0, '2017-12-24 19:48:48'),
(3, 'Parent Login On School ERP', 'uploads/school_facilities_image/3.png', 'Parents can log in from this website which will redirect them to our school ERP and view a host of information of their ward like Attendance, Time Table, Circulars, Monthly Course / Activities, Online Fee Payment, Calendar, Download various Forms, etc.', 1, '2017-12-24 19:48:48'),
(4, 'Smart Classes', 'uploads/school_facilities_image/4.png', 'A Play School with e-techs in practically every class and SAS for classes 1 & 2 is no mean task and outstanding results are witnessed using these educational technology tools.', 1, '2017-12-24 19:48:48'),
(5, 'Computer Lab', 'uploads/school_facilities_image/5.png', 'As we move ahead towards a Digital India we expose our young minds to the Computer Lab once they reach class 1 which is fully equipped.', 0, '2017-12-24 19:48:48'),
(6, 'Kooh Sports', 'uploads/school_facilities_image/6.png', 'In another step forward we have tied up with KOOH SPORTS which is a new age sports company that delivers sports training and coaching programmes of international standards to schools.', 1, '2017-12-24 19:48:48');

-- --------------------------------------------------------

--
-- Table structure for table `tms_school_gallery`
--

CREATE TABLE `tms_school_gallery` (
  `school_gallery_id` int(11) NOT NULL,
  `school_gallery_title` varchar(255) DEFAULT NULL,
  `school_gallery_image` varchar(255) DEFAULT NULL,
  `school_gallery_status` int(11) NOT NULL DEFAULT 1,
  `school_gallery_created` timestamp NOT NULL DEFAULT current_timestamp()
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data for table `tms_school_gallery`
--

INSERT INTO `tms_school_gallery` (`school_gallery_id`, `school_gallery_title`, `school_gallery_image`, `school_gallery_status`, `school_gallery_created`) VALUES
(1, 'Image', 'uploads/school_gallery_image/1618818309.jpg', 1, '2021-04-19 07:45:09'),
(2, 'Image', 'uploads/school_gallery_image/1618818335.jpg', 1, '2021-04-19 07:45:35'),
(3, 'Image', 'uploads/school_gallery_image/1618818354.jpg', 1, '2021-04-19 07:45:54');

-- --------------------------------------------------------

--
-- Table structure for table `tms_school_video_gallery`
--

CREATE TABLE `tms_school_video_gallery` (
  `school_gallery_id` int(11) NOT NULL,
  `school_gallery_title` varchar(255) DEFAULT NULL,
  `school_gallery_link` varchar(255) DEFAULT NULL,
  `type` enum('normal','tour') NOT NULL DEFAULT 'normal',
  `school_gallery_status` int(11) NOT NULL DEFAULT 1,
  `school_gallery_created` timestamp NOT NULL DEFAULT current_timestamp()
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data for table `tms_school_video_gallery`
--

INSERT INTO `tms_school_video_gallery` (`school_gallery_id`, `school_gallery_title`, `school_gallery_link`, `type`, `school_gallery_status`, `school_gallery_created`) VALUES
(1, 'Video One', 'eSREy9vDCrE', 'normal', 1, '2021-04-21 11:06:15'),
(2, 'School Building', 'wZcj-LWU0iA', 'normal', 1, '2021-06-07 16:51:23'),
(3, 'School Building Video', '0lsFSXHcUxo', 'normal', 1, '2021-06-07 16:52:55'),
(4, 'School Building Video', '0lsFSXHcUxo', 'tour', 1, '2021-06-07 16:52:55'),
(5, 'School Building', 'wZcj-LWU0iA', 'tour', 1, '2021-06-07 16:51:23');

-- --------------------------------------------------------

--
-- Table structure for table `tms_slider`
--

CREATE TABLE `tms_slider` (
  `slider_id` int(11) NOT NULL,
  `slider_name` varchar(255) DEFAULT NULL,
  `slider_image` varchar(255) DEFAULT NULL,
  `slider_status` int(11) NOT NULL DEFAULT 1,
  `slider_created` timestamp NOT NULL DEFAULT current_timestamp()
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data for table `tms_slider`
--

INSERT INTO `tms_slider` (`slider_id`, `slider_name`, `slider_image`, `slider_status`, `slider_created`) VALUES
(1, 'Slider1', 'uploads/slider_image/1624344456.jpg', 1, '2021-04-14 09:37:08'),
(2, 'Slider2', 'uploads/slider_image/1618808620.jpg', 1, '2021-04-15 08:34:38'),
(3, 'Slider3', 'uploads/slider_image/1624344258.JPG', 1, '2021-04-15 08:34:54');

-- --------------------------------------------------------

--
-- Table structure for table `tms_state_masters`
--

CREATE TABLE `tms_state_masters` (
  `id` int(12) NOT NULL,
  `name` varchar(200) DEFAULT NULL,
  `country_master_id` int(12) DEFAULT NULL,
  `removed` enum('Y','N') DEFAULT 'N',
  `created` datetime DEFAULT NULL,
  `modified` datetime DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data for table `tms_state_masters`
--

INSERT INTO `tms_state_masters` (`id`, `name`, `country_master_id`, `removed`, `created`, `modified`) VALUES
(1, 'Rajasthan', 1, 'N', '2018-03-23 22:07:35', '2018-03-23 22:07:35'),
(2, 'Uttar Pradesh', 1, 'N', '2018-03-23 22:08:02', '2018-03-23 22:08:02'),
(3, 'Madhya Pradesh', 1, 'N', '2018-03-23 22:08:14', '2018-03-23 22:08:14'),
(4, 'Maharashtra', 1, 'N', '2018-03-23 22:08:34', '2018-03-23 22:08:34'),
(5, 'Jharkhand', 1, 'N', '2018-03-23 22:08:44', '2018-03-23 22:08:44'),
(6, 'Gujarat', 1, 'N', '2018-03-23 22:09:01', '2018-03-23 22:09:01'),
(7, 'Haryana', 1, 'N', '2018-03-23 22:09:15', '2018-03-23 22:09:15'),
(8, 'West Bengal', 1, 'N', '2018-03-23 22:09:30', '2018-03-23 22:09:30'),
(9, 'Bihar', 1, 'N', '2018-03-23 22:10:16', '2018-03-23 22:10:16'),
(10, 'Assam', 1, 'N', '2019-09-30 14:58:11', '2019-09-30 14:58:11'),
(11, 'Delhi', 1, 'N', '2019-09-30 14:58:29', '2019-09-30 14:58:29'),
(12, 'Manipur', 1, 'N', '2019-09-30 14:58:55', '2019-09-30 14:58:55'),
(13, 'Punjab', 1, 'N', '2019-09-30 14:59:31', '2019-09-30 14:59:31'),
(14, 'Uttarakhand', 1, 'N', '2019-09-30 14:59:57', '2019-09-30 14:59:57'),
(15, 'Himachal Pradesh', 1, 'N', '2019-10-03 17:50:57', '2019-10-03 17:50:57'),
(16, 'Tripura', 1, 'N', '2019-10-03 17:51:50', '2019-10-03 17:51:50'),
(17, 'Telangana', 1, 'N', '2019-10-03 17:52:02', '2019-10-03 17:52:02'),
(18, 'Tamil Nadu', 1, 'N', '2019-10-03 17:52:21', '2019-10-03 17:52:21'),
(19, 'Sikkim', 1, 'N', '2019-10-03 17:52:28', '2019-10-03 17:52:28'),
(20, 'Odisha', 1, 'N', '2019-10-03 17:53:02', '2019-10-03 17:53:02'),
(21, 'Nagaland', 1, 'N', '2019-10-03 17:53:12', '2019-10-03 17:53:12'),
(22, 'Mizoram', 1, 'N', '2019-10-03 17:53:20', '2019-10-03 17:53:20'),
(23, 'Meghalaya', 1, 'N', '2019-10-03 17:53:29', '2019-10-03 17:53:29'),
(24, 'Kerala', 1, 'N', '2019-10-03 17:53:49', '2019-10-03 17:53:49'),
(25, 'Karnataka', 1, 'N', '2019-10-03 17:53:57', '2019-10-03 17:53:57'),
(26, 'Goa', 1, 'N', '2019-10-03 17:54:20', '2019-10-03 17:54:20'),
(27, 'Chhattisgarh', 1, 'N', '2019-10-03 17:54:29', '2019-10-03 17:54:29'),
(28, 'Chandigarh', 1, 'N', '2019-10-03 17:54:40', '2019-10-03 17:54:40'),
(29, 'Arunachal Pradesh', 1, 'N', '2019-10-03 17:54:53', '2019-10-03 17:54:53'),
(30, 'Andhra Pradesh', 1, 'N', '2019-10-03 17:55:03', '2019-10-03 17:55:03'),
(31, 'Andaman and Nicobar Islands', 1, 'N', '2019-10-06 11:24:48', '2019-10-06 11:24:48'),
(32, 'Daman and Diu', 1, 'N', '2019-10-06 11:25:27', '2019-10-06 11:25:27'),
(33, 'Jammu and Kashmir', 1, 'N', '2019-10-06 11:26:08', '2019-10-06 11:26:08'),
(34, 'Ladakh', 1, 'N', '2019-10-06 11:26:52', '2019-10-06 11:26:52'),
(35, 'Lakshadweep', 1, 'N', '2019-10-06 11:27:09', '2019-10-06 11:27:09'),
(36, 'Puducherry', 1, 'N', '2019-10-06 11:28:01', '2019-10-06 11:28:01'),
(37, 'Dadra and Nagar Haveli', 1, 'N', '2019-10-06 11:29:51', '2019-10-06 11:29:51');

-- --------------------------------------------------------

--
-- Table structure for table `tms_tcs`
--

CREATE TABLE `tms_tcs` (
  `tc_id` int(11) NOT NULL,
  `tc_name` varchar(255) NOT NULL,
  `s_number` varchar(255) NOT NULL,
  `admission_number` varchar(255) NOT NULL,
  `tc_file` varchar(255) DEFAULT NULL,
  `tc_is_deleted` enum('Y','N') NOT NULL DEFAULT 'N'
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data for table `tms_tcs`
--

INSERT INTO `tms_tcs` (`tc_id`, `tc_name`, `s_number`, `admission_number`, `tc_file`, `tc_is_deleted`) VALUES
(1, 'mudit', '1234', '4321', 'FILE1.pdf', 'N');

-- --------------------------------------------------------

--
-- Table structure for table `tms_testimonials`
--

CREATE TABLE `tms_testimonials` (
  `testimonials_id` int(11) NOT NULL,
  `testimonials_title` varchar(255) COLLATE utf8_unicode_ci DEFAULT NULL,
  `testimonials_description` varchar(1000) COLLATE utf8_unicode_ci DEFAULT NULL,
  `testimonials_user_name` varchar(255) COLLATE utf8_unicode_ci DEFAULT NULL,
  `testimonials_status` enum('0','1') COLLATE utf8_unicode_ci NOT NULL DEFAULT '1'
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;

--
-- Dumping data for table `tms_testimonials`
--

INSERT INTO `tms_testimonials` (`testimonials_id`, `testimonials_title`, `testimonials_description`, `testimonials_user_name`, `testimonials_status`) VALUES
(1, NULL, 'We like the place and culture of TMS. I have got a transferable job, we move often and are experienced to see different schools but, when my child got admitted to TMS, She was beaming with joy -of- learning. Innovative-teaching-approach encapsulating Indian values has literally transformed my child\'s personality. Kudos to TMS team', 'M. D. Ratnu. CEO-Zila Parishad, Barmer', '1'),
(2, NULL, 'We wanted our child\'s first experience at school to be caring and at- home- experience, where they feel secure and loved. TMS has surpassed our expectations in every sense. The best part is, I can even keep a track of my child\'s activities/ results through WhatsApp groups /mails the teachers share information in. Workshops with Pediatricians, Child- Psychologists and teachers acted as a catalyst in knowing my child\'s interest better.', 'Lt Col Amar Madev Bhosale', '1'),
(3, NULL, 'It is wonderful to be able to know exactly what my child do, learn, and play. TMS is a dream place for my child, where she enjoy and learn in a fun- to- do way. It is awesome platform for the mutual benefit of child, parents and teachers, helping us making a strong interpersonal bond for the future endeavour.', 'Major Haldankar N V', '1'),
(4, NULL, 'My child\'s progress is commendable, she really is excited to come to school every morning.', 'Major Pramod Kumar', '1'),
(5, NULL, 'The English - Speaking - Environment at TMS is a blissful additional gift for us along with experienced, child-friendly, approachable teachers! Now my child can speak more than 2 languages. They are happy and confident! I acknowledge with deep sense of appreciation and gratitude TMS\'s concern & dedication in providing quality education.', 'Hemant Dave. Senior Teacher Govt. School, Barmer', '1'),
(6, NULL, 'Ever - positive- developing aura and wide range of co-curricular activities encapsulated in curriculum offered at TMS has developed my child\'s overall personality; whether winning the sports race or being the best singer or her confidence level at stage handling. I\'m thoroughly impressed! TMS touches the future through hearts of children.', 'Binay Kumar Nanda 8s Senior Manager ,Cairn India', '1');

-- --------------------------------------------------------

--
-- Table structure for table `tms_time_slots`
--

CREATE TABLE `tms_time_slots` (
  `time_slot_id` int(255) NOT NULL,
  `meeting_date` varchar(255) NOT NULL,
  `time_slot_from` varchar(255) NOT NULL,
  `time_slot_to` varchar(255) DEFAULT NULL,
  `time_slot_type` enum('Through Video Call','In-person Interaction') NOT NULL DEFAULT 'In-person Interaction',
  `total_appointment` int(10) DEFAULT NULL,
  `my_appointment` varchar(10) NOT NULL DEFAULT '0',
  `time_slot_status` enum('0','1') NOT NULL DEFAULT '1'
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Indexes for dumped tables
--

--
-- Indexes for table `blog`
--
ALTER TABLE `blog`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `migrations`
--
ALTER TABLE `migrations`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `news`
--
ALTER TABLE `news`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `tms_account_settings`
--
ALTER TABLE `tms_account_settings`
  ADD PRIMARY KEY (`account_id`);

--
-- Indexes for table `tms_admin`
--
ALTER TABLE `tms_admin`
  ADD PRIMARY KEY (`admin_id`);

--
-- Indexes for table `tms_admission`
--
ALTER TABLE `tms_admission`
  ADD PRIMARY KEY (`admission_id`);

--
-- Indexes for table `tms_admission_points`
--
ALTER TABLE `tms_admission_points`
  ADD PRIMARY KEY (`admission_point_id`);

--
-- Indexes for table `tms_career`
--
ALTER TABLE `tms_career`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `tms_city_masters`
--
ALTER TABLE `tms_city_masters`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `tms_classes`
--
ALTER TABLE `tms_classes`
  ADD PRIMARY KEY (`class_id`);

--
-- Indexes for table `tms_contact_enquirys`
--
ALTER TABLE `tms_contact_enquirys`
  ADD PRIMARY KEY (`contact_enquiry_id`);

--
-- Indexes for table `tms_downloads`
--
ALTER TABLE `tms_downloads`
  ADD PRIMARY KEY (`download_id`);

--
-- Indexes for table `tms_events`
--
ALTER TABLE `tms_events`
  ADD PRIMARY KEY (`event_id`);

--
-- Indexes for table `tms_faqs`
--
ALTER TABLE `tms_faqs`
  ADD PRIMARY KEY (`faq_id`);

--
-- Indexes for table `tms_important_points`
--
ALTER TABLE `tms_important_points`
  ADD PRIMARY KEY (`important_point_id`);

--
-- Indexes for table `tms_master_pages`
--
ALTER TABLE `tms_master_pages`
  ADD PRIMARY KEY (`master_pages_id`);

--
-- Indexes for table `tms_our_schools`
--
ALTER TABLE `tms_our_schools`
  ADD PRIMARY KEY (`our_school_id`);

--
-- Indexes for table `tms_school_facilities`
--
ALTER TABLE `tms_school_facilities`
  ADD PRIMARY KEY (`school_facilities_id`);

--
-- Indexes for table `tms_school_gallery`
--
ALTER TABLE `tms_school_gallery`
  ADD PRIMARY KEY (`school_gallery_id`);

--
-- Indexes for table `tms_school_video_gallery`
--
ALTER TABLE `tms_school_video_gallery`
  ADD PRIMARY KEY (`school_gallery_id`);

--
-- Indexes for table `tms_slider`
--
ALTER TABLE `tms_slider`
  ADD PRIMARY KEY (`slider_id`);

--
-- Indexes for table `tms_state_masters`
--
ALTER TABLE `tms_state_masters`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `tms_tcs`
--
ALTER TABLE `tms_tcs`
  ADD PRIMARY KEY (`tc_id`);

--
-- Indexes for table `tms_testimonials`
--
ALTER TABLE `tms_testimonials`
  ADD PRIMARY KEY (`testimonials_id`);

--
-- Indexes for table `tms_time_slots`
--
ALTER TABLE `tms_time_slots`
  ADD PRIMARY KEY (`time_slot_id`);

--
-- AUTO_INCREMENT for dumped tables
--

--
-- AUTO_INCREMENT for table `blog`
--
ALTER TABLE `blog`
  MODIFY `id` int(10) UNSIGNED NOT NULL AUTO_INCREMENT;

--
-- AUTO_INCREMENT for table `migrations`
--
ALTER TABLE `migrations`
  MODIFY `id` int(10) UNSIGNED NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=3;

--
-- AUTO_INCREMENT for table `news`
--
ALTER TABLE `news`
  MODIFY `id` int(10) UNSIGNED NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=2;

--
-- AUTO_INCREMENT for table `tms_account_settings`
--
ALTER TABLE `tms_account_settings`
  MODIFY `account_id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=2;

--
-- AUTO_INCREMENT for table `tms_admin`
--
ALTER TABLE `tms_admin`
  MODIFY `admin_id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=2;

--
-- AUTO_INCREMENT for table `tms_admission`
--
ALTER TABLE `tms_admission`
  MODIFY `admission_id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=4;

--
-- AUTO_INCREMENT for table `tms_admission_points`
--
ALTER TABLE `tms_admission_points`
  MODIFY `admission_point_id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=21;

--
-- AUTO_INCREMENT for table `tms_career`
--
ALTER TABLE `tms_career`
  MODIFY `id` int(10) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=10;

--
-- AUTO_INCREMENT for table `tms_city_masters`
--
ALTER TABLE `tms_city_masters`
  MODIFY `id` int(12) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=499;

--
-- AUTO_INCREMENT for table `tms_classes`
--
ALTER TABLE `tms_classes`
  MODIFY `class_id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=7;

--
-- AUTO_INCREMENT for table `tms_contact_enquirys`
--
ALTER TABLE `tms_contact_enquirys`
  MODIFY `contact_enquiry_id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=3;

--
-- AUTO_INCREMENT for table `tms_downloads`
--
ALTER TABLE `tms_downloads`
  MODIFY `download_id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=23;

--
-- AUTO_INCREMENT for table `tms_events`
--
ALTER TABLE `tms_events`
  MODIFY `event_id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=88;

--
-- AUTO_INCREMENT for table `tms_faqs`
--
ALTER TABLE `tms_faqs`
  MODIFY `faq_id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=18;

--
-- AUTO_INCREMENT for table `tms_important_points`
--
ALTER TABLE `tms_important_points`
  MODIFY `important_point_id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=60;

--
-- AUTO_INCREMENT for table `tms_master_pages`
--
ALTER TABLE `tms_master_pages`
  MODIFY `master_pages_id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=31;

--
-- AUTO_INCREMENT for table `tms_our_schools`
--
ALTER TABLE `tms_our_schools`
  MODIFY `our_school_id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=5;

--
-- AUTO_INCREMENT for table `tms_school_facilities`
--
ALTER TABLE `tms_school_facilities`
  MODIFY `school_facilities_id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=7;

--
-- AUTO_INCREMENT for table `tms_school_gallery`
--
ALTER TABLE `tms_school_gallery`
  MODIFY `school_gallery_id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=4;

--
-- AUTO_INCREMENT for table `tms_school_video_gallery`
--
ALTER TABLE `tms_school_video_gallery`
  MODIFY `school_gallery_id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=6;

--
-- AUTO_INCREMENT for table `tms_slider`
--
ALTER TABLE `tms_slider`
  MODIFY `slider_id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=4;

--
-- AUTO_INCREMENT for table `tms_state_masters`
--
ALTER TABLE `tms_state_masters`
  MODIFY `id` int(12) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=38;

--
-- AUTO_INCREMENT for table `tms_tcs`
--
ALTER TABLE `tms_tcs`
  MODIFY `tc_id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=2;

--
-- AUTO_INCREMENT for table `tms_testimonials`
--
ALTER TABLE `tms_testimonials`
  MODIFY `testimonials_id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=7;

--
-- AUTO_INCREMENT for table `tms_time_slots`
--
ALTER TABLE `tms_time_slots`
  MODIFY `time_slot_id` int(255) NOT NULL AUTO_INCREMENT;
COMMIT;

/*!40101 SET CHARACTER_SET_CLIENT=@OLD_CHARACTER_SET_CLIENT */;
/*!40101 SET CHARACTER_SET_RESULTS=@OLD_CHARACTER_SET_RESULTS */;
/*!40101 SET COLLATION_CONNECTION=@OLD_COLLATION_CONNECTION */;
