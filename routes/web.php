<?php

use Illuminate\Support\Facades\Artisan;
use Illuminate\Support\Facades\Route;

error_reporting(0);

Route::get('/clear-cache', function() {
    Artisan::call('cache:clear');
    return "Cache is cleared";
});
Route::get('/clear-config', function() {
    Artisan::call('config:clear');
    return "Cache is cleared";
});


Route::get('404-error', function () {
    return view('404');
});


// Backend

Route::group(['prefix' => 'admin-panel', 'as' => 'admin.'], function () {
    // TC
    Route::any('tc_add', 'School_Gallery\School_GalleryController@add_tc');
    Route::any('save_tc', 'School_Gallery\School_GalleryController@save_tc');
    Route::any('tc_view', 'School_Gallery\School_GalleryController@view_tc');

    // Admin Urls
    Route::any('/', 'Admin\MainController@login');

    Route::any('admin_login', 'Admin\MainController@admin_login');

    Route::any('forgot-password', 'Admin\MainController@forgot_password');

    Route::any('reset-password/{id}', 'Admin\MainController@reset_password');

    Route::any('password_reset', 'Admin\MainController@password_reset');

    Route::any('dashboard', 'Admin\AdminController@dashboard');

    Route::any('profile', 'Admin\AdminController@profile');

    Route::any('change-password', 'Admin\AdminController@change_password');

    Route::any('account-settings', 'Admin\AdminController@account_settings');

    Route::any('logout', 'Admin\AdminController@logout');


    // Admission points url

    Route::any('admission-points/{id?}', 'Admission_Points\Admission_PointsController@admission_points_view');

    Route::any('save-admission-points/{id?}', 'Admission_Points\Admission_PointsController@save_admission_points');

    Route::any('admission-points-status/{id}/{value}', 'Admission_Points\Admission_PointsController@admission_points_status');


    // Important points url

    Route::any('important-points/{id?}', 'Important_points\Important_pointsController@important_points_view');

    Route::any('save-important-points/{id?}', 'Important_points\Important_pointsController@save_important_points');

    Route::any('important-points-status/{id}/{value}', 'Important_points\Important_pointsController@important_points_status');


    // Enquiry urls

    Route::get('contact-enquiry/export', 'Contact_Enquiry\Contact_EnquiryController@export')->name('export_contact_enquiry');
    Route::any('contact-enquiry', 'Contact_Enquiry\Contact_EnquiryController@contact_enquiry');


    Route::any('register-enquiry/export', 'Contact_Enquiry\Contact_EnquiryController@export_register')->name('export_register_enquiry');
    Route::any('register-enquiry', 'Contact_Enquiry\Contact_EnquiryController@register_enquiry');

    Route::any('student-fees', 'Contact_Enquiry\Contact_EnquiryController@student_fees');

    Route::any('contact-enquiry-status/{id}/{value}', 'Contact_Enquiry\Contact_EnquiryController@contact_enquiry_status');


    // Our Schools

    Route::any('add-our-school/{id?}', 'Our_School\Our_SchoolController@add_our_school');

    Route::any('save-our-school/{id?}', 'Our_School\Our_SchoolController@save_our_school');

    Route::any('view-our-school', 'Our_School\Our_SchoolController@view_our_school');

    Route::any('our-school-status/{id}/{value}', 'Our_School\Our_SchoolController@our_school_status');



    // Our Schools

    Route::any('add-testimonials/{id?}', 'Our_School\Our_SchoolController@add_testimonials');

    Route::any('save-testimonials/{id?}', 'Our_School\Our_SchoolController@save_testimonials');

    Route::any('view-testimonials', 'Our_School\Our_SchoolController@view_testimonials');

    Route::any('testimonials-status/{id}/{value}', 'Our_School\Our_SchoolController@testimonials_status');


    // Faqs url

    Route::any('add-faq/{id?}', 'Faqs\FaqController@add_faq');

    Route::any('save-faq/{id?}', 'Faqs\FaqController@save_faq');

    Route::any('view-faq', 'Faqs\FaqController@view_faq');

    Route::any('faq-status/{id}/{value}', 'Faqs\FaqController@faq_status');


    // Schools Facilities

    Route::any('add-school-facilities/{id?}', 'School_Facilities\School_FacilitiesController@add_school_facilities');

    Route::any('save-school-facilities/{id?}', 'School_Facilities\School_FacilitiesController@save_school_facilities');

    Route::any('view-school-facilities', 'School_Facilities\School_FacilitiesController@view_school_facilities');

    Route::any('school-facilities-status/{id}/{value}', 'School_Facilities\School_FacilitiesController@school_facilities_status');


    // We Teach

    Route::any('add-we-teach/{id?}', 'We_Teach\We_TeachController@add_we_teach');

    Route::any('save-we-teach/{id?}', 'We_Teach\We_TeachController@save_we_teach');

    Route::any('view-we-teach', 'We_Teach\We_TeachController@view_we_teach');

    Route::any('we-teach-status/{id}/{value}', 'We_Teach\We_TeachController@we_teach_status');


    // Classes url

    Route::any('add-class/{id?}', 'Classes\ClassController@add_class');

    Route::any('save-class/{id?}', 'Classes\ClassController@save_class');

    Route::any('view-class', 'Classes\ClassController@view_class');

    Route::any('class-status/{id}/{value}', 'Classes\ClassController@class_status');


    // Download url

    Route::any('add-download/{id?}', 'Downloads\DownloadsController@add_download');

    Route::any('save-download/{id?}', 'Downloads\DownloadsController@save_download');

    Route::any('view-download', 'Downloads\DownloadsController@view_download');

    Route::any('download-status/{id}/{value}', 'Downloads\DownloadsController@download_status');


    // Events url

    Route::any('add-event/{id?}', 'Events\EventsController@add_event');

    Route::any('save-event/{id?}', 'Events\EventsController@save_event');

    Route::any('view-event', 'Events\EventsController@view_event');

    Route::any('event-status/{id}/{value}', 'Events\EventsController@event_status');

    // School Gallery url

    Route::any('add-school-gallery/{id?}', 'School_Gallery\School_GalleryController@add_school_gallery');

    Route::any('save-school-gallery/{id?}', 'School_Gallery\School_GalleryController@save_school_gallery');

    Route::any('view-school-gallery', 'School_Gallery\School_GalleryController@view_school_gallery');

    Route::any('school-gallery-status/{id}/{value}', 'School_Gallery\School_GalleryController@school_gallery_status');



    // School Gallery url

    Route::any('add-school-video-gallery/{id?}', 'School_Gallery\School_GalleryController@add_school_video_gallery');

    Route::any('save-school-video-gallery/{id?}', 'School_Gallery\School_GalleryController@save_school_video_gallery');

    Route::any('view-school-video-gallery', 'School_Gallery\School_GalleryController@view_school_video_gallery');

    Route::any('school-video-gallery-status/{id}/{value}', 'School_Gallery\School_GalleryController@school_video_gallery_status');



    // Slider url

    Route::any('add-slider/{id?}', 'Slider\SliderController@add_slider');

    Route::any('save-slider/{id?}', 'Slider\SliderController@save_slider');

    Route::any('view-slider', 'Slider\SliderController@view_slider');

    Route::any('slider-status/{id}/{value}', 'Slider\SliderController@slider_status');

    // Blog url

    Route::any('add-blog/{id?}', 'Blog\BlogController@add_blog');

    Route::any('save-blog/{id?}', 'Blog\BlogController@save_blog');

    Route::any('view-blog', 'Blog\BlogController@view_blog');

    Route::any('blog-status/{id}/{value}', 'Blog\BlogController@blog_status');

    // News url

    Route::any('add-news/{id?}', 'News\NewsController@add_news');

    Route::any('save-news/{id?}', 'News\NewsController@save_news');

    Route::any('view-news', 'News\NewsController@view_news');

    Route::any('news-status/{id}/{value}', 'News\NewsController@news_status');


    // Master pages

    Route::any('master-pages/{slug}', 'Pages\PagesController@master_pages');
    Route::any('save-master-pages/{id}', 'Pages\PagesController@save_pages');
    Route::delete('pages/{page_image}', 'Pages\PagesController@destroy')->name("admin.pages.delete");


    Route::any('time-slot', 'Website\TimeController@time_slot_view');
    Route::any('time-slot/{id?}', 'Website\TimeController@time_slot_view');
    Route::any('time-slot-information/{id?}', 'Website\TimeController@time_slot_information');
    Route::any('save-time-slot/{id?}', 'Website\TimeController@save_time');
    Route::any('time-slot-status/{id}/{value}', 'Website\TimeController@time_slot_status');
    Route::any('time-slot-delete/{id}', 'Website\TimeController@time_slot_delete');
});




/**
 * Website Routes
 */
/* ========================================================================================================= */

Route::get('online-fees-deposite', 'Website\FeesController@index')->name('fee_deposite.index');
Route::post('online-fees-deposite', 'Website\FeesController@store')->name('fee_deposite.submit');
Route::any('online-fees-deposite/success', 'Website\FeesController@success')->name('fees.payment.success');
Route::any('online-fees-deposite/failed', 'Website\FeesController@failure')->name('fees.payment.failure');

/*-----------------------------------------------------------------------------------------------------------*/
Route::any('contact-us', 'Website\HomeController@contact_us');
Route::any('contact_submit', 'Website\HomeController@contact_submit');
Route::any('gallery', 'Website\HomeController@gallery');
Route::any('videogallery', 'Website\HomeController@videogallery');
Route::any('virtual-tour', 'Website\HomeController@virtual_tour');
// Route::any('vision-mission', 'Website\HomeController@vision_mission');
// Route::any('principal-message', 'Website\HomeController@principal_message');
// Route::any('admission-policy', 'Website\HomeController@admission_policy');
// Route::any('academic-policy', 'Website\HomeController@academic_policy');
// Route::any('next-leap', 'Website\HomeController@next_leap');
// Route::any('guidelines-policy', 'Website\HomeController@guidelines_policy');
// Route::any('activities', 'Website\HomeController@activities');
// Route::any('sports', 'Website\HomeController@sports');
// Route::any('laboratories', 'Website\HomeController@laboratories');
// Route::any('building', 'Website\HomeController@building');
// Route::any('transport', 'Website\HomeController@transport');
// Route::any('library', 'Website\HomeController@library');
// Route::any('discliamer-policy', 'Website\HomeController@disclaimer_policy');
// Route::any('privacy-policy', 'Website\HomeController@privacy_policy');
// Route::any('refund-policy', 'Website\HomeController@refund_policy');
// Route::any('shipping-delivery', 'Website\HomeController@shipping_delivery');
// Route::any('terms-conditions', 'Website\HomeController@terms_conditions');

Route::any('/', 'Website\HomeController@home');
Route::any('home', 'Website\HomeController@home');
Route::any('news/{news}', 'Website\HomeController@news');

Route::get('blog', 'Website\BlogController@index');
Route::get('blog/{blog}', 'Website\BlogController@show');

Route::any('about-us', 'Website\HomeController@about_us');
Route::any('download', 'Website\HomeController@download');
Route::any('registration', 'Website\HomeController@registration');
Route::any('registration_submit', 'Website\HomeController@registration_submit');

Route::any('tc', 'Website\HomeController@tc');
Route::any('tcfetch', 'Website\HomeController@tcfetch');


Route::any('career', 'Website\HomeController@career');
Route::any('career_mail', 'Website\HomeController@career_mail');
Route::post('career_submit', 'Website\HomeController@career_submit');

Route::post('register_submitnew', 'Website\HomeController@register_submitnew');
Route::any('reg_mail', 'Website\HomeController@reg_mail');

Route::any('admission_register', 'Website\HomeController@admission_register');
Route::any('payment/{id?}', 'Website\HomeController@payment');
Route::any('response', 'Website\HomeController@response');
Route::post('update_admission', 'Website\HomeController@update_admission');
Route::any('trans_details', 'Website\HomeController@trans_details');


Route::any('backup', 'Database@database_backup');



Route::any('fetch', 'Website\HomeController@city');


Route::any('infrastructure', 'Website\HomeController@infrastructure');

Route::any('modules-activities', 'Website\HomeController@modules_activities');

Route::any('rules-regulations', 'Website\HomeController@rules_regulations');

Route::any('school-uniform', 'Website\HomeController@school_uniform');

Route::any('information-technology', 'Website\HomeController@information_technology');

Route::any('conveyance', 'Website\HomeController@conveyance');

Route::any('school-captains', 'Website\HomeController@school_captains');

Route::any('stars-of-yesteryears', 'Website\HomeController@stars_of_yesteryears');

Route::any('school-hours-security', 'Website\HomeController@school_hours_security');

Route::any('fee-structure', 'Website\HomeController@fee_structure');

Route::any('online-fee-deposit', 'Website\HomeController@online_fee_deposit');

Route::any('neft-payment', 'Website\HomeController@neft_payment');

Route::any('admission', 'Website\HomeController@admission');





Route::any('parents-faq', 'Website\HomeController@parents_faq');




Route::any('disclaimer-policy', 'Website\HomeController@disclaimer_policy');

Route::any('privacy-policy', 'Website\HomeController@privacy_policy');

Route::any('cancellation-refund-policy', 'Website\HomeController@cancellation_refund_policy');

Route::any('shipping-delivery-policy', 'Website\HomeController@shipping_delivery_policy');

Route::any('terms-conditions', 'Website\HomeController@terms_conditions');

Route::any('submit-fees-success', 'Website\HomeController@subit_fees_success');

Route::any('submit-fees-failure', 'Website\HomeController@subit_fees_failure');

Route::any('/submit-fees-online', 'Website\HomeController@submit_fees');

Route::any('/event-details/{slug}', 'Website\HomeController@events_details');


Route::any('{pages}', 'Website\HomeController@pages');
/*-----------------------------------------------------------------------------------------------------------*/